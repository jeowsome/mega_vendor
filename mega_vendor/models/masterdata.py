#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
from random import randint
import logging
import time

_logger = logging.getLogger(__name__)


class VendorCategory(models.Model):
    _name = "vendor.category"
    _order = "name"
    _description = "Vendor Category"
    _inherit = ['mail.thread']

    requirement_ids = fields.One2many('vendor.category.requirements', 'requirement_id', string='PDF Attachment')

    name = fields.Char(string='Vendor Category', required=True, tracking=True)
    notes = fields.Text(string='Resolution Details', tracking=True)



class VendorCategoryRequirements(models.Model):
    _name = "vendor.category.requirements"
    
    TYPE_BUSINESS = [
    ('sole_pro','Sole Proprietorship'),
    ('partnership','Partnership'),
    ('corp','Corporation'),
    ('llc','Limited Liability Company')
    ] 
    
    
    requirement_id = fields.Many2one('vendor.category', 'Document Required', readonly=True, ondelete='cascade')
    doc_id = fields.Many2one('vendor.requirements.list','Document',required=True)
    name = fields.Char(string='Document Name',store=True,related='doc_id.name')
    description = fields.Char(string='Description',store=True, related='doc_id.description')
    is_required = fields.Boolean('Required',default=True)
    business_type = fields.Selection(TYPE_BUSINESS,string="Applicable to (Leave blank if All)")
    # pdf_attachment = fields.Binary(string='Attached PDF File')
    # date_attached = fields.Date(string='Date Attached', default=fields.Date.context_today, readonly=True, tracking=True)


class VendorRequirementsList(models.Model):
    _name ="vendor.requirements.list" 
    
    name = fields.Char(string='Document Name')
    description = fields.Char(string='Description')
    
class VendorGroup(models.Model):
    _name = "vendor.group"
    _description = "Vendor Group"
    _order = "name"
    
    def name_get(self):
        res = []
        for rec in self:
            res.append((rec.id, '[%s] %s' % (rec.category_id.name,rec.name)))
        return res
    
    def _get_default_color(self):
        return randint(1, 11)       

    name = fields.Char(string = 'Vendor Group', required=True, tracking=True)
    color = fields.Integer(string='Color', default=_get_default_color)
    category_id = fields.Many2one('vendor.group.category',string='Category')

    _sql_constraints = [
        ('name_uniq', 'unique (name,category_id)', "Vendor Group already exists!"),
    ]
    

class VendorGroupCategory(models.Model):
    _name ="vendor.group.category" 
    
    name = fields.Char(string='Vendor Group Category',required=True)
