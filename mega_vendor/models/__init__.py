# -*- coding: utf-8 -*-
from . import res_partner_inherit
from . import masterdata
from . import vendor
from . import res_users
from . import survey_input
