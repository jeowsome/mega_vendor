# -*- coding: utf-8 -*-
{
    'name': "MEGA-Vendor Accreditation Project",

    'summary': """
        Manage Megawide Vendor Accreditation System""",

    'description': """
        Manage Megawide Vendor Accreditation System
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -301,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail','survey','contacts','mega_dms','mega_active_directory'],

    # always loaded
    'init_xml': [],
    'data': [
        'views/assets.xml',
        'data/ir_cron_data.xml',
        'data/vendor_registration_survey_data.xml',
        'data/vendor_report_templates.xml',
        'data/vendor_report_templates_epc.xml',
        'data/vendor_email_templates.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/masterdata_views.xml',
        'views/res_partner_views.xml',
        'views/vendor_views.xml',
        # 'views/hr_employee_views.xml',       
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],    
}
