# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo.addons.survey.controllers.main import Survey
import json
import logging
import werkzeug

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, http, _
from odoo.addons.base.models.ir_ui_view import keep_query
from odoo.exceptions import UserError
from odoo.http import request, content_disposition
from odoo.osv import expression
from odoo.tools import format_datetime, format_date, is_html_empty
from odoo import models
from odoo import _, api, exceptions, fields, models, modules
from odoo.addons.web.controllers.main import Binary

_logger = logging.getLogger(__name__)


class SurveyInherit(Survey):

    @http.route('/survey/submit/<string:survey_token>/<string:answer_token>', type='json', auth='public', website=True)
    def survey_submit(self, survey_token, answer_token, **post):
        partner_vals = {}
        user_vals = {}
        #group_vendor_user = request.env['res.groups'].ref('mega_vendor.group_vendor_user').id
        group_vendor_user = request.env['ir.model.data'].get_object_reference('mega_vendor', 'group_vendor_user')
        group_document_user = request.env['ir.model.data'].get_object_reference('mega_dms', 'group_document_user')
        if group_vendor_user:
            user_vals['groups_id'] = [(4, group_vendor_user[1])]
            print(user_vals['groups_id'])
        action_business_unit = request.env['ir.model.data'].get_object_reference('mega_vendor', 'action_vendor_wizard_business_unit')
        #user_vals['action_id'] = action_business_unit[1]
        user_vals['is_vendor'] = True
        tin = ''
        business_unit = ''
        result = super(SurveyInherit, self).survey_submit(survey_token, answer_token, **post)
        is_vendor = False
        cc_emails = {'cc_email': ''}
        for key in post:
            if key.isnumeric():
                question = request.env['survey.question'].sudo().search([('id', '=', key)])
                if question.survey_id.title == "BU Vendor Registration" or question.survey_id.title == "EPC Vendor Registration":
                    if question.survey_id.title == "BU Vendor Registration":
                        business_unit = 'bu'
                        cc_emails['cc_email'] = 'rcastro@megawide.com.ph'
                    elif question.survey_id.title == "EPC Vendor Registration":
                        business_unit = 'epc'
                        cc_emails['cc_email'] = 'afrancisco@megawide.com.ph'
                    is_vendor = True
                    if question.title == 'Company Name':
                        user_vals['name'] = post[key]
                    elif question.title == 'TIN':
                        tin = post[key]
                    elif question.title == 'Email Address':
                        user_vals['login'] = post[key]
                        user_vals['email'] = post[key]
                    # user_vals['cc_email'] = cc_emails['cc_email']
        if is_vendor:
            user_exist = request.env['res.users'].sudo().search([('login', '=', user_vals['email'])])
            tin_exist = request.env['res.partner'].sudo().search([('tin', '=', tin)])
            failed_applications = 0
            if user_exist:
                raise UserError(_("Email Address Already Exists, please Refresh the browser and try again"))
            if tin_exist:
                if tin_exist.is_rejected:
                    if tin_exist.days_remaining > 0:
                        raise UserError(_("You are still restricted with Vendor Registration, Time Remaining : %d days" % tin_exist.days_remaining))
                    else:
                        failed_applications = tin_exist.failed_applications
                        tin_exist.tin = False
                        tin_exist.active = False
                else:
                    raise UserError(_("TIN Already Exists, please Refresh the browser and try again"))
            vendor_user_id = request.env['res.users'].sudo().with_context(cc_email=cc_emails['cc_email']).create(user_vals)
            if group_document_user:
                vendor_user_id.groups_id = [(4, group_document_user[1])]
            vendor_user_id.partner_id.tin = tin
            vendor_user_id.partner_id.failed_applications = failed_applications
            vendor_user_id.company_type = 'company'
            request.env['mega.vendor'].sudo().create({'related_user': vendor_user_id.id, 'business_unit': business_unit, 'vendor_id': vendor_user_id.partner_id.id, 'tin': tin, 'email': vendor_user_id.partner_id.email})
        return result
