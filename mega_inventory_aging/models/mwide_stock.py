#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time
from xlrd import open_workbook
import tempfile
import binascii
import base64
from io import StringIO
from odoo.models import TransientModel
import re

_logger = logging.getLogger(__name__)

class MwideInventoryWarehouse(models.Model):
    _name = 'mwide.inventory.warehouse'
    name = fields.Char(string="Name", required=True)
    description = fields.Char(string="Description", size=128)
    
class MwideInventoryUom(models.Model):
    _name = 'mwide.inventory.uom'
    name = fields.Char(string="Name", required=True)
    description = fields.Char(string="Description", size=128)

class MwideInventoryImport(models.TransientModel):
    _name = 'mwide.inventory.import'
 
    
    name = fields.Char(string="Description")
    inventory_date = fields.Date(string='Inventory Date',default=datetime.today())
    company_id = fields.Many2one('res.company',string='Company', default=lambda self: self.env.user.company_id)
    attachment_id = fields.Binary(string="Upload File", required=True)

    def inventory_upload(self):
        wh_dict = {'2' : 'warehouse_id',}
        uom_dict = { '5': 'uom_id'}
        xls_dict = {
            '0' : 'name',
            '1' : 'description',           
        }
        aging_dict = {
            '8' : '0-30 Days',
            '10' : '31-90 Days',
            '12' : '91-180 Days',
            '14' : '181-300 Days',
            '16' : '301-480 Days',
            '18' : '481-660 Days',
            '20' : '661+ Days',
        }
        aging_value_dict = {
            '9' :'0-30 Days Value',
            '11' : '31-90 Days Value',
            '13' : '91-180 Days Value',
            '15' : '181-300 Days Value',
            '17' : '301-480 Days Value',
            '19' : '481-660 Days Value',
            '21' : '661+ Days Value'
        }
        fp = tempfile.NamedTemporaryFile(suffix=".xls")
        fp.write(base64.standard_b64decode(self.attachment_id))
        fp.seek(0)
        xl = fp.read()
        book = open_workbook(file_contents=xl)
        s = book.sheet_by_index(0)
        mi = self.env['mwide.inventory'].search_count([('company_id','=',self.company_id.id)])
        print(mi)
        if mi > 0:
            self.env['mwide.inventory'].search([('company_id','=',self.company_id.id)]).unlink()
        mii = self.env['mwide.inventory.item'].search_count([('company_id','=',self.company_id.id)])
        print(mii)
        if mii > 0:
            self.env['mwide.inventory.item'].search([('company_id','=',self.company_id.id)]).unlink()
        for row in range(s.nrows):
            vals = {}
            aging = {}
            aging_value = {}
            for col in range(s.ncols): 
                if row > 0:
                    value  = (s.cell(row,col).value).encode('ascii', 'ignore')
                    if col == 0 or col == 1:
                        if not value:
                            break
                    try : 
                        value = str(int(value))
                    except : 
                        pass
                    ins_field = str(col)
                    if value:
                        if ins_field in uom_dict:
                            vals[uom_dict[ins_field]] = self.env['mwide.inventory.uom'].search([('name','=',value.decode("utf-8"))], limit=1).id or self.env['mwide.inventory.uom'].create({'name': value, 'description': value}).id
                        if ins_field in wh_dict:
                            vals[wh_dict[ins_field]] = self.env['mwide.inventory.warehouse'].search([('name','=',value.decode("utf-8"))], limit=1).id or self.env['mwide.inventory.warehouse'].create({'name': value, 'description': value}).id
                        if ins_field in xls_dict:
                            vals[xls_dict[ins_field]] = value
                        if ins_field in aging_dict:
                            aging[aging_dict[ins_field]] = value
                        if ins_field in aging_value_dict:
                            aging_value[aging_value_dict[ins_field]] = value
            item_id = False
            if vals:
                vals['company_id'] = self.company_id.id
                item_id = self.env['mwide.inventory.item'].create(vals)
            for a,b in zip(aging,aging_value):
                vals2 = vals
                if aging[a]:
                    vals2['inventory_date'] = self.inventory_date
                    vals2['item_id'] = item_id.id
                    vals2['qty'] = float(aging[a].decode("utf-8").replace(',',''))
                    vals2['total_value'] = float(aging_value[b].decode("utf-8").replace(',','').replace('PHP ',''))
                    vals2['aging_id'] = self.env['mwide.inventory.aging'].search([('name','=',a)], limit=1).id
                    self.env['mwide.inventory'].create(vals2)
        return True
        
class MwideInventoryCategory(models.Model):
    _name = 'mwide.inventory.category'
    name = fields.Char(string="Name", required=True)
    description = fields.Char(string="Description", size=128)

class MwideInventoryAging(models.Model):
    _name = 'mwide.inventory.aging'
    name = fields.Char(string="Aging Days", required=True)
    category_id = fields.Many2one('mwide.inventory.category',string="Category")  
    
class MwideInventoryItem(models.Model):
    _name = 'mwide.inventory.item'
    name = fields.Char(string="Item Name", required=True)
    description = fields.Char(string="Description", size=128)
    warehouse_id = fields.Many2one('mwide.inventory.warehouse', string="Warehouse")
    uom_id = fields.Many2one('mwide.inventory.uom', string="Unit of Measure")
    company_id = fields.Many2one('res.company', string="Company")
    
class MwideInventory(models.Model):
    _name = 'mwide.inventory'
    _inherit = ['mail.thread']
        
    name = fields.Char(string='Item Name', required=True)
    item_id = fields.Many2one('mwide.inventory.item',string="Item")
    description = fields.Char(string="Description", size=128)
    qty = fields.Float(string="Quantity", default=0, required=True)
    total_value = fields.Float(string='Total Value', default=0, required=True)
    aging_id = fields.Many2one('mwide.inventory.aging',string='Aging Days')
    category_id = fields.Many2one('mwide.inventory.category',string="Category", related='aging_id.category_id', store=True)
    company_id= fields.Many2one('res.company',string='Company')
    warehouse_id = fields.Many2one('mwide.inventory.warehouse', string="Warehouse")
    uom_id = fields.Many2one('mwide.inventory.uom', string="Unit of Measure")
    inventory_date = fields.Date("Inventory Date")
    
    
    @api.depends('total_value')
    def _compute_value(self):
        for record in self:
            if record.total_value > 0 and record.qty > 0:
                record.value = record.total_value / record.qty
            else:
                record.value = 0 
    value = fields.Float(string='Item Value', compute=_compute_value, store=True,readonly=False ) 
    