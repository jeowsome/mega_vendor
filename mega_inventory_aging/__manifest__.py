# -*- coding: utf-8 -*-
{
    'name': "MEGA-Invetory Aging",

    'summary': """
        Inventory Aging""",

    'description': """
       Inventory Aging
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -292,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/mwide_stock_data_view.xml',
        'data/mwide.inventory.uom.csv',
        'views/mwide_stock_views.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],    
}
