# -*- coding: utf-8 -*-
{
    'name': "Megawide Helpdesk Dashboard",

    'summary': """
         Megawide HelpdeskSystem Dashboard""",

    'description': """
         Megawide HelpdeskSystem Dashboard
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -300,

    # any module necessary for this one to work correctly
    'depends': ['base','mega_helpdesk','hr','contacts'],

    'external_dependencies': {
        'python': ['pandas'],
    },

    'data': [
        'security/ir.model.access.csv',
        'views/dashboard_action.xml'
    ],

    'qweb': ["static/src/xml/dashboard.xml"],
    'installable': True,
    'application': True,
    'auto_install': False,
}
