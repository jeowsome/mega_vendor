# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
import pandas as pd
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils
from odoo.exceptions import ValidationError, Warning

ROUNDING_FACTOR = 16
DATE_FILTER = [('ticket_date','!=',False),('ticket_date','!=',False)]

class MwideHelpdeskDashboard(models.Model):
    _inherit = 'mwide.helpdesk'
    
    
    
    
    @api.model
    def get_date_range_selector(self,range):
        date_now = fields.Datetime.now()    
        date_range = date_now - relativedelta(months=range)
        condition1 = ('ticket_date','>=',date_range)
        condition2=('ticket_date','<=',date_now)
        if range ==0:
            condition1 = ('ticket_date','!=',False)
            condition2=('ticket_date','!=',False)
        global DATE_FILTER
        DATE_FILTER = [condition1,condition2]
        return DATE_FILTER
        #return {
        #    'type':'ir.actions.client',
        #    'name':'custom_helpdesk_dashboard_tag',
        #    'tag':'custom_helpdesk_dashboard_tag tag',
        #    'params': {'DATE_FILTER':DATE_FILTER},
        #}
    
    @api.model
    def get_date_date_selector(self,from_date,to_date):
        if from_date == '' or to_date == '':
            raise ValidationError(_('You need to set both dates'))
        if datetime.strptime(from_date,'%Y-%m-%d') > datetime.strptime(to_date,'%Y-%m-%d'):
            raise ValidationError(_('Invalid input! From Date cannot be greater than To Date'))
        condition1 = ('ticket_date','>=', datetime.strptime(from_date,'%Y-%m-%d'))
        condition2=('ticket_date','<=',datetime.strptime(to_date,'%Y-%m-%d'))
        global DATE_FILTER
        DATE_FILTER=[condition1,condition2]
        print(DATE_FILTER)
        #return {
        #    'type': 'ir.actions.client',
        #    'tag':'custom_helpdesk_dashboard_tag'
        #} 
        return DATE_FILTER
    
    
    @api.model
    def get_closed_tickets(self):
        print(DATE_FILTER,"DATE_FILTER")
        closed_tickets = self.env['mwide.helpdesk'].sudo().search_count(['&','&',DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state', '=', '09resolved')]) or 0 
        print(closed_tickets,"closed")
        return {
            'closed_tickets': closed_tickets 
        }
    
    @api.model
    def get_overdue_tickets(self):
        overdue_tickets = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],('is_sla','=',True),'&',('type','=','it'),('html_color','in',["#FEB580","#F78585"])]) or 0 
        return {
            'overdue_tickets': overdue_tickets or 0 
        }

    @api.model
    def get_total_tickets(self):
        total_tickets = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state', '!=', '09resolved')]) or 0 
        return {
            'total_tickets': total_tickets 
        }

    @api.model
    def get_open_tickets(self):
        
        open_tickets = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),'|','&',('state','=','04approved_it'),('approval_selection','=','it_manager'),'|','&',('state_b','=','03approved_head'),('approval_selection','=','manager'),'&',('state_c','=','02request'),('approval_selection','=','none')]) or 0
        return {
            'open_tickets': open_tickets or 0 
        }

    @api.model
    def get_for_approval(self):
        for_approval = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),'|','&',('state','=','03approved_head'),('approval_selection','=','it_manager'),'&',('state_b','=','02request'),('approval_selection','=','manager')]) or 0
        return {
            'for_approval': for_approval or 0 
        }
        
    @api.model
    def get_assigned(self):
        assigned = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state','=','05assigned')]) or 0 
        return {
            'assigned': assigned or 0 
        }
        
    @api.model
    def get_onhold(self):
        onhold = 0
        onhold = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state','=','06on_hold')])
        return {
            'onhold': onhold or 0 
        }
        
        


    @api.model
    def get_open_tickets_access(self):
        
        open_tickets_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_access': open_tickets_access or 0 
        }

    @api.model
    def get_for_approval_access(self):
        
        for_approval_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_access': for_approval_access or 0
        }

    @api.model
    def get_draft_access(self):
        
        draft_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),'|',('state','=','01draft')])
        return {
            'draft_access': draft_access or 0
        }
        
    @api.model
    def get_all_access(self):
        
        all_access = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Access Management')])
        return {
            'all_access': all_access or 0
        }

    @api.model
    def get_assigned_access(self):
        assigned_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),('state','=','05routed')])
        return {
            'assigned_access': assigned_access or 0
        }
        
    @api.model
    def get_inprogress_access(self):
        inprogress_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),('state','=','06in_progress')])
        return {
            'inprogress_access': inprogress_access or 0
        }


    @api.model
    def get_open_tickets_application(self):
        
        open_tickets_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_application': open_tickets_application or 0
        }

    @api.model
    def get_for_approval_application(self):
        
        for_approval_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_application': for_approval_application or 0
        }

    @api.model
    def get_draft_application(self):
        
        draft_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),'|',('state','=','01draft')])
        return {
            'draft_application': draft_application or 0
        }
        
    @api.model
    def get_all_application(self):
        
        all_application = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Application Management')])
        return {
            'all_application': all_application or 0
        }

    @api.model
    def get_assigned_application(self):
        assigned_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),('state','=','05routed')])
        return {
            'assigned_application': assigned_application or 0
        }
        
    @api.model
    def get_inprogress_application(self):
        inprogress_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),('state','=','06in_progress')])
        return {
            'inprogress_application': inprogress_application or 0
        }


    @api.model
    def get_open_tickets_it_clearance(self):
        
        open_tickets_it_clearance= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_it_clearance': open_tickets_it_clearance or 0
        }

    @api.model
    def get_for_approval_it_clearance(self):
        
        for_approval_it_clearance= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_it_clearance': for_approval_it_clearance or 0
        }

    @api.model
    def get_draft_it_clearance(self):
        
        draft_it_clearance = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),'|',('state','=','01draft')])
        return {
            'draft_it_clearance': draft_it_clearance or 0
        }
        
    @api.model
    def get_all_it_clearance(self):
        
        all_it_clearance = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','IT Clearance')])
        return {
            'all_it_clearance': all_it_clearance  or 0
        }

    @api.model
    def get_assigned_it_clearance(self):
        assigned_it_clearance = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),('state','=','05routed')])
        return {
            'assigned_it_clearance': assigned_it_clearance or 0
        }
        
    @api.model
    def get_inprogress_it_clearance(self):
        inprogress_it_clearance = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),('state','=','06in_progress')])
        return {
            'inprogress_it_clearance': inprogress_it_clearance or 0
        }

    @api.model
    def get_open_tickets_network(self):
        
        open_tickets_network= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_network': open_tickets_network or 0
        }

    @api.model
    def get_for_approval_network(self):
        
        for_approval_network= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_network': for_approval_network or 0
        }

    @api.model
    def get_draft_network(self):
        
        draft_network = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),'|',('state','=','01draft')])
        return {
            'draft_network': draft_network or 0
        }
        
    @api.model
    def get_all_network(self):
        
        all_network = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Network')])
        return {
            'all_network': all_network or 0
        }

    @api.model
    def get_assigned_network(self):
        assigned_network = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),('state','=','05routed')])
        return {
            'assigned_network': assigned_network or 0
        }
        
    @api.model
    def get_inprogress_network(self):
        inprogress_network = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),('state','=','06in_progress')])
        return {
            'inprogress_network': inprogress_network or 0
        }

    @api.model
    def get_open_tickets_incident(self):
        
        open_tickets_incident= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_network': open_tickets_incident or 0
        }

    @api.model
    def get_for_approval_incident(self):
        
        for_approval_incident= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_incident': for_approval_incident or 0
        }

    @api.model
    def get_draft_incident(self):
        
        draft_incident = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),'|',('state','=','01draft')])
        return {
            'draft_incident': draft_incident or 0
        }
        
    @api.model
    def get_all_incident(self):
        
        all_incident = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Report an Incident')])
        return {
            'all_incident': all_incident or 0
        }

    @api.model
    def get_assigned_incident(self):
        assigned_incident = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),('state','=','05routed')])
        return {
            'assigned_incident': assigned_incident or 0
        }
        
    @api.model
    def get_inprogress_incident(self):
        inprogress_incident = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),('state','=','06in_progress')])
        return {
            'inprogress_incident': inprogress_incident or 0
        }

    @api.model
    def get_open_tickets_it_gen(self):
        
        open_tickets_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_it_gen': open_tickets_it_gen or 0
        }

    @api.model
    def get_for_approval_it_gen(self):
        
        for_approval_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_it_gen': for_approval_it_gen or 0
        }

    @api.model
    def get_draft_it_gen(self):
        
        draft_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),'|',('state','=','01draft')])
        return {
            'draft_it_gen': draft_it_gen or 0
        }
        
    @api.model
    def get_all_it_gen(self):
        
        all_it_gen = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','IT General Inquiry')])
        return {
            'all_it_gen': all_it_gen or 0
        }

    @api.model
    def get_assigned_it_gen(self):
        assigned_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),('state','=','05routed')])
        return {
            'assigned_it_gen': assigned_it_gen or 0
        }
        
    @api.model
    def get_inprogress_it_gen(self):
        inprogress_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),('state','=','06in_progress')])
        return {
            'inprogress_it_gen': inprogress_it_gen or 0
        }


    @api.model
    def get_open_tickets_hardware(self):
        
        open_tickets_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_hardware': open_tickets_hardware or 0
        }

    @api.model
    def get_for_approval_hardware(self):
        
        for_approval_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_hardware': for_approval_hardware or 0
        }

    @api.model
    def get_draft_hardware(self):
        
        draft_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),'|',('state','=','01draft')])
        return {
            'draft_hardware': draft_hardware or 0
        }
        
    @api.model
    def get_all_hardware(self):
        
        all_hardware = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Hardware')])
        return {
            'all_hardware': all_hardware or 0
        }
        
    @api.model
    def get_assigned_hardware(self):
        assigned_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),('state','=','05routed')])
        return {
            'assigned_hardware': assigned_hardware or 0
        }
        
    @api.model
    def get_inprogress_hardware(self):
        inprogress_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),('state','=','06in_progress')])
        return {
            'inprogress_hardware': inprogress_hardware or 0
        }

        
    @api.model
    def get_open_tickets_asset(self):
        
        open_tickets_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_asset': open_tickets_asset or 0 
        }

    @api.model
    def get_for_approval_asset(self):
        
        for_approval_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_asset': for_approval_asset or 0
        }

    @api.model
    def get_draft_asset(self):
        
        draft_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),'|',('state','=','01draft')])
        return {
            'draft_asset': draft_asset or 0
        }
        
    @api.model
    def get_all_asset(self):
        
        all_asset = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Asset Management')])
        return {
            'all_asset': all_asset or 0
        }

    @api.model
    def get_assigned_asset(self):
        assigned_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),('state','=','05routed')])
        return {
            'assigned_asset': assigned_asset or 0
        }
    
    @api.model
    def get_open_tickets_asset_req(self):
        
        open_tickets_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_asset_req': open_tickets_asset_req or 0 
        }

    @api.model
    def get_for_approval_asset_req(self):
        
        for_approval_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_asset_req': for_approval_asset_req or 0
        }

    @api.model
    def get_draft_asset_req(self):
        
        draft_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),'|',('state','=','01draft')])
        return {
            'draft_asset_req': draft_asset_req or 0
        }
        
    @api.model
    def get_all_asset_req(self):
        
        all_asset_req = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Asset Requisition')])
        return {
            'all_asset_req': all_asset_req or 0
        }

    @api.model
    def get_assigned_asset_req(self):
        assigned_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),('state','=','05routed')])
        return {
            'assigned_asset_req': assigned_asset_req or 0
        }
        
    @api.model
    def get_open_tickets_software(self):
        
        open_tickets_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'open_tickets_software': open_tickets_software or 0 
        }

    @api.model
    def get_for_approval_software(self):
        
        for_approval_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
        return {
            'for_approval_software': for_approval_software or 0
        }

    @api.model
    def get_draft_software(self):
        
        draft_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),'|',('state','=','01draft')])
        return {
            'draft_software': draft_software or 0
        }
        
    @api.model
    def get_all_software(self):
        
        all_software = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Software')])
        return {
            'all_software': all_software or 0
        }

    @api.model
    def get_assigned_software(self):
        assigned_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),('state','=','05routed')])
        return {
            'assigned_software': assigned_software or 0
        }
            

    @api.model
    def get_number_transaction_state(self):
        #query = '''SELECT mh.category_id, mh.state as ticket, count(mh.name) as total FROM mwide_helpdesk mh LEFT JOIN mwide_service_type mst on mh.category_id = mst.id and mh.state in ('') GROUP BY state ORDER by state;'''
        #self._cr.execute(query)
        #docs = self._cr.dictfetchall()
        #trans = []
        mst = []
        ticket_count= [] 
        total_open = []
        total_approval = []
        total_overdue = [] 
        total_assigned = [] 
        #open tickets, waiting for appproval, assigned, in_progress
        #'03approved_head','04approved_it','05routed','06in_progress'
        #service_types = ['Access Management', 'Application Management', 'IT Clearance','Network','Report an Incident', 'IT General Inquiry','Hardware']
        category_ids = self.env['mwide.category'].sudo().search([('type','=','it')])
        for type_id in category_ids:
            #open_tickets = self.sudo().search_count(['&',('category_id','=',type_id.id),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
            open_tickets = 0
            open_tickets = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=',type_id.id),'|','&',('state','=','04approved_it'),('approval_selection','=','it_manager'),'|','&',('state_b','=','03approved_head'),('approval_selection','=','manager'),'&',('state_c','=','02request'),('approval_selection','=','none')])
            #for_approval = self.sudo().search_count(['&',('category_id','=',type_id.id),'|',('state','=','03approved_head'),('state_b','=','02request')])
            for_approval = 0
            for_approval = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=',type_id.id),'|','&',('state','=','03approved_head'),('approval_selection','=','it_manager'),'&',('state_b','=','02request'),('approval_selection','=','manager')]) or 0
            assigned = 0
            assigned = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=',type_id.id),('state','=','05assigned')])
            overdue = 0
            overdue= self.env['mwide.helpdesk'].sudo().search_count([('is_sla','=',True),'&',('category_id','=',type_id.id),('html_color','in',["#FEB580","#F78585"])]) or 0 
            #inprogress = self.sudo().search_count(['&',('category_id','=',type_id.id),('state','=','06in_progress')])
            mst.append(type_id.name)
            total_open.append(open_tickets)
            total_approval.append(for_approval)
            total_overdue.append(overdue)
            total_assigned.append(assigned)
            ticket_count.append([open_tickets,overdue,for_approval,assigned])

        final = [mst, ticket_count,[total_open,total_approval,total_overdue,total_assigned]]
        return final
    
    @api.model
    def get_number_transaction_pie(self):
        total_pass = 0
        total_fail = 0
        open_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('type','=','it'),('state','=','05assigned')])
        for open in open_resolution_tickets:
            if open.html_color == '#F78585':
                if open.is_sla ==True:
                    total_fail+=1
                else:
                    total_pass+=1
            else:
                total_pass +=1
        close_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('type','=','it'),('state','not in',['05assigned','09resolved'])])
        for close in close_resolution_tickets:
            if close.resolution_time + close.response_time > close.time_resolution:
                if close.is_sla ==True:
                    total_fail+=1
                else:
                    total_pass +=1
            else:
                total_pass +=1
        resolved = self.env['mwide.helpdesk'].search(['&',('state','=','09resolved'),('type','=','it')])
        for ticket in resolved:
            if ticket.resolution_time + ticket.response_time > ticket.time_resolution:
                if ticket.is_sla == True:
                    total_fail +=1
                else:
                    total_pass+=1
            else:
                total_pass +=1

        final = [total_pass,total_fail]
        return final

    @api.model
    def get_record(self):
        cr = self._cr
        cr.execute("""SELECT mh.name, he.name, mh.ticket_date, mh.state FROM mwide_helpdesk mh JOIN hr_employee he ON mh.requested_id = he.user_id ORDER BY mh.name DESC LIMIT 5""")
        get_record = cr.fetchall()
        return {
            'get_record': get_record
        }
        
    @api.model
    def get_record_count(self):
        #cr = self._cr
        #cr.execute("""SELECT mh.name, he.name, mh.ticket_date, mh.state FROM mwide_helpdesk mh JOIN hr_employee he ON mh.requested_id = he.user_id ORDER BY mh.name DESC LIMIT 5""")
        #get_record = cr.fetchall()
        categ_ids = self.env['mwide.category'].search([('type','=','it')])
        rec_count = []
        rec_response_count = []
        rec_resolution_count = []
        total_resolved = 0
        total_new = 0
        total_assigned = 0
        total_all = 0
        total_after_15 = 0
        total_within_15=0
        total_response_all =0 
        total_fail = 0
        total_pass = 0
        for categ in categ_ids:
            resolved = 0
            new = 0
            assigned = 0 
            total = 0
            for itss in self.env['mwide.helpdesk'].search([('category_id','=',categ.id)]):
                if itss.state == '09resolved':
                    resolved += 1 
                elif itss.state in ['01draft','02request','03approved_head','04approved_it']:
                    new += 1
                elif itss.state in ['05assigned','06on_hold','07validation','08unresolved']:
                    assigned += 1
            total = resolved + assigned + new
            total_resolved += resolved
            total_new += new
            total_assigned += assigned
            total_all += total
            rec = (categ.name,resolved,new,assigned,total)
            rec_count.append(rec)
            after_15 = 0
            within_15 = 0
            #REPONSE
            open_response_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','in',['02request','03approved_head','04approved_it'])])
            for open in open_response_tickets:
                #if open.html_color == '#F78585':
                #     after_15 += 1
                #else:
                    within_15 +=1
            close_response_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','not in',['02request','03approved_head','04approved_it'])])
            for close in close_response_tickets:
                within_15+=1
            response_all = after_15 + within_15
            total_after_15 += after_15
            total_within_15+= within_15
            total_response_all = total_after_15 + total_within_15
            if response_all <=0:
                response_all =1
            rec_response = (after_15,within_15,after_15 + within_15,str(round((within_15/response_all)*100,2))+'%')
            rec_response_count.append(rec_response)
            #RESOLUTION START
            fail=0
            res_pass=0
            open_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','=','05assigned')])
            for open in open_resolution_tickets:
                if open.html_color == '#F78585':
                    if open.is_sla == True:
                        fail += 1
                    else:
                        res_pass +=1
                else:
                    res_pass +=1
            close_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','not in',['05assigned','09resolved'])])
            for close in close_resolution_tickets:
                
                if close.resolution_time + close.response_time > close.time_resolution:
                    if close.is_sla == True:
                        fail+=1
                    else:
                        res_pass +=1
                else:
                    res_pass +=1
            resolved_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','=','09resolved')])
            for resolved in resolved_resolution_tickets:
                if resolved.resolution_time + resolved.response_time > resolved.time_resolution:
                    if resolved.is_sla == True:
                        fail+=1
                    else:
                        res_pass +=1
                else:
                    res_pass +=1
            res_all = fail+res_pass
            if res_all <=0:
                res_all =1
            rec_resolution = (fail,res_pass,fail+res_pass,str(round((res_pass/res_all)*100,2))+'%')
            rec_resolution_count.append(rec_resolution)
            total_fail += fail
            total_pass += res_pass
            total_resolution_all = total_fail+total_pass    
            #percent
            if total_response_all <=0:
                total_response_all=1
            if total_resolution_all<=0:
                total_resolution_all=1
            total_response_percent = (total_within_15/total_response_all)*100
            total_resolution_percent = (total_pass/total_resolution_all)*100
        return {'get_response_count':rec_response_count,'get_resolution_count':rec_resolution_count,'total_response_percent':str(round(total_response_percent,2))+'%','total_resolution_percent':str(round(total_resolution_percent,2))+'%',
            'get_record_count': rec_count,'total_resolved':total_resolved,'total_new':total_new,'total_assigned':total_assigned,'total_all':total_all,
            'total_after_15':total_after_15,'total_within_15':total_within_15,'total_response_all':total_response_all,'total_pass':total_pass,'total_fail':total_fail,'total_resolution_all':total_resolution_all
        }
