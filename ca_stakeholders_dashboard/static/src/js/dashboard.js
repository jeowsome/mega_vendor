odoo.define('ca_stakeholders_dashboard.BirthdayDashboard', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var core = require('web.core');
var QWeb = core.qweb;
var rpc = require('web.rpc');
var ajax = require('web.ajax');

var StakeholdersBirthdayDashboard = AbstractAction.extend({
    template: 'StakeholdersBirthdayDashboard',

    // Javascript Event upon changing the selected value on the select
    events: {
            'change #select_period':'check_status',
    },

    init: function(parent, context) {
          var self = this;
          this._super(parent, context);
          this.dashboards_templates = ['StakeholdersBirthdayContent'];
      },


      start: function() {
              var self = this;
              this.set("title", 'Dashboard - CA Stakeholders Birthday');
              return this._super().then(function() {
                  self.render_dashboards();
                  // Initializing when opening the dashboard Page
                  let status = 'start';
                  self.check_status(status)
              });
      },

      render_dashboards: function() {
          var self = this;
          _.each(this.dashboards_templates, function(template) {
                self.$('.o_ca_stakeholders_dashboard').append(QWeb.render(template, {widget: self}));
        });
      },

      willStart: function(){
      var self = this;
      return $.when(ajax.loadLibs(this), this._super()).then(function() {
        return self.fetch_data();
        });
      },

      fetch_data: function() {
      var self = this;
      var def1 =  this._rpc({
              model: 'mwide.stakeholders_dashboard',
              method: 'get_labels',
              // args: [startDate,endDate,sbu]
      }).then(function(result) {
              // Labels
              self.label = result['label']
              self.label_this_month = result['label_this_month']
              self.label_next_month = result['label_next_month']

              // Header Labels
              self.today_label = result['today_label']
              self.tomorrow_label = result['tomorrow_label']
              self.twodays_label = result['twodays_label']
              self.threedays_label = result['threedays_label']

              // Header Count Value
              self.today_c = result['today_c']
              self.tomorrow_c = result['tomorrow_c']
              self.twodays_c = result['twodays_c']
              self.threedays_c = result['threedays_c']
      });
      return $.when(def1);
      },

      // Upon Opening The Dashboard Page and Onchange Function
      check_status: function(status){
        var self = this;
        if (status == 'start') {
            self.onchange_select_period(0)
            rpc.query({
                model: 'mwide.stakeholders_dashboard',
                method: 'get_current_side_summary'
        }).then(function(result) {
           let list = result['data'];

           // Populate Table
           list.forEach(data => {
                 let row = `<tr>
                               <td class="td-center t-bold "> ${data['name']} </td>
                               <td class="td-center t-bold c-blue t-white"> ${data['birthday']} </td>
                               <td class="td-center t-bold c-blue t-white"> ${data['days']} </td>
                            </tr>`
                 $('#current_row_data').append(row)
           });
        });

            rpc.query({
                model: 'mwide.stakeholders_dashboard',
                method: 'get_next_side_summary'
        }).then(function(result) {
           let list = result['data'];

           // Populate Table
           list.forEach(data => {
                 let row = `<tr>
                               <td class="td-center t-bold "> ${data['name']} </td>
                               <td class="td-center t-bold c-blue t-white"> ${data['birthday']} </td>
                               <td class="td-center t-bold c-blue t-white"> ${data['days']} </td>
                            </tr>`
                 $('#next_row_data').append(row)
           });
        });

        } else {
            let period = document.getElementById('select_period').value
            self.onchange_select_period(period)
        }


      },

      // Onchange Function of JS when changing a selected option
      onchange_select_period: function(period){
        var self = this;
        rpc.query({
                model: 'mwide.stakeholders_dashboard',
                method: 'get_summary',
                args: [period]
        }).then(function(result) {
           let label = result['label'];
           let list = result['data'];

           // Main Table Label
           $('#main_label_table_text').html(label)

           // Get All TD Class
           let mainTable = document.querySelectorAll(".main-table-data");
           // Remove Currently Loaded Rows
           Array.from(mainTable).forEach(element => {
               element.remove();
           });
           // Populate Table
           list.forEach(data => {
                 let row = `<tr>
                               <td class="td-center t-bold main-table-data"> ${data['name']} </td>
                               <td class="td-center t-bold main-table-data c-blue t-white"> ${data['birthday']} </td>
                               <td class="td-center t-bold main-table-data c-blue t-white"> ${data['days']} </td>
                            </tr>`
                 $('#main_row_data').append(row)
           });
        // End of Asynchronous
        });
      },

      // Onchange Function of JS when changing a selected option


});

core.action_registry.add('custom_ca_stakeholders_dashboard_tag', StakeholdersBirthdayDashboard);

return StakeholdersBirthdayDashboard;

});
