#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta

class MwideStakeholdersDashboard(models.Model):
    _name = "mwide.stakeholders_dashboard"
    _description = "Stakeholders Dashboard"

    # Main Label Action
    @api.model
    def get_labels(self):
        # Initialize Variables
        today = datetime.now().date()
        tomorrow = today + relativedelta(days=1)
        two_days = today + relativedelta(days=2)
        three_days = today + relativedelta(days=3)

        # Label
        label = self.get_as_of_date()
        label_this_month = self.get_summary_this_month()
        label_next_month = self.get_summary_next_month()

        today_label = self.format_full_date_name(today)
        tomorrow_label = self.format_full_date_name(tomorrow)
        twodays_label = self.format_full_date_name(two_days)
        threedays_label = self.format_full_date_name(three_days)
        
        list_date = self.get_three_days(today)
        list_data = []
        for ld in list_date:
            month = ld.month
            day = ld.day
            get_data = self.get_month_count(month, day)
            for gd in get_data:
                list_data.append(gd)
                
        today_c = list_data[0]
        tomorrow_c = list_data[1]
        twodays_c = list_data[2]
        threedays_c = list_data[3]
                    
        # Header Summary
        #today_c = self.env['mega.stake_email_temp'].search_count([('date_email_send','=',today)])
        #tomorrow_c = self.env['mega.stake_email_temp'].search_count([('date_email_send','=',tomorrow)])
        #twodays_c = self.env['mega.stake_email_temp'].search_count([('date_email_send','=',two_days)])
        #threedays_c = self.env['mega.stake_email_temp'].search_count([('date_email_send','=',three_days)])

        #today_c = self.env['res.partner'].search_count([('birthday','=',datetime.now().date())])
        #tomorrow_c = self.env['res.partner'].search_count([('birthday','=',datetime.now().date() + relativedelta(days=1))])
        #twodays_c = self.env['res.partner'].search_count([('birthday','=',datetime.now().date() + relativedelta(days=2))])
        #threedays_c = self.env['res.partner'].search_count([('birthday','=',datetime.now().date() + relativedelta(days=3))])
        
        return {
            'label': label,
            'label_this_month': label_this_month,
            'label_next_month': label_next_month,

            'today_label': today_label,
            'tomorrow_label': tomorrow_label,
            'twodays_label': twodays_label,
            'threedays_label': threedays_label,

            'today_c': today_c,
            'tomorrow_c': tomorrow_c,
            'twodays_c': twodays_c,
            'threedays_c': threedays_c,
        }

    # Main Summary Action
    @api.model
    def get_summary(self, period):
        # Initialize Variable
        today = datetime.now().date()
        tomorrow = today + relativedelta(days=1)
        two_days = today + relativedelta(days=2)
        three_days = today + relativedelta(days=3)
        past_seven_days = today - relativedelta(days=7)
        today_year = today.year

        # Conditional Period Based on the Select
        if int(period) == 0:
            month = today.month
            day = today.day
            label = self.format_full_date_name(today)
            option = 'static'
        elif int(period) == 1:
            month = tomorrow.month
            day = tomorrow.day
            label = self.format_full_date_name(tomorrow)
            option = 'static'
        elif int(period) == 2:
            month = two_days.month
            day = two_days.day
            label = self.format_full_date_name(two_days)
            option = 'static'
        elif int(period) == 3:
            month = three_days.month
            day = three_days.day
            label = self.format_full_date_name(three_days)
            option = 'static'
        else:
            # Format Label
            format_past_seven_days = self.format_full_date_name(past_seven_days)
            get_yesterday = today - relativedelta(days=1)
            format_yesterday = self.format_full_date_name(get_yesterday)
            label = f"{format_past_seven_days} - {format_yesterday}"
            # Get Past 7 dates
            list_date = self.get_seven_days(today)
            # For Else Statement
            option = 'other'

        # Main Table Summary - Function Call Query
        if option == 'static':
            list_data = self.get_main_table_summary(month, day)
            # Function Format Data
            data = self.format_main_table_update_dictionary(list_data, today, today_year)
        else:
            # Append Data with Structure of List Dictionary
            list_data = []
            for ld in list_date:
                month = ld.month
                day = ld.day
                get_data = self.get_main_table_summary(month, day)
                for gd in get_data:
                    list_data.append(gd)
            # Function Format Data
            data = self.format_main_table_update_dictionary_seven_days(list_data, today, today_year)
        return {
            'label': label,
            'data': data
        }
        
    @api.model
    def get_current_side_summary(self):
        today = datetime.now().date()
        today_year = today.year
        
        month = today.month
        
        get_data = self.get_month_table_summary(month)
        
        list_data = []
        for gd in get_data:
            list_data.append(gd)
        data = self.format_month_table_update_dictionary(list_data, today, today_year)

        return {
            'data':data
            }
        
    @api.model
    def get_next_side_summary(self):
        today = datetime.now().date()
        today_year = today.year
        
        month = today.month +1
        
        get_data = self.get_month_table_summary(month)
        
        list_data = []
        for gd in get_data:
            list_data.append(gd)
        data = self.format_month_table_update_dictionary(list_data, today, today_year)

        return {
            'data':data
            }
        
    @api.model
    def get_counts(self):
        today = datetime.now().date()
        today_year = today.year
        
        month = today.month
        
        get_data = self.get_month_table_summary(month)
        
        list_data = []
        for gd in get_data:
            list_data.append(gd)
        data = self.format_month_table_update_dictionary(list_data, today, today_year)

        return {
            'data':data
            }

    # Get Values ------------------------------------------------------------

    @api.model
    def get_as_of_date(self):
        get_date = datetime.now()
        data = get_date.strftime("%B %d, %Y")
        return data

    @api.model
    def get_summary_this_month(self):
        get_date = datetime.now()
        data = self.format_month_name(get_date)
        return data

    @api.model
    def get_summary_next_month(self):
        get_date = datetime.now().strftime("%Y-%m-01")
        format_get_date = datetime.strptime(get_date, '%Y-%m-%d')
        get_next_month = format_get_date + relativedelta(months=1)
        data = self.format_month_name(get_next_month)
        return data

    #
    @api.model
    def get_main_table_summary(self, month, day):
        query = """ SELECT
                    	rp.name,
                    	rp.birthday
                    FROM res_partner rp
                    WHERE
                    	EXTRACT(MONTH FROM rp.birthday) = %(month)s AND EXTRACT(day FROM rp.birthday) = %(day)s
                    ORDER BY rp.name asc """
        args = {
            'month': month,
            'day': day
        }
        self._cr.execute(query,args)
        data = self.env.cr.dictfetchall()
        return data
    
    @api.model
    def get_month_table_summary(self, month):
        query = """ SELECT
                        rp.name,
                        rp.birthday
                    FROM res_partner rp
                    WHERE
                        EXTRACT(MONTH FROM rp.birthday) = %(month)s
                    ORDER BY (EXTRACT(DAY FROM rp.birthday)) asc """
        args = {
            'month': month,
        }
        self._cr.execute(query,args)
        data = self.env.cr.dictfetchall()
        return data
    
    @api.model
    def get_month_count(self, month, day):
        query = """ SELECT COUNT(*)
                    FROM res_partner rp
                    WHERE
                        EXTRACT(MONTH FROM rp.birthday) = %(month)s AND EXTRACT(day FROM rp.birthday) = %(day)s """
        args = {
            'month': month,
            'day': day,
        }
        self._cr.execute(query,args)
        data = self.env.cr.fetchall()
        return data

    # Universal ------------------------------------------------------------

    @api.model
    def format_main_table_update_dictionary(self, list_data, today, today_year):
        for ld in list_data:
            # Assign Birthday
            birthday = ld['birthday']
            # Format Birthday into this year
            format_this_year_birthday = birthday.strftime(f"{today_year}-%m-%d")
            # Typecast to Date Object - Type is Date
            date_format_this_year_birthday = datetime.strptime(format_this_year_birthday, '%Y-%m-%d').date()

            # Clean Values -->
            # Get Full Name Birthday
            format_birthday = self.format_full_date_name(date_format_this_year_birthday)
            # Get Days Difference
            int_days = int((date_format_this_year_birthday - today).days)
            if int_days == 1:
                ld['days'] = f"{int_days} day left"
            # elif int_days == 0:
            #     ld['days'] = "Today"
            else:
                ld['days'] = f"{int_days} days left"
            # Update data on birthday with string Full Birthday Name
            ld.update({"birthday": format_birthday})
        return list_data

    @api.model
    def format_main_table_update_dictionary_seven_days(self, list_data, today, today_year):
        for ld in list_data:
            # Assign Birthday
            birthday = ld['birthday']
            # Format Birthday into this year
            format_this_year_birthday = birthday.strftime(f"{today_year}-%m-%d")
            # Typecast to Date Object - Type is Date
            date_format_this_year_birthday = datetime.strptime(format_this_year_birthday, '%Y-%m-%d').date()

            # Clean Values -->
            # Get Full Name Birthday
            format_birthday = self.format_full_date_name(date_format_this_year_birthday)
            # Get Days Difference
            int_days = int((date_format_this_year_birthday - today).days) * -1
            if int_days == 1:
                ld['days'] = f"{int_days} day ago"
            else:
                ld['days'] = f"{int_days} days ago"
            # Update data on birthday with string Full Birthday Name
            ld.update({"birthday": format_birthday})
        return list_data
    
    @api.model
    def format_month_table_update_dictionary(self, list_data, today, today_year):
        for ld in list_data:
            # Assign Birthday
            birthday = ld['birthday']
            # Format Birthday into this year
            format_this_year_birthday = birthday.strftime(f"{today_year}-%m-%d")
            # Typecast to Date Object - Type is Date
            date_format_this_year_birthday = datetime.strptime(format_this_year_birthday, '%Y-%m-%d').date()

            # Clean Values -->
            # Get Full Name Birthday
            format_birthday = self.format_full_date_name(date_format_this_year_birthday)
            # Get Days Difference
            int_days = int((date_format_this_year_birthday - today).days)
            if int_days > 0:
                if int_days == 1:
                    ld['days'] = f"{int_days} day left"
                else:
                    ld['days'] = f"{int_days} days left"
            elif int_days == 0:
                ld['days'] = "Today"
            else:
                int_past_days = int((date_format_this_year_birthday - today).days) * -1
                if int_past_days == 1:
                    ld['days'] = f"{int_past_days} day ago"
                else:
                    ld['days'] = f"{int_past_days} days ago"
                
            # Update data on birthday with string Full Birthday Name
            ld.update({"birthday": format_birthday})
        return list_data

    @api.model
    def get_seven_days(self, today):
        list_date = []
        for i in (range(8)):
            if i != 0:
                date = today - relativedelta(days=i)
                list_date.append(date)
        return list_date
    
    @api.model
    def get_three_days(self, today):
        list_date = []
        list_date.append(today)
        for i in (range(4)):
            if i != 0:
                date = today + relativedelta(days=i)
                list_date.append(date)
        return list_date

    @api.model
    def format_month_name(self, date):
        data = date.strftime("%B")
        return data

    @api.model
    def format_full_date_name(self, day):
        data = day.strftime("%B %d, %Y")
        return data
