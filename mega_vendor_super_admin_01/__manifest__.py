# -*- coding: utf-8 -*-
{
    'name': "MEGA-Vendor Accreditation Project Admin",

    'summary': """
        Manage Megawide Vendor Accreditation System Admin""",

    'description': """
        Manage Megawide Vendor Accreditation System Admin
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -301,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail','contacts','mega_vendor','mega_active_directory'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/vendor_views_admin.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
