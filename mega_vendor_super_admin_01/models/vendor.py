# Inherit from Odoo base res.partners
from urllib import parse
from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning, UserError
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time
from pickle import TRUE


_logger = logging.getLogger(__name__)


class MegaVendorAdmin(models.Model):
    _description = "List of Vendor Super Admin"
    # _name = 'vendor.super_admin'
    # _inherit = ['mail.thread','mega.vendor']
    _inherit = 'mega.vendor'

    is_admin = fields.Boolean('Super Admin', compute='_compute_super_admin')

    def _compute_super_admin(self):
        user_group = self.env['res.users'].has_group('mega_vendor_super_admin.group_super_admin')
        for rec in self:
            if user_group:
                rec.is_admin = True
            else:
                rec.is_admin = False

    def action_draft(self):
        print('Action Run ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        for rec in self:
            rec.state_bu = '01draft'
            rec.state_epc = '01draft'
