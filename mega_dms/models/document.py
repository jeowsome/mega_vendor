#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time

_logger = logging.getLogger(__name__)

class MwideDocumentLikes(models.Model):
    _name = 'mwide.document.likes'
    _description ='Document Likes'
    
    
    name = fields.Char(string="Name")
    document_id = fields.Many2one('mwide.document',string="Document")
    user_id = fields.Many2one('res.users',string="User")

        

class MwideDocument(models.Model):
    _name = "mwide.document"
    _description = "Document"
    _parent_name = "doc_parent_id"
    _parent_store = True
    _parent_order = 'sequence,id'
    _order = 'doc_parent_id,sequence,id'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    '''def _default_record_type(self):
        res = 'file' 
        if self.env.user.has_group('mega_dms.group_document_user'):
            res = 'file'
        elif  self.env.user.has_group('mega_dms.group_document_manager') or self.env.user.has_group('mega_dms.group_document_publisher'):
            res = 'folder'
        return res'''
    @api.model
    def create(self,vals):
        if vals['record_type'] == 'folder':
            vals['state'] = 'published'
        return super(MwideDocument, self).create(vals)
    
    def write(self,vals):
        if 'record_type' in vals:
            if vals['record_type'] == 'folder':
                vals['state'] = 'published'
        return super(MwideDocument, self).write(vals)
    
    parent_path = fields.Char(index=True)
    parent_left = fields.Integer(index=True)
    parent_right = fields.Integer(index=True)
    active = fields.Boolean(default=True)
    sequence = fields.Integer(default=0)
    color = fields.Integer()
    name = fields.Char('Name', required=True)
    full_name = fields.Char('Path', compute='_compute_full_name')
    description = fields.Char('Description')
    content = fields.Html('Content')
    doc_parent_id = fields.Many2one('mwide.document', "Parent", ondelete="cascade", index=True)
    parent_full_name = fields.Char("Path")
    child_ids = fields.One2many('mwide.document', 'doc_parent_id', string='Child')
    child_count = fields.Integer(compute='_compute_child_count', string='Child Count')
    record_type = fields.Selection([('file','File'),('folder','Folder')],'Type', default='folder')
    like_ids = doc_attachment_id = fields.Many2many('mwide.document.likes', 'doc_likes_rel', 'document_id', 'like_id', string="Likes",track_visibility='onchange')
    doc_attachment_id = fields.Many2many('ir.attachment', 'doc_attach_rel', 'doc_id', 'attach_id3', string="Attachment", help='You can attach the copy of your document', copy=False)
    like_count = fields.Integer(compute='_compute_like_count',string="Like Count")
    is_like = fields.Boolean(compute='_compute_like_boolean',string="Is Like")
    others_like = fields.Integer(compute='_compute_like_count_others',string="Like Count Others")
    company_id = fields.Many2one('res.company',string="Company")
    is_publisher = fields.Boolean(string="Is Publisher?",compute="_compute_is_publisher")
    
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submit'),
        ('published', 'Published'),
    ], 'Status', default="draft", tracking=True)      

    _sql_constraints = [
        ('doc_parent_id_name_uniq', 'unique(doc_parent_id, name)', 'Name already exists!'),
    ]
    
    def _compute_is_publisher(self):
        if self.env.user.has_group('mega_dms.group_document_publisher') or self.env.user.has_group('mega_dms.group_document_manager'):
            print("TRUE")
            self.is_publisher = True
        else:
            print("FALSE")
            self.is_publisher = False
        
    def action_open_folder(self):
        self.ensure_one()
        view = {
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,tree,form',
            'res_model': 'mwide.document',
            'name': self.name,
            'search_view_id': self.env.ref('mega_dms.view_document_search').id,
            'context': {'search_default_doc_parent_id':self.id,'default_doc_parent_id':self.id,'search_default_filter_published':1}
        }
        return view

    def document_like(self):
        liked = False
        for like_id in self.like_ids:
            if like_id.user_id == self.env.user:
                liked = True
                self.like_ids = [(2,like_id.id)]
        if not liked:
            l = self.env['mwide.document.likes'].create({'document_id':self.id,'user_id':self.env.uid,'name':self.env.user.name})
            self.like_ids = [(4,l.id)]
        
    def _compute_like_boolean(self):
        liked = False
        for like_id in self.like_ids:
            if like_id.user_id == self.env.user:
                liked = True
        self.is_like = liked 
    
    @api.onchange('doc_attachment_id')
    def _onchange_attachment_id(self):
        if self.doc_attachment_id:
            self.record_type = 'file'
            self.company_id = self.env.user.company_id
        else:
            self.record_type = 'folder'
            self.company_id = False
            
    @api.onchange('record_type')
    def _onchange_record_type(self):
        if self.record_type == 'file':
            self.company_id = self.env.user.company_id
        else:
            self.company_id = False
            
    
    @api.constrains('doc_parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('Parent already recursive!'))

    def _compute_child_count(self):
        relative_field = self._fields.get("child_ids")
        comodel_name = relative_field.comodel_name
        inverse_name = relative_field.inverse_name
        count_data = self.env[comodel_name].read_group(['&',(inverse_name, 'in', self.ids),('state','=','published')], [inverse_name], [inverse_name])
        mapped_data = dict([(count_item[inverse_name][0], count_item['%s_count' % inverse_name]) for count_item in count_data])
        for record in self:
            record.child_count = mapped_data.get(record.id, 0)

    def _compute_like_count(self):
        self.like_count = self.env['mwide.document.likes'].search_count([('document_id','=',self.id)]) or 0
    
    def _compute_like_count_others(self):
        count = self.env['mwide.document.likes'].search_count([('document_id','=',self.id)]) or 0
        self.others_like = count - 1
        
    def name_get(self):
        if self.env.context.get('display_full_name', False):
            pass
        else:
            return super(MwideDocument, self).name_get()
        def get_names(record):
            res = []
            while record:
                res.append(record.name or '')
                record = record.doc_parent_id
            return res
        return [(record.id, " / ".join(reversed(get_names(record)))) for record in self]

    def _compute_full_name(self):
        for rec in self:
            parent_name = '' 
            if rec.doc_parent_id:
                parent_name = rec.doc_parent_id.full_name
            else:
                pass
            if not parent_name:
                parent_name = ''
            rec.full_name = parent_name + '/' + rec.name

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        default.update(name=_("%s (copy)") % (self.name or ''))
        return super(MwideDocument, self).copy(default)

    def action(self):
        self.ensure_one()
        context = self.env.context
        action_id = context.get('module_action_id')
        if action_id:
            action_dict = self.env.ref(action_id).read([
                "type", "res_model", "view_mode", "domain"
            ])[0]
            action_dict["name"] = self.name
        return action_dict

    def set_submit(self):
        for p in self:
            p.state = 'submit'
            # p.on_hold_id = self._uid
            # p.on_hold_date = fields.Datetime.now()

    def set_publish(self):
        for p in self:
            p.state = 'published'
            # p.user_closed_id = self._uid
            # p.close_date = fields.Datetime.now()

    def set_draft(self):
        for p in self:
            p.state = 'draft'
            # p.user_closed_id = self._uid
            # p.close_date = fields.Datetime.now() 

class DocumentMany2manyAttachment(models.Model):
    _inherit = 'ir.attachment'

    doc_attach_rel = fields.Many2many('mwide.document', 'doc_attachment_id', 'attach_id3', 'doc_id', string="Attachment", invisible=1)