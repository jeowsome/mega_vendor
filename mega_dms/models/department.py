#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time

_logger = logging.getLogger(__name__)


class MegaDepartment(models.Model):
    _name = "mwide.department"
    _description = "Departments"
    _order = "name"

    name = fields.Char(string = 'Department Name', required=True, tracking=True)

 



# plan_to_change_car = fields.Boolean('Plan To Change Car', default=False)

# class partners(models.Model):
#     _name = 'partners.partners'
#     _description = 'partners.partners'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()

#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
