# -*- coding: utf-8 -*-
{
    'name': "MEGA-Helpdesk Self-Service",

    'summary': """
        Manage Megawide Helpdesk Self-Service""",

    'description': """
        Manage Megawide Helpdesk Self-Service
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Services',
    'version': '1.0.0',
    'sequence': -299,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail','hr','mega_active_directory'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/helpdesk_channel_data.xml',
        'data/mwide.severity.csv',
        'data/hr.department.csv',
        'data/mwide.priority.csv',
        'data/helpdesk_email_templates.xml',
        #'data/res_company_data.xml',
        'data/mwide_category_data_view.xml',
        'data/mwide.service_type.csv',
        'views/category_views.xml',
        'views/masterdata_views.xml',
        'views/helpdesk_views.xml',
        'views/hr_services_views.xml',
        'views/boarding_helpdesk.xml',
        'views/heldesk_data.xml',
        'views/hr_employee_inherit.xml',
        'views/ir_cron_data.xml'

        #'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],    
}
