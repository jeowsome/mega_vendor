#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners
from urllib import parse
from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time
from pickle import TRUE
#from custom_addons.mega_helpdesk.controllers.BusinessHours import BusinessHours


_logger = logging.getLogger(__name__)


class Helpdesk(models.Model):
    _name = "mwide.helpdesk"
    _description = "Helpdesk Ticket"
    _order = "name, company_id"
    _inherit = ['mail.thread']
    name = fields.Char(string = 'Ticket No.', 
        default='00000000',
        required=True,
        readonly=True,
        state={'1draft': [('readonly', False)]})

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('mwide.name') or '00000000'
        if 'parent_id' in vals:
            seq_name = self.env['mwide.helpdesk'].search([('id','=',vals['parent_id'])])
            count = self.env['mwide.helpdesk'].search_count([('parent_id','=',vals['parent_id'])])
            if not count:
                count = 0
            if seq_name:           
                seq = seq_name.name + "-0" + str(count+1)
            else:
                pass
        vals['name'] = seq   
        vals['resolution_time'] = 0
        vals['response_time'] = 0
        if 'severity_id' in vals:
            print(vals['severity_id'])
            s = self.env['mwide.severity'].search([('id','=',vals['severity_id'])])
            vals['time_remaining'] = s.time_resolution
        return super(Helpdesk, self).create(vals)  
    
        
        


    date_trans = fields.Date("Date Created",
        required=True,
        default=fields.Date.context_today,
        copy=False,
        readonly=True)

    sbu = fields.Selection([('holdco','HOLDCO'),('epc','EPC'),('cpi','CPI'),('pitx','PITX'),('ph','PH1WORLD'),('bu','BU'),('c2w','CEBU2WORLD')],string="SBU")
    location_id = fields.Many2one("mwide.location", string="Location", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).location_id)
    project_id = fields.Many2one("mwide.project", string="Project", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).project_id)
    department_id = fields.Many2one("hr.department", string="Department", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).department_id)
    category_id = fields.Many2one("mwide.category", string="Category", required=True, ondelete="restrict", track_visibility='onchange')
    service_type_id = fields.Many2one("mwide.service_type", string="Service Type", required=True, ondelete="restrict", track_visibility='onchange', domain="[('category_id', '=', category_id)]")
    specific_ids = fields.Many2many("mwide.specifics", string="Specifics", track_visibility='onchange')
    bol_it_mngr = fields.Boolean(string="Needs IT Head Approval", related='service_type_id.bol_it_mngr')
    bol_manager = fields.Boolean(string="Needs Manager Approval", related='service_type_id.bol_manager')
    requested_id = fields.Many2one("res.users", required=True, readonly=True, ondelete='restrict', auto_join=True,
        string='Requested by', help='Partner-related data of the user',
        default=lambda self: self.env.user)
    ticket_date = fields.Datetime('Date Submitted')
    sap_user_ids = fields.Char('SAP Users')
    approver_id = fields.Many2one('res.users','IT Approver',default=lambda self: self.env['hr.department'].search([('is_itdep','=',True)],limit=1).severity_approver_id.id)
    board_id = fields.Many2one('mwide.helpdesk.boarding','Off/On-Boarding Reference')
    it_date = fields.Datetime("Approval Date (IT)")
    it_department_id = fields.Many2one("hr.department", string="IT Department", required=True, ondelete="restrict", default=lambda self: self.env['hr.department'].search([('is_itdep','=',True)],limit=1))
    it_mngr_id = fields.Many2one("res.users", string="IT Manager", required=False, ondelete="restrict", default=lambda self: self.env['hr.department'].search([('is_itdep','=',True)],limit=1).manager_id.user_id)
    assigned_id = fields.Many2one("res.users", string="Assigned To", required=False, ondelete="restrict", track_visibility='onchange', domain="[('employee_id.department_id', '=', it_department_id)]")
    assigned_date = fields.Datetime("Assigned Date")

    in_progress_id = fields.Many2one("res.users", string="User In-Progress", required=False, ondelete="restrict", track_visibility='onchange')
    in_progress_date = fields.Datetime("In-Progress Date")

    on_hold_id = fields.Many2one("res.users", string="User Pending", required=False, ondelete="restrict")
    on_hold_date = fields.Datetime("Pending Date")

    user_closed_id = fields.Many2one("res.users", string="User Closed", required=False, ondelete="restrict")
    close_date = fields.Datetime("Closed Date")
    type = fields.Selection([('hr','Human Resources'),('it','IT')],string="Type",related='category_id.type')
    user_draft_id = fields.Many2one("res.users", string="User Draft", required=False, ondelete="restrict")
    close_date = fields.Datetime("Draft Date")    
    manager_id = fields.Many2one("res.users", string="Reporting Manager", required=False, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).parent_id.user_id)   
    manager_date = fields.Datetime("Approved Date")         
    severity_id = fields.Many2one("mwide.severity", string="Severity", ondelete="restrict", track_visibility='onchange',readonly=False, store=True)
    change_severity_id = fields.Many2one("mwide.severity", string="Proposed Severity", readonly=False, store=True)
    severity_approval = fields.Selection([('pending','Pending'),('approve','Approved')],string="Severity Change Approval")
    is_sla = fields.Boolean('SLA',default=True)
    priority_id = fields.Many2one("mwide.priority", string="Priority", ondelete="restrict", track_visibility='onchange', related='service_type_id.priority_id',readonly=False, store=True)
    time_acknowledged = fields.Integer('Response Time (mins)',related='severity_id.time_acknowledged')
    time_resolution = fields.Integer('Resolution Time (hrs)',related='severity_id.time_resolution')
    description = fields.Text(string = 'Enter Description Here', track_visibility='onchange')
    resolution_notes = fields.Text(string='Resolution Details', track_visibility='onchange')
    response_time = fields.Float('Actual Response Time (HH:MM)', readonly=True, default=0, help="Indicates the amount of time elapsed in minutes from when the ticket was assigned to when the ticket was set in progress. Indicative of Assignee Performance")
    resolution_time = fields.Float('Actual Resolution Time (HH:MM)', readonly=True, default=0,help="Indicates the amount of time elapsed in minutes from when the ticket was set in progress to when the ticket was resolved. Indicative of Assignee Performance")
    total_elapsed = fields.Float('Total Elapsed Time (HH:MM)',compute="_get_elapsed",readonly=True,help="Total amount of time elapsed from the time the ticket was opened to when the ticket was sent for validation")
    validation_date = fields.Datetime('Validation Date')
    unresolved_date = fields.Datetime('Unresolved Date')
    continue_date = fields.Datetime('Continue Date')
    resolved_date = fields.Datetime('Resolved Date')
    roadblock_ids = fields.One2many('mwide.helpdesk.roadblock','helpdesk_id',string="Roadblock",track_visibility="onchange")
    onhold_remarks = fields.Text(string="Remarks")
    response_alert = fields.Float('Response Time Alert')
    resolution_alert = fields.Float('Resolution Time Alert')
    alert_color = fields.Integer('Alert Color',default=2)
    rating_status = fields.Selection([('pass','Pass'),('fail','Fail')],string="Overall Rating",default="pass")
    manager_hr = fields.Many2one("hr.employee", string="Reporting Manager", required=False, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).parent_id.id)   
    #html_color = fields.Char('Color',default="#FFFFFF")
    html_color = fields.Selection([('#FFFFFF','#FFFFFF'),('#A5F99D','#A5F99D'),('#FEB580','#FEB580'),('#F78585','#F78585')],string="Color",default="#FFFFFF")
    parent_id = fields.Many2one('mwide.helpdesk','Parent Ticket')
    ticket_level = fields.Selection([('l1','L1'),('l2','L2'),('l3','L3')],'Ticket Level', default="l1")
    bol_escalate = fields.Boolean('Escalated',default=False)
    resolution_ids = fields.One2many('mwide.helpdesk.resolution','helpdesk_id',string="Breakdown",track_visibility="onchange")
    group_id = fields.Many2one('res.groups','IT Support Group')
    helpdesk_bot = fields.Many2one('res.users',"Helpdesk Bot",default=lambda self: self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1))
    time_remaining = fields.Float('Time Remaining')
    child_ids = fields.One2many('mwide.helpdesk','parent_id',string="Related Tickets")
    is_reopen = fields.Boolean('Reopen',default=False)
    SEL_RATING = [('0','0'),
                  ('1','1'),
                  ('2','2'),
                  ('3','3'),
                  ('4','4'),
                  ('5','5')
    ]
    rating = fields.Selection(SEL_RATING,string="Rating",default="0")
    review_comment = fields.Text("Comment")
    priority_sel = fields.Selection([
        ('1', 'Low'),
        ('2', 'Medium'),
        ('3', 'High'),
        ], string = 'Priority',default="1",select=True,readonly=False,store=True)
    approval_selection = fields.Selection([
        ('none', 'None'),
        ('manager', 'Reporting Manager'),
        ('it_manager', 'Reporting Manager and IT Manager'),
    ], 'Approval',default=lambda self: self.service_type_id.approval_selection)
    
    @api.onchange('requested_id')
    def onchange_by_pass(self):
        for rec in self:
            rec.approval_selection = rec.service_type_id.approval_selection
            if rec.requested_id.employee_id.no_approval == True:
                rec.approval_selection = 'none'
            else:
                rec.approval_selection = rec.service_type_id.approval_selection
    
    @api.onchange('service_type_id')
    def onchange_service_type(self):
        for rec in self:
            rec.approval_selection = rec.service_type_id.approval_selection
            if rec.requested_id.employee_id.no_approval == True:
                rec.approval_selection = 'none'
            else:
                rec.approval_selection = rec.service_type_id.approval_selection
    
    state = fields.Selection([
        ('01draft', 'Draft'),
        ('02request', 'Request'),
        ('03approved_head', 'Approved by Manager'),
        ('04approved_it', 'Approved by IT'),
        ('05assigned', 'Assigned/In-Progress'),
        ('06on_hold', 'Pending'),
        ('07validation', 'For Closure/Confirmation'),
        ('08unresolved', 'Unresolved'),
        ('09resolved', 'Resolved'),


    ], 'Status', default="01draft", track_visibility='onchange')    
    
    state_b = fields.Selection([
        ('01draft', 'Draft'),
        ('02request', 'Submitted'),
        ('03approved_head', 'Approved by Manager'),
        ('05assigned', 'Assigned/In-Progress'),
        ('06on_hold', 'Pending'),
        ('07validation', 'For Closure/Confirmation'),
        ('08unresolved', 'Unresolved'),
        ('09resolved', 'Resolved'),

    ], 'Status', default="01draft")  
    
    
    state_c = fields.Selection([
        ('01draft', 'Draft'),
        ('02request', 'Submitted'),
        ('05assigned', 'Assigned/In-Progress'),
        ('06on_hold', 'Pending'),
        ('07validation', 'For Closure/Confirmation'),
        ('08unresolved', 'Unresolved'),
        ('09resolved', 'Resolved'),

    ], 'Status', default="01draft")
    
    company_id = fields.Many2one('res.company', string='Company / Group', readonly=True,
        default=lambda self: self.env['res.company']._company_default_get('mwide.partners'))
    
    check_user = fields.Boolean('Check', compute='get_user')
    check_severity_user = fields.Boolean('Check', compute='get_severity_user')
    check_requestor = fields.Boolean('Is Requestor', compute="get_check_requestor")
    #cur_user = fields.Many2one('res.users','Current User', compute="_get_current_user")
    is_it_support = fields.Boolean('is IT Support',compute="_get_it_support",default=lambda self: True if self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l1') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l2_dev') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l2_server') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l2_network') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l3_server') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l3_network') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l3_dev') else False)
    #SAP Field
    sap_type = fields.Selection([
        ('na','N/A'),
        ('vendor', 'Vendor'),
        ('item', 'Item'),
        ('account','Account'),
        ('project','Project'),
        ('coa','COA')

    ], 'Request Type', related='service_type_id.sap_type', track_visibility='onchange')  
    
    #vendor
    vendor_ids = fields.One2many('mwide.helpdesk.vendor','helpdesk_id',string='Partners')

    #Item 
    stock_ids = fields.One2many('mwide.helpdesk.stock','helpdesk_id',string='Item Codes')
    coa_ids = fields.One2many('mwide.helpdesk.coa','ticket_id',string='COA')

    #Account
    account_ids = fields.One2many('mwide.helpdesk.account','helpdesk_id',string='Account')
    project_ids = fields.One2many('mwide.helpdesk.project','helpdesk_id',string='Project')
    
    @api.onchange('severity_id')
    def _get_sla(self):
        self.is_sla = self.severity_id.is_sla
    
    @api.depends('resolution_time') 
    def _get_elapsed(self):
        self.total_elapsed = self.response_time + self.resolution_time
    
    @api.depends('is_it_support') 
    def _get_it_support(self):
        """
 
        :param self: 
        :return: 
        """
        if self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l1') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l2_dev') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l2_server') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l2_network') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l3_server') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l3_network') or self.env.user.has_group('mega_helpdesk.helpdesk_it_team_l3_dev'):
            self.is_it_support = True
        else:
            self.is_it_support = False
    
    @api.depends('check_user') 
    def get_user(self):
        """
 
        :param self: 
        :return: 
        """
        if self.assigned_id.id == self._uid:
            self.check_user = True    

        else:
            if self.env.user.has_group('mega_helpdesk.helpdesk_helpdesk_group') or self.env.user.has_group('mega_helpdesk.helpdesk_admin'):
                self.check_user = True
            else:
                self.check_user = False
        
    
    @api.depends('check_severity_user') 
    def get_severity_user(self):
        """
 
        :param self: 
        :return: 
        """
        if self.env['hr.department'].search([('is_itdep','=',True)],limit=1).severity_approver_id.id == self._uid:
            self.check_severity_user = True    

        else:
            self.check_severity_user = False
            
    def submit_coa(self):
        if not self.coa_ids:
            raise ValidationError(_('Missing COA Details! You need to add the COA details for your request'))
        self.state = '06on_hold'
        self.state_b = '06on_hold'
        self.state_c = '06on_hold'
        for i in self.coa_ids:
            i.action_submit()
    
    @api.onchange('service_type_id')
    def _onchange_service_type_id(self):
        for rec in self:
            if rec.service_type_id:
                rec.severity_id = rec.service_type_id.severity_id.id 
            
    @api.onchange('requested_id')
    def _onchange_requested_id(self):
        for rec in self:
            if rec.requested_id:
                manager_id = self.env['hr.employee'].search([('user_id','=',rec.requested_id.id)])
                if manager_id:
                    rec.manager_hr = manager_id.parent_id.id
                    if manager_id.parent_id.user_id:
                        rec.manager_id = manager_id.parent_id.user_id.id
    @api.onchange('manager_hr')
    def _onchange_manager_id(self):
        for rec in self:
            if rec.manager_hr:
                if rec.manager_hr.user_id:
                    rec.manager_id = rec.manager_hr.user_id.id
    
    @api.depends('check_requestor') 
    def get_check_requestor(self):
        """
 
        :param self: 
        :return: 
        """
        if self.requested_id.id == self._uid:
            self.check_requestor = True    

        else:
            self.check_requestor = False
       
    '''@api.onchange('department_id')
    def _onchange_department_id(self):
        if self.department_id:
            self.manager_id = self.department_id.manager_id.user_id.id
            approver_ids = []
            for i in self.department_id.approver_ids:
                approver_ids.append(i.user_id.id) 
            return {'domain': {'manager_id':['|',('id','in',approver_ids),('id','=',self.department_id.manager_id.user_id.id)]}}'''
                        
            
    
    
    def set_request(self):
        for p in self:
            if p.sap_type == 'coa':
                raise ValidationError(_('Approval for your SAP Masterdata - COA Request is still in progress, this ticket will be automatically be submitted once all approvals have been met'))
            p.state = '02request'
            p.state_b = '02request'
            p.state_c = '02request'
            p.ticket_date = fields.Datetime.now()
            msg_vals = {}
            if not p.sbu:
                raise ValidationError(_('Missing Field, Pleaes Specify SBU'))
            if self.approval_selection == 'none':
                new_follower = self.requested_id.partner_id - self.message_partner_ids
                if new_follower: self.message_subscribe(new_follower.ids)
                
                
                message = """
                    <p>Hi %s,</p>
                    <p>Please be informed that your ticket has been submitted, the IT Team will attend to your request shortly.</p>
                    <p>Thanks,<br/>%s</p>
                """
                        # msg_vals['subject'] = self.subject
                
                helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
                if not helpdesk_bot:
                    raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
                else:
                    self.helpdesk_bot = helpdesk_bot
                email = self.env.ref('mega_helpdesk.email_template_ticket_submitted_open')
                if email:
                    email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
                msg_vals['body'] = message % (self.requested_id.name,helpdesk_bot.partner_id.name)
                msg_vals['partner_ids'] = [helpdesk_bot.partner_id.id]
                
                channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_it_l1')
                if p.sap_type in ['vendor','item','project','coa']:
                    channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_sap')
                    sap_user_ids = ''
                    sap_group = self.env.ref('mega_helpdesk.helpdesk_sap_team')
                    for i in sap_group.users:
                        sap_user_ids = sap_user_ids + str(i.partner_id.id) + "," 
                    p.sap_user_ids = sap_user_ids
                    email = self.env.ref('mega_helpdesk.email_template_ticket_sap_all')
                    if email:
                        email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
                url = p.get_link_to_document()  
                notification = ('<a href="%s">%s</a>') % (url,p.name)
                channel_id.sudo().message_post(
                        body='New Open Ticket, Requested by '+str(p.requested_id.name)+'. Link : '+notification,author_id = helpdesk_bot.partner_id.id,channel_ids=[channel_id.id])
                
            else:
                if not self.manager_hr:
                    raise ValidationError(_('Reporting Manager not set, Please assign Reporting Manager'))
                email = self.env.ref('mega_helpdesk.email_template_ticket_request')
                if email:
                    email.send_mail(self.id, force_send=True)
                
                # Send notification in Odoo
                new_follower = self.manager_id.partner_id - self.message_partner_ids
                if new_follower: self.message_subscribe(new_follower.ids)
                    
                
                message = """
                    <p>Hi %s,</p>
                    <p>Please be reminded that this ticket is requesting for Reporting Approval.</p>
                    <p>Thanks,<br/>%s</p>
                """
                            # msg_vals['subject'] = self.subject
                msg_vals['body'] = message % (self.manager_id.name,self.env.user.name)
                msg_vals['partner_ids'] = [self.manager_id.partner_id.id]
            
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
    def set_request_coa(self):
        for p in self:
            p.state = '02request'
            p.state_b = '02request'
            p.state_c = '02request'
            p.ticket_date = fields.Datetime.now()
            msg_vals = {}
            if not p.sbu:
                raise ValidationError(_('Missing Field, Pleaes Specify SBU'))
            if self.approval_selection == 'none':
                new_follower = self.requested_id.partner_id - self.message_partner_ids
                if new_follower: self.message_subscribe(new_follower.ids)
                
                
                message = """
                    <p>Hi %s,</p>
                    <p>Please be informed that your ticket has been submitted, the IT Team will attend to your request shortly.</p>
                    <p>Thanks,<br/>%s</p>
                """
                        # msg_vals['subject'] = self.subject
                
                helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
                if not helpdesk_bot:
                    raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
                else:
                    self.helpdesk_bot = helpdesk_bot
                email = self.env.ref('mega_helpdesk.email_template_ticket_submitted_open')
                if email:
                    email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
                msg_vals['body'] = message % (self.requested_id.name,helpdesk_bot.partner_id.name)
                msg_vals['partner_ids'] = [helpdesk_bot.partner_id.id]
                
                channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_it_l1')
                if p.sap_type in ['vendor','item','project','coa']:
                    channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_sap')
                    sap_user_ids = ''
                    sap_group = self.env.ref('mega_helpdesk.helpdesk_sap_team')
                    for i in sap_group.users:
                        sap_user_ids = sap_user_ids + str(i.partner_id.id) + "," 
                    p.sap_user_ids = sap_user_ids
                    email = self.env.ref('mega_helpdesk.email_template_ticket_sap_all')
                    if email:
                        email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
                url = p.get_link_to_document()  
                notification = ('<a href="%s">%s</a>') % (url,p.name)
                channel_id.sudo().message_post(
                        body='New Open Ticket, Requested by '+str(p.requested_id.name)+'. Link : '+notification,author_id = helpdesk_bot.partner_id.id,channel_ids=[channel_id.id])
                
            else:
                if not self.manager_hr:
                    raise ValidationError(_('Reporting Manager not set, Please assign Reporting Manager'))
                email = self.env.ref('mega_helpdesk.email_template_ticket_request')
                if email:
                    email.send_mail(self.id, force_send=True)
                
                # Send notification in Odoo
                new_follower = self.manager_id.partner_id - self.message_partner_ids
                if new_follower: self.message_subscribe(new_follower.ids)
                    
                
                message = """
                    <p>Hi %s,</p>
                    <p>Please be reminded that this ticket is requesting for Reporting Approval.</p>
                    <p>Thanks,<br/>%s</p>
                """
                            # msg_vals['subject'] = self.subject
                msg_vals['body'] = message % (self.manager_id.name,self.env.user.name)
                msg_vals['partner_ids'] = [self.manager_id.partner_id.id]
    
    def set_dept_approve(self):
        for p in self:
            p.state = '03approved_head'
            p.state_b = '03approved_head'
            #p.manager_id = self._uid
            p.manager_date = fields.Datetime.now()
            
            # Send notification in Odoo
            email = False
            msg_vals = {}
            if self.approval_selection == 'manager':
                email = self.env.ref('mega_helpdesk.email_template_ticket_manager_approved')

                message = """
                    <p>Hi %s,</p>
                    <p>Please be reminded that this ticket Approved by Department Head..</p>
                    <p>Thanks,<br/>%s</p>
                """
                msg_vals['body'] = message % (self.requested_id.name,self.env.user.name)
                msg_vals['partner_ids'] = [self.requested_id.id]
                if email:
                    email.send_mail(self.id, force_send=True)
                helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
                if not helpdesk_bot:
                    raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
                else:
                    self.helpdesk_bot = helpdesk_bot
                channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_it_l1')
                if p.sap_type in ['vendor','item','project','coa']:
                    channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_sap')
                    sap_user_ids = ''
                    sap_group = self.env.ref('mega_helpdesk.helpdesk_sap_team')
                    for i in sap_group.users:
                        sap_user_ids = sap_user_ids + str(i.partner_id.id) + "," 
                    p.sap_user_ids = sap_user_ids
                    email = self.env.ref('mega_helpdesk.email_template_ticket_sap_all')
                    if email:
                        email.send_mail(self.id, force_send=True)
                url = p.get_link_to_document()  
                notification = ('<a href="%s">%s</a>') % (url,p.name)
                channel_id.sudo().message_post(
                        body='New Open Ticket, Requested by '+str(p.requested_id.name)+'. Link : '+notification,author_id = helpdesk_bot.partner_id.id,channel_ids=[channel_id.id])
                
            elif self.approval_selection == 'it_manager':
                if not self.it_mngr_id: 
                    raise ValidationError(_('Please configure IT Department and IT Manager in Employee/Configuration/Department or Contact your Administrator'))
                email = self.env.ref('mega_helpdesk.email_template_ticket_it_approval')
                new_follower = self.it_mngr_id.partner_id - self.message_partner_ids
                if new_follower: self.message_subscribe(new_follower.ids)
                message = """
                    <p>Hi %s,</p>
                    <p>Please be reminded that this ticket Approved by Department Head and is requesting for IT Approval.</p>
                    <p>Thanks,<br/>%s</p>
                """
                msg_vals['body'] = message % (self.it_mngr_id.name,self.env.user.name)
                msg_vals['partner_ids'] = [self.it_mngr_id.partner_id.id]
            # msg_vals['subject'] = self.subject
                if email:
                    email.send_mail(self.id, force_send=True)
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
            

    def set_it_approve(self):
        for p in self:
            p.state = '04approved_it'
            
            
            #p.it_mngr_id = self._uid
            p.it_date = fields.Datetime.now()
            email = self.env.ref('mega_helpdesk.email_template_ticket_it_approved')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo
            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>Please be reminded that this ticket Approved by IT Department.</p>
                <p>Thanks,<br/>%s</p>
            """
            
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name, self.env.user.name)
            msg_vals['partner_ids'] = [self.requested_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
            helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
            if not helpdesk_bot:
                    raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
            else:
                self.helpdesk_bot = helpdesk_bot
            channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_it_l1')
            if p.sap_type in ['vendor','item','project','coa']:
                channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_sap')
                sap_user_ids = ''
                sap_group = self.env.ref('mega_helpdesk.helpdesk_sap_team')
                for i in sap_group.users:
                    sap_user_ids = sap_user_ids + str(i.partner_id.id) + "," 
                p.sap_user_ids = sap_user_ids
                email = self.env.ref('mega_helpdesk.email_template_ticket_sap_all')
                if email:
                    email.send_mail(self.id, force_send=True)
            url = p.get_link_to_document()  
            notification = ('<a href="%s">%s</a>') % (url,p.name)
            channel_id.message_post(body='New Open Ticket, Requested by '+str(p.requested_id.name)+'. Link : '+notification,author_id = helpdesk_bot.partner_id.id,channel_ids=[channel_id.id])
    
    def set_assign(self):
        for p in self:
            if not p.assigned_id:
                raise ValidationError(_('You cannot send assignment if the incident report is not assigned to anyone.'))
            p.state = '05assigned'
            p.state_b = '05assigned'
            p.state_c = '05assigned'
            #p.assigned_id = self._uid
            p.assigned_date = fields.Datetime.now()
            p.resolution_ids = [(0,0,{'assigned_id':p.assigned_id.id,'resolution_time':0,'assigned_date':fields.Datetime.now()})]
            
                    # Send email to user
#         email = self.env.ref('synersys_int_support.email_template_incident_report_assignment')
            email = self.env.ref('mega_helpdesk.email_template_ticket_assignment')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo
            new_follower = self.assigned_id.partner_id - self.message_partner_ids
            if new_follower: self.message_subscribe(new_follower.ids)
            
            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>Please be reminded that this case is being assigned to you.</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.assigned_id.name, self.env.user.name)
            msg_vals['partner_ids'] = [self.assigned_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)

    def set_accept(self):
        for p in self:
            p.assigned_id = self.env.user.id
            #d1=datetime.strptime(str(self.assigned_date),'%Y-%m-%d %H:%M:%S') 
            #d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S'
            d2 = fields.Datetime.now()
            if p.approval_selection == 'none':
                #d3=fields.Datetime.now() - self.ticket_date 
                d3 = p.compute_response(self.ticket_date +relativedelta(hours=8),d2+relativedelta(hours=8))
                mins = d3 / 60
                self.response_time=mins/60
            elif p.approval_selection == 'manager':
                #d3=fields.Datetime.now() - self.manager_date 
                d3 = p.compute_response(self.manager_date +relativedelta(hours=8),d2+relativedelta(hours=8))
                mins = d3 / 60
                self.response_time=mins/60
            elif p.approval_selection == 'it_manager':  
                #d3=fields.Datetime.now() - self.it_date 
                d3 = p.compute_response(self.it_date +relativedelta(hours=8),d2+relativedelta(hours=8))
                mins = d3 / 60
                self.response_time=mins/60
            p.state = '05assigned'
            p.state_b = '05assigned'
            p.state_c = '05assigned'
            p.html_color = "#FFFFFF"
            p.assigned_date = fields.Datetime.now()
            p.resolution_ids = [(0,0,{'assigned_id':self.env.user.id,'resolution_time':0,'assigned_date':fields.Datetime.now()})]
                    # Send email to user
#         email = self.env.ref('synersys_int_support.email_template_incident_report_assignment')
            email = self.env.ref('mega_helpdesk.email_template_ticket_accept')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo
            new_follower = self.assigned_id.partner_id - self.message_partner_ids
            if new_follower: self.message_subscribe(new_follower.ids)
            
            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This ticket has been accepted</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name, self.env.user.name)
            msg_vals['partner_ids'] = [self.assigned_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)



    def set_in_progress(self):
        for p in self:
            p.state = '06in_progress'
            p.state_b = '06in_progress'
            p.state_c = '06in_progress'
            p.in_progress_id = self._uid
            p.in_progress_date = fields.Datetime.now()
            email = self.env.ref('mega_helpdesk.email_template_ticket_in_progress')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This Ticket is in Progress</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name,self.env.user.name)
            msg_vals['partner_ids'] = [self.requested_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)

    def set_validation(self):
        for p in self:
            d1=datetime.strptime(str(p.assigned_date),'%Y-%m-%d %H:%M:%S') 
            if p.continue_date:
                d1=datetime.strptime(str(p.continue_date),'%Y-%m-%d %H:%M:%S')
            d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
            d3=p.compute_response(d1+relativedelta(hours=8),d2+relativedelta(hours=8))
            mins = d3 / 60
            p.resolution_time=p.resolution_time + (mins/60)
            p.state = "07validation"
            p.state_b = "07validation"
            p.state_c = "07validation" 
            p.html_color = "#FFFFFF"
            p.validation_date = fields.Datetime.now()
            
            #resolution breakdown computation
            for line in p.resolution_ids:
                if line.assigned_id == p.assigned_id: 
                    date1 = datetime.strptime(str(line.assigned_date),'%Y-%m-%d %H:%M:%S') 
                    if line.continue_date:
                        date1=datetime.strptime(str(line.continue_date),'%Y-%m-%d %H:%M:%S')
                    date2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
                    date3=p.compute_response(date1+relativedelta(hours=8),date2+relativedelta(hours=8))
                    mins = date3 / 60
                    line.resolution_time=line.resolution_time + (mins/60)
            email = self.env.ref('mega_helpdesk.email_template_ticket_for_validation')
            if email:
                email.send_mail(self.id, force_send=True)
            #if p.parent_id:
            #    p.parent_id.set_validation()
            #    print("validate")
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This is to remind you that this ticket has set for validation, please confirm if the ticket has been Resolved by click the Resolved button</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name,self.assigned_id.name)
            msg_vals['partner_ids'] = [self.requested_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
            
            
    def set_unresolved(self):
        for p in self:
            p.state = "08unresolved"
            p.state_b = "08unresolved"  
            p.state_c = "08unresolved"  
            p.is_reopen = True
            p.unresolved_date = fields.Datetime.now()
            email = self.env.ref('mega_helpdesk.email_template_ticket_unresolved')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This is to remind you that the ticket is unresolved, Please look again into this ticket</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.assigned_id.name,self.env.user.name)
            msg_vals['partner_ids'] = [self.assigned_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
            
    def action_reassign(self,assignee,ticket_level,group_id):
        for p in self:
            if not assignee:
                raise ValidationError(_('You cannot send assignment if the incident report is not assigned to anyone.'))
            if p.state == '05assigned':
                d1=datetime.strptime(str(p.assigned_date),'%Y-%m-%d %H:%M:%S') 
                if p.continue_date:
                    d1=datetime.strptime(str(p.continue_date),'%Y-%m-%d %H:%M:%S')
                d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
                d3=p.compute_response(d1+relativedelta(hours=8),d2+relativedelta(hours=8))
                mins = d3 / 60
                p.resolution_time=p.resolution_time + (mins/60)
                #resolution breakdown computation
                for line in p.resolution_ids:
                    if line.assigned_id == p.assigned_id: 
                        date1 = datetime.strptime(str(line.assigned_date),'%Y-%m-%d %H:%M:%S') 
                        if line.continue_date:
                            date1=datetime.strptime(str(line.continue_date),'%Y-%m-%d %H:%M:%S')
                        date2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
                        date3=p.compute_response(date1+relativedelta(hours=8),date2+relativedelta(hours=8))
                        mins = date3 / 60
                        line.resolution_time=line.resolution_time + (mins/60)
            p.state = '05assigned'
            p.state_b = '05assigned'
            p.state_c = '05assigned'
            p.assigned_id = assignee
            p.ticket_level = ticket_level
            p.assigned_date = fields.Datetime.now()
            if group_id:
                p.group_id = group_id
            p.resolution_ids = [(0,0,{'assigned_id':assignee.id,'resolution_time':0,'assigned_date':fields.Datetime.now()})]
            #p.assigned_date = fields.Datetime.now()

                    # Send email to user
#         email = self.env.ref('synersys_int_support.email_template_incident_report_assignment')
            
            email = self.env.ref('mega_helpdesk.email_template_ticket_assignment')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo
            new_follower = self.assigned_id.partner_id - self.message_partner_ids
            if new_follower: self.message_subscribe(new_follower.ids)
            
            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>Please be reminded that this case is being assigned to you.</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.assigned_id.name, self.env.user.name)
            msg_vals['partner_ids'] = [self.assigned_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
            
    def action_escalate_wizard(self): 
        ticket_level = "l1"
        if self.ticket_level == "l1":
            ticket_level = "l2"
        elif self.ticket_level == "l2":
            ticket_level = "l3" 
        return {
            'name': 'Escalate Ticket',
            'res_model': 'mwide.helpdesk.escalate',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id,'default_category_id':self.category_id.id,'default_service_type_id':self.service_type_id.id,'default_ticket_level':ticket_level},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }
        
    def action_severity_wizard(self): 
        return {
            'name': 'Change Severity',
            'res_model': 'mwide.helpdesk.severity',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }
    
    def severity_approve(self):
        self.severity_approval = 'approve' 
        self.severity_id = self.change_severity_id.id
    
    def action_validation_wizard(self): 
        return {
            'name': 'Resolution Details',
            'res_model': 'mwide.helpdesk.validation',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }  
    
    def action_reassign_wizard(self): 
        ticket_level = "l1"
        return {
            'name': 'Re-Assign Ticket',
            'res_model': 'mwide.helpdesk.reassign',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id,'default_category_id':self.category_id.id,'default_service_type_id':self.service_type_id.id,'default_ticket_level':ticket_level},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }     
    
    def action_escalate(self,category_id=False,service_type_id=False,group_id=False,ticket_level=False,assigned_id=False):
        for p in self:
            if p.state == '05assigned':
                d1=datetime.strptime(str(self.assigned_date),'%Y-%m-%d %H:%M:%S') 
                if self.continue_date:
                    d1=datetime.strptime(str(self.continue_date),'%Y-%m-%d %H:%M:%S')
                d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
                d3=p.compute_response(d1+relativedelta(hours=8),d2+relativedelta(hours=8))
                mins = d3 / 60
                p.resolution_time=p.resolution_time + (mins/60)
                #resolution breakdown computation
                for line in p.resolution_ids:
                    if line.assigned_id == p.assigned_id: 
                        date1 = datetime.strptime(str(line.assigned_date),'%Y-%m-%d %H:%M:%S') 
                        if line.continue_date:
                            date1=datetime.strptime(str(line.continue_date),'%Y-%m-%d %H:%M:%S')
                        date2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
                        date3=p.compute_response(date1+relativedelta(hours=8),date2+relativedelta(hours=8))
                        mins = date3 / 60
                        line.resolution_time=line.resolution_time + (mins/60)
            p.state = '06on_hold'
            p.state_b = '06on_hold'
            p.state_c = '06on_hold'
            p.group_id = group_id
            p.bol_escalate = True
            p.on_hold_id = self._uid
            p.on_hold_date = fields.Datetime.now()
            
            email = self.env.ref('mega_helpdesk.email_template_ticket_on_hold')
            if email:
                email.send_mail(p.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This is to inform you that this ticket has been Escalated for Higher Level of support and set to Pending</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (p.requested_id.name,  self.env.user.name)
            msg_vals['partner_ids'] = [p.requested_id.partner_id.id]
            # Post new message.
            #p.message_post(type='notification', **msg_vals)
            vals = {'parent_id':p.id,
                    'state':'04approved_it',
                    'state_b':'03approved_head',
                    'state_c':'02request',
                    'ticket_level':ticket_level,
                    'approval_selection': 'none',
                    'category_id':category_id.id,
                    'service_type_id':service_type_id.id,
                    'bol_escalate':False,
                    'ticket_date': fields.Datetime.now(),
                    'response_time':0,
                    'resolution_time':0,
                    'assigned_id':False,
                    'assigned_date':False,
                    'group_id': group_id.id,
                    'requested_id':self.env.user.id
                    }
            if assigned_id:
                vals['assigned_id'] = assigned_id.id
                vals['assigned_date'] = fields.Datetime.now()
            new_ticket = p.copy(vals)   
            
            if assigned_id:   
                new_ticket.set_assign() 
            else:
                helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
                channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_it_l2')
                if new_ticket.ticket_level == 'l3':
                    channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_it_l3')
                if new_ticket.sap_type in ['vendor','item','project','coa']:
                    channel_id = self.env.ref('mega_helpdesk.helpdesk_channel_sap')
                url = new_ticket.get_link_to_document()  
                notification = ('<a href="%s">%s</a>') % (url,p.name)
                channel_id.message_post(body='New Open Ticket, Requested by '+str(p.requested_id.name)+'. Link : '+notification,author_id = helpdesk_bot.partner_id.id,channel_ids=[channel_id.id])
            return {
                'res_model': 'mwide.helpdesk',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_id': new_ticket.id,
                'action': self.env.ref('mega_helpdesk.helpdesk_action')
                } 
            
    def set_resolved(self):
        for p in self:
            p.state = "09resolved"
            p.state_b = "09resolved"
            p.state_c = "09resolved"
            p.resolved_date = fields.Datetime.now()
            email = self.env.ref('mega_helpdesk.email_template_ticket_resolved')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This is to inform you that this Ticket has been resolved</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.assigned_id.name,self.env.user.name)
            msg_vals['partner_ids'] = [self.assigned_id.partner_id.id]
            # Post new message.
            #p.message_post(type='notification', **msg_vals)
            if p.parent_id:
                p.parent_id.set_validation()
        return {
            'res_model': 'mwide.helpdesk.review',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }    
            
    def action_onhold_reason(self): 
        return {
            'name': 'Reason',
            'res_model': 'mwide.helpdesk.roadblock',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id,'default_roadblock_type':'onhold'},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }    
        
    def action_review(self): 
        return {
            'name':'User Feedback',
            'res_model': 'mwide.helpdesk.review',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }    

            
    def action_unresolved_reason(self): 
        return {
            'name': 'Reason',
            'res_model': 'mwide.helpdesk.roadblock',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id,'default_roadblock_type':'unresolved'},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }        

    def action_split_ticket(self):
        print(self.name)
        return {
            'name' : 'Split Ticket',
            'res_model': 'mwide.helpdesk.split',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
            }
    
    def set_on_hold(self):
        for p in self:
            d1=datetime.strptime(str(p.assigned_date),'%Y-%m-%d %H:%M:%S') 
            if p.continue_date:
                d1=datetime.strptime(str(p.continue_date),'%Y-%m-%d %H:%M:%S')
            d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
            d3=p.compute_response(d1+relativedelta(hours=8),d2+relativedelta(hours=8))
            mins = d3 / 60
            p.resolution_time=p.resolution_time +(mins/60)
            
            #resolution breakdown computation
            for line in p.resolution_ids:
                if line.assigned_id == p.assigned_id: 
                    date1 = datetime.strptime(str(line.assigned_date),'%Y-%m-%d %H:%M:%S') 
                    if line.continue_date:
                    
                        date1=datetime.strptime(str(line.continue_date),'%Y-%m-%d %H:%M:%S')
                    date2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
                    date3=p.compute_response(date1+relativedelta(hours=8),date2+relativedelta(hours=8))
                    mins = date3 / 60
                    line.resolution_time=line.resolution_time + (mins/60)
            p.state = '06on_hold'
            p.state_b = '06on_hold'
            p.state_c = '06on_hold'
            p.on_hold_id = self._uid
            p.on_hold_date = fields.Datetime.now()
            email = self.env.ref('mega_helpdesk.email_template_ticket_on_hold')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This is to inform you that this ticket set to On Hold</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name,  self.env.user.name)
            msg_vals['partner_ids'] = [self.requested_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
            
    def set_closed(self):
        for p in self:
            p.state = '10closed'
            p.state_b = '10closed'
            p.state_c = '10closed'
            p.user_closed_id = self._uid
            p.close_date = fields.Datetime.now()    
            email = self.env.ref('mega_helpdesk.email_template_ticket_closed')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This Ticket has been closed</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name,  self.env.user.name)
            msg_vals['partner_ids'] = [self.requested_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)                

    def set_draft(self):
        for p in self:
            p.state = '01draft'
            p.state_b = '01draft'
            p.state_c = '01draft'
            p.user_closed_id = self._uid
            p.close_date = fields.Datetime.now() 
            email = self.env.ref('mega_helpdesk.email_template_ticket_set_draft')
            if email:
                email.send_mail(self.id, force_send=True)
            
            # Send notification in Odoo

            msg_vals = {}
            message = """
                <p>Hi %s,</p>
                <p>This Ticket has been set back to Draft</p>
                <p>Thanks,<br/>%s</p>
            """
            # msg_vals['subject'] = self.subject
            msg_vals['body'] = message % (self.requested_id.name, self.env.user.name )
            msg_vals['partner_ids'] = [self.requested_id.partner_id.id]
            # Post new message.
            #self.message_post(type='notification', **msg_vals)
    
    
    def set_continue(self):
        for p in self:
            p.state = "05assigned"
            p.state_b = "05assigned"
            p.state_c = "05assigned" 
            p.continue_date = fields.Datetime.now()
            for line in p.resolution_ids:
                line.continue_date = fields.Datetime.now()
                
    def get_link_to_document(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action = action = self.env.ref('mega_helpdesk.helpdesk_action')
        
        db = {'db': self._cr.dbname}
        query = {
            'id'        : self.id,
            'view_type' : 'form',
            'model'     : self._name,
            'action'    : action.id,
        }
        
        path = "/web?%s#%s" % (parse.urlencode(db), parse.urlencode(query))
        full_url = base_url + path
        
        return full_url
    
    
    def get_color_response(self,diff_seconds):
        html_color = "#FFFFFF"
        if diff_seconds < 600:
            html_color = "#FFFFFF" 
        elif diff_seconds >= 600 and diff_seconds < 900:
            html_color = "#A5F99D"
        elif diff_seconds >= 900 and diff_seconds < 960:
            html_color = "#FEB580"
        elif diff_seconds > 960:
            html_color = "#F78585"  
        return html_color
        
    '''    @api.model
    def action_alert_color_response(self):
        d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
        tickets_1 = self.search([('state_c','=','02request'),('approval_selection','=','none')])
        for rec in tickets_1:
            diff = (d2 - datetime.strptime(str(rec.ticket_date),'%Y-%m-%d %H:%M:%S'))
            rec.html_color = rec.get_color_response(int(diff.total_seconds()))
            bh(datetime.strptime(str(rec.ticket_date),'%Y-%m-%d %H:%M:%S'),d2,worktiming=[])
        tickets_2 = self.search([('state_b','=','03approved_head'),('approval_selection','=','manager')])
        for rec in tickets_2:
            diff = (d2 - datetime.strptime(str(rec.manager_date),'%Y-%m-%d %H:%M:%S'))
            rec.html_color = rec.get_color_response(int(diff.total_seconds()))
        tickets_3 = self.search([('state','=','04approved_it'),('approval_selection','=','it_manager')])
        for rec in tickets_3:
            diff = (d2 - datetime.strptime(str(rec.it_date),'%Y-%m-%d %H:%M:%S'))
            rec.html_color = rec.get_color_response(int(diff.total_seconds()))'''
    @api.model
    def action_auto_resolve(self):
        date_now = fields.Datetime.now()
        tickets = self.search([('state','=','07validation')])
        if tickets:
            print(tickets)
            for rec in tickets:
                diff = (date_now - rec.validation_date).days
                print(diff)
                if diff >= 3:
                    rec.state = "09resolved"
                    rec.state_b = "09resolved"
                    rec.state_c = "09resolved"
                    rec.resolved_date = fields.Datetime.now()
                    
    @api.model
    def action_followup_approval(self):
        date_now = fields.Datetime.now()
        tickets = self.search(['&',('approval_selection','in',['manager','it_manager']),('state','=','02request')])
        if tickets:
            print(tickets)
            for rec in tickets:
                diff = (date_now - rec.ticket_date).days
                if diff >= 1:
                    email = self.env.ref('mega_helpdesk.email_template_ticket_request_followup')
                    if email:
                        email.send_mail(rec.id)
    @api.model
    def action_alert_color_response(self):
        d2 = fields.Datetime.now()
        tickets_1 = self.search([('state_c','=','02request'),('approval_selection','=','none'),('is_sla','=',True)])
        for rec in tickets_1:
            print("start")
            print(rec.name)
            d3 = datetime.strptime(str(rec.ticket_date),'%Y-%m-%d %H:%M:%S')
            diff = rec.compute_response(d3+relativedelta(hours=8),d2+relativedelta(hours=8))
            rec.html_color = rec.get_color_response(int(diff))   
            print(diff)
            print("end")
            
        tickets_2 = self.search([('state_b','=','03approved_head'),('approval_selection','=','manager')])
        for rec in tickets_2:
            d3= datetime.strptime(str(rec.manager_date),'%Y-%m-%d %H:%M:%S')
            diff = rec.compute_response(d3+relativedelta(hours=8),d2+relativedelta(hours=8))
            rec.html_color = rec.get_color_response(int(diff))   
        tickets_3 = self.search([('state','=','04approved_it'),('approval_selection','=','it_manager')])
        for rec in tickets_3:
            d3= datetime.strptime(str(rec.it_date),'%Y-%m-%d %H:%M:%S')  
            diff = rec.compute_response(d3+relativedelta(hours=8),d2+relativedelta(hours=8))
            rec.html_color = rec.get_color_response(int(diff))   
     
    def compute_response(self,d2,d3):
        holidays = []
        holiday_ids = self.env['mwide.helpdesk.calendar'].search([('holiday_date','>=',d3),('holiday_date','<=',d2)])
        if holiday_ids:
            for hol in holiday_ids:
                holidays.append(hol.holiday_date)
        schedule = self.env['mwide.helpdesk.schedule'].search([('active','=',True)],limit=1)
        days = []
        for workday in schedule.workdays_ids:
            days.append(workday.value)
        all_days = [1,2,3,4,5,6,7]
        rest_days = []
        for day in all_days:
            if day not in days:
                rest_days.append(day) 
        opening = schedule.opening
        closing = schedule.closing
        diff = BusinessHours(d2,d3,worktiming=[opening,closing],weekends=rest_days,holidayfile=holidays).gethours() * 60 
        return diff

            
    def get_color_resolution(self,diff_seconds,time_resolution):
        print(self.name)
        html_color = "#FFFFFF"
        time_res= time_resolution * 3600
        if time_res - diff_seconds > 7200:
            html_color = "#FFFFFF" 
        elif time_res - diff_seconds <= 7200  and time_res - diff_seconds > 0:
            html_color = "#A5F99D"
        elif time_res - diff_seconds <= 0 and time_res - diff_seconds > -3600:
            html_color = "#FEB580"
        elif time_res - diff_seconds <= -3600:
            html_color = "#F78585"  
            self.rating_status = 'fail'
        return html_color
                    
    @api.model
    def action_alert_color_resolution(self):
        tickets = self.search([('state','=','05assigned'),('is_sla','=',True)])
        for rec in tickets:
            d1=datetime.strptime(str(rec.assigned_date),'%Y-%m-%d %H:%M:%S') 
            d2=datetime.strptime(str(datetime.today().replace(microsecond=0)),'%Y-%m-%d %H:%M:%S')
            diff = rec.compute_response(d1+relativedelta(hours=8),d2+relativedelta(hours=8))
            if rec.continue_date:
                res_time = (rec.resolution_time * 60) * 60
                res_timeb =  rec.compute_response(datetime.strptime(str(rec.continue_date),'%Y-%m-%d %H:%M:%S')+relativedelta(hours=8),d2+relativedelta(hours=8))
                diff = res_time + res_timeb
            rec.time_remaining = ((rec.time_resolution * 3600 ) - diff)/3600
            rec.html_color = rec.get_color_resolution(diff,rec.time_resolution)
            
        
    

    
class MwideHelpdeskVendorDocs(models.Model):
    _name = "mwide.helpdesk.vendor.docs" 
    
    helpdesk_vendor_id = fields.Many2one('mwide.helpdesk.vendor','Helpdesk')
    name = fields.Char('Supporting Docs')
    attachment_ids = fields.Many2many(comodel_name='ir.attachment',string="Attachment",readonly="False")
    
class MwideHelpdeskRoadblock(models.Model):
    _name = "mwide.helpdesk.roadblock"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    roadblock_type = fields.Selection([('onhold','Pending'),('unresolved','Unresolved')],string="type")
    onhold_reason = fields.Text("Reason")
    reason_id = fields.Many2one('mwide.reason','Reason',domain="[('type','=',roadblock_type)]")
    remarks = fields.Text("Remarks")
    
    
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        if self.env.context['default_roadblock_type']== 'onhold':
            ticket.set_on_hold()
        else:
            ticket.set_unresolved()
            
class MwideHelpdeskOnholdReason(models.Model):
    _name = "mwide.helpdesk.onhold.reason"
    
    name = fields.Char("Name")
    
class MwideReason(models.Model):
    _name = "mwide.reason"
    
    name = fields.Char("Reason")
    type = fields.Selection([('onhold','Pending'),('unresolved','Unresolved')],string="Type")
    
    
class MwideHelpdeskReason(models.Model):
    _name = "mwide.helpdesk.reason"
    
    name = fields.Char("Reason")
    type = fields.Selection([('onhold','Pending'),('unresolved','Unresolved')],string="Type")

class MwideHelpdeskSurvey(models.TransientModel):
    _name = "mwide.helpdesk.survey"            
            
            
class MwideHelpdeskReview(models.Model):
    _name = "mwide.helpdesk.review"
    SEL_RATING = [('0','0'),
                  ('1','1'),
                  ('2','2'),
                  ('3','3'),
                  ('4','4'),
                  ('5','5')
    ]
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    user_id = fields.Many2one('res.users','User')
    evaluator_id = fields.Many2one('res.users','Reviewed by')
    rating = fields.Selection(SEL_RATING,string="Rating",default="5")
    review_comment = fields.Text("Comment")
    
    
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        ticket.rating = self.rating
        ticket.review_comment = self.review_comment

class MwideHelpdeskReassign(models.Model):
    _name = "mwide.helpdesk.reassign"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    category_id = fields.Many2one('mwide.category','Category',required=True)
    service_type_id = fields.Many2one('mwide.service_type','Service Type',required=True)
    group_id = fields.Many2one('res.groups','IT Group',domain="[('it_level','=',['l1','l2','l3'])]")
    assigned_id = fields.Many2one('res.users','IT Assignment',domain="")
    
    @api.onchange('group_id')
    def _onchange_group_id(self):
        if self.group_id:
            user_ids = []
            for i in self.group_id.users:
                user_ids.append(i.id) 
            return {'domain': {'assigned_id':[('id','in',user_ids)]}}
    
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
    
        if not self.assigned_id:
            raise ValidationError(_('You cannot send assignment if the incident report is not assigned to anyone.'))
        else:
            ticket.action_reassign(self.assigned_id,"l1",False)
            
class MwideHelpdeskValidation(models.Model):
    _name = "mwide.helpdesk.validation"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    resolution_notes = fields.Text(string='Resolution Details', track_visibility='onchange')
    
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        ticket.resolution_notes = self.resolution_notes
        ticket.set_validation()
        
class MwideHelpdeskEscalate(models.Model):
    _name = "mwide.helpdesk.escalate"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    category_id = fields.Many2one('mwide.category','Category',required=True,domain="[('type','=','it')]")
    next_action = fields.Selection([('reassign','Re-Assign'),('new_ticket','Create Sub-Ticket')],default="reassign",required=True)
    service_type_id = fields.Many2one('mwide.service_type','Service Type',required=True,domain="[('category_id','=',category_id)]")
    ticket_level = fields.Selection([('l1','L1'),('l2','L2'),('l3','L3')],'Ticket Level', default="l2")
    group_id = fields.Many2one('res.groups','IT Group',domain="[('it_level','=',ticket_level)]")
    assigned_id = fields.Many2one('res.users','IT Assignment',domain="")

    @api.onchange('group_id')
    def _onchange_group_id(self):
        if self.group_id:
            user_ids = []
            for i in self.group_id.users:
                user_ids.append(i.id) 
            return {'domain': {'assigned_id':[('id','in',user_ids)]}}
    
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        if self.next_action == 'reassign':
            if not self.assigned_id:
                raise ValidationError(_('You cannot send assignment if the incident report is not assigned to anyone.'))
            else:
                ticket.action_reassign(self.assigned_id,self.ticket_level,self.group_id)
        else:
            if not self.assigned_id:
                ticket.action_escalate(self.category_id,self.service_type_id,self.group_id,self.ticket_level)
            else:
                ticket.action_escalate(self.category_id,self.service_type_id,self.group_id,self.ticket_level,self.assigned_id)

class MwideHelpdeskResolution(models.Model):
    _name = "mwide.helpdesk.resolution"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Ticket')
    assigned_id = fields.Many2one('res.users','Assigned by')
    resolution_time = fields.Float('Time Spent',readonly=True )
    assigned_date = fields.Datetime('Assigned Date')
    continue_date = fields.Datetime('Conitnue Date')
    
    
    
class MwideHelpdeskVendor(models.Model):
    _name = "mwide.helpdesk.vendor" 
    bu_sel = [
    ('epc','EPC'),
    ('cels','CELS'),
    ('fw','FORMWORKS'),
    ('bptg','BPTG'),
    ('precase','PRECAST'),
    ('holdco','HOLDCO'),
    ('tmf','TMF'),
    ('bum','BUM'),
    ('mh','MEGAHOMES')
    
    ]
    helpdesk_id = fields.Many2one('mwide.helpdesk','Ticket')
    vendor_code = fields.Char('Code')
    partner_type = fields.Selection([('customer','Customer'),('vendor','Vendor')],string="Partner Type")
    vendor_name = fields.Char('Name')
    vendor_purpose = fields.Char('Purpose')
    vendor_address = fields.Char('Vendor Address',size=64)
    vendor_tin = fields.Char('TIN')
    vendor_tax_code = fields.Char('Tax Code')
    vendor_tax_rate = fields.Char('WTax Rate')
    vendor_vat_type = fields.Char('VAT Type')
    vendor_type = fields.Selection([('local','Local'),('foreign','Foreign')],'Local/Foreign')
    vendor_terms = fields.Char('Terms')
    vendor_contact_person = fields.Char('Contact Person')
    vendor_contact_details = fields.Char('Contact Details')
    vendor_website = fields.Char('Website')
    vendor_docs_ids = fields.One2many('mwide.helpdesk.vendor.docs', 'helpdesk_vendor_id', string='Supporting Docs')
    business_unit = fields.Selection(bu_sel,string="Business Unit")
    remarks = fields.Text('Remarks')
    
class MwideHelpdeskStock(models.Model):
    _name = "mwide.helpdesk.stock" 
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Ticket')      
    item_maintenance = fields.Char('Item Maintenance')
    item_code = fields.Char('Item Code')
    item_description = fields.Char('Item Description')
    item_unit = fields.Char('Unit')
    item_group = fields.Char('Item Group')
    item_uom_group = fields.Char('UoM Group')
    item_type = fields.Char('Item Type')
    is_asset = fields.Boolean('Asset',default=False)
    other_specs = fields.Char('Other Specs')
    brand= fields.Char('Brand')
    attachment_ids = fields.Many2many(comodel_name='ir.attachment',string="Supplier Quotations",readonly="False")
    
class MwideHelpdeskAccount(models.Model):
    _name = 'mwide.helpdesk.account'
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Ticket')
    license_type = fields.Char('License Type')
    full_name = fields.Char('Full Name')
    position = fields.Char('Position')
    department_id = fields.Many2one('hr.department','Department')
    project_id = fields.Many2one('mwide.project','Project')
    manager_id = fields.Many2one('hr.employee','Manager')
    sbu = fields.Selection([('holdco','HOLDCO'),('epc','EPC'),('cpi','CPI'),('pitx','PITX'),('ph','PH1WORLD'),('bu','BU'),('c2w','CEBU2WORLD')],string="SBU")
    
class MwideHelpdeskProject(models.Model):
    _name = 'mwide.helpdesk.project'
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Ticket')
    project_code = fields.Char('Project Code')
    project_name = fields.Char('Project Name')
    from_date = fields.Date('Validty From')
    to_date = fields.Date('Validty To')

class BusinessHours:
 
    def __init__(self,datetime1,datetime2,worktiming=[9 ,18],weekends=[6,7],holidayfile=None):
        self.holidayfile = holidayfile
        self.weekends = weekends
        self.worktiming = worktiming
        self.datetime1 = datetime1
        self.datetime2 = datetime2

    def getdays(self):
        days = (self.datetime2-self.datetime1).days 
            #exclude any day in the week marked as holiday (ex: saturday , sunday)
        noofweeks = days / 7 
        extradays = days % 7
        startday = self.datetime1.isoweekday();
        days = days - (noofweeks * self.weekends.__len__())
        # this is for the extra days that dont make up 7 days , to remove holidays from them
        for weekend in self.weekends:
            if(startday==weekend):
                days = days - 1;
            else:
                if(weekend >= startday):
                    if(startday+extradays >= weekend):
                        days = days - 1 ;
                else:
                    if(7-startday+extradays>=weekend):
                            days = days - 1 ;
        # Read company holiday's from the file 
        if(self.holidayfile is not None):                 
        #exclude any holidays that have been marked in the companies academic year
            for definedholiday in self.holidayfile:
                flag=0;
                #year , month , day = definedholiday.split('-')
                #holidate = datetime(year=int(year) , month=int(month) , day=int(day))
                for weekend in self.weekends:
                    #check if mentioned holiday lies in defined weekend , shouldnt deduct twice
                    if(definedholiday.isoweekday==weekend): 
                        flag=1;
                        break; 
                if(flag==1):
                    continue;
                if(self.dateinbetween(definedholiday)):
                    days = days - 1
        return days
     
    def gethours(self):
        days = self.getdays()
        #calculate hours 
        days = round(days, 0)
        days = days - 1 # (-2)To remove the start day and the last day
        if days <= 0:
            days = 0
        hoursinaday = self.worktiming[1]-self.worktiming[0]
        hours = (days * hoursinaday)*60
        # To calculate working hours in the first day.
        if(self.datetime1.hour < self.worktiming[0] ):
            hoursinfirstday = hoursinaday * 60;
        elif(self.datetime1.hour > self.worktiming[1]):
            hoursinfirstday = 0;
        else:
            hoursinfirstday = ((self.worktiming[1]-self.datetime1.hour)*60) - self.datetime1.minute
        # To calculate working hours in the last day
        if(self.datetime2.hour > self.worktiming[1] ):
            hoursinlastday = hoursinaday * 60;
        elif(self.datetime2.hour < self.worktiming[0]):
            hoursinlastday = 0;
        else:
            hoursinlastday = ((self.datetime2.hour-self.worktiming[0])*60) + self.datetime2.minute
        hours = hours + hoursinfirstday + hoursinlastday
        if self.datetime2.strftime('%d %m %Y') == self.datetime1.strftime('%d %m %Y'):
            if self.datetime1.hour >= self.worktiming[0] and self.datetime2.hour < self.worktiming[1]:
                hours =( (self.datetime2.hour - self.datetime1.hour) * 60 )  + self.datetime2.minute - self.datetime1.minute
            elif self.datetime1.hour < self.worktiming[0] and self.datetime2.hour < self.worktiming[1]:
                hours =( (self.datetime2.hour - self.worktiming[0]) * 60 ) + self.datetime2.minute
            elif self.datetime1.hour < self.worktiming[0] and self.datetime2.hour >= self.worktiming[1]:
                hours =( (self.worktiming[1] - self.worktiming[0]) * 60 ) 
            elif self.datetime1.hour >= self.worktiming[0] and self.datetime1.hour < self.worktiming[1] and self.datetime2.hour >= self.worktiming[1]:
                hours =( (self.worktiming[1] - self.datetime1.hour) * 60 ) - self.datetime1.minute 
            if self.datetime1.hour >= self.worktiming[1] or self.datetime2.hour < self.worktiming[0]:
                hours = 0 
            hours = abs(hours)
        #    minutes =int((self.datetime2 - self.datetime1).total_seconds())
        #    hours = minutes
        return hours

    def dateinbetween(self,holidate):
        if(holidate.year > self.datetime1.year and holidate.year <= self.datetime2.year):
            return True
        if(holidate.year >= self.datetime1.year and holidate.year < self.datetime2.year):
            return True
        if(holidate.year == self.datetime1.year and holidate.year == self.datetime2.year):
            if(holidate.month > self.datetime1.month and holidate.month <= self.datetime2.month):
                return True
            if(holidate.month >= self.datetime1.month and holidate.month < self.datetime2.month):
                return True
            if(holidate.month == self.datetime1.month and holidate.month == self.datetime2.month):
                if(holidate.day >= self.datetime1.day and holidate.day <= self.datetime2.day):
                    return True
        return False;
    
class MwideHelpdeskSplit(models.Model):
    _name = "mwide.helpdesk.split"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    no_of_tickets = fields.Integer('No. of Tickets to Generate',required=True)
    ticket_ids = fields.One2many('mwide.helpdesk.split.lines','ticket_id',string="Generated Tickets")
    
    def generate(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        print(ticket.name)
        res = []
        if self.no_of_tickets < 1:
            raise ValidationError(_('Invalid Input for Field : No. of Tickets'))
        for i in range(self.no_of_tickets):
            c = self.env['mwide.helpdesk.split.lines'].create({'category_id':ticket.category_id.id,'service_type_id':ticket.service_type_id.id})
            res.append(c.id)
        self.ticket_ids = [(6,0,res)]
        return {
            'name' : 'Split Ticket',
            'res_model': 'mwide.helpdesk.split',
            'type': 'ir.actions.act_window',
            'context': {'default_helpdesk_id': ticket.id},
            'view_mode': 'form',
            'view_type': 'form',
            'res_id':self.id,
            'target': 'new'
            }
            
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        #res = []
        print(self.env.context['default_helpdesk_id'])
        if self.ticket_ids:
            print(ticket.name)
            for i in self.ticket_ids:
                if not i.assigned_id:
                    raise ValidationError(_('Missing IT Assignment'))
                vals = {'parent_id':ticket.id,
                        'ticket_level':'l1',
                        'approval_selection': 'none',
                        'category_id':i.category_id.id,
                        'service_type_id':i.service_type_id.id,
                        'bol_escalate':False,
                        'ticket_date': fields.Datetime.now(),
                        'response_time':0,
                        'resolution_time':0,
                        'assigned_id':i.assigned_id.id,
                        'assigned_date':fields.Datetime.now(),
                        'group_id': i.group_id.id,
                        'requested_id':ticket.requested_id.id
                        }
                new_ticket = ticket.copy(vals)
                new_ticket.set_assign()
                #res.append(new_ticket.id)
            #ticket.child_ids = [(6,0,res)]
        else:
            raise ValidationError(_('Invalid Input for Field : No. of Tickets, No tickets will be generated'))
        
            
    
    
class MwideHelpdeskSplitLines(models.Model):
    _name = "mwide.helpdesk.split.lines"
     
    ticket_id = fields.Many2one('mwide.helpdesk.split','Ticket')
    category_id = fields.Many2one('mwide.category','Category',required=True)
    service_type_id = fields.Many2one('mwide.service_type','Service Type',required=True)
    group_id = fields.Many2one('res.groups','IT Group',domain="[('it_level','=',['l1','l2','l3'])]")
    assigned_id = fields.Many2one('res.users','IT Assignment',domain="")
    
    @api.onchange('group_id')
    def _onchange_group_id(self):
        if self.group_id:
            user_ids = []
            for i in self.group_id.users:
                user_ids.append(i.id) 
            return {'domain': {'assigned_id':[('id','in',user_ids)]}}
    
class HrEmployee(models.Model):
    _inherit = "hr.employee"
    
    group_comptroller = fields.Boolean('Financial Reporting Group Comptroller',default=False)
    finance_manager = fields.Boolean('Financial Reporting Supervisor',default=False)
    
class MwideHelpdeskCoaReject(models.TransientModel):
    _name = "mwide.helpdesk.coa.reject" 
    
    remarks = fields.Text('Remarks')
    
    def submit(self):
        ticket = self.env['mwide.helpdesk.coa'].search([('id','=',self.env.context['default_coa_id'])])
        ticket.action_reject()
        ticket.ticket_id.sudo().resolution_notes = self.remarks
        
        
class MwideHelpdeskCoa(models.Model):
    _name = "mwide.helpdesk.coa"
     
     
    ticket_id = fields.Many2one('mwide.helpdesk','Ticket')
    
    
    
    name = fields.Char('Name',related="ticket_id.name")
    ticket_state = fields.Selection([
        ('01draft', 'Draft'),
        ('02request', 'Request'),
        ('03approved_head', 'Approved by Manager'),
        ('04approved_it', 'Approved by IT'),
        ('05assigned', 'Assigned/In-Progress'),
        ('06on_hold', 'Pending'),
        ('07validation', 'For Closure/Confirmation'),
        ('08unresolved', 'Unresolved'),
        ('09resolved', 'Resolved'),


    ], 'Status', default="01draft", track_visibility='onchange',related="ticket_id.state")  
    assigned_id = fields.Many2one('res.users','IT Assignment',related="ticket_id.assigned_id")  
    business_unit = fields.Many2one('mwide.helpdesk.coa.bu')
    requestor_id = fields.Many2one('res.users','Requestor',default=lambda self: self.env.user.id)
    change_type = fields.Selection([('activation','Activation'),('add','Addition'),('change','Change Name'),('change_map','Change Mapping'),('deactivation','Deactivation'),('others','Others (pls specify)')],string="Change Type")
    others_type = fields.Char('Others')
    account_name = fields.Char('Account Name')
    account_code = fields.Char('Account Code')
    purpose = fields.Char('Purpose')
    lvl1_code = fields.Many2one('mwide.helpdesk.coa.lvl1','Level 1 : FS Account Type')
    lvl2_code = fields.Many2one('mwide.helpdesk.coa.lvl2','Level 2 : FS Account Classification')
    lvl3_code = fields.Many2one('mwide.helpdesk.coa.lvl3','Level 3 : FS Account Name')
    lvl4_code = fields.Many2one('mwide.helpdesk.coa.lvl4','Level 4 : Notes to FS')
    approver_1 = fields.Many2one('res.users','Approver 1 : Business Unit Manager')
    approver_2 = fields.Many2one('hr.employee','Approver 2: Financial Reporting Supervisor',domain="[('finance_manager','=',True)]",default=lambda self: self.env['hr.employee'].search([('finance_manager','=',True)],limit=1))
    approver_3 = fields.Many2one('hr.employee','Approver 3: Group Comptroller',domain="[('group_comptroller','=',True)]",default=lambda self: self.env['hr.employee'].search([('group_comptroller','=',True)],limit=1))
    state = fields.Selection([('draft','Draft'),('approval1','For 1st Approval'),('approval2','For 2nd Approval'),('approval3','Final Approval'),('reject','Rejected'),('submit','Submitted')],string="Status",default="draft")
    helpdesk_bot = fields.Many2one('res.users',"Helpdesk Bot",default=lambda self: self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1))
    is_approver1 = fields.Boolean(string="Is Approver 1",compute="get_approver1",default=lambda self: True if self.approver_1 == self.env.user  else False)
    is_approver2= fields.Boolean(string="Is Approver 2",compute="get_approver2",default=lambda self: True if self.approver_2.user_id== self.env.user  else False)
    is_approver3 = fields.Boolean(string="Is Approver 3",compute="get_approver3",default=lambda self: True if self.approver_3.user_id == self.env.user  else False)
   
    def action_reject_wizard(self): 
        return {
            'name': 'Reject Request',
            'res_model': 'mwide.helpdesk.coa.reject',
            'type': 'ir.actions.act_window',
            'context': {'default_coa_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }
    def get_approver1(self):
        if self.approver_1 == self.env.user:
            self.is_approver1= True
        else:
            self.is_approver1 = False
    
    def get_approver2(self):
        if self.approver_2.user_id == self.env.user:
            self.is_approver2= True
        else:
            self.is_approver2 = False
    
    def get_approver3(self):
        if self.approver_3.user_id == self.env.user:
            self.is_approver3= True
        else:
            self.is_approver3 = False
    
    @api.onchange('business_unit')
    def onchange_business_unit(self):
        if self.business_unit:
            self.approver_1 = self.business_unit.approver_id.id
    
    def action_reject(self):
        self.state = 'reject'
        self.sudo().ticket_id.set_resolved()
        
    def get_link_to_document(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action = action = self.env.ref('mega_helpdesk.action_mwide_helpdesk_coa_wizard')
        
        db = {'db': self._cr.dbname}
        query = {
            'id'        : self.id,
            'view_type' : 'form',
            'model'     : self._name,
            'action'    : action.id,
        }
        
        path = "/web?%s#%s" % (parse.urlencode(db), parse.urlencode(query))
        full_url = base_url + path
        
        return full_url
    
    
    def action_submit(self):
        helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
        if not helpdesk_bot:
            raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
        else:
            self.helpdesk_bot = helpdesk_bot
            email = self.env.ref('mega_helpdesk.email_template_coa_approval1')
            if email:
                email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
            self.state = 'approval1'
        
    def action_approved1(self):
        helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
        if not helpdesk_bot:
            raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
        else:
            self.helpdesk_bot = helpdesk_bot
            email = self.env.ref('mega_helpdesk.email_template_coa_approval2')
            if email:
                email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
            self.state = 'approval2'
     
    def action_approved2(self):
        helpdesk_bot = self.env['res.users'].search([('login','=','__system__'),('active','=',False)],limit=1)
        if not helpdesk_bot:
            raise ValidationError(_('Helpdesk Bot Account is not configured, Please Contact the Helpdesk Administrator'))
        else:
            self.helpdesk_bot = helpdesk_bot
            email = self.env.ref('mega_helpdesk.email_template_coa_approval3')
            if email:
                email.send_mail(self.id, force_send=True,email_values={'author_id':helpdesk_bot.partner_id.id})
            self.state = 'approval3'
    
    def action_approved3(self):
        self.state = 'submit'
        self.sudo().ticket_id.set_request_coa()
    
                
class MwideHelpdeskCoaBu(models.Model):
    _name = "mwide.helpdesk.coa.bu"    

    name = fields.Char('Name',required=True)
    approver_id = fields.Many2one('res.users','Approver',required=True)
    
class MwideHelpdeskCoaLvl1(models.Model):
    _name = "mwide.helpdesk.coa.lvl1"  
    
    name = fields.Char('Name')
    
class MwideHelpdeskCoaLvl2(models.Model):
    _name = "mwide.helpdesk.coa.lvl2"  
    
    name = fields.Char('Name')

class MwideHelpdeskCoaLvl3(models.Model):
    _name = "mwide.helpdesk.coa.lvl3"  
    
    name = fields.Char('Name')
class MwideHelpdeskCoaLvl4(models.Model):
    _name = "mwide.helpdesk.coa.lvl4"  
    
    name = fields.Char('Name')
    
class MwideHelpdeskSeverity(models.TransientModel):
    _name = "mwide.helpdesk.severity"
    
    helpdesk_id = fields.Many2one('mwide.helpdesk','Helpdesk')
    severity_id = fields.Many2one('mwide.severity','Change Severity to')
    approver_id = fields.Many2one('res.users','IT Approver',default=lambda self: self.env['hr.department'].search([('is_itdep','=',True)],limit=1).severity_approver_id.id)

    
    def submit(self):
        ticket = self.env['mwide.helpdesk'].search([('id','=',self.env.context['default_helpdesk_id'])])
        if self.severity_id == ticket.severity_id:
            raise ValidationError(_('You can not select the same Severity level'))
        else:
            ticket.change_severity_id = self.severity_id.id
            email = self.env.ref('mega_helpdesk.email_template_severity_change')
            if email:
                email.send_mail(ticket.id, force_send=True)
            ticket.severity_approval = 'pending'

class MwideHelpdeskOnActivitiesLines(models.Model):            
    _name = 'mwide.helpdesk.activities.lines'
    
    activity_id = fields.Many2one('mwide.helpdesk.activities','Activity')
    category_id = fields.Many2one("mwide.category", string="Category", required=True,track_visibility='onchange')
    service_type_id = fields.Many2one("mwide.service_type", string="Service Type", required=True, track_visibility='onchange', domain="[('category_id', '=', category_id)]")
    group_id = fields.Many2one('res.groups','IT Group',domain="[('it_level','=',['l1','l2','l3'])]")
    assigned_id = fields.Many2one('res.users','Responsible User',domain="")
    
    @api.onchange('group_id')
    def _onchange_group_id(self):
        if self.group_id:
            user_ids = []
            for i in self.group_id.users:
                user_ids.append(i.id) 
            return {'domain': {'assigned_id':[('id','in',user_ids)]}}

class MwideHelpdeskOnActivities(models.Model):            
    _name = 'mwide.helpdesk.activities'
    
    name = fields.Char('Activity',required=True)
    is_account = fields.Boolean('is Onboarding?',default=False)
    ticket_ids = fields.One2many('mwide.helpdesk.activities.lines','activity_id','Activities/Tasks') 
     
class MwideHelpdeskBoardingLines(models.Model):
    _name = "mwide.helpdesk.boarding.lines"
     
    board_id = fields.Many2one('mwide.helpdesk.boarding','boarding')
    category_id = fields.Many2one('mwide.category','Category',required=True)
    service_type_id = fields.Many2one('mwide.service_type','Service Type',required=True)
    group_id = fields.Many2one('res.groups','IT Group',domain="[('it_level','=',['l1','l2','l3'])]")
    assigned_id = fields.Many2one('res.users','IT Assignment',domain="")
    ticket_notes = fields.Text('Ticket Notes')
    
    @api.onchange('group_id')
    def _onchange_group_id(self):
        if self.group_id:
            user_ids = []
            for i in self.group_id.users:
                user_ids.append(i.id) 
            return {'domain': {'assigned_id':[('id','in',user_ids)]}}

class MwideHelpdeskBoarding(models.Model):            
    _name = 'mwide.helpdesk.boarding'
    _description = 'Off-Boarding/On-Boarding'
    _inherit = ['mail.thread']
    
    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('mwide.board') or 'OBRD-00000000'
        vals['name'] = seq
        return super(MwideHelpdeskBoarding, self).create(vals)  
    
    name = fields.Char('Name')
    sbu = fields.Selection([('holdco','HOLDCO'),('epc','EPC'),('cpi','CPI'),('pitx','PITX'),('ph','PH1WORLD'),('bu','BU'),('c2w','CEBU2WORLD')],string="SBU")
    location_id = fields.Many2one("mwide.location", string="Location", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).location_id)
    project_id = fields.Many2one("mwide.project", string="Project", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).project_id)
    department_id = fields.Many2one("hr.department", string="Department", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]).department_id)
    requestor_id = fields.Many2one("res.users",string='Requested by',default=lambda self: self.env.user,track_visibility="onchange")
    type_id = fields.Many2one('mwide.helpdesk.activities','Type')
    board_date = fields.Datetime('Date',default=fields.Datetime.now(),track_visibility="onchange")
    license_type = fields.Char('License Type')
    full_name = fields.Char('Full Name')
    position = fields.Char('Position')
    is_account = fields.Boolean('is Onboarding?',default=False)
    manager_id = fields.Many2one('hr.employee','Manager')
    ticket_ids = fields.One2many('mwide.helpdesk.boarding.lines','board_id',string="Tickets")
    helpdesk_ids = fields.One2many('mwide.helpdesk','board_id',string="Tickets")
    state = fields.Selection([('draft','Draft'),('inprogress','In-Progress'),('done','Done')],string="status",default="draft")
    
    @api.onchange('type_id')
    def onchange_type_id(self):
        self.is_account = self.type_id.is_account
    
    def done(self):
        for i in self.helpdesk_ids:
            if i.state != 'done':
                raise ValidationError(_('Tickets still in Progress'))
        self.state = 'done'
        
    def generate(self):
        res = []
        for i in self.type_id.ticket_ids:
            c = self.env['mwide.helpdesk.boarding.lines'].create({'category_id':i.category_id.id,'service_type_id':i.service_type_id.id,'group_id':i.group_id.id,'assigned_id':i.assigned_id.id})
            res.append(c.id)
        self.ticket_ids = [(6,0,res)]
        
    def submit(self):
        if self.ticket_ids:
            for i in self.ticket_ids:
                if not i.ticket_notes:
                    raise ValidationError(_('Ticket Notes is required for each Generated Task'))
                vals = {
                        'ticket_level':'l1',
                        'approval_selection': 'none',
                        'category_id':i.category_id.id,
                        'service_type_id':i.service_type_id.id,
                        'bol_escalate':False,
                        'ticket_date': fields.Datetime.now(),
                        'response_time':0,
                        'sbu':self.sbu,
                        'location_id':self.location_id.id,
                        'project_id':self.project_id.id,
                        'department_id':self.department_id.id,
                        'board_id':self.id,
                        'description':i.ticket_notes,
                        'resolution_time':0,
                        'assigned_id':i.assigned_id.id,
                        'assigned_date':fields.Datetime.now(),
                        'group_id': i.group_id.id,
                        'requested_id':self.requestor_id.id
                        }
                if i.service_type_id.sap_type == 'account':
                    account = {'license_type':self.license_type,
                               'full_name':self.full_name,
                               'position': self.position,
                               'manager_id':self.manager_id.id,
                               'sbu':self.sbu,
                               'department_id':self.department_id.id,
                               'project_id':self.project_id.id
                               }
                    
                    account_id = self.env['mwide.helpdesk.account'].create(account)
                    vals['account_ids'] = [(6,0,[account_id.id])]
                new_ticket = self.env['mwide.helpdesk'].create(vals)
                if i.assigned_id:
                    new_ticket.set_assign()
                else:
                    new_ticket.set_request()
            self.state = 'inprogress'
        else:
            raise ValidationError(_('No tickets Generated, Please Contact your Hepdesk Administrator'))