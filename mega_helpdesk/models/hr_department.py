from odoo import fields, models

    
class HrDepartment(models.Model):
    _inherit = "hr.department"
    
    is_itdep= fields.Boolean(string="IT Department?", default=False)
    approver_ids = fields.Many2many('hr.employee',string="Approvers")
    severity_approver_id =fields.Many2one('res.users','Severity Approver')