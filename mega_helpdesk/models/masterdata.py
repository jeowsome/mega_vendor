#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
from random import randint

import random
import logging
import time

_logger = logging.getLogger(__name__)


class Location(models.Model):
    _name = "mwide.location"
    _description = "Location"
    _order = "name"

    name = fields.Char(string = 'Location', required=True, track_visibility='onchange')

 

class Project(models.Model):
    _name = "mwide.project"
    _description = "Project"
    _order = "name"

    def name_get(self):
        res = []
        for rec in self:
            res.append((rec.id, '[%s] %s' % (rec.project_code,rec.name)))
        return res

    name = fields.Char(string = 'Project', required=True, track_visibility='onchange')
    location_id = fields.Many2one('mwide.location', string = 'Location', ondelete='restrict')
    project_code = fields.Char(string="Project Code",required=True)


class Department(models.Model):
    _name = "mwide.department"
    _description = "Department"
    _order = "name"

    name = fields.Char(string = 'Department', required=True, track_visibility='onchange')



class Specifics(models.Model):
    _name = "mwide.specifics"
    _description = "Specifics (Tags)"
    _order = "name"

    def _get_default_color(self):
        return randint(1, 11)       

    name = fields.Char(string = 'Specifics', required=True, track_visibility='onchange')
    color = fields.Integer(string='Color', default=_get_default_color)

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists!"),
    ]    

 
class MwideHelpdeskSchedule(models.Model):
    _name = "mwide.helpdesk.schedule"
    _description = "Service Schedule"
    _order = "name"


    name = fields.Char(string='Service Schedule', required=True, track_visibility='onchange')
    opening = fields.Float('Opening',default=8.0)
    closing = fields.Float('Closing',default=20.0)
    workdays_ids = fields.Many2many(comodel_name='mwide.helpdesk.days',string="Workdays",readonly="False")
    active = fields.Boolean('Active')
    #constraint
    @api.constrains('opening')
    def _check_opening(self):
        if self.opening < 0.0 or self.opening >24.0:
            raise ValidationError(_('Incorrect Input in Opening Field'))
        
    @api.constrains('closing')
    def _check_closing(self):
        if self.closing < 0.0 or self.closing>24.0:
            raise ValidationError(_('Incorrect Input in Closing field'))    
    
    @api.constrains('active')
    def _check_active(self):
        if self.active == True:
            recs = self.env['mwide.helpdesk.schedule'].search(['&',('active','=',True),('id','!=',self.id)])
            if recs:
                raise ValidationError(_('Only one schedule can be set to active, deactivate other record then try again'))  
            
class MwideHelpdeskCalendar(models.Model):
    _name = 'mwide.helpdesk.calendar'
    
    name = fields.Char('Holiday')
    holiday_date = fields.Date('Effective Date')        
        
class MwideHelpdeskDays(models.Model):
    _name = 'mwide.helpdesk.days'
    _description = 'Days'
    
    name = fields.Char('Day of the week',required=True)
    value = fields.Integer('Value (1-7 e.g. 1 = Monday)')