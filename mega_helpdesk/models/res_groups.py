import logging
from ast import literal_eval
from collections import defaultdict
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools.misc import ustr

from odoo.addons.base.models.ir_mail_server import MailDeliveryException
from odoo.addons.auth_signup.models.res_partner import SignupError, now

_logger = logging.getLogger(__name__)


class ResGroupsInherit(models.Model):
    _inherit = 'res.groups'
    
    it_level = fields.Selection([('l1','L1'),('l2','L2'),('l3','L3')],'IT Support Level')



class ResUsers(models.Model):
    _inherit = 'res.users'
    
    @api.model
    def _microsoft_generate_signup_values(self, provider, params):
        email = params.get('email')
        ad = self.env['mega.active.directory'].search([('email','=',email)])
        grp_ids = []
        grp_ids.append(self.env.ref('mega_helpdesk.helpdesk_user').id)
        if ad:
            for i in ad:
                for grp in i.template_user_id.groups_id:
                    if grp.id not in grp_ids:
                        grp_ids.append(grp.id)  
        hr_emp = self.env['hr.employee'].search([('work_email','=',email)],limit=1)
        employee_id = False
        if hr_emp:
            for he in hr_emp:
                employee_id = [(6,0,[he.id])]
                #employee_id = he.id
        return {
            'name': params.get('name', email),
            'login': email,
            'email': email,
            'groups_id': [(6,0, grp_ids)],
            'action_id':False,
            'employee_ids':employee_id,
            'company_id': 1,
            'oauth_provider_id': provider,
            'oauth_uid': params['user_id'],
            'microsoft_refresh_token': params['microsoft_refresh_token'],
            'oauth_access_token': params['access_token'],
            'active': True

        }


'''    def _create_user_from_template(self, values):
        template_user_id = literal_eval(self.env['ir.config_parameter'].sudo().get_param('base.template_portal_user_id', 'False'))
        template_user = self.browse(template_user_id)
        if 'email' in values:
            ad_rec = self.env['mega.active.directory'].search([('email','=',values['email'])])
            if ad_rec:
                template_user = ad_rec.template_user_id
                if ad_rec.company_id:
                    values['company_id'] = ad_rec.company_id.id
                if ad_rec.company_ids:
                    com_ids = []
                    for com in ad_rec.company_ids:
                        com_ids.append(com.id)
                    values['company_ids'] = [(6,0,com_ids)]
        hr_emp = self.env['hr.employee'].search([('work_email','=',values['email'])],limit=1)
        employee_id = False
        if hr_emp:
            for he in hr_emp:
                employee_id = he.id
            values['employee_ids'] = [(6,0,employee_id.id)]
        if not template_user.exists():
            raise ValueError(_('Signup: invalid template user'))

        if not values.get('login'):
            raise ValueError(_('Signup: no login given for new user'))
        if not values.get('partner_id') and not values.get('name'):
            raise ValueError(_('Signup: no name or partner given for new user'))

        # create a copy of the template user (attached to a specific partner_id if given)
        values['active'] = True
        try:
            with self.env.cr.savepoint():
                return template_user.with_context(no_reset_password=True).copy(values)
        except Exception as e:
            # copy may failed if asked login is not available.
            raise SignupError(ustr(e))'''