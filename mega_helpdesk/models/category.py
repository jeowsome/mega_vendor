#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
from ast import literal_eval
import random
import logging
import time

_logger = logging.getLogger(__name__)


class Category(models.Model):
    _name = "mwide.category"
    _inherit = ['mail.thread']
    _description = "Category"
    _order = "name"

    category_ids = fields.One2many('mwide.service_type', 'category_id', string='Category')
    color = fields.Integer('Color')
    name = fields.Char(string = 'Category', required=True, track_visibility='onchange')
    description = fields.Char(string = 'Description', required=True, track_visibility='onchange',
        help="Enter Full Description for this Category...")
    image_128 = fields.Image("Logo", max_width=64, max_height=64)
    category_count = fields.Integer(compute="_compute_category_count", string="", store=True)
    category_count = fields.Integer(compute="_compute_category_count", string="", store=True)
    type = fields.Selection([('hr','Human Resources'),('it','IT')],default="it",string="Type")


 
    @api.depends('category_ids')
    def _compute_category_count(self):
        Model = self.env['mwide.service_type']
        for record in self:
            record.category_count = Model.search_count([('category_id', '=', record.id)])
            
    def _get_action(self, action_xmlid):
        action = self.env["ir.actions.actions"]._for_xml_id(action_xmlid)
        if self:
            action['display_name'] = self.display_name

        context = {
            'default_category_id': self.id,
        }
        
        action_context = literal_eval(action['context'])
        context = {**action_context, **context}
        action['context'] = context
        action['view_mode'] = 'form'
        return action
    
    def get_action_helpdesk_action(self):
        return {
            #'name': self.order_id,
            'res_model': 'mwide.category',
            'type': 'ir.actions.act_window',
            'context': {'default_category_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': self.env["ir.actions.actions"]._for_xml_id('mega_helpdesk.helpdesk_action'),
            'target': 'new'
        }
       #return self._get_action('mega_helpdesk.helpdesk_action')

    def action_service_type_model(self):
        self.ensure_one()
        view = {
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'mwide.service_type',
            'name': 'Service Types',
            'search_view_id': self.env.ref('mega_helpdesk.service_type_search_filter').id,
            'context': {'search_default_category_id': self.id}
        }
        return view
        
    def action_service_type_kanban(self):
        self.ensure_one()
        view = {
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban',
            'res_model': 'mwide.service_type',
            'name': 'Service Types',
            'search_view_id': self.env.ref('mega_helpdesk.service_type_search_filter').id,
            'context': {'search_default_category_id':self.id}
        }
        return view

    # @api.model
    # def default_image(self):
    #     image_path = get_module_resource('wc_member', 'static/src/img', 'default_image.png')
    #     return tools.image_resize_image_big(open(image_path, 'rb').read().encode('base64'))    


class ServiceType(models.Model):
    _name = "mwide.service_type"
    _description = "Service Type"
    _order = "name"
    _inherit = ['mail.thread']
    
    AVAILABLE_PRIORITIES = [
    ('0','Very Low'),
    ('1', 'Low'),
    ('2', 'Medium'),
    ('3', 'High'),
    ]
    
    category_id = fields.Many2one('mwide.category', 'Category', readonly=True, ondelete='cascade')
    
    name = fields.Char(string = 'Service Type', required=True, track_visibility='onchange')

    severity_id = fields.Many2one("mwide.severity", string="Severity", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['mwide.severity'].search([], limit=1))
    time_acknowledge = fields.Integer(string='Time to Acknowledge (mins)',related='severity_id.time_acknowledged', store=True , default=15, track_visibility='onchange' , readonly=False)
    time_resolution = fields.Integer(string='Resolution Time (hrs)', related='severity_id.time_resolution', default = 0 , store=True, track_visibility='onchange', readonly=False)
    priority_id = fields.Many2one("mwide.priority", string="Priority", required=True, ondelete="restrict", track_visibility='onchange', default=lambda self: self.env['mwide.priority'].search([], limit=1))
    bol_it_mngr = fields.Boolean(string="IT Manager Approval", default=False, track_visibility='onchange')
    bol_manager = fields.Boolean(string="Manager Approval", default=False, track_visibility='onchange')
    priority = fields.Selection(AVAILABLE_PRIORITIES, string = 'Priority',default="1")
    approval_selection = fields.Selection([
        ('none', 'None'),
        ('manager', 'Reporting Manager'),
        ('it_manager', 'Reporting Manager and IT Manager'),
    ], 'Approval', default="none",required=True)
    
    sap_type = fields.Selection([
        ('na','N/A'),
        ('account','Creation of Account/License'),
        ('vendor', 'Vendor'),
        ('item', 'Item'),
        ('project','Project'),
        ('coa','COA'),

    ], 'Type', default='na', track_visibility='onchange')  
    
    
    def action_helpdesk_form(self):
        self.ensure_one()
        view = {
            'type': 'ir.actions.act_window',
            'view_mode': 'form,tree,kanban',
            'res_model': 'mwide.helpdesk',
            'name': 'Tickets',
            'context': {'default_service_type_id': self.id, 'default_category_id': self.category_id.id}
        }
        return view







class Severity(models.Model):
    _name = "mwide.severity"
    _description = "Severity (SLA)"
    _order = "name"

    name = fields.Char(string = 'Service Type', required=True, track_visibility='onchange')
    description = fields.Char(string = 'Description', required=True, track_visibility='onchange',
        help="Enter Full Description for this Severity...")
    time_resolution = fields.Integer(string = 'Resolution Time (hrs)', required=True, track_visibility='onchange', default=0)
    time_acknowledged = fields.Integer(string = 'Time to Acknowledge Ticket (mins)', required=True, track_visibility='onchange', default=15)
    is_sla = fields.Boolean('SLA',default=True)
    # time_acknowledge = fields.Integer(string='Time to Acknowledge (Minutes)')
    # time_resolution = fields.Integer(string='Resolution Time (Minutes)')        


class Priority(models.Model):
    _name = "mwide.priority"
    _description = "Priority"
    _order = "name"
    AVAILABLE_PRIORITIES = [
    ('1', 'Low'),
    ('2', 'Medium'),
    ('3', 'High'),
    ]
    name = fields.Char(string = 'Priority', required=True, track_visibility='onchange')
    description = fields.Char(string = 'Description', required=True, track_visibility='onchange',
        help="Enter Full Description for this Severity...")
    priority = fields.Selection(AVAILABLE_PRIORITIES, string = 'Priority Level')
