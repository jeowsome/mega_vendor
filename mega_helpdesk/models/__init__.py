# -*- coding: utf-8 -*-
from . import hr_department
from . import helpdesk
from . import category
from . import masterdata
from . import hr_employee
from . import mail_message
from . import res_groups