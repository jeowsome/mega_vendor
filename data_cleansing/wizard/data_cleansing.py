import io
import xlrd
import babel
import logging
import tempfile
import binascii
from io import StringIO
from datetime import date, datetime, time, timedelta
from odoo import api, fields, models, tools, _
from odoo.exceptions import Warning, UserError, ValidationError
_logger = logging.getLogger(__name__)

# Include these imports
import os
import sys
import xmlrpc.client
from xmlrpc.client import Transport as XMLTransport
import ssl

import requests
import json

class DataCleansingCleansing(models.TransientModel):
    _name = 'data.cleansing'
    _description = 'Action for Data Cleansing'

    target_model = fields.Many2one('ir.model', string='Target Model')
    target_field = fields.Many2one('ir.model.fields', string='Target Field')

    old_value_char = fields.Char(string='Old Value')
    old_value_many2one = fields.Many2one('data.dump_old', string='Old Value')
    old_value_date = fields.Char(string='Old Value')
    old_value_datetime = fields.Date(string='Old Value')
    old_value_boolean = fields.Boolean(string='Old Value')

    new_value_char = fields.Char(string='New Value')
    new_value_many2one = fields.Many2one('data.dump', string='New Value')
    new_value_date = fields.Date(string='New Value')
    new_value_datetime = fields.Char(string='New Value')
    new_value_boolean = fields.Boolean(string='New Value')

    total_record = fields.Integer(string='Records to Update')
    total_record_updated = fields.Integer(string='Records Updated')

    field_type = fields.Char(string='Field Type')
    remarks = fields.Text(string='Remarks', default='After Selecting the Target Model and Target Field - Click on the Generate Button')
    isGenerated = fields.Boolean(string='Is Generated?', default=False)
    isComplete = fields.Boolean(string='Is Complete?', default=False)

    @api.onchange('target_field')
    def _onchange_field_type(self):
        for rec in self:
            rec.field_type = rec.target_field.ttype

    @api.onchange('old_value_char','old_value_many2one','old_value_date','old_value_datetime','old_value_boolean')
    def _onchange_value(self):
        for rec in self:
            if rec.field_type == 'many2one':
                target_model = rec.target_model.model
                target_field = rec.target_field.name
                source_model = rec.target_field.relation
                value = rec.old_value_many2one.name
                old_value = self.env[source_model].search([('name','=',value)]).name
                rec.total_record = self.env[target_model].search_count([(target_field,'=',old_value)])

    def action_retry(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'data.cleansing',
            'name': 'Data Cleansing',
            'context': {
                'default_target_model': self.target_model.id,
                'default_target_field': self.target_field.id,
                'default_field_type': self.field_type,
                'default_remarks': 'Select Old value and its New Value - Click Execute Button',
                'default_isGenerated': True,
                'default_isComplete': False,
            },
            'target': 'new',
        }

    def action_generate(self):
        for rec in self:
            field = rec.target_field.name

            source_model = rec.target_field.relation
            new_data = self.env[source_model].search([])

            target_model = rec.target_model.model
            old_data = self.env[target_model].search_read([])

        self.env['data.dump'].search([]).unlink()
        for nd in new_data:
            self.env['data.dump'].create({'name': nd.name})

        self.env['data.dump_old'].search([]).unlink()
        for od in old_data:
            check = od[field]
            if check != False and check != '':
                value = od[field][-1]
                ctr = self.env['data.dump_old'].search_count([('name','=',value)])
                if ctr < 1:
                    self.env['data.dump_old'].create({'name': value})
                    self._cr.commit()

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'data.cleansing',
            'name': 'Data Cleansing',
            'context': {
                'default_target_model': self.target_model.id,
                'default_target_field': self.target_field.id,
                'default_field_type': self.field_type,
                'default_remarks': 'Select Old value and its New Value - Click Execute Button',
                'default_isGenerated': True,
            },
            'target': 'new',
        }

    def action_execute(self):
        for rec in self:
            target_model = rec.target_model.model
            target_field = rec.target_field.name
            old_value = rec.old_value_many2one.name
            new_value = rec.new_value_many2one.name

            source_model = rec.target_field.relation

        target_ids = self.env[target_model].search([(target_field,'=',old_value)])
        count_target_ids = len(target_ids)
        record= {}
        for ti in target_ids:
            value_id = self.env[source_model].search([('name','=',new_value)]).id
            record[target_field] = value_id
            ti.write(record)

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'data.cleansing',
            'name': 'Data Cleansing Feedback',
            'context': {
                'default_target_model': self.target_model.id,
                'default_target_field': self.target_field.id,
                'default_field_type': self.field_type,
                'default_old_value_many2one': self.old_value_many2one.id,
                'default_new_value_many2one': self.new_value_many2one.id,
                'default_total_record': self.total_record,
                'default_total_record_updated': count_target_ids,
                'default_remarks': 'Successfully - Updated Records',
                'default_isGenerated': True,
                'default_isComplete': True,
            },
            'target': 'new',
        }
