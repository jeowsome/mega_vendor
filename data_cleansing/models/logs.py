#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
import random
import logging
import time
import os
import sys
import xmlrpc.client
from xmlrpc.client import Transport as XMLTransport
import ssl

_logger = logging.getLogger(__name__)

class DataCleansingLogs(models.Model):
    _name = "data.logs"
    _description = "Data Cleansing Logs"

    name = fields.Char(string='Name')
    date_executed = fields.Datetime(string='Date Executed')
    target_model = fields.Char(string='Target Model')
    target_field = fields.Char(string='Target Field')
    old_value = fields.Char(string='Old Value')
    new_value = fields.Char(string='New Value')
    total_record = fields.Integer(string='Total Record')
