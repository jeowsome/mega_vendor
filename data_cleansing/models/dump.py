#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
import random
import logging
import time
import os
import sys
import xmlrpc.client
from xmlrpc.client import Transport as XMLTransport
import ssl

_logger = logging.getLogger(__name__)

class DataCleansingDumpOld(models.Model):
    _name = "data.dump_old"
    _description = "Data Cleansing Dump Old Values"

    name = fields.Char(string='Name')

class DataCleansingDump(models.Model):
    _name = "data.dump"
    _description = "Data Cleansing Dump Values"

    name = fields.Char(string='Name')
