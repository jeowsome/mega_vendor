import logging

from ast import literal_eval
from collections import defaultdict
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools.misc import ustr

from odoo.addons.base.models.ir_mail_server import MailDeliveryException
from odoo.addons.auth_signup.models.res_partner import SignupError, now

_logger = logging.getLogger(__name__)

class MegaActiveDirectory(models.Model):
    _name = 'mega.active.directory'
    
    email = fields.Char('Email', required=True)
    name = fields.Char('Name')
    template_user_id = fields.Many2one('res.users',string="Template User",required=True)
    company_id = fields.Many2one('res.company',string="Default Company")
    company_ids = fields.Many2many('res.company', 'res_company_ad_rel', 'ad_id', 'cid',
        string='Companies')
    
    _sql_constraints = [
        ('email_uniq', 'unique(email)', 'Email already exists!'),
    ]
    