{
    "name": "Megawide Active Directory",
    "version": "1.0",
    "author": "Christopher Yang",
    "website": "",
    "summary": "Provision to Import User file for user access mapping for OAuth Signup",
    "depends": ["auth_oauth","odoo_microsoft_azure_sso_integration"],
    'data': [
        'security/ir.model.access.csv',
        'views/active_directory_views.xml'
    ],
'images': ['static/description/icon.png'],


}
