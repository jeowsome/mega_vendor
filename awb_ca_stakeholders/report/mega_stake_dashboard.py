# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, tools


class StakeholderDashboard(models.Model):
    _name = "mega.stake.dashboard"
    _description = "Stakeholder Dashboard Report"
    _auto = False
    _rec_name = 'partner_id'

    """Created View-Dashboard Report """

    id = fields.Integer('View id')
    partner_id = fields.Many2one('res.partner', string="Name of Stakeholder")
    # Added partner name in view
    name = fields.Char(string="Partner Name")
    position_id = fields.Many2one('mega.stake.position', string="Position")
#     Command this code to remove meeting details from dashboard
#     subject = fields.Char('Subject',)
#     rrule_type = fields.Char('Engagement Frequency')
    target_date = fields.Date('Target Date',)
    # added company Filter in Dashboard,
    # added Engagement Approach, Power, Interest field
    # Change field name "Company" to "Business Unit"
    company_id = fields.Many2one('res.company', string='Business Unit')
    power_id = fields.Selection([('low', 'Low'),
                                 ('medium', 'Medium'),
                                 ('high', 'High'),
                                 ('very_high', 'Very High')
                                 ], string="Power")
    interest_id = fields.Selection([('low', 'Low'),
                                    ('medium', 'Medium'),
                                    ('high', 'High'),
                                    ('very_high', 'Very High')
                                    ], string="Interest",)
    engagement_approach = fields.Selection([
                            ('keep_informed', 'Keep Informed'),
                            ('keep_satisfied', 'Keep Satisfied'),
                            ('manage_closely', 'Manage Closely'),
                            ('monitor', 'Monitor (Minimum Effort)'),
                            ('null', 'N / A')
                            ], string="Engagement Approach")
#     Command this code to remove meeting details from dashboard
#     actual_engagement = fields.Selection([
#                             ('official_visit', 'Official Visit'),
#                             ('lunch_dinner_meeting', 'Lunch / Dinner meeting'),
#                             ('sponsorhip_of_project', 'Sponsorhip of project/ program'),
#                             ('invite_as_special_guest', 'Invite as special guest for event'),
#                             ('send_gift', 'Send gift (special occasion)')
#                             ], string="Actual Engagement")
#     date = fields.Datetime('Actual Date', readonly=True)
#     description = fields.Char('Remarks/Notes')
#     next_steps = fields.Selection([
#                     ('official_visit', 'Official Visit'),
#                     ('lunch_dinner_meeting', 'Lunch / Dinner meeting'),
#                     ('sponsorhip_of_project', 'Sponsorhip of project/ program'),
#                     ('invite_as_special_guest', 'Invite as special guest for event'),
#                     ('send_gift', 'Send gift (special occasion)')
#                     ], string="Next Steps")
    # Added new fields to display partner details
    sector_id = fields.Many2one('mega.stake.sector', string="Sector")
    group_id = fields.Many2one('mega.stake.group', string="Group")
    institution_id = fields.Many2one('mega.stake.institution',
                                     string="Institution / Area")
    area_of_impact_id = fields.Many2one('mega.stake.area.impact',
                                        string="Area of impact")
    responsible_id = fields.Many2one('hr.employee',
                                     string="Responsible")
    current_relationship_id = fields.Selection([
                                ('low', 'Low'),
                                ('medium', 'Medium'),
                                ('high', 'High'),
                                ('very_high', 'Very High')
                                ], string="Current Relationship")
    target_relationship_id = fields.Selection([
                                ('low', 'Low'),
                                ('medium', 'Medium'),
                                ('high', 'High'),
                                ('very_high', 'Very High')
                                ], string="Target Relationship")
    engagement_strategy_id = fields.Selection([
                                ('maintain', 'Maintain'),
                                ('catch_up', 'Catch_up'),
                                ('enhance', 'Enhance'),
                                ('intensify', 'Intensify'),
                                ('null', 'N / A'),
                                ], string="Engagement Strategy")
    status_id = fields.Selection([
                        ('in_progress', 'In-Progress'),
                        ('not_yet_started', 'Not yet started'),
                        ('done','Done')
                        ], string="Status")
    criticality_id = fields.Selection([
                                ('low', 'Low'),
                                ('medium', 'Medium'),
                                ('high', 'High'),
                                ('very_high', 'Very High')
                                ], string="Criticality", store=True)
    link_to_profile = fields.Char('Link To Profile')
# Added search filter for institution table
    institution_root_id = fields.Many2one('mega.stake.institution.search',
                                     string="Institution / Area root")

    def init(self):
        """ Create View for Dashboard report"""
        tools.drop_view_if_exists(self._cr, 'mega_stake_dashboard')
        # Get data from Partner, Company and Stake holder Engagement
        # get all columns from Stake holder Engagement
        # se.criticality_id,
        self._cr.execute("""
            create or replace view mega_stake_dashboard as (
                select row_number() over () as id,rc.id as company_id,
                    rp.sector_id, rp.group_id,
                    rp.institution_id, rp.position_id,
                    rp.id as partner_id,
                    rp.name as name,
                    rp.area_of_impact_id,
                    rp.search_institution_id as institution_root_id,
                    se.power_id,
                    se.criticality_id,
                    se.interest_id,
                    se.engagement_approach as engagement_approach,
                    se.current_relationship_id,
                    se.target_relationship_id,
                    se.engagement_strategy_id,
                    se.target_date,
                    se.status_id,
                    se.responsible_id,
                    se.link_to_profile
                from res_partner rp
                Join res_company as rc on rc.id = rp.company_id
                left Join mega_stake_details_mapping_engagement as se on se.partner_id = rp.id
                order by id,partner_id
            )""")
