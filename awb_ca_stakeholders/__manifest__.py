# -*- coding: utf-8 -*-
{
    'name': "Corporate Affairs Stakeholder Management System (CASMS)",
    'author': "Achieve Without Borders, Inc",
    'website': '"http://www.achievewithoutborders.com"',
    'summary': "Corporate Affairs Stakeholder Management System (CASMS)",
    'description': """
        Corporate Affairs Stakeholder Management System (CASMS)
    """,
    'category': 'website',
    'version': '14.1',
    'installable': True,
    'application': True,
    'depends': ['hr', 'calendar', 'contacts', 'web'],
    'data': [
            'security/dashboard_security.xml',
            'security/ir.model.access.csv',
            'data/stakeholder_email_templates.xml',
            'data/cron.xml',
            'wizard/import_ar_data_view.xml',
            'wizard/import_stake_name_view.xml',
            'wizard/import_stake_o2m_view.xml',
            'views/res_partner.xml',
            'views/calendar_event.xml',
            'views/asset_template.xml',
            'views/email_template.xml',
            #'report/mega_stack_dashboard.xml',
            #--------------------------------- 'security/stakeholder_groups.xml'
            ],
    'qweb': ['static/src/xml/switch_company.xml',
             'static/src/xml/groupby_expand.xml',
             ],
    'license': "OPL-1",
}
