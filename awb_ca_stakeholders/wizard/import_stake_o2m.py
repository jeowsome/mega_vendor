# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import io
import xlrd
import babel
import logging
import tempfile
import binascii
from io import StringIO
from datetime import date, datetime, time
from odoo import api, fields, models, tools, _
from odoo.exceptions import Warning, UserError, ValidationError
from passlib.tests.utils import limit
_logger = logging.getLogger(__name__)

try:
	import csv
except ImportError:
	_logger.debug('Cannot `import csv`.')
try:
	import xlwt
except ImportError:
	_logger.debug('Cannot `import xlwt`.')
try:
	import cStringIO
except ImportError:
	_logger.debug('Cannot `import cStringIO`.')
try:
	import base64
except ImportError:
	_logger.debug('Cannot `import base64`.')


class ImportDataAR(models.TransientModel):
	_name = 'import.stake_o2m'
	_description = 'Import Stake o2m'

	name = fields.Char(string="File Name")
	file_type = fields.Selection([('XLS', 'XLSX File')],string='File Type', default='XLS')
	file = fields.Binary(string="Upload File")
	file_name = fields.Char(string="File Name")

	def import_data(self):
		
		if not self.file:
			raise ValidationError(_("Please Upload File to Import Data !"))

		if self.file_type == 'CSV':
			line = keys = ['UserName','Locked','Professional']
			#line = keys = ['vendor_id','run_date','proj_code','proj_name','vendor_code','name','balance_due','d0','d30','d60','d90','d120','d180','d365','d365Up','posting_date','due_date','doc_num','trans_type','ref_num','days']
			#line = keys = ['vendor_id','proj_code','proj_name','vendor_code','name','balance_due','d0','d30','d60','d90','d120','d180','d365','d365Up']
			try:
				csv_data = base64.b64decode(self.file)
				data_file = io.StringIO(csv_data.decode("utf-8"))
				data_file.seek(0)
				file_reader = []
				csv_reader = csv.reader(data_file, delimiter=',')
				file_reader.extend(csv_reader)
			except Exception:
				raise ValidationError(_("Please Select Valid File Format !"))

			values = {}
			for i in range(len(file_reader)):
				field = list(map(str, file_reader[i]))
				values = dict(zip(keys, field))
				if values:
					if i == 0:
						continue
					else:
						
						remove = self.env['mega.sap_license_excel'].search([]).unlink()

			for i in range(len(file_reader)):
				field = list(map(str, file_reader[i]))
				values = dict(zip(keys, field))
				if values:
					if i == 0:
						continue
					else:
						#if (values['type'] == 'CRM-LTD' or values['type'] == 'FINANCIALS-LTD' or values['type'] == 'LOGISTICS-LTD' or values['type'] == 'PROFESSIONAL'):
						res = self.env['mega.sap_license_excel'].create(values)
						
		else:
			try:
				file = tempfile.NamedTemporaryFile(delete= False,suffix=".xlsx")
				file.write(binascii.a2b_base64(self.file))
				file.seek(0)
				values = {}
				workbook = xlrd.open_workbook(file.name)
				sheet = workbook.sheet_by_index(0)
			except Exception:
				raise ValidationError(_("Please Select Valid File Format !"))

			for row_no in range(sheet.nrows):
				val = {}
				if row_no <= 0:
					fields = list(map(lambda row:row.value.encode('utf-8'), sheet.row(row_no)))
				else:
					line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))
					#0-22
					
					values.update( {
							'name':line[0],

							})
					#check = self.env['ar.bu_vendors'].search_count([('bu_group','=',values['bu_group']),('run_date','=',datetime.strptime(values['run_date'],'%m/%d/%Y'))])
					#check = self.env['ar.bu_vendors'].search_count([])
					#if check > 0:
					#remove = self.env['mega.cis'].search([]).unlink()
						#remove = self.env['ar.bu_vendors'].search([('bu_group','=',values['bu_group']),('run_date','=',datetime.strptime(values['run_date'],'%m/%d/%Y'))]).unlink()

			for row_no in range(sheet.nrows):
				val = {}
				if row_no <= 0:
					fields = list(map(lambda row:row.value.encode('utf-8'), sheet.row(row_no)))
				else:
					line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))

					if(line[0] != ''):
						stake = line[0].strip()
					else:
						stake = line[0]
						
					if(line[1] != ''):
						sbu = line[1].strip()
					else:
						sbu = line[1]
					
					if(line[2] != ''):
						sect = line[2].strip()
					else:
						sect = line[2]
						
					if(line[3] != ''):
						group = line[3].strip()
					else:
						group = line[3]
						
					if(line[4] != ''):
						institution = line[4].strip()
					else:
						institution = line[4]
						
					if(line[5] != ''):
						posi = line[5].strip()
					else:
						posi = line[5]
						
					if(line[6] != ''):
						impact = line[6].strip()
					else:
						impact = line[6]
						
					if(self.env['mega.stake.position'].search([('name','=',posi)]).id):
						pass
					else:
						self.env['mega.stake.position'].sudo().create({'name':posi})
								
					if(self.env['mega.stake.sector'].search([('name','=',sect)]).id):
						pass
					else:
						self.env['mega.stake.sector'].sudo().create({'name':sect})
								
					if(self.env['mega.stake.group'].search([('name','=',group)]).id):
						pass
					else:
						self.env['mega.stake.group'].sudo().create({'name':group})
								
					if(self.env['mega.stake.institution'].search([('name','=',institution)]).id):
						pass
					else:
						self.env['mega.stake.institution'].sudo().create({'name':institution})
								
					if(self.env['mega.stake.area.impact'].search([('name','=',impact)]).id):
						pass
					else:
						self.env['mega.stake.area.impact'].sudo().create({'name':impact})
								
					if(self.env['res.company'].search([('name','=',sbu)]).id):
						pass
					else:
						self.env['res.company'].sudo().create({'name':sbu})
								
					pos = self.env['mega.stake.position'].search([('name','=',posi)]).id
					sec = self.env['mega.stake.sector'].search([('name','=',sect)]).id
					grp = self.env['mega.stake.group'].search([('name','=',group)]).id
					ins = self.env['mega.stake.institution'].search([('name','=',institution)]).id
					aoi = self.env['mega.stake.area.impact'].search([('name','=',impact)]).id
					com = self.env['res.company'].search([('name','=',sbu)]).id
					
					stake_name = self.env['res.partner'].search([('name','=',stake)])
					
					print(self.env['res.partner'].search([('name','=','Daniel Low')]))
					
					vals = {
							'position_id': pos,
							'sector_id': sec,
							'group_id': grp,
							'institution_id': ins,
							'area_of_impact_id': aoi,
							'company_id': com,
							}
					if(self.env['res.partner'].search([('name','=',stake),('company_id','=',com)])):
						pass
					else:
						stake_name.sudo().write(vals)
					
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
