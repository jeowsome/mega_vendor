# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import io
import xlrd
import babel
import logging
import tempfile
import binascii
from io import StringIO
from datetime import date, datetime, time
from odoo import api, fields, models, tools, _
from odoo.exceptions import Warning, UserError, ValidationError
from passlib.tests.utils import limit
_logger = logging.getLogger(__name__)

try:
	import csv
except ImportError:
	_logger.debug('Cannot `import csv`.')
try:
	import xlwt
except ImportError:
	_logger.debug('Cannot `import xlwt`.')
try:
	import cStringIO
except ImportError:
	_logger.debug('Cannot `import cStringIO`.')
try:
	import base64
except ImportError:
	_logger.debug('Cannot `import base64`.')


class ImportDataAR(models.TransientModel):
	_name = 'import.stake_name'
	_description = 'Import Stake name'

	name = fields.Char(string="File Name")
	file_type = fields.Selection([('XLS', 'XLSX File')],string='File Type', default='XLS')
	file = fields.Binary(string="Upload File")
	file_name = fields.Char(string="File Name")

	def import_data(self):
		
		if not self.file:
			raise ValidationError(_("Please Upload File to Import Data !"))

		if self.file_type == 'CSV':
			line = keys = ['UserName','Locked','Professional']
			#line = keys = ['vendor_id','run_date','proj_code','proj_name','vendor_code','name','balance_due','d0','d30','d60','d90','d120','d180','d365','d365Up','posting_date','due_date','doc_num','trans_type','ref_num','days']
			#line = keys = ['vendor_id','proj_code','proj_name','vendor_code','name','balance_due','d0','d30','d60','d90','d120','d180','d365','d365Up']
			try:
				csv_data = base64.b64decode(self.file)
				data_file = io.StringIO(csv_data.decode("utf-8"))
				data_file.seek(0)
				file_reader = []
				csv_reader = csv.reader(data_file, delimiter=',')
				file_reader.extend(csv_reader)
			except Exception:
				raise ValidationError(_("Please Select Valid File Format !"))

			values = {}
			for i in range(len(file_reader)):
				field = list(map(str, file_reader[i]))
				values = dict(zip(keys, field))
				if values:
					if i == 0:
						continue
					else:
						
						remove = self.env['mega.sap_license_excel'].search([]).unlink()

			for i in range(len(file_reader)):
				field = list(map(str, file_reader[i]))
				values = dict(zip(keys, field))
				if values:
					if i == 0:
						continue
					else:
						#if (values['type'] == 'CRM-LTD' or values['type'] == 'FINANCIALS-LTD' or values['type'] == 'LOGISTICS-LTD' or values['type'] == 'PROFESSIONAL'):
						res = self.env['mega.sap_license_excel'].create(values)

		else:
			try:
				file = tempfile.NamedTemporaryFile(delete= False,suffix=".xlsx")
				file.write(binascii.a2b_base64(self.file))
				file.seek(0)
				values = {}
				workbook = xlrd.open_workbook(file.name)
				sheet = workbook.sheet_by_index(0)
			except Exception:
				raise ValidationError(_("Please Select Valid File Format !"))

			for row_no in range(sheet.nrows):
				val = {}
				if row_no <= 0:
					fields = list(map(lambda row:row.value.encode('utf-8'), sheet.row(row_no)))
				else:
					line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))
					#0-22
					

					
					values.update( {
							'name':line[0],

							})
					#check = self.env['ar.bu_vendors'].search_count([('bu_group','=',values['bu_group']),('run_date','=',datetime.strptime(values['run_date'],'%m/%d/%Y'))])
					#check = self.env['ar.bu_vendors'].search_count([])
					#if check > 0:
					#remove = self.env['mega.cis'].search([]).unlink()
						#remove = self.env['ar.bu_vendors'].search([('bu_group','=',values['bu_group']),('run_date','=',datetime.strptime(values['run_date'],'%m/%d/%Y'))]).unlink()

			for row_no in range(sheet.nrows):
				val = {}
				if row_no <= 0:
					fields = list(map(lambda row:row.value.encode('utf-8'), sheet.row(row_no)))
				else:
					line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))

					if(line[0] != ''):
						stake = line[0].strip()
					else:
						stake = line[0]
						
					vals = {
							'name':stake,
							}
					
					if(self.env['res.partner'].search([('name','=',stake)]).id):
						print(self.env['res.partner'].search([('name','=',stake)]).id)
						pass
					else:
						print(self.env['res.partner'].search([('name','=',stake)]))
						res = self.env['res.partner'].sudo().create(vals)
						
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
