# -*- coding: utf-8 -*-
# import of odoo addons
from odoo import fields, api, models, _


# Create a new model
class StakeholderEngagementTab(models.Model):
    _name = "mega.stake.details.mapping.engagement"
    _description = "StakeholderEngagement"

    partner_id = fields.Many2one('res.partner', track_visibility='onchange')
    name = fields.Selection([
                            ('keep_informed', 'Keep Informed'),
                            ('keep_satisfied', 'Keep Satisfied'),
                            ('manage_closely', 'Manage Closely'),
                            ('monitor', 'Monitor (Minimum Effort)'),
                            ('null', 'N / A')
                            ], string="Engagement Approach", track_visibility='onchange',
                            compute="_compute_engagement", readonly=False)
    engagement_approach = fields.Selection([
                            ('keep_informed', 'Keep Informed'),
                            ('keep_satisfied', 'Keep Satisfied'),
                            ('manage_closely', 'Manage Closely'),
                            ('monitor', 'Monitor (Minimum Effort)'),
                            ('null', 'N / A')
                            ], related='name', store = True) # Added related field
    
    #partner_name = fields.Char('Stakeholder Name', store = True, compute='_compute_name')
    
    company_id = fields.Many2one('res.company', string='Business Unit', track_visibility='onchange', default=lambda self: self.env.company)
    power_id = fields.Selection([
                                ('low', 'Low'), ('medium', 'Medium'),
                                ('high', 'High'), ('very_high', 'Very High')
                                ], string="Power", track_visibility='onchange')
    interest_id = fields.Selection([
                                ('low', 'Low'), ('medium', 'Medium'),
                                ('high', 'High'), ('very_high', 'Very High')
                                ], string="Interest", track_visibility='onchange')
    current_relationship_id = fields.Selection([
                                ('low', 'Low'), ('medium', 'Medium'),
                                ('high', 'High'), ('very_high', 'Very High')
                                ], string="Current Relationship", track_visibility='onchange')
    target_relationship_id = fields.Selection([
                                ('low', 'Low'), ('medium', 'Medium'),
                                ('high', 'High'), ('very_high', 'Very High')
                                ], string="Target Relationship", track_visibility='onchange')
    engagement_strategy_id = fields.Selection([
                                ('maintain', 'Maintain'), ('catch_up', 'Catch_up'),
                                ('enhance', 'Enhance'), ('intensify', 'Intensify'),
                                ('null', 'N / A'),
                                ], string="Engagement Strategy", track_visibility='onchange')
    target_date = fields.Date('Target Date', track_visibility='onchange')
    status_id = fields.Selection([
                                   ('in_progress', 'In-Progress'),
                                   ('not_yet_started', 'Not yet started'),
                                   ('active', 'Active'),
                                   ('inactive', 'Inactive'),
                                   ('pending', 'Pending'),
                                   ('undefined', 'Undefined'),
                                   ('done','Done')
                                    ], track_visibility='onchange')
    responsible_id = fields.Many2one('hr.employee', string="Responsible", track_visibility='onchange')
    criticality_id = fields.Selection([
                                ('low', 'Low'), ('medium', 'Medium'),
                                ('high', 'High'), ('very_high', 'Very High')
                                ], string="Criticality", track_visibility='onchange', compute='_compute_criticality', store=True)
    link_to_profile = fields.Char('Link To Profile')
    
    sector_id = fields.Many2one('mega.stake.sector', string="Sector", track_visibility='onchange')
    group_id = fields.Many2one('mega.stake.group', string="Group", track_visibility='onchange')
    institution_id = fields.Many2one('mega.stake.institution', string="Institution / Area", track_visibility='onchange')
    position_id = fields.Many2one('mega.stake.position', string="Position", track_visibility='onchange')
    area_of_impact_id = fields.Many2one('mega.stake.area.impact', string="Area of impact", track_visibility='onchange')

    # Added new fields
    user_prepared_id = fields.Many2one('res.users', string='Prepared By',
                                       default=lambda self: self.env.user,
                                       track_visibility='onchange', readonly=True)
    user_approved_id = fields.Many2one('res.users', string='Approved By',
                                       track_visibility='onchange', readonly=True)
    user_noted_id = fields.Many2one('res.users', string='Noted By',
                                    track_visibility='onchange', readonly=True)
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('prepared_by', 'Submit'),
                            ('noted_by', 'Approved by SBU Head'),
                            ('approved_by', 'Cleared By Holdco'),
                            ], default='draft', string="State",
                            track_visibility='onchange', readonly=True)
    
    primary = fields.Char('Primary', track_visibility='onchange')
    secondary = fields.Char('Secondary', track_visibility='onchange')

    is_bu_staff = fields.Boolean('BU Staff', compute = '_compute_is_bu_staff')
    frequency_of_engagement_id = fields.Many2one('mega.stake.frequency.of.engagement', string="Frequency Of Engagement")
    return_to_reprepared = fields.Html(sanitize=False,
                                compute='_compute_css', store=False)

# if groups is CA Manager then automatically hidden the return to prepared button
    @api.depends('state')
    def _compute_css(self):
        for record in self:
            if (self.env.user.has_group('awb_ca_stakeholders.stake_ca_manager')):
                record.return_to_reprepared = False
            else:
                record.return_to_reprepared = '<style>.return_to_prepare {display: none !important;}</style>'

# if stakeholder role is BU Staff company field is readonly
# Added Depends field in stakeholder engagement table
    @api.depends('create_date', 'partner_id')
    def _compute_is_bu_staff(self):
        if (self.env.user.has_group('awb_ca_stakeholders.stake_ca_manager')):
            self.is_bu_staff = False
        else:
            self.is_bu_staff = True
            
    # @api.depends('partner_id')
    # def _compute_name(self):
    #     for record in self:
    #         self._origin.id
    #         record.partner_name = self.env['res.partner'].search([('id','=',record.partner_id.id)]).name

# Added Track history in Stakeholder Engagement tab
    @api.model
    def create(self, vals):
        rec = super(StakeholderEngagementTab, self).create(vals)
        if rec.partner_id:
            msg = "<b>" + _("Stakeholder Criticality") + "</b><ul>"
            for vals in rec:
                if(vals.power_id is not False):
                    msg += _("<li>Power : %s", vals.power_id) + "<li/>"
                if(vals.interest_id is not False):
                    msg += _("Interest : %s", vals.interest_id) + "<br/>"
                if(vals.name is not False):
                    msg += _("Engagement Approach : %s", vals.name) + "<br/>"
                if(vals.company_id is not False):
                    msg += _("Business Unit : %s", vals.company_id.name) + "<br/>"
                if(vals.current_relationship_id is not False):
                    msg += _("Current Relationship : %s", vals.current_relationship_id) + "<br/>"
                if(vals.target_relationship_id is not False):
                    msg += _("Target Relationship : %s", vals.target_relationship_id) + "<br/>"
                if(self.env['res.users'].search([('id','=',vals.user_approved_id.id)]).id is not False):
                    msg += _("Approved By : %s", vals.user_approved_id.name) + "<br/>"
                if(vals.state is not False):
                    msg += _("Status : %s", vals.status_id) + "<br/>"
                if(vals.user_prepared_id is not False):
                    msg += _("prepared By : %s", vals.user_prepared_id.name) + "<br/>"
                if(vals.engagement_strategy_id is not False):
                    msg += _("Engagement Strategy : %s", vals.engagement_strategy_id) + "<br/>"
                if(vals.target_date is not False):
                    msg += _("Target Date : %s", vals.target_date) + "<br/>"
                if(self.env['res.users'].search([('id','=',vals.responsible_id.id)]).id is not False):
                    msg += _("Responsible : %s", vals.responsible_id.name) + "<br/>"
                if(vals.criticality_id is not False):
                    msg += _("Criticality : %s", vals.criticality_id) + "<br/>"
                if(vals.link_to_profile is not False):
                    msg += _("Link To Profile : %s", vals.link_to_profile) + "<br/>"
                if(vals.state is not False):
                    msg += _("State : %s", vals.state) + "<br/>"
            rec.partner_id.message_post(body=msg)
        return rec

    def write(self, vals):
        if self.partner_id:
            for rec in self:
                msg = "<b>" + _("Stakeholder Criticality") + "</b><ul>"
                msg += _("Changes made in: %s <br/>", self.power_id)
                
                try:
                    if(vals['power_id']):
                        msg += _("Power")
                        msg += _("<li>Old: %s", self.power_id) + "</li>"
                        msg += _("<li>New: %s", vals['power_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['interest_id']):
                        msg += _("Interest")
                        msg += _("<li>Old: %s", self.interest_id) + "</li>"
                        msg += _("<li>New: %s", vals['interest_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['name']):
                        dict_get_state = dict(self._fields['name'].selection)
                        msg += _("Engagement Approach")
                        
                        msg += _("<li>Old: %s", dict_get_state.get(self.name)) + "</li>"
                        msg += _("<li>New: %s", dict_get_state.get(vals['name'])) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['company_id']):
                        msg += _("Business Unit")
                        msg += _("<li>Old: %s", self.company_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.company'].search([('id','=',vals['company_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['current_relationship_id']):
                        msg += _("Current Relationship")
                        msg += _("<li>Old: %s", self.current_relationship_id) + "</li>"
                        msg += _("<li>New: %s", vals['current_relationship_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['target_relationship_id']):
                        msg += _("Target Relationship")
                        msg += _("<li>Old: %s", self.target_relationship_id) + "</li>"
                        msg += _("<li>New: %s", vals['target_relationship_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_approved_id']):
                        msg += _("Approved by")
                        msg += _("<li>Old: %s", self.user_approved_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_approved_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['status_id']):
                        msg += _("Status")
                        msg += _("<li>Old: %s", self.status_id) + "</li>"
                        msg += _("<li>New: %s", vals['status_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_prepared_id']):
                        msg += _("Prepared by")
                        msg += _("<li>Old: %s", self.user_prepared_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_prepared_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['engagement_strategy_id']):
                        msg += _("Engagement Stragegy")
                        msg += _("<li>Old: %s", self.engagement_strategy_id) + "</li>"
                        msg += _("<li>New: %s", vals['engagement_strategy_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['target_date']):
                        msg += _("Target Date")
                        msg += _("<li>Old: %s", self.target_date) + "</li>"
                        msg += _("<li>New: %s", vals['target_date']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['responsible_id']):
                        msg += _("Responsible")
                        msg += _("<li>Old: %s", self.responsible_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['responsible_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_noted_id']):
                        msg += _("Noted By")
                        msg += _("<li>Old: %s", self.user_noted_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_noted_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['criticality_id']):
                        msg += _("Criticality")
                        msg += _("<li>Old: %s", self.criticality_id) + "</li>"
                        msg += _("<li>New: %s", vals['criticality_id']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['link_to_profile']):
                        msg += _("Link To Profile")
                        msg += _("<li>Old: %s", self.link_to_profile) + "</li>"
                        msg += _("<li>New: %s", vals['link_to_profile']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['state']):
                        
                        dict_get_state = dict(self._fields['state'].selection)
                        
                        msg += _("State")
                        msg += _("<li>Old: %s", dict_get_state.get(self.state)) + "</li>"
                        msg += _("<li>New: %s", dict_get_state.get(vals['state'])) + "</li> <br/>"
                except KeyError:
                    pass
            self.partner_id.message_post(body=msg)
        rec = super(StakeholderEngagementTab, self).write(vals)
        return rec


# Auto-populate Engagement Approach value
    @api.depends('interest_id', 'power_id')
    def _compute_engagement(self):
        for rec in self:
            interest = rec.interest_id
            power = rec.power_id
            if power == 'low' and interest == 'low':
                rec.name = 'monitor'
            elif power == 'low' and interest == 'medium':
                rec.name = 'monitor'
            elif power == 'low' and interest == 'high':
                rec.name = 'keep_informed'
            elif power == 'low' and interest == 'very_high':
                rec.name = 'keep_informed'
            elif power == 'medium' and interest == 'low':
                rec.name = 'monitor'
            elif power == 'medium' and interest == 'medium':
                rec.name = 'monitor'
            elif power == 'medium' and interest == 'high':
                rec.name = 'keep_informed'
            elif power == 'medium' and interest == 'very_high':
                rec.name = 'keep_informed'
            elif power == 'high' and interest == 'low':
                rec.name = 'keep_satisfied'
            elif power == 'high' and interest == 'medium':
                rec.name = 'keep_satisfied'
            elif power == 'high' and interest == 'high':
                rec.name = 'manage_closely'
            elif power == 'high' and interest == 'very_high':
                rec.name = 'manage_closely'
            elif power == 'very_high' and interest == 'low':
                rec.name = 'keep_satisfied'
            elif power == 'very_high' and interest == 'medium':
                rec.name = 'keep_satisfied'
            elif power == 'very_high' and interest == 'high':
                rec.name = 'manage_closely'
            elif power == 'very_high' and interest == 'very_high':
                rec.name = 'manage_closely'

# Auto-populate criticality value
    @api.depends('name')
    def _compute_criticality(self):
        for rec in self:
            if rec.name == 'manage_closely':
                rec.criticality_id = 'very_high'
            elif rec.name == 'keep_satisfied':
                rec.criticality_id = 'high'
            elif rec.name == 'keep_informed':
                rec.criticality_id = 'medium'
            elif rec.name == 'monitor':
                rec.criticality_id = 'low'
            else:
                rec.criticality_id = ''

    def action_approved_by(self):
        " Updated approved user and state when click approve"
        self.user_approved_id = self.env.user.id
        self.state = 'approved_by'
    def action_noted_by(self):
        " Updated noted user and state when click note "
        self.user_noted_id = self.env.user.id
        self.state = 'noted_by'

    def action_prepared(self):
        "Updated state to prepared when click return to prepared"
        #self.user_prepared_id = self.env.user.id
        self.state = 'prepared_by'
        
    def action_return_to_draft(self):
        "Updated state to prepared when click return to draft"
        self.state = 'draft'
        
        