# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api, tools


# Create a new model
class StakeholderGroup(models.Model):
    _name = "mega.stake.criticality"
    _description = "Stakeholder Criticality"

    name = fields.Char('Criticality')
    num_val = fields.Integer('Value')
