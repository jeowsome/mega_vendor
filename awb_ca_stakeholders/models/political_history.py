# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)
# Create a new model


class MegaStakeDetails(models.Model):
    _name = "mega.stake.details.political.history"
    _description = "MegaStakeDetails"

    partner_id = fields.Many2one('res.partner', track_visibility='onchange')
    name = fields.Char('Political Career', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Business Unit',
                                 track_visibility='onchange',
                                 default=lambda self: self.env.company)
    political_leanings_party = fields.Char('Political Leanings / Political Party',
                                           track_visibility='onchange')
    interest_advocacies = fields.Text('Interest / Advocacies',
                                      track_visibility='onchange')
    issues = fields.Text('Issues', track_visibility='onchange')
    known_businesses_affiliated = fields.Char('Known Businesses / Businessmen Affiliated to',
                                              track_visibility='onchange')
    # Added new fields
    user_prepared_id = fields.Many2one('res.users', string='Prepared By',
                                       default=lambda self: self.env.user,
                                       track_visibility='onchange', readonly=True)
    user_approved_id = fields.Many2one('res.users', string='Approved By',
                                       track_visibility='onchange', readonly=True)
    user_noted_id = fields.Many2one('res.users', string='Noted By',
                                    track_visibility='onchange', readonly=True, store=True)
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('prepared_by', 'Submit'),
                            ('noted_by', 'Approved by SBU Head'),
                            ('approved_by', 'Cleared By Holdco'),
                            ], default='draft', string="State",
                            track_visibility='onchange', readonly=True)
    is_bu_staff = fields.Boolean('BU Staff', compute='_compute_is_bu_staff')
    sector_id = fields.Many2one('mega.stake.sector', string="Sector", track_visibility='onchange')
    group_id = fields.Many2one('mega.stake.group', string="Group", track_visibility='onchange')
    institution_id = fields.Many2one('mega.stake.institution', string="Institution / Area", track_visibility='onchange')
    area_of_impact_id = fields.Many2one('mega.stake.area.impact', string="Area of impact", track_visibility='onchange')

    return_to_reprepared = fields.Html(sanitize=False,
                                compute='_compute_css', store=False)

# if groups is CA Manager then automatically hidden the return to prepared button
    @api.depends('state')
    def _compute_css(self):
        for record in self:
            if (self.env.user.has_group('awb_ca_stakeholders.stake_ca_manager')):
                record.return_to_reprepared = False
            else:
                record.return_to_reprepared = '<style>.return_to_prepare {display: none !important;}</style>'
       
# if stakeholder role is BU Staff company field is readonly
    @api.depends('create_date', 'partner_id')
    def _compute_is_bu_staff(self):
        _logger.info('****compute staff***')
        if (self.env.user.has_group('awb_ca_stakeholders.stake_ca_manager')):
            _logger.info('Manager')
            self.is_bu_staff = False
        else:
            _logger.info('Ca Staff')
            self.is_bu_staff = True

    def action_approved_by(self):
        " Updated approved user and state when click approve"
        self.user_approved_id = self.env.user.id
        self.state = 'approved_by'
    def action_noted_by(self):
        " Updated noted user and state when click note "
        self.user_noted_id = self.env.user.id
        self.state = 'noted_by'

    def action_prepared(self):
        "Updated state to prepared when click return to prepared"
        #self.user_prepared_id = self.env.user.id
        self.state = 'prepared_by'
        
    def action_return_to_draft(self):
        "Updated state to prepared when click return to draft"
        self.state = 'draft'

# Added Track history in Political History tab
    @api.model
    def create(self, vals):
        rec = super(MegaStakeDetails, self).create(vals)
        if rec.partner_id:
            msg = "<b>" + _("Profile Information") + "</b><ul>"
            for vals in rec:
                if(vals.name is not False):
                    msg += _("Political Career: %s", vals.name) + "<br/>"
                if(vals.company_id is not False):
                    msg += _("Business Unit: %s", vals.company_id.name) + "<br/>"
                if(vals.political_leanings_party is not False):
                    msg += _("Political Leanings / Political Party: %s", vals.political_leanings_party) + "<br/>"
                if(vals.interest_advocacies is not False):
                    msg += _("Interest / Advocacies: %s", vals.interest_advocacies) + "<br/>"
                if(vals.issues is not False):
                    msg += _("Issues: %s", vals.issues) + "<br/>"
                if(vals.known_businesses_affiliated is not False):
                    msg += _("Known Businesses / Businessmen Affiliated to: %s", vals.known_businesses_affiliated) + "<br/>"
                if(vals.user_prepared_id is not False):
                    msg += _("Prepared By: %s", vals.user_prepared_id.name) + "<br/>"
                if(self.env['res.users'].search([('id','=',vals.user_approved_id.id)]).id is not False):
                    msg += _("Approved By: %s", vals.user_approved_id.name) + "<br/>"
                if(self.env['res.users'].search([('id','=',vals.user_noted_id.id)]).id is not False):
                    msg += _("Noted By: %s", vals.user_noted_id.name) + "<br/>"
                if(vals.state is not False):
                    msg += _("Status: %s", vals.state) + "<br/>"
            rec.partner_id.message_post(body=msg)
            #print(self.env['res.users'].search([('id','=',vals.user_approved_id.id)]).id)
        return rec

    def write(self, vals):
        print(vals)
        #print(vals.name)
        #rec = super(MegaStakeDetails, self).write(vals)
        if self.partner_id:
            for rec in self:
                
                msg = "<b>" + _("Profile Information") + "</b><ul>"
                msg += _("Changes made in: %s <br/>", self.name)
                
                try:
                    if(vals['name']):
                        msg += _("Political Career")
                        msg += _("<li>Old: %s", self.name) + "</li>"
                        msg += _("<li>New: %s", vals['name']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['company_id']):
                        msg += _("Business Unit")
                        msg += _("<li>Old: %s", self.company_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.company'].search([('id','=',vals['company_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['political_leanings_party']):
                        msg += _("Political Leanings / Political Party")
                        msg += _("<li>Old: %s", self.political_leanings_party) + "</li>"
                        msg += _("<li>New: %s", vals['political_leanings_party']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['interest_advocacies']):
                        msg += _("Interest / Advocacies")
                        msg += _("<li>Old: %s", self.interest_advocacies) + "</li>"
                        msg += _("<li>New: %s", vals['interest_advocacies']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['issues']):
                        msg += _("Issues")
                        msg += _("<li>Old: %s", self.issues) + "</li>"
                        msg += _("<li>New: %s", vals['issues']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['known_businesses_affiliated']):
                        msg += _("Known Businesses / Businessmen Affiliated to")
                        msg += _("<li>Old: %s", self.known_businesses_affiliated) + "</li>"
                        msg += _("<li>New: %s", vals['known_businesses_affiliated']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['issues']):
                        msg += _("Issues")
                        msg += _("<li>Old: %s", self.issues) + "</li>"
                        msg += _("<li>New: %s", vals['issues']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_prepared_id']):
                        msg += _("Prepared by")
                        msg += _("<li>Old: %s", self.user_prepared_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_prepared_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_approved_id']):
                        msg += _("Approved by")
                        msg += _("<li>Old: %s", self.user_approved_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_approved_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_noted_id']  or vals['user_noted_id'] is not None):
                        msg += _("Noted By")
                        msg += _("<li>Old: %s", self.user_noted_id.name) + "</li>"
                        #if(self.env['res.users'].search([('id','=',vals['user_noted_id'].id)]).id is False):
                        if(self.env['res.users'].search([('id','=',vals['user_noted_id'])]).name is False):
                            msg += _("<li>New: None </li> <br/>")
                        else:
                            msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_noted_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['state']):
                        
                        dict_get_state = dict(self._fields['state'].selection)
                        #print(dict_get_state.get(vals['state']))
                        msg += _("State")
                        msg += _("<li>Old: %s", dict_get_state.get(self.state)) + "</li>"
                        msg += _("<li>New: %s", dict_get_state.get(vals['state'])) + "</li> <br/>"
                except KeyError:
                    pass
                
            self.partner_id.message_post(body=msg)
        rec = super(MegaStakeDetails, self).write(vals)
        return rec
