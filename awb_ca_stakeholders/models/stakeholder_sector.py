# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models


# Create a new model
class StakeholderSector(models.Model):
    _name = "stakeholder.sector"
    _description = "StakeholderSector"

    name = fields.Char('Sector')
