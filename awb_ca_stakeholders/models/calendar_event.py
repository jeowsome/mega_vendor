# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api
from odoo.addons.calendar.models.calendar_recurrence import RRULE_TYPE_SELECTION


# Inherit calendar event Table
class InheritCalendarEvent(models.Model):
    _inherit = "calendar.event"
#notification mail action

    @api.model
    def _default_partners(self):
        """ When active_model is res.partner, the current partners should be attendees """
        partners = self.env.user.partner_id
        active_id = self._context.get('active_id')
        default_active_id = self._context.get('default_active_id')
        if self._context.get('active_model') == 'res.partner' and active_id or default_active_id:
            if active_id not in partners.ids:
                partners |= self.env['res.partner'].browse(active_id) or self.env['res.partner'].browse(default_active_id)
        return partners

    meeting_attire = fields.Char('Meeting Attire', track_visibility='onchange')
    name = fields.Char('Meeting Objectives / Agenda', required=True, track_visibility='onchange')
    actual_engagement = fields.Selection([
                                           ('official_visit','Official Visit'),
                                           ('lunch_dinner_meeting','Lunch / Dinner meeting'),
                                           ('sponsorhip_of_project','Sponsorhip of project/ program'),
                                           ('invite_as_special_guest','Invite as special guest for event'),
                                           ('send_gift','Send gift (special occasion)') 
                                            ])
    next_steps = fields.Selection([
                                   ('official_visit','Official Visit'),
                                   ('lunch_dinner_meeting','Lunch / Dinner meeting'),
                                   ('sponsorhip_of_project','Sponsorhip of project/ program'),
                                   ('invite_as_special_guest','Invite as special guest for event'),
                                   ('send_gift','Send gift (special occasion)') 
                                    ])
    company_id = fields.Many2one('res.company', string="Business Unit", default=lambda self: self.env.company) # set default company name 
    is_bu_staff = fields.Boolean('BU Staff', compute='_compute_is_bu_staff')
    is_ca_manager = fields.Boolean('CA Manager', related='is_bu_staff', groups='awb_ca_stakeholders.stake_ca_manager')
    calendar_type = fields.Selection([
                                    ('meeting', 'Meeting'),
                                    ('notification', 'Notification')
                                    ], string="Calendar Type", default='meeting')

# if stakeholder role is BU Staff company field is readonly 
# Added Depends field in calendar table
    @api.depends('create_date','partner_id')
    def _compute_is_bu_staff(self):
        if (self.env.user.has_group('awb_ca_stakeholders.stake_ca_manager')):
            self.is_bu_staff = False
        else:
            self.is_bu_staff = True

    # overwrite this field to store in db
    rrule_type = fields.Selection(RRULE_TYPE_SELECTION, string='Recurrence',
                                  help="Let the event automatically repeat at that interval",
                                  compute='_compute_recurrence',
                                  store=True, readonly=False)
    partner_ids = fields.Many2many(
        'res.partner', 'calendar_event_res_partner_rel',
        string='Attendees', default=_default_partners)
