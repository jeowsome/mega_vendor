# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api, tools, _


# Create a new model
class StakeholderEngagementPlan(models.Model):
    _name = "mega.ca.engagement_plan"
    _description = "StakeholderEngagementPlan"
    
    partner_id = fields.Many2one('res.partner', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Business Unit',
                                 track_visibility='onchange',
                                 default=lambda self: self.env.company, readonly=True)
    user_prepared_id = fields.Many2one('res.users', string='Prepared By',
                                       default=lambda self: self.env.user,
                                       track_visibility='onchange', readonly=True)
    
    name = fields.Char('Engagement')
    remarks = fields.Char('Remarks')
    engagement_frequency = fields.Char('Engagement Frequency')
    target_date = fields.Date('Target Date')
    actual_engagement = fields.Char('Actual Engagement')
    actual_date = fields.Date('Actual Date')
    remarks_outcome = fields.Char('Remarks/Outcome')
    next_step = fields.Char('Next Step')
    
    @api.model
    def create(self, vals):
        rec = super(StakeholderEngagementPlan, self).create(vals)
        if rec.partner_id:
            msg = "<b>" + _("New Engagement Plan") + "</b><ul>"
            for vals in rec:
                if(vals.name is not False):
                    msg += _("<li>Engagement: %s", vals.name) + "</li>"
                if(vals.user_prepared_id):
                    msg += _("<li>Prepared By: %s", vals.user_prepared_id.name) + "</li>"
                if(vals.company_id is not False):
                    msg += _("<li>Business Unit: %s", vals.company_id.name) + "</li>"
                if(vals.remarks is not False):
                    msg += _("<li>Remarks: %s", vals.remarks) + "</li>"
                if(vals.engagement_frequency is not False):
                    msg += _("<li>Engagement Frequency: %s", vals.engagement_frequency) + "</li>"
                if(vals.target_date is not False):
                    msg += _("<li>Target Date: %s", vals.target_date) + "</li>"
                if(vals.actual_engagement is not False):
                    msg += _("<li>Actual Engagement: %s", vals.actual_engagement) + "</li>"
                if(vals.actual_date is not False):
                    msg += _("<li>Actual Date: %s", vals.actual_date) + "</li>"
                if(vals.remarks_outcome is not False):
                    msg += _("<li>Remarks/Outcome: %s", vals.remarks_outcome) + "</li>"
                if(vals.next_step is not False):
                    msg += _("<li>Next Step: %s", vals.next_step) + "</li>"
            rec.partner_id.message_post(body=msg)
        return rec

    def write(self, vals):
        if self.partner_id:
            for rec in self:
                msg = "<b>" + _("Engagement Plan") + "</b><ul>"
                msg += _("Changes made in: %s <br/>", self.name)
                try:
                    if(vals['name']):
                        msg += _("Engagement")
                        msg += _("<li>Old: %s", self.name) + "</li>"
                        msg += _("<li>New: %s", vals['name']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['user_prepared_id']):
                        msg += _("Prepared by")
                        msg += _("<li>Old: %s", self.user_prepared_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.users'].search([('id','=',vals['user_prepared_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['company_id']):
                        msg += _("Business Unit")
                        msg += _("<li>Old: %s", self.company_id.name) + "</li>"
                        msg += _("<li>New: %s", self.env['res.company'].search([('id','=',vals['company_id'])]).name) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['remarks']):
                        msg += _("Remarks")
                        msg += _("<li>Old: %s", self.remarks) + "</li>"
                        msg += _("<li>New: %s", vals['remarks']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['engagement_frequency']):
                        msg += _("Engagement Frequency")
                        msg += _("<li>Old: %s", self.engagement_frequency) + "</li>"
                        msg += _("<li>New: %s", vals['engagement_frequency']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['target_date']):
                        msg += _("Target Date")
                        msg += _("<li>Old: %s", self.target_date) + "</li>"
                        msg += _("<li>New: %s", vals['target_date']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['actual_engagement']):
                        msg += _("Actual Engagement")
                        msg += _("<li>Old: %s", self.actual_engagement) + "</li>"
                        msg += _("<li>New: %s", vals['actual_engagement']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['actual_date']):
                        msg += _("Actual Date")
                        msg += _("<li>Old: %s", self.actual_date) + "</li>"
                        msg += _("<li>New: %s", vals['actual_date']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['remarks_outcome']):
                        msg += _("Remarks/Outcome")
                        msg += _("<li>Old: %s", self.remarks_outcome) + "</li>"
                        msg += _("<li>New: %s", vals['remarks_outcome']) + "</li> <br/>"
                except KeyError:
                    pass
                
                try:
                    if(vals['next_step']):
                        msg += _("Next Step")
                        msg += _("<li>Old: %s", self.next_step) + "</li>"
                        msg += _("<li>New: %s", vals['next_step']) + "</li> <br/>"
                except KeyError:
                    pass
            rec = super(StakeholderEngagementPlan, self).write(vals)
            self.partner_id.message_post(body=msg)
            return rec

    