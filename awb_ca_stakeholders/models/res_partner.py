# -*- coding: utf-8 -*-

# import of python lib
from datetime import date, datetime

# import of odoo addons

from odoo import fields, api, models, _
from odoo.exceptions import RedirectWarning

# Inherit res partner Table

class InheritResPartner(models.Model):
    _inherit = "res.partner"
    _description = "ResPartner"

    political_information_ids = fields.One2many('mega.stake.details.political.history','partner_id',domain=lambda self:[('company_id', 'in', self.env.user.company_ids.ids)])
    stakeholder_engagement_ids = fields.One2many('mega.stake.details.mapping.engagement','partner_id',domain=lambda self:[('company_id', 'in', self.env.user.company_ids.ids)])
    stakeholder_engagement_plan_ids = fields.One2many('mega.ca.engagement_plan','partner_id')

    image = fields.Image(string='Image')

    name = fields.Char(index=True, track_visibility='onchange')
    title = fields.Many2one('res.partner.title', track_visibility='onchange')
    birthday = fields.Date(string='Birthday', track_visibility='onchange')
    computed_month = fields.Char(string='Whole Month Bday', compute='_compute_date')
    age = fields.Integer('Age', compute="_compute_age", default=0, track_visibility='onchange')
    religion_id = fields.Char('Religion', track_visibility='onchange')
    education = fields.Char('Education', track_visibility='onchange')
    personal_background = fields.Char('Personal Background', track_visibility='onchange')
    alias = fields.Char('Nickname', track_visibility='onchange')
    sector_id = fields.Many2one('mega.stake.sector', string="Sector", track_visibility='onchange')
    group_id = fields.Many2one('mega.stake.group', string="Group", track_visibility='onchange')
    institution_id = fields.Many2one('mega.stake.institution', string="Institution / Area", track_visibility='onchange')
    position_id = fields.Many2one('mega.stake.position', string="Position", track_visibility='onchange')
    area_of_impact_id = fields.Many2one('mega.stake.area.impact', string="Area of impact", track_visibility='onchange')
    meeting_ids = fields.Many2many('calendar.event', 'calendar_event_res_partner_rel', 'res_partner_id', 'calendar_event_id', string='Meetings', copy=False)

    state = fields.Selection([('01draft','Draft'),('02submit','Submit'),('03approved','Approved')], string='Status', default='01draft')
    isCaAdmin = fields.Boolean(string='IS CA Admin?', compute='_compute_check_ca_admin')

    sbu_department_ids = fields.Many2many('res.company', string='SBU Departments', compute='_compute_sbu_department_ids', store=True)
    primary_responsible = fields.Char(string='Primary Responsible', compute='_compute_primary_responsible', store=True)
    
    # criticality_id = fields.Selection([
    #                             ('low', 'Low'), ('medium', 'Medium'),
    #                             ('high', 'High'), ('very_high', 'Very High')
    #                             ], string="Criticality", track_visibility='onchange')

# default company name set
    company_id = fields.Many2one('res.company', string='Business Unit', index=True, default=lambda self: self.env.company)
# search filter fields
    search_group_id = fields.Many2one(related='group_id.group_search_id', store=True)
    search_sector_id = fields.Many2one(related='sector_id.sector_search_id', store=True)
    search_institution_id = fields.Many2one(related='institution_id.institution_search_id', store=True)
    search_position_id = fields.Many2one(related='position_id.position_search_id', store=True)
    search_area_of_impact_id = fields.Many2one(related='area_of_impact_id.area_of_impact_search_id', store=True)
    # contact_mode = fields.Selection([('whatsapp','WhatsApp'),('viber','Viber')], string = 'Contact Mode')
    # mobile = fields.Char(help="This mobile number is used to send the notification messages via WhatsApp or Viber.")

# Added Unique Constraints
    #------------------------------------------------------ _sql_constraints = [
      #--------------- ('partner_name', 'unique(name)', 'Name must be unique!'),
     #------------------------------------------------------------------------ ]

    @api.depends('stakeholder_engagement_ids.company_id')
    def _compute_sbu_department_ids(self):
        for r in self:
            list_sbu = []
            line = r.stakeholder_engagement_ids
            for l in line:
                list_sbu.append(l.company_id.id)

            r.sbu_department_ids = [(6, 0, list_sbu)]

    @api.depends('stakeholder_engagement_ids.primary')
    def _compute_primary_responsible(self):
        for r in self:
            list_pr = []
            line = r.stakeholder_engagement_ids
            for l in line:
                if l.primary:
                    list_pr.append(l.primary)

            val = ', '.join(list_pr)
            r.primary_responsible = val
            
            
    def update_primary_responsible(self):
        line = self.env['res.partner'].search([])
        for r in line:
            r._compute_primary_responsible()
            
    # def _compute_stake_criticality(self):
    #     initial_val = 0
    #     final_val = 0
    #     comparator = self.env['mega.stake.criticality'].search([])
    #     for com in comparator:
    #         pass
    #     for r in self:
    #         list_sbu = []
    #         line = r.stakeholder_engagement_ids
    #         for l in line:
    #             list_sbu.append(l.criticality_id.id)
    #
    #         r.sbu_department_ids = [(6, 0, list_sbu)]
 
 
    def update_sbu_department_ids(self):
        env = self.env['res.partner']
        data = env.search([])
        for d in data:
            list_sbu = []
            line = d.stakeholder_engagement_ids
            for l in line:
                list_sbu.append(l.company_id.id)
            d.sbu_department_ids = [(6,0,list_sbu)]

    @api.depends('birthday')
    def _compute_date(self):
        for rec in self:
            if rec.birthday:
                birth_date = rec.birthday
                today = birth_date.strftime("%B %d, %Y")
                rec.computed_month = today
            else:
                rec.computed_month = False

    def _compute_check_ca_admin(self):
        for r in self:
            if self.env.user.has_group('awb_ca_stakeholders.stake_ca_admin'):
                r.isCaAdmin = True
            else:
                r.isCaAdmin = False

    def action_submit(self):
        for r in self:
            r.state = '02submit'

    def action_approve(self):
        for r in self:
            r.state = '03approved'

    def action_draft(self):
        for r in self:
            r.state = '01draft'

# compute based on birthday date
    @api.depends('birthday')
    def _compute_age(self):
        for rec in self:
            if rec.birthday:
                birth_date = rec.birthday
                today = date.today()
                age = today.year - birth_date.year -((today.month, today.day) <(birth_date.month, birth_date.day))
                rec.age = age
            else:
                rec.age = 0

# default set company type is individual
    @api.onchange('company_type')
    def _onchange_company_type(self):
        self.company_type = 'person'

    @api.model
    def create(self, vals):
    # redirect to existing parter page
        existing_id = self.env['res.partner'].search([('name', '=', vals.get('name'))])
        action = {
                'name': _('Duplicate Name'),
                'view_mode': 'form',
                'res_model': 'res.partner',
                'type': 'ir.actions.act_window',
                'id': existing_id.id,
                'views': [[self.env.ref('awb_ca_stakeholders.view_partner_Stakeholder_form').id, 'form']],
                'view_type': 'form',
                'res_id': existing_id.id,
            }
        if existing_id:
            msg = _('Partner Name Already Exist.')
            raise RedirectWarning(msg, action, _('Go to the Existing Partner'))
        res = super(InheritResPartner, self).create(vals)
        msg = "<b>" + _("Stakeholder") + "</b><ul>"
        if(res.name is not False):
            msg += _("Name") + ": %s <br/>" % (res.name)
        if(res.alias is not False):
            msg += _("Alias") + ": %s <br/>" % (res.alias)
        if(res.birthday is not False):
            msg += _("Birthday") + ": %s <br/>" % (res.birthday)
        if(res.age is not False):
            msg += _("Age") + ": %s <br/>" % (res.age)
        if(res.title is not False):
            msg += _("Title") + ": %s <br/>" % (res.title.name)
        if(res.mobile is not False):
            msg += _("Mobile") + ": %s <br/>" % (res.mobile)
        if(res.email is not False):
            msg += _("Email") + ": %s <br/>" % (res.email)
        if(res.religion_id is not False):
            msg += _("Religion") + ": %s <br/>" % (res.religion_id)
        if(res.personal_background is not False):
            msg += _("Personal") + ": %s <br/>" % (res.personal_background)
        if(res.education is not False):
            msg += _("Education") + ": %s <br/>" % (res.education)
        if(res.sector_id is not False):
            msg += _("Sector") + ": %s <br/>" % (res.sector_id.name)
        if(res.group_id is not False):
            msg += _("Group") + ": %s <br/>" % (res.group_id.name)
        if(res.institution_id is not False):
            msg += _("Institution/Area") + ": %s <br/>" % (res.institution_id.name)
        if(res.area_of_impact_id is not False):
            msg += _("Area of Impact") + ": %s <br/>" % (res.area_of_impact_id.name)
        res.message_post(body=msg)
        return res

 # redirect to existing parter page for write action
    def write(self, vals):
        existing_id = self.env['res.partner'].search([('name', '=', vals.get('name')),('name', '!=', self.name)])
        action = {
                'name': _('Duplicate Name'),
                'view_mode': 'form',
                'res_model': 'res.partner',
                'type': 'ir.actions.act_window',
                'id': existing_id.id,
                'views': [[self.env.ref('awb_ca_stakeholders.view_partner_Stakeholder_form').id, 'form']],
                'view_type': 'form',
                'res_id': existing_id.id,
            }
        if existing_id:
            msg = _('Partner Name Already Exist.')
            raise RedirectWarning(msg, action, _('Go to the Existing Partner'))
        return super(InheritResPartner, self).write(vals)
