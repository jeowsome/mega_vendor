# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models


# Create a new model
class StakeholderPosition(models.Model):
    _name = "stakeholder.position"
    _description = "StakeholderPosition"

    name = fields.Char('Position')
