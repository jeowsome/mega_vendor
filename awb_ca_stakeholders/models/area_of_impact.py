# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api, tools


# Create a new model
class AreaOfImpact(models.Model):
    _name = "mega.stake.area.impact"
    _description = "AreaOfImpact"
    _order = 'name'

    name = fields.Char('Area Of Impact')
    area_of_impact_search_id = fields.Many2one('mega.stake.area.impact.search', compute='_compute_area_of_impact_search', string="Area Of Impact", store=True)

    @api.depends('name')
    def _compute_area_of_impact_search(self):
        for record in self:
            record.area_of_impact_search_id = (ord(record.name[0]) * 1000 + ord(record.name[1:2] or '\x00')) if record.name else False

# Added search filter for area of impact table
class StakeholderAreaOfImpactSearch(models.Model):
    _name = 'mega.stake.area.impact.search'
    _description = 'Area of Impact name first 2 letter'
    _auto = False

    name = fields.Char()
    parent_id = fields.Many2one('mega.stake.area.impact.search')

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute('''
            CREATE OR REPLACE VIEW %s AS (
            SELECT DISTINCT ASCII(name) * 1000 + ASCII(SUBSTRING(name,2,1)) AS id,
                   LEFT(name,2) AS name,
                   ASCII(name) AS parent_id
            FROM mega_stake_area_impact WHERE name IS NOT NULL
            UNION ALL
            SELECT DISTINCT ASCII(name) AS id,
                   LEFT(name,1) AS name,
                   NULL::int AS parent_id
            FROM mega_stake_area_impact WHERE name IS NOT NULL
            )''' % (self._table,)
        )
