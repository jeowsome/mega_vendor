# -*- coding: utf-8 -*-

# import of python lib
from datetime import date, datetime

# import of odoo addons
from odoo import fields, api, models, _
from odoo.exceptions import RedirectWarning

import base64
from dateutil.relativedelta import relativedelta
from builtins import int
from _datetime import timedelta
#from pip._vendor.typing_extensions import Self
from wsgiref.validate import check_status

class MegaStakeEmailTemplate(models.Model):
    _name = "mega.stake_email_temp"
    _description = "Email Template"

    line_item_ids = fields.One2many('mega.stake_email_temp_from', 'line_item_id', string='Line Item ID')

    bg_img = fields.Image(string='Background Image')
    name = fields.Text(string='Description')
    date_email_send = fields.Date(string='Date Email Send')
    date_email_send_name = fields.Char(string='Date Email Send Name')
    partner_id = fields.Many2one('res.partner', string='Name')
    email = fields.Char(string='Email')
    city = fields.Char(string='City')
    position_id = fields.Many2one('mega.stake.position', string='Position')
    institution_id = fields.Many2one('mega.stake.institution', string='Institution')
    title_id = fields.Many2one('res.partner.title', string='Title')
    primary_responsible = fields.Char(string='Primary Responsible')

    state = fields.Selection([('01draft','Draft'),('02sent','Sent'),('03failed','Failed')], string='Status', default='01draft')

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        for r in self:
            r.email = r.partner_id.email
            r.position_id = r.partner_id.position_id.id
            r.institution_id = r.partner_id.institution_id.id
            r.title_id = r.partner_id.title.id
            r.primary_responsible = r.partner_id.primary_responsible
            r.date_email_send = r.partner_id.birthday

    @api.onchange('date_email_send')
    def _onchange_date_email_send(self):
        for r in self:
            if r.date_email_send:
                get_date = r.date_email_send
                format_date = get_date.strftime("%B %d, %Y")
                r.date_email_send_name = format_date
                
    
    def action_update_birthday_greeting(self):
        stakeholders = self.env['res.partner'].search([])
        for r in stakeholders:
            stake_name = r.name
            stake_id = r.id
            card_name = stake_name + " birthday greeting"
            pos_id = r.position_id.id
            ins_id = r.institution_id.id
            title_id = r.title.id
            prim_res = r.primary_responsible
            partner_details = self._onchange_partner_id()
            
            year_to_bday = datetime.now()
            
            if r.birthday:
                year_now = datetime.now().year
                bday = r.birthday
                year_to_bday = bday + relativedelta(year=year_now)
                if(year_to_bday < (datetime.now().date() + relativedelta(days=-3))):
                    year_to_bday = year_to_bday + relativedelta(years=1)
            #else:
                #year_to_bday = datetime.strptime('2000/01/01','%Y/%m/%d')
            
            get_date = year_to_bday
            format_date = get_date.strftime("%B %d, %Y")
            date_email_send_name = format_date
            
            email = r.email
            
            
            val = {
                    'name':card_name,
                    'date_email_send':year_to_bday,
                    'partner_id': stake_id,
                    'email': email,
                    'title_id': title_id,
                    'position_id': pos_id,
                    'primary_responsible':prim_res,
                    'institution_id': ins_id,
                    'date_email_send_name': date_email_send_name,   
                }
            val2 = {
                    'date_email_send':year_to_bday,
                    'email': email,
                    'title_id': title_id,
                    'position_id': pos_id,
                    'primary_responsible':prim_res,
                    'institution_id': ins_id,
                    'date_email_send_name': date_email_send_name,   
                }
            update_stake = self.env['mega.stake_email_temp'].search([('partner_id','=',stake_id)])
            check_stake = self.env['mega.stake_email_temp'].search_count([('partner_id','=',stake_id)])
            if(check_stake > 0):
                for r in update_stake:
                    r.write(val2)
            else:
                if (email and r.birthday):
                    self.env['mega.stake_email_temp'].create(val)
            
    # def action_create_birthday_greeting(self):
    #     stakeholders = self.env['res.partner'].search([])
    #     for r in stakeholders:
    #         stake_name = r.name
    #         stake_id = r.id
    #         card_name = stake_name + " birthday greeting"
    #         pos_id = r.position_id.id
    #         ins_id = r.institution_id.id
    #         title_id = r.title.id
    #         prim_res = r.primary_responsible
    #
    #
    #         if r.birthday:
    #             year_now = datetime.now().year
    #             bday = r.birthday
    #             year_to_bday = bday + relativedelta(year=year_now)
    #             if(year_to_bday < datetime.now().date()):
    #                 year_to_bday = year_to_bday + relativedelta(years=1)
    #         else:
    #             year_to_bday = datetime.strptime('2000/01/01','%Y/%m/%d')
    #
    #         get_date = year_to_bday
    #         format_date = get_date.strftime("%B %d, %Y")
    #         date_email_send_name = format_date
    #
    #         email = r.email
    #
    #
    #         val = {
    #                 'name':card_name,
    #                 'date_email_send':year_to_bday,
    #                 'partner_id': stake_id,
    #                 'email': email,
    #                 'title_id': title_id,
    #                 'position_id': pos_id,
    #                 'primary_responsible':prim_res,
    #                 'institution_id': ins_id,
    #                 'date_email_send_name': date_email_send_name,   
    #             }
    #         check_stake = self.env['mega.stake_email_temp'].search_count([('partner_id','=',stake_id)])
    #         if(check_stake > 0):
    #             pass
    #         else:
    #             self.env['mega.stake_email_temp'].create(val)
        
    def action_send_email(self):
        email_sent_day = self.date_email_send
        domain = self.env['mega.stake_email_temp'].search([('date_email_send', '=', datetime.now().date())])
        
        template_id = self.env.ref('awb_ca_stakeholders.email_template_birthday_greeting').id
        for d in domain:
            automatic = False
            line = d.line_item_ids
            list_from = []
            for l in line:
                list_from.append(l.name)
            get_from_names = ", ".join(list_from)

            if d.primary_responsible:
                primary_reponsible = d.primary_responsible
            else:
                primary_reponsible = 'Edgar Saavedra'

            if d.title_id.name:
                title = f"{d.title_id.name}"
            else:
                title = ''

            if d.position_id.name:
                position = d.position_id.name
            else:
                position = ''

            if d.institution_id.name:
                institution = d.institution_id.name
            else:
                institution = ''

            greeting = {}
            list_data = []
            data = {}
            data['email_from'] = 'harveytestcase@gmail.com'
            data['email_to'] = d.email
            data['name'] = d.partner_id.name
            data['title_id'] = title
            data['position_id'] = position
            data['institution_id'] = institution
            data['primary_responsible'] = primary_reponsible
            data['date_email_send_name'] = d.date_email_send_name
            data['city'] = d.city
            data['bg_img'] = d.bg_img
            data['from'] = get_from_names
            list_data.append(data)
            greeting['data'] = list_data
            context = {}
            context.update(greeting)
            
            crit_dom = self.env['mega.stake.details.mapping.engagement'].search([('partner_id', '=', d.partner_id.id)])
            for cd in crit_dom:
                if ((cd.criticality_id == 'high') or (cd.criticality_id == 'very_high')):
                    automatic = True

            #print(context)
            #if(automatic):
                #send_email = self.env['mail.template'].browse(template_id).with_context(context).send_mail(self.id, email_values={'model': None, 'res_id': self.id},force_send=True)
            send_email = self.env['mail.template'].browse(template_id).with_context(context).send_mail(self.id, email_values={'model': None, 'res_id': self.id},force_send=True)
        #for d in domain:
            #pass
            #d.write({'date_email_send': d.date_email_send + relativedelta(years=1)})

class MegaStakeEmailTemplateFrom(models.Model):
    _name = "mega.stake_email_temp_from"
    _description = "Email Template From"

    line_item_id = fields.Many2one('mega.stake_email_temp', string='Line Item ID')

    name = fields.Char(string='Name')
