# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models


# Create a new model
class StakeholderGroup(models.Model):
    _name = "stakeholder.group"
    _description = "StakeholderGroup"

    name = fields.Char('Group')
