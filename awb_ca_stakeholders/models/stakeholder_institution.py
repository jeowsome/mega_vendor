# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models


# Create a new model
class StakeholderInstitution(models.Model):
    _name = "stakeholder.institution"
    _description = "StakeholderInstitution"

    name = fields.Char('Institution')
