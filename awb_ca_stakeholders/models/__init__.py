# -*- coding: utf-8 -*-

from.import email_template
from.import res_partner
from.import political_history
from.import mapping_engagement
from.import calendar_event
from.import mega_stake_sector
from.import mega_stake_group
from.import mega_stake_institution
from.import mega_stake_position
from.import area_of_impact
from.import frequency_of_engagement
from.import mega_ca_engagement_plan
# from.import criticality