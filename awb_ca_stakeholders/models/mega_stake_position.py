# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api, tools

# Create a new model


class StakeholderPosition(models.Model):
    _name = "mega.stake.position"
    _description = "StakeholderPosition"

    name = fields.Char('Position')
    position_search_id = fields.Many2one('mega.stake.position.search', compute='_compute_position_search', string="Position", store=True)

    @api.depends('name')
    def _compute_position_search(self):
        for record in self:
            record.position_search_id = (ord(record.name[0]) * 1000 + ord(record.name[1:2] or '\x00')) if record.name else False


# Added search filter for position table
class StakeholderPositionSearch(models.Model):
    _name = 'mega.stake.position.search'
    _description = 'Position name first 2 letter'
    _auto = False

    name = fields.Char()
    parent_id = fields.Many2one('mega.stake.position.search')

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute('''
            CREATE OR REPLACE VIEW %s AS (
            SELECT DISTINCT ASCII(name) * 1000 + ASCII(SUBSTRING(name,2,1)) AS id,
                   LEFT(name,2) AS name,
                   ASCII(name) AS parent_id
            FROM mega_stake_position WHERE name IS NOT NULL
            UNION ALL
            SELECT DISTINCT ASCII(name) AS id,
                   LEFT(name,1) AS name,
                   NULL::int AS parent_id
            FROM mega_stake_position WHERE name IS NOT NULL
            )''' % (self._table,)
        )
