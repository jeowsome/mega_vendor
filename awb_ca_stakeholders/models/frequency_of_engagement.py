# -*- coding: utf-8 -*-

# import of odoo addons
from odoo import fields, models, api, tools


# Create a new model
class StakeholderFrequencyOfEngagement(models.Model):
    _name = "mega.stake.frequency.of.engagement"
    _description = "StakeholderFrequencyOfEngagement"

    name = fields.Char('Frequency Of Engagement')
    frequency_search_id = fields.Many2one('mega.stake.frequency.of.engagement', string="Frequency Of Engagement")

