odoo.define('awb_ca_stakeholders.calendarModels', function (require) {
	"use strict";
	const calendarModel = require('web.CalendarModel');
	const core = require('web.core');
	const fieldUtils = require('web.field_utils');
	const session = require('web.session');
	var _t = core._t;
	
	calendarModel.include({
		 init: function () {
       this._super.apply(this, arguments);
   },
   //if groups is BU Staff then Everybody's calendars check box will be invisible
   _loadFilter: function (filter) {
       if (!filter.write_model) {
           return Promise.resolve();
       }
       var field = this.fields[filter.fieldName];
       var fieldName = this.fields
       return this._rpc({
               model: filter.write_model,
               method: 'search_read',
               domain: [["user_id", "=", session.uid]],
               fields: [filter.write_field],
           })
           .then(function (res) {
               var records = _.map(res, function (record) {
                   var _value = record[filter.write_field];
                   var value = _.isArray(_value) ? _value[0] : _value;
                   var f = _.find(filter.filters, function (f) {return f.value === value;});
                   var formater = fieldUtils.format[_.contains(['many2many', 'one2many'], field.type) ? 'many2one' : field.type];
                   return {
                       'id': record.id,
                       'value': value,
                       'label': formater(_value, field),
                       'active': !f || f.active,
                   };
                   
               });
               records.sort(function (f1,f2) {
                   return _.string.naturalCmp(f2.label, f1.label);
               });
             
               // add my profile
               if (field.relation === 'res.partner' || field.relation === 'res.users') {
                   var value = field.relation === 'res.partner' ? session.partner_id : session.uid;
                   var me = _.find(records, function (record) {
                       return record.value === value;
                   });
                   if (me) {
                       records.splice(records.indexOf(me), 1);
                   } else {
                       var f = _.find(filter.filters, function (f) {return f.value === value;});
                       me = {
                           'value': value,
                           'label': session.name + _t(" [Me]"),
                           'active': !f || f.active,
                       };
                   }
                   records.unshift(me);
               }
               if(fieldName.is_ca_manager){
	                // add all selection
	                records.push({
	                    'value': 'all',
	                    'label': field.relation === 'res.partner' || field.relation === 'res.users'? _t("Everybody's calendars") : _t("Everything"),
	                    'active': filter.all,
	                });
               }
               filter.filters = records;
           });
   },
	});

});