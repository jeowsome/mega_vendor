odoo.define('awb_ca_stakeholders.calendarEvent', function (require) {
"use strict";
const calendarEvent = require('web.CalendarModel');

//If you click calendar, default viewing is per month

calendarEvent.include({
    /**
     * @override
     */
    init: function () {
        this._super.apply(this, arguments);
    },
    
    setScale: function (scale) {
        if (!_.contains(this.scales, scale)) {
            scale = "month";
        }
        this.data.scale = scale;
        this.setDate(this.data.target_date);
    },
});
});