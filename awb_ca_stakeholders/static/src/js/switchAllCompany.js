odoo.define('awb_ca_stakeholders.switchAllCompany', function(require) {
"use strict";

var core = require('web.core');
var session = require('web.session');
var switchCompanyMenu = require('web.SwitchCompanyMenu');

var _t = core._t;


switchCompanyMenu.include({
	    events: _.extend({}, switchCompanyMenu.prototype.events, {
	    	'click div.toggle_all_company': '_onToggleAllCompanyClick',
	    }),

	    /**
	     * @override
	     */
	    willStart: function () {
	        var self = this;
	        var def = this._super.apply(this, arguments);
	        // Get user companies and selected companies
	        var allCompanyIds = this.user_companies
	        var allowedCompanyIds = this.allowed_company_ids
	        this.selectedAll = false;
	        // When user companies and allowed companies are equal then set selectedAll as true
	        if (allCompanyIds.length === allowedCompanyIds.length) {
	        	this.selectedAll = true;
	        }
	        return def;
	    },
	    //--------------------------------------------------------------------------
	    // Handlers
	    //--------------------------------------------------------------------------

	    /**
	     * @private
	     * @param {MouseEvent|KeyEvent} ev
	     */
	    _onToggleAllCompanyClick: function (ev) {
	        if (ev.type == 'keydown' && ev.which != $.ui.keyCode.ENTER && ev.which != $.ui.keyCode.SPACE) {
	            return;
	        }
	        ev.preventDefault();
	        ev.stopPropagation();
	        
	        var dropdownItem = $(ev.currentTarget).parent();
	        var allowedComapnyIds = this.allowed_company_ids;
	        var currentCompanyId = allowedComapnyIds[0];
	        var userCompanies = this.user_companies;
	        // Check all selected or not
	        if (dropdownItem.find('.fa-square-o').length) {
	        	var allowAllCompanies = this.allowed_company_ids;
	        	// update usercomapies in allowed companies id
	        	for (var i=0; i < userCompanies.length; i++) {
	        		allowAllCompanies.push(userCompanies[i][0]);
	        	}
	            if ($('.dropdown-item').attr('menu') === 'company') {
	            	dropdownItem.find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square');
	            	$('.dropdown-item').attr('aria-checked', 'true');
	            }
	            // Remove duplicate values
	            var uniqueChars = allowAllCompanies.filter((c, index) => {
	                return allowAllCompanies.indexOf(c) === index;
	            });
	            this.allowed_company_ids = uniqueChars;
	        } else {
	        	for (var i=0; i < userCompanies.length; i++) {
	        		allowedComapnyIds.splice(allowedComapnyIds.indexOf(userCompanies[i][0]), 1);
	        	}
	            if ($('.dropdown-item').attr('menu') === 'company') {
		            dropdownItem.find('.fa-check-square').addClass('fa-square-o').removeClass('fa-check-square');
		            $('.dropdown-item').attr('aria-checked', 'false');
	            }
	        }
	        session.setCompanies(currentCompanyId, this.allowed_company_ids);
	    },
});

});