# -*- coding: utf-8 -*-
{
    'name': "Data Cleansing 2.0",

    'summary': """
        Data Cleansing 2.0""",

    'description': """
        Data Cleansing 2.0
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Utility',
    'version': '1.0.0',
    'sequence': -1,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/masterdata.xml',
        'views/data.xml',
        'views/menu_items.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
