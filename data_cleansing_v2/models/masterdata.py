#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
import random
import logging
import time
import os
import sys
import xmlrpc.client
from xmlrpc.client import Transport as XMLTransport
import ssl

_logger = logging.getLogger(__name__)

class DataCleansingTwoPreset(models.Model):
    _name = "data_two.preset"
    _description = "Data Cleansing Preset Form"

    line_item_ids = fields.One2many('data_two.preset_lines', 'line_item_id', string='Preset Line Items')
    name = fields.Char(string='Preset Name')

    source_model_id = fields.Many2one('ir.model', string='Source Model')
    source_field_id = fields.Many2one('ir.model.fields', string='Source Field')
    field_id = fields.Many2one('ir.model.fields', string='Field')

class DataCleansingTwoPresetLines(models.Model):
    _name = "data_two.preset_lines"
    _description = "Data Cleansing Preset Form"

    line_item_id = fields.Many2one('data_two.preset', string='Preset Line Item')
    name = fields.Many2one('ir.model', string='Model')
