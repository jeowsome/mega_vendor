#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
import random
import logging
import time
import os
import sys
import xmlrpc.client
from xmlrpc.client import Transport as XMLTransport
import ssl

_logger = logging.getLogger(__name__)

class DataCleansingTwoData(models.Model):
    _name = "data_two.data"
    _description = "Data Cleansing Form"

    line_data_ids = fields.One2many('data_two.data_lines','line_data_id',string='Line Data IDS')

    name = fields.Char(string='Description')
    preset_id = fields.Many2one('data_two.preset', string='Preset')
    target_model = fields.Many2one('ir.model', string='Target Model')
    target_field = fields.Many2one('ir.model.fields', string='Target Field')
    domain = fields.Char(string='Domain')
    value = fields.Many2one('data_two.dump', string='New Value')

    isDetailsGenerated = fields.Boolean(string="Is Details Generated?", default=False)
    isGenerated = fields.Boolean(string="Is Data Generated?", default=False)

    source_model_id = fields.Many2one('ir.model', string='Source Model')
    source_field_id = fields.Many2one('ir.model.fields', string='Source Field')

    target_model_ids = fields.Many2many('ir.model', string='Target Models')

    @api.onchange('preset_id')
    def onchange_preset(self):
        preset = self.preset_id
        self.source_model_id = preset.source_model_id.id
        self.source_field_id = preset.source_field_id.id
        self.target_field = preset.field_id.id
        target_model = []
        for p in preset.line_item_ids:
            target_model.append(p.name.id)

        self.target_model_ids = [(6,0,target_model)]

    def action_generate_details(self):
        self.isDetailsGenerated = True
        source_model = 'data_two.data'
        sEnv = self.env[source_model]
        target_model_ids = self.target_model_ids

        for tmi in target_model_ids:
            source_model = self.source_model_id.model
            source_field = self.source_field_id.name

            target_model = tmi.model
            target_field = self.target_field.name
            env = self.env[target_model]
            domain = self.domain

            # data = sEnv.search_query(target_model, target_field, domain)
            data = sEnv.search_query(source_model, source_field, domain)
            # list = sEnv.store_list(data, target_field)
            list = sEnv.store_list(data, source_field)

            masterdata = set(list)

            model_new_value = 'data_two.dump'
            mEnv = self.env[model_new_value]
            mEnv.search([]).unlink()

            for m in masterdata:
                mEnv.create({'name': m})

    # Function Search
    def search_query(self, source_model, source_field, domain):
        # env = self.env[target_model]
        # data = env.search_read([(f"{target_field}.name",'=ilike',f"%{domain}%")])
        env = self.env[source_model]
        data = env.search_read([(f"{source_field}",'=ilike',f"%{domain}%")])
        return data

    # Function for storing data in a list
    def store_list(self, data, source_field):
        list = []
        for d in data:
            # list.append(d[target_field][1])
            list.append(d[source_field])
        return list

    # Function Target Search
    def search_target_query(self, target_model, target_field, domain):
        env = self.env[target_model]
        data = env.search_read([(f"{target_field}.name",'=ilike',f"%{domain}%")])
        # env = self.env[source_model]
        # data = env.search_read([(f"{source_field}",'=ilike',f"%{domain}%")])
        return data

    # Function for storing data in a list for target
    def store_target_list(self, data, target_field):
        list = []
        for d in data:
            list.append(d[target_field][1])
            # list.append(d[source_field])
        return list

    def action_generate_data(self):
        self.isGenerated = True
        source_model = 'data_two.data'
        sEnv = self.env[source_model]
        target_model_ids = self.target_model_ids

        model_data = 'data_two.data_lines'
        lEnv = self.env[model_data].search([('line_data_id','=',self.id)]).unlink()
        lEnvCreate = self.env[model_data]

        for tmi in target_model_ids:
            target_model = tmi.model

            target_field = self.target_field.name
            env = self.env[target_model]
            domain = self.domain

            data = sEnv.search_target_query(target_model, target_field, domain)
            list = sEnv.store_target_list(data, target_field)

            masterdata = set(list)

            for m in masterdata:
                lEnvCreate.create({'line_data_id': self.id,
                             'target_model': self.target_model.id,
                             'old_value': m,
                             'model': tmi.name,
                             'new_value': self.value.name})

    def execute(self):
        line = self.line_data_ids
        target_model_ids = self.target_model_ids
        target_field = self.target_field.name
        relation_field = self.target_field.relation

        for tmi in target_model_ids:
            target_model = tmi.model
            env = self.env[target_model]
            mEnv = self.env[relation_field]
            for l in line:
                if l.isNotIncluded == False:
                    record = env.search([(f"{target_field}.name",'=',l.old_value)])
                    for r in record:
                        new_data = mEnv.search([('name','=',l.new_value)]).id
                        r.write({target_field: new_data})

    def reset(self):
        self.isDetailsGenerated = False
        self.isGenerated = False
        for d in self.line_data_ids:
            self.env['data_two.data_lines'].search([('line_data_id','=',d.id)]).unlink()

class DataCleansingTwoDataLines(models.Model):
    _name = "data_two.data_lines"
    _description = "Data Cleansing Form Lines"

    line_data_id = fields.Many2one('data_two.data',string='Line Data ID')

    name = fields.Char(string='Name')
    target_model = fields.Many2one('ir.model', string='Target Model')
    old_value = fields.Char(string='Old Value')
    new_value = fields.Char(string='New Value')
    isNotIncluded = fields.Boolean(string='Is not Included?', default=False)
    model = fields.Char(string='Model')
    data_count = fields.Integer(string='Data Count', compute='_compute_data_count')

    def _compute_data_count(self):
        for r in self:
            target_model_ids = r.line_data_id.target_model_ids
            for tmi in target_model_ids:
                target_model = tmi.model
                target_field = r.line_data_id.target_field.name
                env = self.env[target_model]
                count = env.search_count([(f"{target_field}.name",'=',r.old_value)])
                r.data_count = count
