# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
import pandas as pd
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils
from odoo.exceptions import ValidationError, Warning

ROUNDING_FACTOR = 16
DATE_FILTER = [('ticket_date','!=',False),('ticket_date','!=',False)]

class MwideHelpdeskDashboard(models.Model):
    _inherit = 'mwide.helpdesk'

    @api.model
    def get_summary(self,start_date,end_date,sbu):
        query = """SELECT data.category as category,
        SUM(data.draft) as draft, SUM(data.request) as request, SUM(data.pending) as pending, SUM(data.assigned) as assigned,
        SUM(data.confirmation) as confirmation, SUM(data.unresolved) as unresolved, SUM(data.resolved) as resolved, SUM(data.total) as total
        FROM (
        SELECT mc.name as category, count(*) as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (mh.state = '01draft'
        OR mh.state = '03approved_head'
        OR mh.state = '04approved_it')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, count(*) as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '02request'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, count(*) as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '06on_hold'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, count(*) as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '05assigned'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, count(*) as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '07validation'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, count(*) as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '08unresolved'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, count(*) as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '09resolved'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        ) as data
        GROUP BY data.category
        ORDER BY data.category asc
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_draft = 0
        for i in range(0, len(get_record)):
            total_draft += get_record[i][1]

        total_request = 0
        for i in range(0, len(get_record)):
            total_request += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_assigned = 0
        for i in range(0, len(get_record)):
            total_assigned += get_record[i][4]

        total_confirmation = 0
        for i in range(0, len(get_record)):
            total_confirmation += get_record[i][5]

        total_unresolved = 0
        for i in range(0, len(get_record)):
            total_unresolved += get_record[i][6]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][7]

        total_summary = 0
        for i in range(0, len(get_record)):
            total_summary += get_record[i][8]

        result_draft = round(total_draft)
        result_request = round(total_request)
        result_pending = round(total_pending)
        result_assigned = round(total_assigned)
        result_confirmation = round(total_confirmation)
        result_unresolved = round(total_unresolved)
        result_resolved = round(total_resolved)
        result_summary = round(total_summary)

        result_summary_hold = result_draft + result_pending
        result_summary_resolve = result_confirmation + result_resolved
        result_summary_outstanding = result_request + result_assigned

        return {
            'summary': get_record,
            'result_draft': result_draft,
            'result_request': result_request,
            'result_pending': result_pending,
            'result_assigned': result_assigned,
            'result_confirmation': result_confirmation,
            'result_unresolved': result_unresolved,
            'result_resolved': result_resolved,
            'result_summary': result_summary,
            'result_summary_hold': result_summary_hold,
            'result_summary_resolve': result_summary_resolve,
            'result_summary_outstanding': result_summary_outstanding,
        }

    @api.model
    def get_severity(self,start_date,end_date,sbu):
        query = """SELECT data.category as category,
        SUM(data.severity1) as severity1, SUM(data.severity2) as severity2, SUM(data.severity3) as severity3, SUM(data.severity4) as severity4, SUM(data.false) as false, SUM(data.total) as total
        FROM
        (
        SELECT mc.name as category, count(*) as severity1, 0 as severity2, 0 as severity3, 0 as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 1'
        AND mh.sbu in %(sbu)s
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  count(*) as severity2, 0 as severity3, 0 as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 2'
        AND mh.sbu in %(sbu)s
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, count(*) as severity3, 0 as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 3'
        AND mh.sbu in %(sbu)s
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, 0 as severity3, count(*) as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 4'
        AND mh.sbu in %(sbu)s
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, 0 as severity3, 0 as severity4, count(*) as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND ms.name is NULL
        AND mh.sbu in %(sbu)s
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, 0 as severity3, 0 as severity4, 0 as false, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.sbu in %(sbu)s
        GROUP BY category
        ) as data
        GROUP BY data.category
        ORDER BY data.category asc
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_severity1 = 0
        for i in range(0, len(get_record)):
            total_severity1 += get_record[i][1]

        total_severity2 = 0
        for i in range(0, len(get_record)):
            total_severity2 += get_record[i][2]

        total_severity3 = 0
        for i in range(0, len(get_record)):
            total_severity3 += get_record[i][3]

        total_severity4 = 0
        for i in range(0, len(get_record)):
            total_severity4 += get_record[i][4]

        total_false = 0
        for i in range(0, len(get_record)):
            total_false += get_record[i][5]

        total_severity = 0
        for i in range(0, len(get_record)):
            total_severity += get_record[i][6]

        result_severity1 = round(total_severity1)
        result_severity2 = round(total_severity2)
        result_severity3 = round(total_severity3)
        result_severity4 = round(total_severity4)
        result_false = round(total_false)
        result_severity = round(total_severity)

        return {
            'severity': get_record,
            'result_severity1': result_severity1,
            'result_severity2': result_severity2,
            'result_severity3': result_severity3,
            'result_severity4': result_severity4,
            'result_false': result_false,
            'result_severity': result_severity,
        }

    @api.model
    def get_comparison_week(self, start_date, end_date, sbu):
        query = """SELECT data.name as category,
        SUM(data.created_case) as created_case, SUM(data.total_resolved_cases) as total_resolved_cases, SUM(data.pending_cases) as pending_cases,
        SUM(data.incoming_created_case) as incoming_created_case, SUM(data.total_outstanding_case) as total_outstanding_case
        FROM
        (
        SELECT mc.name  as name, count(mc.name) as created_case, 0 as total_resolved_cases, 0 as pending_cases, 0 as incoming_created_case, 0 as total_outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date - INTERVAL '7 days'
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date - INTERVAL '7 days'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as name, 0 as case_created_case, count(mc.name) as total_resolved_cases, 0 as pending_cases, 0 as incoming_created_case, 0 as total_outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date - INTERVAL '7 days'
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date - INTERVAL '7 days'
        AND mh.state = '09resolved'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name  as name, 0 as case_created_case, 0 as total_resolved_cases, count(mc.name) as pending_cases, 0 as incoming_created_case, 0 as total_outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date - INTERVAL '7 days'
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date - INTERVAL '7 days'
        AND mh.state != '09resolved'
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as name, 0 as case_created_case, 0 as total_resolved_cases, 0 as pending_cases, count(mc.name) as case_created, 0 as outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as name, 0 as case_created_case, 0 as total_resolved_cases, 0 as pending_cases, 0 as case_created, count(mc.name) as outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '01draft' OR state = '02request' OR state = '03approved_head' OR state = '04approved_it' OR state = '05assigned' OR state = '06on_hold' OR state = '08unresolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        ) as data
        GROUP BY data.name
        ORDER BY data.name
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_cases = 0
        for i in range(0, len(get_record)):
            total_cases += get_record[i][1]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_incoming = 0
        for i in range(0, len(get_record)):
            total_incoming += get_record[i][4]

        total_outstanding = 0
        for i in range(0, len(get_record)):
            total_outstanding += get_record[i][5]

        result_total_cases = round(total_cases)
        result_total_resolved = round(total_resolved)
        result_total_pending = round(total_pending)
        result_total_incoming = round(total_incoming)
        result_total_outstanding = round(total_outstanding)

        result_summary_incoming = abs(result_total_pending + result_total_incoming)

        return {
            'get_comparison': get_record,
            'result_total_cases': result_total_cases,
            'result_total_resolved': result_total_resolved,
            'result_total_pending': result_total_pending,
            'result_total_incoming': result_total_incoming,
            'result_total_outstanding': result_total_outstanding,
            'result_summary_incoming': result_summary_incoming,
        }

    @api.model
    def get_sbu(self, start_date, end_date, sbu):
        query = """SELECT data.group_sbu as group_sbu,
        SUM(data.draft) as draft, SUM(data.request) as request, SUM(data.pending) as pending, SUM(data.assigned) as assigned,
        SUM(data.confirmation) as confirmation, SUM(data.unresolved) as unresolved, SUM(data.resolved) as resolved, SUM(data.total) as total
        FROM (
        SELECT mh.sbu as group_sbu, count(*) as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (mh.state = '01draft'
        OR mh.state = '03approved_head'
        OR mh.state = '04approved_it')
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, count(*) as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '02request'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, count(*) as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '06on_hold'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, count(*) as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '05assigned'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, count(*) as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '07validation'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, count(*) as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '08unresolved'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, count(*) as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '09resolved'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        GROUP BY mh.sbu
        ) as data
        WHERE data.group_sbu in %(sbu)s
        GROUP BY data.group_sbu
        ORDER BY data.group_sbu asc
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_draft = 0
        for i in range(0, len(get_record)):
            total_draft += get_record[i][1]

        total_request = 0
        for i in range(0, len(get_record)):
            total_request += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_assigned = 0
        for i in range(0, len(get_record)):
            total_assigned += get_record[i][4]

        total_confirmation = 0
        for i in range(0, len(get_record)):
            total_confirmation += get_record[i][5]

        total_unresolved = 0
        for i in range(0, len(get_record)):
            total_unresolved += get_record[i][6]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][7]

        total_summary = 0
        for i in range(0, len(get_record)):
            total_summary += get_record[i][8]

        result_sbu_draft = round(total_draft)
        result_sbu_request = round(total_request)
        result_sbu_pending = round(total_pending)
        result_sbu_assigned = round(total_assigned)
        result_sbu_confirmation = round(total_confirmation)
        result_sbu_unresolved = round(total_unresolved)
        result_sbu_resolved = round(total_resolved)
        result_sbu_summary = round(total_summary)

        return {
            'get_sbu': get_record,
            'result_sbu_draft': result_sbu_draft,
            'result_sbu_request': result_sbu_request,
            'result_sbu_pending': result_sbu_pending,
            'result_sbu_assigned': result_sbu_assigned,
            'result_sbu_confirmation': result_sbu_confirmation,
            'result_sbu_unresolved': result_sbu_unresolved,
            'result_sbu_resolved': result_sbu_resolved,
            'result_sbu_summary': result_sbu_summary,
        }

    @api.model
    def get_sap(self, start_date, end_date, sbu):
        query = """SELECT data.sap_case as sap_case,
        SUM(data.draft) as draft, SUM(data.request) as request, SUM(data.pending) as pending, SUM(data.assigned) as assigned,
        SUM(data.confirmation) as confirmation, SUM(data.unresolved) as unresolved, SUM(data.resolved) as resolved, SUM(data.total) as total
        FROM (
        SELECT mst.name as sap_case, count(*) as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (mh.state = '01draft'
        OR mh.state = '03approved_head'
        OR mh.state = '04approved_it')
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, count(*) as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '02request'
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, count(*) as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '06on_hold'
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, count(*) as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '05assigned'
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, count(*) as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '07validation'
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation,  count(*) as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '08unresolved'
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, count(*) as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.state = '09resolved'
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND mh.sbu in %(sbu)s
        GROUP BY mst.name
        ) as data
        WHERE data.sap_case LIKE 'SAP%%'
        GROUP BY data.sap_case
        ORDER BY data.sap_case asc
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_draft = 0
        for i in range(0, len(get_record)):
            total_draft += get_record[i][1]

        total_request = 0
        for i in range(0, len(get_record)):
            total_request += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_assigned = 0
        for i in range(0, len(get_record)):
            total_assigned += get_record[i][4]

        total_confirmation = 0
        for i in range(0, len(get_record)):
            total_confirmation += get_record[i][5]

        total_unresolved = 0
        for i in range(0, len(get_record)):
            total_unresolved += get_record[i][6]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][7]

        total_summary = 0
        for i in range(0, len(get_record)):
            total_summary += get_record[i][8]

        result_sap_draft = round(total_draft)
        result_sap_request = round(total_request)
        result_sap_pending = round(total_pending)
        result_sap_assigned = round(total_assigned)
        result_sap_confirmation = round(total_confirmation)
        result_sap_unresolved = round(total_unresolved)
        result_sap_resolved = round(total_resolved)
        result_sap_summary = round(total_summary)

        return {
            'get_sap': get_record,
            'result_sap_draft': result_sap_draft,
            'result_sap_request': result_sap_request,
            'result_sap_pending': result_sap_pending,
            'result_sap_assigned': result_sap_assigned,
            'result_sap_confirmation': result_sap_confirmation,
            'result_sap_unresolved': result_sap_unresolved,
            'result_sap_resolved': result_sap_resolved,
            'result_sap_summary': result_sap_summary,
        }

    @api.model
    def get_response_resolution(self, start_date, end_date,sbu):
        query = """SELECT data.category as category, SUM(data.response_fail) as response_fail, SUM(data.response_pass) as response_pass, SUM(data.response_total) as response_total,
        SUM(data.resolution_fail) as resolution_fail, SUM(data.resolution_pass) as resolution_pass, SUM(data.resolution_total) as resolution_total
        FROM
        (
        SELECT mc.name as category, count(*) as response_fail, 0 as response_pass, 0 as response_total, 0 as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mh.response_time * 60 > ms.time_acknowledged
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '05assigned' OR state = '06on_hold' OR state = '07validation' OR state = '09resolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail, count(*) as response_pass, 0 as response_total, 0 as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE (mh.response_time * 60 <= ms.time_acknowledged OR ms.time_acknowledged IS NULL)
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '05assigned' OR state = '06on_hold' OR state = '07validation' OR state = '09resolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, count(*) as response_total, 0 as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE
        DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '05assigned' OR state = '06on_hold' OR state = '07validation' OR state = '09resolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, 0 as response_total, count(*) as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE ms.time_resolution < mh.resolution_time
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '07validation' OR state = '09resolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, 0 as response_total, 0 as resolution_fail, count(*) as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE (mh.resolution_time * 60 >= 0 AND mh.resolution_time * 60 <= ms.time_resolution * 60 OR ms.time_acknowledged IS NULL)
        AND DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '07validation' OR state = '09resolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, 0 as response_total, 0 as resolution_fail, 0 as resolution_pass, count(*) as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE
        DATE(mh.date_trans) >= %(start)s::timestamp::date
        AND DATE(mh.date_trans) <= %(end)s::timestamp::date
        AND (state = '07validation' OR state = '09resolved')
        AND mh.sbu in %(sbu)s
        GROUP BY mc.name
        ) AS data
        WHERE data.category IS NOT NULL
        GROUP BY data.category
        ORDER BY data.category asc
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_response_fail = 0
        for i in range(0, len(get_record)):
            total_response_fail += get_record[i][1]

        total_response_pass = 0
        for i in range(0, len(get_record)):
            total_response_pass += get_record[i][2]

        total_response = 0
        for i in range(0, len(get_record)):
            total_response += get_record[i][3]

        total_resolution_fail = 0
        for i in range(0, len(get_record)):
            total_resolution_fail += get_record[i][4]

        total_resolution_pass = 0
        for i in range(0, len(get_record)):
            total_resolution_pass += get_record[i][5]

        total_resolution = 0
        for i in range(0, len(get_record)):
            total_resolution += get_record[i][6]

        result_response_fail = round(total_response_fail)
        result_response_pass = round(total_response_pass)
        result_response = round(total_response)
        result_resolution_fail = round(total_resolution_fail)
        result_resolution_pass = round(total_resolution_pass)
        result_resolution = round(total_resolution)

        if total_response == 0:
            response_percentage = 0
        else:
            response_percentage = round((total_response_pass / total_response) * 100, 2)

        if total_resolution == 0:
            resolution_percentage = 0

        else:
            resolution_percentage = round((total_resolution_pass / total_resolution) * 100,2)

        return {
            'get_response_resolution': get_record,
            'result_response_fail': result_response_fail,
            'result_response_pass': result_response_pass,
            'result_response': result_response,
            'result_resolution_fail': result_resolution_fail,
            'result_resolution_pass': result_resolution_pass,
            'result_resolution': result_resolution,
            'response_percentage': response_percentage,
            'resolution_percentage': resolution_percentage,
        }

    @api.model
    def get_failed_resolution(self, start_date, end_date,sbu):
        query = """SELECT mh.id
                    FROM mwide_helpdesk mh
                    LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
                    WHERE  ms.time_resolution < mh.resolution_time
                    AND date_trans >= %(start)s
                    AND date_trans <= %(end)s
                    AND (state = '07validation' OR state = '09resolved')
                    AND mh.sbu in %(sbu)s
        """
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        self._cr.execute(query,args)
        get_record = self.env.cr.dictfetchall()

        failed_resolution = []
        for rec in get_record:
            failed_resolution.append(rec.get('id'))

        return failed_resolution

    @api.model
    def get_ratings(self, start_date, end_date,sbu):
        query = """SELECT mh.rating, count(*) as rating
                    FROM mwide_helpdesk mh
                    LEFT JOIN mwide_category mc ON mh.category_id = mc.id
                    WHERE DATE(mh.date_trans) >= %(start)s::timestamp::date
                    AND DATE(mh.date_trans) <= %(end)s::timestamp::date
                    AND mh.sbu in %(sbu)s
                    AND state = '09resolved'
                    GROUP BY mh.rating
                    ORDER BY mh.rating"""
        args = {
            'start': start_date,
            'end': end_date,
            'sbu': tuple(sbu)
        }
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_rating = 0

        for rec in get_record:
            total_rating += rec[1]

        return {
            'get_rating': get_record,
            'result_rating': total_rating,
        }
