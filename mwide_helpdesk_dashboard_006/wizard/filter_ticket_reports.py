# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import io
import xlrd
import babel
import logging
import tempfile
import binascii
from io import StringIO
from datetime import date, datetime, time, timedelta
from odoo import api, fields, models, tools, _
from odoo.exceptions import Warning, UserError, ValidationError
_logger = logging.getLogger(__name__)

class FilterItssTicketReport(models.TransientModel):
	_name = 'itss.filter_ticket_reports'
	_description = 'Filter Viewing of Dashboard Data by Week Selected'

	#year = fields.Char(string='Year', default=datetime.now().year)
	start_date = fields.Date(string="Start Date", reqired=True)
	end_date = fields.Date(string="End Date", reqired=True)
	sbu = fields.Selection([('all','ALL'),('holdco','HOLDCO'),('epc','EPC'),('cpi','CPI'),('pitx','PITX'),('ph','PH1WORLD'),('bu','BU'),('c2w','CEBU2WORLD')],string="SBU")

	def action_filter_itss_ticket_reports(self):
		return {
		    'type': 'ir.actions.client',
		    'name':'Helpdesk Ticket Report',
		    'tag':'custom_helpdesk_dashboard_tag',
			'params': {
		        'year' : self.start_date,
		        'month' : self.end_date
        	},
			'context': {'default_start_date': self.start_date, 'default_end_date': self.end_date, 'default_sbu': self.sbu}
		}
		# return super().action_filter_ap_dashboard()
