odoo.define('mwide_helpdesk_dashboard.HelpdeskDashboard', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var core = require('web.core');
var QWeb = core.qweb;
var rpc = require('web.rpc');
var ajax = require('web.ajax');

var HelpdeskDashboard = AbstractAction.extend({
    template: 'HelpdeskDashboard',

    events: {
            'click .button-ticket-summary':'ticket_summary',
            'click .button-severity-summary':'severity_summary',
            'click .button-sbu-summary':'sbu_summary',
            'click .button-sap-summary':'sap_summary',
            'click .button-previous-summary':'previous_summary',
            'click #btn-failed-response':'failed_response',
            'click #btn-failed-resolution':'action_failed_resolution',
            'click .button-rating-summary':'rating_summary',
   },

    init: function(parent, context) {
          this._super(parent, context);
          this.dashboards_templates = ['DashboardTable'];
          this.total_request = [];
          this.new_request = [];
          this.ext_request = [];
          this.cari_form = [];
          this.eari_form = [];
          this.pbi_form = [];
          this.sbi_form = [];
          this.transaction = [];

          //For State Table
          this.summary = [];
          this.draft = [];
          this.request = [];
          this.pending = [];
          this.assigned = [];
          this.confirmation = [];
          this.resolved = [];
          this.total_summary = [];

          // For Severity Table
          this.severity = [];
          this.severity1 = [];
          this.severity2 = [];
          this.severity3 = [];
          this.severity4 = [];
          this.severityFalse = [];
          this.severityTotal = [];

          // For Week Comparison Table
          this.comparison = [];
          this.prev_total_cases = [];
          this.prev_total_resolved = [];
          this.prev_total_prev = [];
          this.total_incoming = [];
          this.total_outstanding = [];

          //For SBU Table
          this.sbu = [];
          this.sbu_draft = [];
          this.sbu_request = [];
          this.sbu_pending = [];
          this.sbu_assigned = [];
          this.sbu_confirmation = [];
          this.sbu_unresolved = [];
          this.sbu_resolved = [];
          this.total_sbu_summary = [];

          //For Sap Cases Table
          this.sap = [];
          this.sap_draft = [];
          this.sap_request = [];
          this.sap_pending = [];
          this.sap_assigned = [];
          this.sap_confirmation = [];
          this.sap_unresolved = [];
          this.sap_resolved = [];
          this.total_sap_summary = [];

          //For Response Resolution Table
          this.response_resolution = [];
          this.response_fail = [];
          this.response_pass = [];
          this.response = [];
          this.resolution_fail = [];
          this.resolution_pass = [];
          this.resolution = [];
          this.response_percentage = [];
          this.resolution_percentage = [];

          // For Ratings Table
          this.rating = [];
          this.result_rating = [];

          // Summary Header
          this.total_summary_incoming =[];
          this.result_summary_hold = [];
          this.result_summary_resolve = [];
          this.result_summary_outstanding = [];

          //Get Report Name Label
          let sbu_checker = this.searchModelConfig.context.default_sbu;
          let sbu = [];
          if (sbu_checker == 'all'){

          }
          else{
              let group = [this.searchModelConfig.context.default_sbu]
              for(let i=0;i<1;i++){
                sbu[i] = group[i];
              }
          }
          this.ticket_label = [sbu]

          // Get Date Labels
          const monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

          let startDate = this.searchModelConfig.context.default_start_date;
          let endDate = this.searchModelConfig.context.default_end_date;
          let startDateParse = new Date(Date.parse(startDate));
          // startDateParse.getFullYear();
          // Current Week
          let startDateFormat = monthList[startDateParse.getMonth()] + ' ' + startDateParse.getDate();
          this.startDateLabel = [startDateFormat];

          let endDateParse = new Date(Date.parse(endDate));
          let endDateFormat = monthList[endDateParse.getMonth()] + ' ' + endDateParse.getDate();
          this.endDateLabel = [endDateFormat];

          // Previous Week
          let previousStartDateParse = new Date(Date.parse(startDate));
          let previousStartDate = previousStartDateParse.setDate(previousStartDateParse.getDate() - 7);
          let getPreviousStartDate = new Date(previousStartDate);
          let previousStartDateFormat = monthList[getPreviousStartDate.getMonth()] + ' ' + getPreviousStartDate.getDate();
          this.previousStartDateLabel = [previousStartDateFormat]

          let previousEndDateParse = new Date(Date.parse(endDate));
          let previousEndDate = previousEndDateParse.setDate(previousEndDateParse.getDate() - 7);
          let getPreviousEndDate = new Date(previousEndDate);
          let previousEndDateFormat = monthList[getPreviousEndDate.getMonth()] + ' ' + getPreviousEndDate.getDate();
          this.previousEndDateLabel = [previousEndDateFormat]

      },


      start: function() {
              var self = this;
              this.set("title", 'HelpdeskDashboard');
              return this._super().then(function() {
                  self.render_dashboards();
                  // self.render_graphs();
              });
      },


      render_dashboards: function() {
          var self = this;
          _.each(this.dashboards_templates, function(template) {
                self.$('.o_helpdesk_dashboard').append(QWeb.render(template, {widget: self}));
        });
      },

      willStart: function(){
      var self = this;
      return $.when(ajax.loadLibs(this), this._super()).then(function() {
        return self.fetch_data();
        });
      },


      fetch_data: function() {
      var self = this;
      let startDate = self.searchModelConfig.context.default_start_date;
      let endDate = self.searchModelConfig.context.default_end_date;
      let sbu_checker = self.searchModelConfig.context.default_sbu;
      let sbu = [];
      if (sbu_checker == 'all'){
          let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
          for(let i=0;i<7;i++){
            sbu[i] = groups[i];
          }
      }
      else{
          let group = [self.searchModelConfig.context.default_sbu]
          for(let i=0;i<1;i++){
            sbu[i] = group[i];
          }
      }
      var def17 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_severity',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.severity = result['severity']
              self.severity1 = result['result_severity1']
              self.severity2 = result['result_severity2']
              self.severity3 = result['result_severity3']
              self.severity4 = result['result_severity4']
              self.severityFalse = result['result_false']
              self.severityTotal = result['result_severity']
      });
      var def18 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_comparison_week',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.comparison = result['get_comparison']
              self.prev_total_cases = result['result_total_cases']
              self.prev_total_resolved = result['result_total_resolved']
              self.prev_total_pending = result['result_total_pending']
              self.total_incoming = result['result_total_incoming']
              self.total_outstanding = result['result_total_outstanding']
              self.result_summary_incoming = result['result_summary_incoming']
      });
      var def19 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_summary',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.summary = result['summary']
              self.draft = result['result_draft']
              self.request = result['result_request']
              self.pending = result['result_pending']
              self.assigned = result['result_assigned']
              self.confirmation = result['result_confirmation']
              self.unresolved = result['result_unresolved']
              self.resolved = result['result_resolved']
              self.total_summary =  result['result_summary']
              self.result_summary_hold =  result['result_summary_hold']
              self.result_summary_resolve =  result['result_summary_resolve']
              self.result_summary_outstanding =  result['result_summary_outstanding']
      });
      var def20 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_sbu',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.sbu = result['get_sbu']
              self.sbu_draft = result['result_sbu_draft']
              self.sbu_request = result['result_sbu_request']
              self.sbu_pending = result['result_sbu_pending']
              self.sbu_assigned = result['result_sbu_assigned']
              self.sbu_confirmation = result['result_sbu_confirmation']
              self.sbu_unresolved = result['result_sbu_unresolved']
              self.sbu_resolved = result['result_sbu_resolved']
              self.total_sbu_summary = result['result_sbu_summary']
      });
      var def21 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_sap',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.sap = result['get_sap']
              self.sap_draft = result['result_sap_draft']
              self.sap_request = result['result_sap_request']
              self.sap_pending = result['result_sap_pending']
              self.sap_assigned = result['result_sap_assigned']
              self.sap_confirmation = result['result_sap_confirmation']
              self.sap_unresolved = result['result_sap_unresolved']
              self.sap_resolved = result['result_sap_resolved']
              self.total_sap_summary = result['result_sap_summary']
      });
      var def22 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_response_resolution',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.response_resolution = result['get_response_resolution']
              self.response_fail = result['result_response_fail']
              self.response_pass = result['result_response_pass']
              self.response = result['result_response']
              self.resolution_fail = result['result_resolution_fail']
              self.resolution_pass = result['result_resolution_pass']
              self.resolution = result['result_resolution']
              self.response_percentage = result['response_percentage']
              self.resolution_percentage = result['resolution_percentage']
      });
      var def23 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_failed_resolution',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.failed_resolution = result
      });
      var def24 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_ratings',
              args: [startDate,endDate,sbu]
      }).then(function(result) {
              self.rating = result['get_rating']
              self.result_rating = result['result_rating']
      });
      return $.when(def17,def18,def19,def20,def21,def22,def23,def24);
      },

      // Event Actions
      ticket_summary: function(ev){
        var self = this;
        ev.stopPropagation();
        ev.preventDefault();
        let select_category = ev.currentTarget.childNodes[1].innerHTML;
        let startDate = self.searchModelConfig.context.default_start_date;
        let endDate = self.searchModelConfig.context.default_end_date;
        let sbu_checker = self.searchModelConfig.context.default_sbu;
        let sbu = [];
        if (sbu_checker == 'all'){
            let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
            for(let i=0;i<7;i++){
              sbu[i] = groups[i];
            }
        }
        else{
            let group = [self.searchModelConfig.context.default_sbu]
            for(let i=0;i<1;i++){
              sbu[i] = group[i];
            }
        }

        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };

        this.do_action({
            name: (select_category),
            type: 'ir.actions.act_window',
            res_model: 'mwide.helpdesk',
            view_mode: 'tree,form',
            views: [[false, 'list'],[false, 'form']],
            domain: [['category_id.name','=', select_category],['date_trans','>=', startDate],['date_trans','<=', endDate],['sbu','in',sbu]],
            context:{'group_by':'state'},
            target: 'current'
          });
        },

        severity_summary: function(ev){
          var self = this;
          ev.stopPropagation();
          ev.preventDefault();
          let select_category = ev.currentTarget.childNodes[1].innerHTML;
          let startDate = self.searchModelConfig.context.default_start_date;
          let endDate = self.searchModelConfig.context.default_end_date;
          let sbu_checker = self.searchModelConfig.context.default_sbu;
          let sbu = [];
          if (sbu_checker == 'all'){
              let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
              for(let i=0;i<7;i++){
                sbu[i] = groups[i];
              }
          }
          else{
              let group = [self.searchModelConfig.context.default_sbu]
              for(let i=0;i<1;i++){
                sbu[i] = group[i];
              }
          }

          var options = {
              on_reverse_breadcrumb: this.on_reverse_breadcrumb,
          };

          this.do_action({
              name: (select_category),
              type: 'ir.actions.act_window',
              res_model: 'mwide.helpdesk',
              view_mode: 'tree,form',
              views: [[false, 'list'],[false, 'form']],
              domain: [['category_id.name','=', select_category],['date_trans','>=', startDate],['date_trans','<=', endDate],['sbu','in',sbu]],
              context:{'group_by':'severity_id'},
              target: 'current'
          });
        },

        sbu_summary: function(ev){
          var self = this;
          ev.stopPropagation();
          ev.preventDefault();
          let select_group = ev.currentTarget.childNodes[1].innerHTML;
          let startDate = self.searchModelConfig.context.default_start_date;
          let endDate = self.searchModelConfig.context.default_end_date;

          var options = {
              on_reverse_breadcrumb: this.on_reverse_breadcrumb,
          };

          this.do_action({
              name: (select_group),
              type: 'ir.actions.act_window',
              res_model: 'mwide.helpdesk',
              view_mode: 'tree,form',
              views: [[false, 'list'],[false, 'form']],
              domain: [['sbu','=', select_group],['date_trans','>=', startDate],['date_trans','<=', endDate]],
              context:{'group_by':'state'},
              target: 'current'
            });
          },

          sap_summary: function(ev){
            var self = this;
            ev.stopPropagation();
            ev.preventDefault();
            let select_category = ev.currentTarget.childNodes[1].innerHTML;
            let startDate = self.searchModelConfig.context.default_start_date;
            let endDate = self.searchModelConfig.context.default_end_date;
            let sbu_checker = self.searchModelConfig.context.default_sbu;
            let sbu = [];
            if (sbu_checker == 'all'){
                let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
                for(let i=0;i<7;i++){
                  sbu[i] = groups[i];
                }
            }
            else{
                let group = [self.searchModelConfig.context.default_sbu]
                for(let i=0;i<1;i++){
                  sbu[i] = group[i];
                }
            }

            var options = {
                on_reverse_breadcrumb: this.on_reverse_breadcrumb,
            };

            this.do_action({
                name: (select_category),
                type: 'ir.actions.act_window',
                res_model: 'mwide.helpdesk',
                view_mode: 'tree,form',
                views: [[false, 'list'],[false, 'form']],
                domain: [['service_type_id.name','=', select_category],['date_trans','>=', startDate],['date_trans','<=', endDate],['sbu','in',sbu]],
                context:{'group_by':'state'},
                target: 'current'
              });
            },

            previous_summary: function(ev){
              var self = this;
              ev.stopPropagation();
              ev.preventDefault();
              let select_category = ev.currentTarget.childNodes[1].innerHTML;
              let startDate = self.searchModelConfig.context.default_start_date;
              let endDate = self.searchModelConfig.context.default_end_date;
              let sd = new Date(startDate);
              sd.setDate(sd.getDate() - 7)
              let formatStartDate = `${sd.getFullYear()}-${sd.getMonth() + 1}-${sd.getDate()}`

              let ed = new Date(endDate);
              ed.setDate(ed.getDate() - 7)
              let formatEndDate = `${ed.getFullYear()}-${ed.getMonth() + 1}-${ed.getDate()}`

              let sbu_checker = self.searchModelConfig.context.default_sbu;
              let sbu = [];
              if (sbu_checker == 'all'){
                  let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
                  for(let i=0;i<7;i++){
                    sbu[i] = groups[i];
                  }
              }
              else{
                  let group = [self.searchModelConfig.context.default_sbu]
                  for(let i=0;i<1;i++){
                    sbu[i] = group[i];
                  }
              }

              var options = {
                  on_reverse_breadcrumb: this.on_reverse_breadcrumb,
              };

              this.do_action({
                  name: (select_category),
                  type: 'ir.actions.act_window',
                  res_model: 'mwide.helpdesk',
                  view_mode: 'tree,form',
                  views: [[false, 'list'],[false, 'form']],
                  domain: [['category_id.name','=', select_category],['date_trans','>=', formatStartDate],['date_trans','<=', formatEndDate],['sbu','in',sbu]],
                  context:{'group_by':'state'},
                  target: 'current'
                });
              },

              failed_response: function(ev){
                var self = this;
                ev.stopPropagation();
                ev.preventDefault();
                let startDate = self.searchModelConfig.context.default_start_date;
                let endDate = self.searchModelConfig.context.default_end_date;
                let sbu_checker = self.searchModelConfig.context.default_sbu;
                let sbu = [];
                if (sbu_checker == 'all'){
                    let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
                    for(let i=0;i<7;i++){
                      sbu[i] = groups[i];
                    }
                }
                else{
                    let group = [self.searchModelConfig.context.default_sbu]
                    for(let i=0;i<1;i++){
                      sbu[i] = group[i];
                    }
                }

                var options = {
                    on_reverse_breadcrumb: this.on_reverse_breadcrumb,
                };

                this.do_action({
                    name: "(Failed Response)",
                    type: 'ir.actions.act_window',
                    res_model: 'mwide.helpdesk',
                    view_mode: 'tree,form',
                    views: [[false, 'list'],[false, 'form']],
                    domain: [['date_trans','>=', startDate],['date_trans','<=', endDate],['sbu','in',sbu],['response_time','>', 0.25],['state','in',['05assigned','06on_hold','07validation','09resolved']]],
                    context:{'group_by':'category_id'},
                    target: 'current'
                  });
                },

                action_failed_resolution: function(ev){
                  var self = this;
                  ev.stopPropagation();
                  ev.preventDefault();
                  let startDate = self.searchModelConfig.context.default_start_date;
                  let endDate = self.searchModelConfig.context.default_end_date;
                  let sbu_checker = self.searchModelConfig.context.default_sbu;
                  let sbu = [];
                  if (sbu_checker == 'all'){
                      let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
                      for(let i=0;i<7;i++){
                        sbu[i] = groups[i];
                      }
                  }
                  else{
                      let group = [self.searchModelConfig.context.default_sbu]
                      for(let i=0;i<1;i++){
                        sbu[i] = group[i];
                      }
                  }

                  var options = {
                      on_reverse_breadcrumb: this.on_reverse_breadcrumb,
                  };

                  this.do_action({
                      name: "(Failed Resolution)",
                      type: 'ir.actions.act_window',
                      res_model: 'mwide.helpdesk',
                      view_mode: 'tree,form',
                      views: [[false, 'list'],[false, 'form']],
                      domain:[['id','in',self.failed_resolution]],
                      context:{'group_by':'category_id'},
                      target: 'current'
                    });
                  },

                  rating_summary: function(ev){
                    var self = this;
                    ev.stopPropagation();
                    ev.preventDefault();
                    let select_rating = ev.currentTarget.childNodes[1].innerHTML;
                    let startDate = self.searchModelConfig.context.default_start_date;
                    let endDate = self.searchModelConfig.context.default_end_date;
                    let sd = new Date(startDate);
                    sd.setDate(sd.getDate())
                    let formatStartDate = `${sd.getFullYear()}-${sd.getMonth() + 1}-${sd.getDate()}`

                    let ed = new Date(endDate);
                    ed.setDate(ed.getDate())
                    let formatEndDate = `${ed.getFullYear()}-${ed.getMonth() + 1}-${ed.getDate()}`

                    let sbu_checker = self.searchModelConfig.context.default_sbu;
                    let sbu = [];
                    if (sbu_checker == 'all'){
                        let groups = ['holdco','epc','cpi','pitx','ph','bu','c2w']
                        for(let i=0;i<7;i++){
                          sbu[i] = groups[i];
                        }
                    }
                    else{
                        let group = [self.searchModelConfig.context.default_sbu]
                        for(let i=0;i<1;i++){
                          sbu[i] = group[i];
                        }
                    }

                    var options = {
                        on_reverse_breadcrumb: this.on_reverse_breadcrumb,
                    };

                    this.do_action({
                        name: select_rating + ' Rating',
                        type: 'ir.actions.act_window',
                        res_model: 'mwide.helpdesk',
                        view_mode: 'tree,form',
                        views: [[false, 'list'],[false, 'form']],
                        domain: [['date_trans','>=', formatStartDate],['date_trans','<=', formatEndDate],['rating','=', select_rating],['sbu','in',sbu],['state','=','09resolved']],
                        context:{'group_by':'category_id'},
                        target: 'current'
                      });
                    },

});

core.action_registry.add('custom_helpdesk_dashboard_tag', HelpdeskDashboard);

return HelpdeskDashboard;

});
