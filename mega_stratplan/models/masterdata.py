#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time

_logger = logging.getLogger(__name__)


class MegaStratCategory(models.Model):
    _name = "mwide.strat.category"

    name = fields.Char(string='Category', required=True)


class MegaObjectives(models.Model):
    _name = "mwide.strat.objective"

    name = fields.Char(string='Objectives', required=True)
 

class MegaStrategy(models.Model):
    _name = "mwide.strat.strategy"

    name = fields.Char(string='Strategies', required=True)