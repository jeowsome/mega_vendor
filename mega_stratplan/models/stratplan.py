#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
from random import randint
import logging
import time

_logger = logging.getLogger(__name__)



class MegaStratPlan(models.Model):
    _name = "mwide.stratplan"
    # _parent_name = "parent_id"


    category_id = fields.Many2one('mwide.strat.category', string='Category', required=True,
        help="Check Configuration --> Category Masterdata.")

    parent_id = fields.Many2one('mwide.stratplan', "Parent Strategy", ondelete="cascade", index=True)
    child_ids = fields.One2many('mwide.stratplan', 'parent_id', string='Line No.')

    name = fields.Char(string='Plans/Activities', required=True, tracking=True)
    objective_id = fields.Many2one('mwide.strat.objective', "Objective", ondelete="cascade", index=True)
    strategy_id = fields.Many2one('mwide.strat.strategy', "Strategy", ondelete="cascade", index=True)

    description = fields.Char(string='Description', tracking=True)
    # department_ids = fields.Many2many('hr.department', string='Department')
    department_id = fields.Many2one('hr.department', string='Department')
    deliverables = fields.Char(string='Deliverables', tracking=True)
    uom_target = fields.Char(string='UOM / Targets', tracking=True)
    kpi = fields.Integer(string='KPI (Measurable)', required=True, default=0,
        help='Field type is Integer, must be measurable.') 
    poc = fields.Float(string='POC (%)', default=0.00,
        help='Field type is Float/Double, must be measurable.')

    date_start = fields.Date(string='Project Date Start', required=True, index=True)
    date_end = fields.Date(string='Project Date End', required=True, index=True)

    sequence = fields.Integer(string='Sequence', required=True, index=True, default=100)
    
# start ****NEW ADDED FIELD FROM GANTT CHART JS-VIEWS
    # objective_id = fields.Many2one('mwide.stratplan', "Objectives", ondelete="cascade", index=True)
    color = fields.Integer('StratPlan Color', default=5)
    progress = fields.Float('Progress', default=75.00)
    task_type = fields.Selection([
        ('task', 'Task'),
        ('milestone', 'Milestone')
        ], string="Task Type", required=True, default='task')
    date_deadline = fields.Date(string='Deadline', index=True, tracking=True)
    task_link_ids = fields.One2many('task.link', 'task_id', string="Task Links")
    task_priority = fields.Selection([
        ('normal', 'Normal'),
        ('low', 'Low'),        
        ('high', 'High')
    ], string='Priority', required=True, default='normal')

# end ****NEW ADDED FIELD FROM GANTT CHART JS-VIEWS

    state = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In-Progress'),
        ('on_hold', 'On-Hold'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled')
        ], string='Project Status', readonly=True, default="draft")

    _sql_constraints = [
        ('parent_id_name_uniq', 'unique(name)', 'Strat-Plan Name already exists!'),
    ]

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('Parent already recursive!'))


    # @api.onchange('date_end')
    # def _onchange_date_end(self):
    #     self.date_deadline = self.date_end


    def action_open_folder(self):
        self.ensure_one()
        view = {
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'mwide.stratplan',
            'name': self.name,
            'search_view_id': self.env.ref('mega_stratplan.search_stratplan').id,
            'context': {'search_default_parent_id':self.id,'default_parent_id':self.id,'search_default_filter_published':1}
        }
        return view


    @api.model
    def search_read_links(self, domain=None):
        datas = []
        tasks = self.env['mwide.stratplan'].search(domain)
        for task in tasks:
            if task.task_link_ids:                
                for link in task.task_link_ids:                    
                    link_vals = {
                        'id' : link.id,
                        'source' : task.id,
                        'target': link.target_task_id.id, 
                        'type': link.link_type,
                    }
                    datas.append(link_vals)
        return datas


 
    def set_progress(self):
        for p in self:
            p.state = 'in_progress'
            
            # res = []
            # res.append(record.name or '')
            # record = record.parent_id
            # _logger.debug("++++++++RECORD: %s", record)
            # _logger.debug("++++++++SELF.NAME: %s", p.name)
            # _logger.debug("++++++++SELF.ID: %s", p.id)
            # _logger.debug("++++++++SELF.PARENT.ID: %s", p.parent_id)               
    
    def set_hold(self):
        for p in self:
            p.state = 'on_hold'

    def set_done(self):
        for p in self:
            p.state = 'done'
    
    def set_cancel(self):
        for p in self:
            p.state = 'cancelled'

    def back_draft(self):
        for p in self:
            p.state = 'draft'
    
    

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('Parent Task Already Recursive!'))




class TaskLink(models.Model):
    _name = "task.link"
    _description = "Task Links"

    task_id = fields.Many2one('mwide.stratplan', string='Task')
    target_task_id = fields.Many2one('mwide.stratplan', string='Target Task', required=True)
    link_type = fields.Selection([
        ('0', "Finish to Start"), 
        ('1', "Start to Start"), 
        ('2', "Finish to Finish"),
        ('3', "Start to Finish")
        ], string="Link Type", required=True, default='1')