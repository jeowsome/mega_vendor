# -*- coding: utf-8 -*-
{
    'name': "MEGA-Strategic Plan Project",

    'summary': """
        Manage Megawide Strategic Plan System""",

    'description': """
        Manage Megawide Strategic Plan System
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -315,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'hr'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',

    # Gantt-Chart JS module purchased from CF Info Solutions Team --big-Thanks
        "views/assets.xml",       


        'views/masterdata_views.xml',
        'views/stratplan_views.xml',


    ],
    'demo': [
    ],
    'test': [
    ],
    'css': [
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'js': [
    ],
    'post_load': None,
    'post_init_hook': 'post_init_hook',
    'installable': True,
    'application': True,
    'auto_install': False,
}
