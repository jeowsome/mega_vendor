import logging
from ast import literal_eval
from collections import defaultdict
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools.misc import ustr

from odoo.addons.base.models.ir_mail_server import MailDeliveryException
from odoo.addons.auth_signup.models.res_partner import SignupError, now

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = "res.partner"

    tin = fields.Char('TIN')
    vendor_status = fields.Selection([('ACTIVE', 'ACTIVE'), ('INACTIVE', 'INACTIVE')], string="Vendor Status", default='ACTIVE')
    is_accredited = fields.Boolean('Accredited Vendor', default=False)
    is_rejected = fields.Boolean('Rejected')
    failed_applications = fields.Integer('Number of Failed Applications', default=0)
    date_failed = fields.Datetime('Last Failed Application')
    days_remaining = fields.Integer('Days Remaining', default=0)
    days_restricted = fields.Integer('Days Restricted', default=0)
    vendor_category = fields.Selection([('subcon', 'Sub-Contractor'), ('supplier', 'Supplier')], string="Vendor Category")
    vendorcode = fields.Selection([('One Time', 'One Time'), ('Trial Code', 'Trial Code'), ('Permanent Code', 'Permanent Code')], string="Vendor Code")
    contact_person = fields.Char('Contact Person')
    payment_terms = fields.Char(string='Payment Terms')
    bu_selection = fields.Many2one('mega.vendor.bu', string="BU Selection", required=True)
    vendor_group = fields.Char('Vendor Group')
    date_accredited = fields.Datetime('Date Accredited')
    remarks_vendor = fields.Text('Remarks')
    vendor_details = fields.Selection([
        ('local', 'Local'),
        ('foreign', 'Foreign'),
    ], 'Vendor Detail', track_visibility='onchange')
    re_apply = fields.Selection([
        ('not_allowed', 'Not Allowed'),
        ('1_month', 'After 1 Month'),
        ('6_months', 'After 6 Months'),
        ('1_year', 'After 1 Year'),
    ], 'Re-Apply')

    _sql_constraints = [('tin', 'UNIQUE (tin)', 'TIN already exists'), ]

    def action_days_remaining_cron(self):
        vendors = self.search([('is_rejected', '=', True)])
        for vendor in vendors:
            days_remaining = fields.Datetime.now() - vendor.date_failed
            new_days_remaining = vendor.days_restricted - int(days_remaining.days)
            if new_days_remaining < 0:
                new_days_remaining = 0
            vendor.days_remaining = new_days_remaining


class MegaActiveDirectory(models.Model):
    _inherit = 'mega.active.directory'

    bu_selection = fields.Many2one('mega.vendor.bu', string="BU Selection")
    bu_selection_ids = fields.Many2many(comodel_name='mega.vendor.bu', string="BU Selection")
    business_unit = fields.Selection([
        ('bu', 'BU'),
        ('epc', 'EPC'),
    ], string='Business Unit')


class ResUsersInherit(models.Model):
    _inherit = "res.users"

    bu_selection = fields.Many2one('mega.vendor.bu', string="Old BU Selection")
    bu_selection_ids = fields.Many2many(comodel_name='mega.vendor.bu', string="BU Selection")
    is_vendor = fields.Boolean('Vendor', default=True)
    business_unit = fields.Selection([
        ('bu', 'BU'),
        ('epc', 'EPC'),
    ], string='Business Unit')
    chatter_position = fields.Selection(
        [("normal", "Normal"), ("sided", "Sided")],
        string="Chatter Position",
        default="normal",
    )

    @api.model
    def _microsoft_generate_signup_values(self, provider, params):
        email = params.get('email')
        ad = self.env['mega.active.directory'].search([('email', '=', email)])
        grp_ids = []
        bu_selection = False
        business_unit = False
        grp_ids.append(self.env.ref('base.group_user').id)
        if ad:
            for i in ad:
                bu_list = []
                for bu in i.bu_selection_ids:
                    bu_list.append(bu.id)
                bu_selection = [(6, 0, bu_list)]
                business_unit = i.business_unit
                for grp in i.template_user_id.groups_id:
                    if grp.id not in grp_ids:
                        grp_ids.append(grp.id)
        return {
            'name': params.get('name', email),
            'login': email,
            'email': email,
            'bu_selection_ids': bu_selection,
            'business_unit': business_unit,
            'groups_id': [(6, 0, grp_ids)],
            'action_id': False,
            'company_id': 1,
            'oauth_provider_id': provider,
            'oauth_uid': params['user_id'],
            'microsoft_refresh_token': params['microsoft_refresh_token'],
            'oauth_access_token': params['access_token'],
            'active': True

        }

    def _create_user_from_template(self, values):
        template_user_id = literal_eval(self.env['ir.config_parameter'].sudo().get_param('base.template_portal_user_id', 'False'))
        template_user = self.browse(template_user_id)
        if 'email' in values:
            ad_rec = self.env['mega.active.directory'].search([('email', '=', values['email'])])
            if ad_rec:
                bu_list = []
                for bu in ad_rec.bu_selection_ids:
                    bu_list.append(bu.id)
                template_user = ad_rec.template_user_id
                if ad_rec.bu_selection_ids:
                    values['bu_selection_ids'] = [(6, 0, bu_list)]
                if ad_rec.company_id:
                    values['company_id'] = ad_rec.company_id.id
                if ad_rec.company_ids:
                    com_ids = []
                    for com in ad_rec.company_ids:
                        com_ids.append(com.id)
                    values['company_ids'] = [(6, 0, com_ids)]
        if not template_user.exists():
            raise ValueError(_('Signup: invalid template user'))

        if not values.get('login'):
            raise ValueError(_('Signup: no login given for new user'))
        if not values.get('partner_id') and not values.get('name'):
            raise ValueError(_('Signup: no name or partner given for new user'))

        # create a copy of the template user (attached to a specific partner_id if given)
        values['active'] = True
        try:
            with self.env.cr.savepoint():
                return template_user.with_context(no_reset_password=True).copy(values)
        except Exception as e:
            # copy may failed if asked login is not available.
            raise SignupError(ustr(e))
