# Inherit from Odoo base res.partners
from urllib import parse
from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning, UserError
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time
from pickle import TRUE


_logger = logging.getLogger(__name__)


class MegaVendor(models.Model):
    _name = "mega.vendor"
    _description = "List of Vendors"
    _order = "name"
    _inherit = ['mail.thread']

    TYPE_BUSINESS = [
        ('sole_pro', 'Sole Proprietorship'),
        ('partnership', 'Partnership'),
        ('corp', 'Corporation'),
        ('llc', 'Limited Liability Company')
    ]

    vendor_id = fields.Many2one('res.partner', string="Vendor Name", default=lambda self: self.env.user.partner_id)
    vendor_requirement_ids = fields.One2many('vendor.requirements', 'vendor_requirement_id', string='PDF Attachment')
    business_type = fields.Selection(TYPE_BUSINESS, string="Type of Business")
    name = fields.Char(string='Name', track_visibility='onchange', related='vendor_id.name')
    tin = fields.Char(string='TIN', track_visibility='onchange', related='vendor_id.tin', store=True)
    address = fields.Char(string='Address', track_visibility='onchange')
    email = fields.Char(string='Email Address', required=True, track_visibility='onchange', related="vendor_id.email", store=True)
    contact_person = fields.Char(string='Contact Person', track_visibility='onchange')
    contact_number = fields.Char(string='Contact Number', track_visibility='onchange')
    position = fields.Char('Position', track_visibility="onchange")
    bu_selection = fields.Many2one('mega.vendor.bu', string="BU Selection")
    control_no = fields.Char('Control No.')
    business_unit = fields.Selection([
        ('epc', 'EPC'),
        ('bu', 'BU'),
    ], 'Business Unit', default='bu', track_visibility='onchange')
    accred_status = fields.Selection([
        ('inprogress', 'In Progress'),
        ('incomplete', 'Incomplete'),
        ('failed', 'Failed'),
        ('complete', 'Completed')
    ], 'Accreditation Status', default='inprogress', track_visibility='onchange')
    vendor_details = fields.Selection([
        ('local', 'Local'),
        ('foreign', 'Foreign'),
    ], 'Vendor Detail', track_visibility='onchange')

    vendor_category = fields.Many2one('vendor.category', string='Vendor Category', track_visibility='onchange')
    vendor_group = fields.Many2many('vendor.group', string='Vendor Group', track_visibility='onchange', help="Select all that apply")
    vendor_group_cert = fields.Char('Vendor Group Certificate')
    date_applied = fields.Date(string='Application Date', default=fields.Date.context_today, readonly=True, track_visibility='onchange')
    notes = fields.Text(string='Notes', track_visibility='onchange')
    related_user = fields.Many2one("res.users", auto_join=True,
                                   string='Requested by', help='Partner-related data of the user',
                                   default=lambda self: self.env.user)
    qaqc_user_id = fields.Many2one('res.users', string="QAQC User", track_visibility="onchange")
    tax_user_id = fields.Many2one('res.users', string="Tax/Finance Approver", track_visibility="onchange")
    move_ids = fields.One2many('mega.vendor.move', 'vendor_id', string="History", track_visibility="onchange")
    payment_terms = fields.Selection([
        ('30d', '30-Days'),
        ('60d', '60-Days'),
        ('90d', '90-Days'),
        ('120d', '120-Days'),
    ], 'Payment Terms', track_visibility='onchange')
    vm_id = fields.Many2one('res.users', string="Vendor Management User")
    pu_user_id = fields.Many2one('res.users', string="Procurement User", track_visibility="onchange")
    pm_user_id = fields.Many2one('res.users', string="Procurement User Assignment (Approval)", track_visibility="onchange")
    scm_user_id = fields.Many2one('res.users', string="SCM Head")
    payment_terms_id = fields.Many2one('mega.vendor.terms', string='Payment Terms', track_visibility="onchange")
    other_payment = fields.Char('Other Payment Terms', track_visibility="onchange")
    is_others = fields.Boolean('Other Payment, Please specify', default=False, related="payment_terms_id.is_others")
    third_party = fields.Selection([('dnb', 'D&B Assessment'), ('internal', 'Internal Assessment')], string="Third Party Assessment", track_visibility="onchange")
    certificate_issued = fields.Boolean('Certificate Issued', default=False)
    validity_date = fields.Datetime('Certificate Validity Date')
    day_name = fields.Char('Suffix')
    issue_day = fields.Char('Day')
    regret_issued = fields.Boolean('Regret Letter Issued', default=False)
    date_issued = fields.Datetime('Date Issued')
    all_user_ids = fields.Char('All Users', compute="_get_all_user_ids")
    it_user_ids = fields.Char('IT Users', compute="_get_it_user_ids")

    #certificate_file = fields.Many2many(comodel_name='ir.attachment', string="Certificate of Accreditation", help='Copy of Certificate of Accreditation', copy=False)
    state_bu = fields.Selection([
        ('01draft', 'Draft'),
        ('06submitted', 'Submitted'),
        ('02validation', 'For Validation and Preliminary Evaluation'),
        #('03bu_selection', 'BU Selection'),
        ('04procurement_approval', 'Procurement Approval'),
        ('05procurement_approved', 'Procurement Approved'),
        ('07qaqc_approval', 'End User/QAQC Approval'),
        ('08qaqc_approved', 'End User/QAQC Approved'),
        ('09dnb_approval', 'D&B/Internal Assessment'),
        ('10dnb_result', 'D&B/Internal Assessment Report'),
        ('11final_approval', 'Review and Final Approval'),
        ('12passed', 'Passed'),
        ('13failed', 'Failed'),
        ('14vendor_request', 'Add Vendor Request'),
        ('15tax_approved', 'Tax Code Approved'),
        ('16vendor_added', 'Done'),

    ], 'Status', default="01draft", track_visibility='onchange')

    state_epc = fields.Selection([
        ('01draft', 'Draft'),
        ('02submitted', 'Submitted'),
        ('03dnb_approval', 'D&B/Internal Assessment'),
        ('04dnb_result', 'D&B/Internal Assessment Report'),
        ('05qaqc_approval', 'QAQC Approval'),
        ('06qaqc_approved', 'QAQC Approved'),
        ('07hse_approval', 'HSSE Evaluation'),
        ('08hse_approved', 'HSSE Approved'),
        ('09finance_approval', 'Finance Approval'),
        ('10finance_approved', 'Finance Approved'),
        ('11final_approval', 'Review and Final Approval'),
        ('12passed', 'Passed'),
        ('13failed', 'Failed'),
        ('14vendor_request', 'Add Vendor Request'),
        ('15tax_approved', 'Tax Code Approved'),
        ('16vendor_added', 'Done'),

    ], 'Status', default="01draft", track_visibility='onchange')

    manager_ids = fields.Char('Manager Ids', compute="_get_manager_ids")
    hsse_ids = fields.Char('HSSE Ids', compute="_get_hsse_ids")
    finance_ids = fields.Char('Finance Ids', compute="_get_finance_ids")
    scm_ids = fields.Char('SCM Head Ids', compute="_get_scm_ids")
    #tax_code = fields.Char('Withholding Tax Code')
    #vat_type = fields.Char('Vat Added Tax Type')
    tax_code = fields.Many2one('mega.vendor.taxcode', 'Witholding Tax Code')
    vat_type = fields.Many2one('mega.vendor.vattype', 'Vat Type')
    vendorcode = fields.Selection([('One Time', 'One Time'), ('Trial Code', 'Trial Code'), ('Permanent Code', 'Permanent Code')], string="Vendor Code")

    def get_link_to_document(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action = action = self.env.ref('mega_vendor.action_vendor_bu')
        if self.business_unit == 'epc':
            action = action = self.env.ref('mega_vendor.action_vendor_epc')

        db = {'db': self._cr.dbname}
        query = {
            'id': self.id,
            'view_type': 'form',
            'model': self._name,
            'action': action.id,
        }

        path = "/web?%s#%s" % (parse.urlencode(db), parse.urlencode(query))
        full_url = base_url + path

        return full_url

    def action_application_cron(self):
        recs = self.search(['|', '&', ('state_bu', '=', '01draft'), ('business_unit', '=', 'bu'), '&', ('state_epc', '=', '01draft'), ('business_unit', '=', 'epc')])
        for rec in recs:
            void_date = (fields.Datetime.now() - rec.create_date)
            void_days = void_date.days
            if void_days > 14:
                rec.vendor_id.re_apply = '1_month'
                rec.vendor_id.date_failed = fields.Datetime.now()
                rec.vendor_id.failed_applications = rec.vendor_id.failed_applications + 1
                rec.vendor_id.is_rejected = True
                rec.vendor_id.days_remaining = 30
                rec.vendor_id.days_restricted = 30
                rec.failed()
                rec.accred_status = 'incomplete'
                rec.related_user.sudo().unlink()

    def vendor_send_email(self, template):
        email = self.env.ref(template)
        if email:
            email.send_mail(self.id, force_send=True)

    @api.depends("it_user_ids")
    def _get_it_user_ids(self):
        user_ids = self.env['res.users'].search([])
        it_user_ids = ''
        it_group = self.env.ref('mega_vendor.group_vendor_it_support')
        for i in it_group.users:
            it_user_ids = it_user_ids + str(i.partner_id.id) + ","
        self.it_user_ids = it_user_ids

    @api.depends("manager_ids")
    def _get_manager_ids(self):
        user_ids = self.env['res.users'].search([])
        manager_ids = ''
        vm_group = self.env.ref('mega_vendor.group_vendor_management')
        for i in vm_group.users:
            if i.business_unit == self.business_unit:
                manager_ids = manager_ids + str(i.partner_id.id) + ","
        print(manager_ids)
        self.manager_ids = manager_ids

    def _get_hsse_ids(self):
        user_ids = self.env['res.users'].search([])
        manager_ids = ''
        vm_group = self.env.ref('mega_vendor.group_vendor_hse')
        for i in vm_group.users:
            if i.business_unit == self.business_unit:
                manager_ids = manager_ids + str(i.partner_id.id) + ","
        print(manager_ids)
        self.hsse_ids = manager_ids

    def _get_finance_ids(self):
        user_ids = self.env['res.users'].search([])
        manager_ids = ''
        vm_group = self.env.ref('mega_vendor.group_vendor_finance')
        for i in vm_group.users:
            if i.business_unit == self.business_unit:
                manager_ids = manager_ids + str(i.partner_id.id) + ","
        print(manager_ids)
        self.finance_ids = manager_ids

    def _get_scm_ids(self):
        user_ids = self.env['res.users'].search([])
        manager_ids = ''
        vm_group = self.env.ref('mega_vendor.group_vendor_scm_head')
        for i in vm_group.users:
            if i.business_unit == self.business_unit:
                manager_ids = manager_ids + str(i.partner_id.id) + ","
        print(manager_ids)
        self.scm_ids = manager_ids

    @api.depends("all_user_ids")
    def _get_all_user_ids(self):
        user_ids = self.env['res.users'].search([('is_vendor', '=', False)])
        all_user_ids = str(self.vendor_id.id)
        for i in user_ids:
            if i.has_group('mega_vendor.group_vendor_management') or i.has_group('mega_vendor.group_vendor_procurement_user') or i.has_group('mega_vendor.group_vendor_end_user') or i.has_group('mega_vendor.group_vendor_hse') or i.has_group('mega_vendor.group_vendor_finance') or i.has_group('mega_vendor.group_vendor_scm_head') or i.has_group('mega_vendor.group_vendor_tax') or i.has_group('mega_vendor.group_vendor_admin'):
                all_user_ids = all_user_ids + str(i.partner_id.id) + ","
        self.all_user_ids = all_user_ids

    @api.onchange('vendor_category')
    def _onchange_vendor_category(self):
        print(self.vendor_category)
        res = []
        for rec in self.vendor_category.requirement_ids:
            is_required = rec.is_required
            if is_required:
                if rec.business_type == self.business_type or not rec.business_type:
                    is_required = rec.is_required
                elif rec.business_type != self.business_type:
                    is_required = False
            v = self.env['vendor.requirements'].create({'name': rec.doc_id.name, 'description': rec.description, 'is_required': is_required})
            res.append(v.id)
        self.vendor_requirement_ids = [(6, 0, res)]

    def action_move(self, pid, action, state, bu):
        return{
            'name': action,
            'res_model': 'mega.vendor.move',
            'type': 'ir.actions.act_window',
            'context': {'default_vendor_id': pid, 'default_action': action, 'default_next_state': state, 'default_business_unit': bu},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }

    def set_draft(self):
        for p in self:
            p.state_bu = 'draft'

    # BU action buttons
    def for_validation(self):
        for p in self:
            p.state_bu = '02validation'

    def bu_selection_action(self):
        return {
            'name': 'BU Selection',
            'res_model': 'mega.vendor.bu.wizard',
            'type': 'ir.actions.act_window',
            'context': {'default_mega_vendor_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }

    def procurement_approval(self):
        for p in self:
            if p.bu_selection not in self.env.user.bu_selection_ids:
                raise UserError(_("You're not allowed for this Application's BU Selection"))
            p.pu_user_id = self.env.user.id
            #p.state_bu = '04procurement_approval'
            next_state = '04procurement_approval'
            return {
                'name': 'For Procurement Approval',
                'res_model': 'mega.vendor.procurement',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_bu, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def procurement_approved(self):
        for p in self:
            if p.bu_selection not in self.env.user.bu_selection_ids:
                raise UserError(_("You're not allowed for this Application's BU Selection"))
            #p.state_bu = '05procurement_approved'
            next_state = '05procurement_approved'
            return{
                'name': 'Procurement Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_bu, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }
            # p.action_move(p.id,p.state_bu,next_state,p.business_unit)

    def submit(self):
        for p in self:
            for doc in p.vendor_requirement_ids:
                if doc.is_required:
                    if not doc.pdf_attachment:
                        raise UserError(_("Missing Attachment for Required Document : %s " % doc.name))
                    else:
                        pass
            p.state_bu = '06submitted'
            p.date_applied = fields.Datetime.now()

    def qaqc_approval(self):
        for p in self:
            if p.bu_selection not in self.env.user.bu_selection_ids:
                raise UserError(_("You're not allowed for this Application's BU Selection"))
            p.vm_id = self.env.user.id
            next_state = '07qaqc_approval'
            return {
                'name': 'For QAQC Approval',
                'res_model': 'mega.vendor.qaqc',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }
            # p.action_move(self.state_bu,next_state,self.business_unit)
            #p.state_bu = '07qaqc_approval'

    def qaqc_approved(self):
        for p in self:
            if p.bu_selection not in self.env.user.bu_selection_ids:
                raise UserError(_("You're not allowed for this Application's BU Selection"))
            #p.state_bu = '08qaqc_approved'
            next_state = '08qaqc_approved'
            return{
                'name': 'QAQC Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_bu, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def vm_approved(self):
        for p in self:
            next_state = '08qaqc_approved'
            return{
                'name': 'Vendor Management Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': 'vmapproved', 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def dnb_approval(self):
        for p in self:
            p.state_bu = '09dnb_approval'
            p.third_party = 'dnb'

    def internal_approval(self):
        for p in self:
            p.state_bu = '09dnb_approval'
            p.third_party = 'internal'

    def dnb_approved(self):
        for p in self:
            #p.state_bu = '10dnb_result'
            action_name = 'Assessment Report'
            if p.third_party == 'dnb':
                action_name = 'D&B Assessment Report'
            elif p.third_party == 'internal':
                action_name = 'Internal Assessment Report'
            next_state = '10dnb_result'
            return{
                'name': action_name,
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_bu, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    #---------- ----------------#
    # EPC action buttons

    def submit_epc(self):
        for p in self:
            for doc in p.vendor_requirement_ids:
                if doc.is_required:
                    if not doc.pdf_attachment:
                        raise UserError(_("Missing Attachment for Required Document : %s " % doc.name))
                    else:
                        pass
            p.vendor_send_email('mega_vendor.email_template_vendor_submit')
            p.state_epc = '02submitted'
            p.date_applied = fields.Datetime.now()

    def dnb_approval_epc(self):
        for p in self:
            p.state_epc = '03dnb_approval'
            p.third_party = 'dnb'

    def internal_approval_epc(self):
        for p in self:
            p.state_epc = '03dnb_approval'
            p.third_party = 'internal'

    def dnb_approved_epc(self):
        for p in self:
            #p.state_epc = '04dnb_result'
            action_name = 'Assessment Report'
            if p.third_party == 'dnb':
                action_name = 'D&B Assessment Report'
            elif p.third_party == 'internal':
                action_name = 'Internal Assessment Report'
            next_state = '04dnb_result'
            return{
                'name': action_name,
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_epc, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def qaqc_approval_epc(self):
        for p in self:
            #p.state_epc = '05qaqc_approval'
            self.vm_id = self.env.user.id
            next_state = '05qaqc_approval'
            return {
                'name': 'For QAQC Approval',
                'res_model': 'mega.vendor.qaqc',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def qaqc_approved_epc(self):
        for p in self:
            #p.state_epc = ''
            next_state = '06qaqc_approved'
            return {
                'name': 'QAQC Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_epc, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def hse_approval_epc(self):
        for p in self:
            email = self.vendor_send_email('mega_vendor.email_template_vendor_hsse')
            if email:
                email.send_mail(p.id, force_send=True)
            p.state_epc = '07hse_approval'

    def hse_approved_epc(self):
        for p in self:
            #p.state_epc = '08hse_approved'
            email = self.vendor_send_email('mega_vendor.email_template_vendor_hsse_approved')
            if email:
                email.send_mail(p.id, force_send=True)
            next_state = '08hse_approved'
            return {
                'name': 'HSSE Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_epc, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def finance_approval_epc(self):
        for p in self:
            email = self.vendor_send_email('mega_vendor.email_template_vendor_finance')
            if email:
                email.send_mail(p.id, force_send=True)
            p.state_epc = '09finance_approval'

    def finance_approved_epc(self):
        for p in self:
            #p.state_epc = '10finance_approved'
            email = self.vendor_send_email('mega_vendor.email_template_vendor_finance_approved')
            if email:
                email.send_mail(p.id, force_send=True)
            next_state = '10finance_approved'
            return {
                'name': 'Finance Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': p.state_epc, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    # COMMON BUTTON ACTIONS
    def final_approval(self):
        for p in self:
            #p.state_bu = '11final_approval'
            #p.state_epc = '11final_approval'
            if p.business_unit == 'epc':
                email = self.vendor_send_email('mega_vendor.email_template_vendor_final')
                if email:
                    email.send_mail(p.id, force_send=True)
            if p.business_unit == 'bu':
                state = p.state_bu
            else:
                state = p.state_epc
            next_state = '11final_approval'
            return{
                'name': 'Final Approval',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': state, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def passed(self):
        for p in self:
            #p.state_bu = '12passed'
            #p.state_epc = '12passed'
            if p.business_unit == 'bu':
                state = p.state_bu
            else:
                state = p.state_epc
            next_state = '12passed'
            p.scm_user_id = self.env.user.id
            return {
                'name': 'Passed',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': state, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def issue_certificate(self):
        self.vm_id = self.env.user.id
        self.date_issued = fields.Datetime.now()
        self.validity_date = fields.Datetime.now() + relativedelta(years=1)
        day_suffix = 'th'
        if not self.is_others:
            self.other_payment = self.payment_terms_id.name
        print(self.date_issued.day)
        if self.date_issued.day in [1, 21, 31]:
            day_suffix = 'st'
        elif self.date_issued.day in [2, 22]:
            day_suffix = 'nd'
        elif self.date_issued.day in [3, 23]:
            day_suffix = 'rd'
        self.day_name = day_suffix
        self.issue_day = self.date_issued.day
        count = 0
        group_str = ''
        for group in self.vendor_group:
            if count == 0:
                group_str = group_str + 'ENGAGED IN ' + group.name.upper()
            else:
                group_str = group_str + ', ' + group.name.upper()
            count += 1
        self.vendor_group_cert = group_str
        seq = self.env['ir.sequence'].next_by_code('mega.vendor') or '000'
        self.control_no = fields.Datetime.now().strftime('%Y%m%d') + "-" + seq
        print(self.name)
        if self.business_unit == 'bu':
            self.vendor_send_email('mega_vendor.email_template_vendor_certificate')
        elif self.business_unit == 'epc':
            self.vendor_send_email('mega_vendor.email_template_vendor_certificate_epc')
        self.certificate_issued = True
        self.date_issued = fields.Datetime.now()

    def print_certificate(self):
        if self.business_unit == 'bu':
            return self.env.ref('mega_vendor.vendor_certificate').report_action(self)
        elif self.business_unit == 'epc':
            return self.env.ref('mega_vendor.vendor_certificate_epc').report_action(self)

    def action_failed_wizard(self):
        return {
            'name': 'Failed Application',
            'res_model': 'mega.vendor.failed',
            'type': 'ir.actions.act_window',
            'context': {'default_vendor_id': self.id},
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }

    def failed(self):
        for p in self:
            p.state_bu = '13failed'
            p.state_epc = '13failed'
            p.accred_status = 'failed'
            p.scm_user_id = self.env.user.id

    def issue_regret(self):
        self.vendor_send_email('mega_vendor.email_template_vendor_regret')
        self.regret_issued = True

    def vendor_request_wizard(self):
        vendor_name = self.vendor_id.name
        vendor_context = {'default_vendor_id': self.id,
                          'default_company_name': vendor_name,
                          'default_vendor_categ': self.vendor_category.id,
                          'default_address': self.address,
                          'default_tin': self.tin,
                          'default_payment_terms_id': self.payment_terms_id.id,
                          'default_is_others': self.is_others,
                          'default_other_payment': self.other_payment,
                          'default_contact_person': self.contact_person,
                          'default_mobile': self.contact_number,
                          'default_email': self.email
                          }
        return {
            'name': 'Request to Add Vendor',
            'res_model': 'mega.vendor.sap',
            'type': 'ir.actions.act_window',
            'context': vendor_context,
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new'
        }

    def vendor_request(self):
        for p in self:
            p.state_bu = '14vendor_request'
            p.state_epc = '14vendor_request'
            email = self.vendor_send_email('mega_vendor.email_template_tax_approval')
            p.vm_id = self.env.user.id
            if email:
                email.send_mail(p.id, force_send=True)

    def tax_approved(self):
        for p in self:
            #p.state_bu = '15tax_approved'
            #p.state_epc = '15tax_approved'
            if p.business_unit == 'bu':
                state = p.state_bu
            else:
                state = p.state_epc
            next_state = '15tax_approved'
            p.vm_id = self.env.user.id
            return {
                'name': 'Tax Approved',
                'res_model': 'mega.vendor.move',
                'type': 'ir.actions.act_window',
                'context': {'default_vendor_id': p.id, 'default_action': state, 'default_next_state': next_state, 'default_business_unit': p.business_unit},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }

    def add_vendor(self):
        for p in self:
            p.vendor_id.is_accredited = True
            p.vendor_id.contact_person = p.contact_person
            p.vendor_id.street = p.address
            group_str = ''
            for group in self.vendor_group:
                group_str = group_str + group.name.upper() + '|'
            p.vendor_id.vendor_group = group_str

            p.vendor_id.vendorcode = p.vendorcode
            if 'Supplier' in p.vendor_category.name:
                p.vendor_id.vendor_category = 'supplier'
            else:
                p.vendor_id.vendor_category = 'subcon'
            p.vendor_id.date_accredited = p.date_issued
            p.vendor_id.payment_terms = p.other_payment
            p.state_bu = '16vendor_added'
            p.state_epc = '16vendor_added'
            p.accred_status = 'complete'
            if p.bu_selection:
                p.vendor_id.bu_selection = p.bu_selection


class VendorRequirements(models.Model):
    _name = "vendor.requirements"

    vendor_requirement_id = fields.Many2one('mega.vendor', 'PDF Attachment', readonly=True, ondelete='cascade')

    name = fields.Char(string='Document Name', required=True, track_visibility='onchange')
    description = fields.Char(string='Document Description', required=True, track_visibility='onchange')
    pdf_attachment = fields.Many2many(comodel_name='ir.attachment', string="Attachment", help='You can attach the copy of your document', copy=False)
    date_attached = fields.Date(string='Date Attached', readonly=True, track_visibility='onchange')
    remarks = fields.Char(string='Remarks', track_visibility='onchange')
    filename = fields.Char(string='Filename')
    is_required = fields.Boolean('Required', default=False)

    @api.onchange('pdf_attachment')
    def _onchange_pdf_attachment(self):
        if self.pdf_attachment:
            self.date_attached = fields.Datetime.now()
        else:
            self.date_attached = False


class MegaVendorBu(models.Model):
    _name = 'mega.vendor.bu'

    name = fields.Char('Name')


class MegaVendorWizard(models.TransientModel):
    _name = 'mega.vendor.wizard'

    business_unit = fields.Selection([
        ('bu', 'BU'),
        ('epc', 'EPC'),
    ], 'Business Unit', default='bu', required=True)

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('vendor_id', '=', self.env.user.partner_id.id)])
        mega_vendor.business_unit = self.business_unit
        if self.business_unit == 'epc':
            self.env.user.action_id = self.env.ref('mega_vendor.action_vendor_epc').id
        elif self.business_unit == 'bu':
            self.env.user.action_id = self.env.ref('mega_vendor.action_vendor_bu').id
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }


class MegaVendorBuWizard(models.TransientModel):
    _name = 'mega.vendor.bu.wizard'

    bu_selection = fields.Many2one('mega.vendor.bu', string="BU Selection", required=True)

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_mega_vendor_id'])])
        mega_vendor.bu_selection = self.bu_selection.id
        mega_vendor.state_bu = '03bu_selection'


class MegaVendorTerms(models.Model):
    _name = 'mega.vendor.terms'

    name = fields.Char('Payment Terms', required=True)
    is_others = fields.Boolean('Specify if not included?')


class DocumentVendorMany2manyAttachment(models.Model):
    _inherit = 'ir.attachment'

    doc_attach_rel = fields.Many2many('vendor.requirements', 'pdf_attachment', 'attach_id3', 'doc_id', string="Attachment", invisible=1)


class MegaVendorFailed(models.TransientModel):
    _name = 'mega.vendor.failed'

    re_apply = fields.Selection([
        ('not_allowed', 'Not Allowed'),
        ('6_months', 'After 6 Months'),
        ('1_year', 'After 1 Year'),
    ], 'Re-Apply', default='not_allowed', required=True)
    attachment_id = fields.Many2many(comodel_name='ir.attachment', string="Attachment", help='You can attach the copy of your document', copy=False)
    remarks = fields.Text('Remarks')

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_vendor_id'])])
        mega_vendor.vendor_id.re_apply = self.re_apply
        mega_vendor.vendor_id.date_failed = fields.Datetime.now()
        mega_vendor.vendor_id.failed_applications = mega_vendor.vendor_id.failed_applications + 1
        mega_vendor.vendor_id.is_rejected = True
        a_list = []
        for a in self.attachment_id:
            a_list.append(a.id)
        move = self.env['mega.vendor.move'].create({'vendor_id': mega_vendor.id, 'action': '13failed', 'attachment_id': [(6, 0, a_list)], 'remarks': self.remarks}, context={'default_action': '13failed', 'default_next_state': '13failed'})
        mega_vendor.move_ids = [(4, move.id)]
        if self.re_apply == 'not_allowed':
            mega_vendor.vendor_id.days_remaining = -1
            mega_vendor.vendor_id.days_restricted = -1
        elif self.re_apply == '6_months':
            mega_vendor.vendor_id.days_remaining = 182
            mega_vendor.vendor_id.days_restricted = 182
        elif self.reapply == '1_year':
            mega_vendor.vendor_id.days_remaining = 364
            mega_vendor.vendor_id.days_restricted = 364
        mega_vendor.related_user.sudo().unlink()
        mega_vendor.failed()


class MegaVendorMove(models.Model):
    _name = 'mega.vendor.move'

    AVAIL_ACTION = [
        ('01draft', 'Draft'),
        ('06submitted', 'Submitted'),
        ('02validation', 'For Validation and Preliminary Evaluation'),
        ('04procurement_approval', 'Procurement Approval'),
        ('05procurement_approved', 'Procurement Approved'),
        ('07qaqc_approval', 'End User/QAQC Approval'),
        ('08qaqc_approved', 'End User/QAQC Approved'),
        ('09dnb_approval', 'D&B Assessment'),
        ('10dnb_result', 'D&B Assessment Report'),
        ('11final_approval', 'Review and Final Approval'),
        ('12passed', 'Passed'),
        ('13failed', 'Failed'),
        ('14vendor_request', 'Add Vendor Request'),
        ('15tax_approved', 'Tax Code Approved'),
        ('16vendor_added', 'Done'),
        ('02submitted', 'Submitted'),
        ('03dnb_approval', 'D&B Assessment'),
        ('04dnb_result', 'D&B Assessment Report'),
        ('05qaqc_approval', 'QAQC Approval'),
        ('06qaqc_approved', 'QAQC Approved'),
        ('07hse_approval', 'HSSE Evaluation'),
        ('08hse_approved', 'HSSE Approved'),
        ('09finance_approval', 'Finance Approval'),
        ('10finance_approved', 'Finance Approved'),
        ('vmapproved', 'Vendor Management Approved')
    ]
    vendor_id = fields.Many2one('mega.vendor', string="Vendor Application")
    action = fields.Selection(AVAIL_ACTION, string="Action", default=lambda self: self.env.context['default_action'])
    next_state = fields.Selection(AVAIL_ACTION, string="Next State", default=lambda self: self.env.context['default_next_state'])
    attachment_id = fields.Many2many(comodel_name='ir.attachment', string="Attachment", help='You can attach the copy of your document', copy=False)
    remarks = fields.Text('Remarks')

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_vendor_id'])])
        if self.env.context['default_business_unit'] == 'bu':
            mega_vendor.state_bu = self.env.context['default_next_state']
        elif self.env.context['default_business_unit'] == 'epc':
            mega_vendor.state_epc = self.env.context['default_next_state']
        if self.next_state == "05procurement_approved" or self.next_state == '02submitted':
            mega_vendor.vendor_send_email('mega_vendor.email_template_vendor_submit')
        elif self.next_state == '08qaqc_approved':
            mega_vendor.vendor_send_email('mega_vendor.email_template_vendor_qaqc_approved')
        elif self.next_state == '12passed':
            mega_vendor.vendor_send_email('mega_vendor.email_template_vendor_passed')
        elif self.next_state == '13failed':
            mega_vendor.vendor_send_email('mega_vendor.email_template_vendor_failed')
        elif self.next_state == '15tax_approved':
            mega_vendor.vendor_send_email('mega_vendor.email_template_vendor_it')
        return True


class MegaVendorTax(models.TransientModel):
    _name = 'mega.vendor.tax'

    def _filter_users(self):
        user_ids = []
        qaqc_group = self.env.ref('mega_vendor.group_vendor_end_user')
        for i in qaqc_group.users:
            user_ids.append(i.id)
        return [('id', 'in', user_ids)]
        # return {'domain': {'qaqc_user_id':[('id','in',user_ids)]}}
    tax_group = fields.Many2one('res.groups', string="Tax Group", default=lambda self: self.env.ref('mega_vendor.group_vendor_tax'))
    tax_user_id = fields.Many2one('res.users', string="Tax/Finance Assignment", track_visibility="onchange", required=True)

    @api.onchange('tax_group')
    def _tax_onchange(self):
        user_ids = []
        for i in self.tax_group.users:
            user_ids.append(i.id)
        for i in self.env.ref('mega_vendor.group_vendor_finance').users:
            user_ids.append(i.id)
        return {'domain': {'tax_user_id': [('id', 'in', user_ids)]}}

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_vendor_id'])])
        mega_vendor.tax_user_id = self.tax_user_id.id
        if self.env.context['default_business_unit'] == 'bu':
            mega_vendor.state_bu = self.env.context['default_next_state']
        elif self.env.context['default_business_unit'] == 'epc':
            mega_vendor.state_epc = self.env.context['default_next_state']
        email = self.env.ref('mega_vendor.email_template_vendor_tax_approval')
        if email:
            email.send_mail(mega_vendor.id, force_send=True)
        return True


class MegaVendorQaqc(models.TransientModel):
    _name = 'mega.vendor.qaqc'

    def _filter_users(self):
        user_ids = []
        qaqc_group = self.env.ref('mega_vendor.group_vendor_end_user')
        for i in qaqc_group.users:
            user_ids.append(i.id)
        return [('id', 'in', user_ids)]
        # return {'domain': {'qaqc_user_id':[('id','in',user_ids)]}}
    qaqc_group = fields.Many2one('res.groups', string="QAQC Group", default=lambda self: self.env.ref('mega_vendor.group_vendor_end_user'))
    qaqc_user_id = fields.Many2one('res.users', string="QAQC User Assignment", track_visibility="onchange", required=True)

    @api.onchange('qaqc_group')
    def _qaqc_onchange(self):
        user_ids = []
        for i in self.qaqc_group.users:
            user_ids.append(i.id)
        return {'domain': {'qaqc_user_id': [('id', 'in', user_ids)]}}

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_vendor_id'])])
        mega_vendor.qaqc_user_id = self.qaqc_user_id.id
        if self.env.context['default_business_unit'] == 'bu':
            mega_vendor.state_bu = self.env.context['default_next_state']
        elif self.env.context['default_business_unit'] == 'epc':
            mega_vendor.state_epc = self.env.context['default_next_state']
        email = self.env.ref('mega_vendor.email_template_vendor_qaqc_approval')
        if email:
            email.send_mail(mega_vendor.id, force_send=True)
        return True


class MegaVendorTaxCode(models.Model):
    _name = 'mega.vendor.taxcode'

    name = fields.Char('Name', required=True)


class MegaVendorVatType(models.Model):
    _name = 'mega.vendor.vattype'

    name = fields.Char('Name', required=True)


class MegaVendorSap(models.TransientModel):
    _name = 'mega.vendor.sap'

    # Vendor Category : One Time / Trial Code / Permanent Code

    #vendor_id = fields.Many2one('res.partner','Company Name',required=True,default=lambda self:self.env.context['default_company_name'])
    vendor_name = fields.Char('Company Name', required=True, default=lambda self: self.env.context['default_company_name'])
    vandor_category = fields.Many2one('vendor.category', 'Vendor Category', required=True, default=lambda self: self.env.context['default_vendor_categ'])
    vendorcode = fields.Selection([('One Time', 'One Time'), ('Trial Code', 'Trial Code'), ('Permanent Code', 'Permanent Code')], string="Vendor Code", required=True)
    address = fields.Char('Company Address', required=True, default=lambda self: self.env.context['default_address'])
    payment_terms_id = fields.Many2one('mega.vendor.terms', string='Payment Terms', required=True, default=lambda self: self.env.context['default_payment_terms_id'])
    is_others = fields.Boolean('Other Payment, Please specify', related="payment_terms_id.is_others", default=lambda self: self.env.context['default_is_others'])
    other_payment = fields.Char('Other Payment Terms', track_visibility="onchange", default=lambda self: self.env.context['default_other_payment'])
    tin = fields.Char('TIN', required=True, default=lambda self: self.env.context['default_tin'])
    #tax_code = fields.Char('Withholding Tax Code')
    #vat_type = fields.Char('Vat Type')
    tax_code = fields.Many2one('mega.vendor.taxcode', 'Witholding Tax Code', required=True)
    vat_type = fields.Many2one('mega.vendor.vattype', 'Vat Type', required=True)
    contact_person = fields.Char('Contact Person', required=True, default=lambda self: self.env.context['default_contact_person'])
    position = fields.Char('Position')
    mobile = fields.Char('Mobile No.', required=True, default=lambda self: self.env.context['default_mobile'])
    email = fields.Char('Email Address', required=True, default=lambda self: self.env.context['default_email'])
    tax_group = fields.Many2one('res.groups', string="Tax Group", default=lambda self: self.env.ref('mega_vendor.group_vendor_tax'))
    tax_user_id = fields.Many2one('res.users', string="Tax/Finance Assignment", track_visibility="onchange", required=True)

    @api.onchange('tax_group')
    def _tax_onchange(self):
        user_ids = []
        for i in self.tax_group.users:
            user_ids.append(i.id)
        for i in self.env.ref('mega_vendor.group_vendor_finance').users:
            user_ids.append(i.id)
        return {'domain': {'tax_user_id': [('id', 'in', user_ids)]}}

    def submit(self):
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_vendor_id'])])
        mega_vendor.tax_code = self.tax_code
        mega_vendor.vat_type = self.vat_type
        mega_vendor.vendorcode = self.vendorcode
        mega_vendor.tax_user_id = self.tax_user_id.id
        mega_vendor.vendor_request()


class MegaVendorProcurement(models.TransientModel):
    _name = 'mega.vendor.procurement'

    def _filter_users(self):
        user_ids = []
        pro_group = self.env.ref('mega_vendor.group_vendor_procurement_user')
        for i in pro_group.users:
            user_ids.append(i.id)
        return [('id', 'in', user_ids)]
        # return {'domain': {'qaqc_user_id':[('id','in',user_ids)]}}
    pro_group = fields.Many2one('res.groups', string="Procurement Group", default=lambda self: self.env.ref('mega_vendor.group_vendor_procurement_user'))
    pm_user_id = fields.Many2one('res.users', string="Procurement User", track_visibility="onchange", required=True)
    attachment_id = fields.Many2many(comodel_name='ir.attachment', string="Attachment", help='You can attach the copy of your document', copy=False)
    remarks = fields.Char('Remarks')

    @api.onchange('pro_group')
    def _pro_onchange(self):
        user_ids = []
        for i in self.pro_group.users:
            user_ids.append(i.id)
        return {'domain': {'pm_user_id': [('id', 'in', user_ids)]}}

    def submit(self):
        if not self.attachment_id:
            raise ValidationError(_('Missing Attachment'))
        mega_vendor = self.env['mega.vendor'].search([('id', '=', self.env.context['default_vendor_id'])])
        mega_vendor.pm_user_id = self.pm_user_id.id
        if self.env.context['default_business_unit'] == 'bu':
            mega_vendor.state_bu = self.env.context['default_next_state']
        elif self.env.context['default_business_unit'] == 'epc':
            mega_vendor.state_epc = self.env.context['default_next_state']
        a_list = []
        for a in self.attachment_id:
            a_list.append(a.id)
        move = self.env['mega.vendor.move'].create({'vendor_id': mega_vendor.id, 'action': self.env.context['default_action'], 'attachment_id': [(6, 0, a_list)], 'remarks': self.remarks})
        mega_vendor.move_ids = [(4, move.id)]
        email = self.env.ref('mega_vendor.email_template_vendor_procurement_approval')
        if email:
            #email.attachment_ids = [(    6,0,)]
            email.send_mail(mega_vendor.id, force_send=True)
        return True
