
import logging
import uuid

from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools import float_is_zero


class SurveyUserInput(models.Model):
    _inherit = "survey.user_input"

    cc_email = fields.Char()
