# -*- coding: utf-8 -*-
{
    'name': "MEGA-Asset Tagging Project",

    'summary': """
        Manage Megawide Asset Inventory System""",

    'description': """
        Manage Megawide Asset Inventory System
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -305,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'contacts', 'hr', 'stock'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',

        # 'data/eric_data.xml',
        # 'data/eric_template_data.xml',
        # 'data/eric.policy.csv',
        # 'data/eric.policy.new.requirements.csv',
        # 'data/eric.policy.extension.requirements.csv',
        # #'data/eric.project.csv',

        'views/masterdata_views.xml',
        'views/transaction_views.xml',
        # 'views/res_partner_views.xml',
        # 'views/requirements_views.xml',
        # 'views/reports_view.xml'
        # 'views/hr_employee_views.xml',

        'views/insurance_data.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
