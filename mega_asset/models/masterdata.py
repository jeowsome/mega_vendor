#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time

_logger = logging.getLogger(__name__)


class MegaLocation(models.Model):
    _name = "mwide.asset.location"

    name = fields.Char(string='Project / Location Name', required=True, tracking=True)
    address = fields.Char(string='Address', required=True, tracking=True)
    location_type_id = fields.Many2one('mwide.asset.location_type',string="Location Type")
    employee_id = fields.Many2one('hr.employee', string='Project Manager', tracking=True, domain="[('job_id', '=', 'Project Manager')]")
    customer_id = fields.Many2one('res.partner', string='Project Owner', tracking=True)
    contact_no = fields.Char(string="Contact No.", tracking=True)
    

    notes = fields.Text(string='Notes', default='No notes', tracking=True)

    # @api.onchange('customer_id')
    # def onchange_customer_id(self):s
    #     if self.customer_id:
    #         if self.customer_id.mobile:
    #             self.contact_no = self.customer_id.mobile
    #     else:
    #         self.contact_no=""


class MegaLocationType(models.Model):
    _name = "mwide.asset.location_type"

    name = fields.Char(string='Location Type', tracking=True)

