#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners
from urllib import parse
from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time
from pickle import TRUE


_logger = logging.getLogger(__name__)

 
class TransAssetHeader(models.Model):
    _name = "mwide.asset.header"
    _description = "Request for Insurance"
    _order = "name desc"
    _inherit = ['mail.thread']

    name = fields.Char(string = 'Request Form',
        default='New',
        required=True,
        readonly=True,
        state={'1_draft': [('readonly', False)]})

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('mwide.asset.header') or '000000'
        vals['name'] = seq
        return super(TransAssetHeader, self).create(vals)


    trans_details_ids = fields.One2many('mwide.asset.details', 'trans_details_id', string='Trans Details')

    date_trans = fields.Date("Date Requested",
        required=True,
        default=fields.Date.context_today,
        copy=False,
        readonly=True)

    requested_id = fields.Many2one("res.users", required=True, readonly=True, ondelete='restrict', auto_join=True,
        string='Requested By', help='Partner-related data of the User Login',
        default=lambda self: self.env.user)

    lead_id = fields.Many2one("hr.employee", required=True, ondelete='restrict', string='Lead Accountant', 
        help='Partner-related data of the HR Employee', default=lambda self: self.env.user)        

    project_id = fields.Many2one("mwide.asset.location", string="Project/Location Name", ondelete="restrict", track_visibility='onchange',required=True)
    project_address = fields.Char(string='Location', track_visibility='onchange', related='project_id.address',readonly=False, store=True)
    contact_no = fields.Char(string="Contact No.", related='project_id.contact_no',readonly=False, store=True, track_visibility='onchange')

    state = fields.Selection([
        ('1_draft', 'Draft'),
        ('2_approval', 'Approval'),
        ('3_on_going', 'On-Going Asset Tagging'),
        ('4_done', 'Done'),
        ('5_cancelled', 'Cancelled'),

    ], 'Status', default="1_draft", track_visibility='onchange')

  
 
    def action_draft(self):
        self.ensure_one()
        self.state = '1_draft'

        email_id = self.env.ref('mega_eric.email_template_back_to_draft').id
        email = self.env['mail.template'].browse(email_id)
        email.send_mail(self.ids[0], force_send=True)

    def action_submit(self):
        self.ensure_one()
        self.state = '2_submitted'

        email_id = self.env.ref('mega_eric.email_template_project_manager_approval').id
        email = self.env['mail.template'].browse(email_id)
        email.send_mail(self.ids[0], force_send=True)

        #print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        #print(self.requested_id)
        #print(self.requested_id.email)
        #print(self.employee_id.work_email)
        #print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

    def action_approve(self):
        self.ensure_one()
        for p in self:
            p.state = '3_approved'
            p.employee_approved_date = fields.Datetime.now()

            email_id = self.env.ref('mega_eric.email_template_approved_by_project_manager').id
            email = self.env['mail.template'].browse(email_id)
            email.send_mail(self.ids[0], force_send=True)

    def action_open_bid(self):
        self.ensure_one()
        self.state = '4_open_bid'

    def action_close_bid(self):
        self.ensure_one()
        self.state = '5_close_bid'

    def action_initial_payment(self):
        self.ensure_one()
        self.state = '6_initial_payment'

    def action_fully_paid(self):
        self.ensure_one()
        self.state = '7_fully_paid'

    def action_done(self):
        self.ensure_one()
        self.state = '8_done'

  

class TransAssetDetails(models.Model):
    _name = "mwide.asset.details"
    _description = "Tagging Details"
    _order = "name"
    _inherit = ['mail.thread']
 

    trans_details_id = fields.Many2one('mwide.asset.header', 'Asset Header', readonly=True, ondelete='cascade')

    # name = fields.Many2one('res.partner', string='Name of Bidder', required=True, track_visibility='onchange',domain="[('partner_type_id.is_broker','=',True)]")
    # name = fields.Many2one('product.template', string='Barcode', track_visibility='onchange', required=True)

    name = fields.Char(string='Barcode', track_visibility='onchange', required=True)
    asset_name = fields.Char(string='Asset Name', store=True)
    date_trans = fields.Date("Date Trans.",
        required=True,
        default=fields.Date.context_today,
        copy=False,
        readonly=True)
    date_stamp = fields.Datetime('Date Stamp', default=lambda self: fields.Datetime.now(), readonly=True)        
    employee_id = fields.Many2one("res.users", required=True, readonly=True, ondelete='restrict', auto_join=True,
        string='Requested By', help='Partner-related data of the User Login',
        default=lambda self: self.env.user)
    remarks = fields.Char(string='Remarks', track_visibility='onchange')
     

    @api.onchange('name')
    def onchange_asset_id(self):
        a_name = self.env['product.template'].search([('barcode','=',self.name),], limit=1)
        for r in a_name:
            self.asset_name = r.name