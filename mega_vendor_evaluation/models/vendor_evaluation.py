#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners
from urllib import parse
from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning, UserError
from odoo.modules.module import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
import random
import logging
import time
from pickle import TRUE
import collections
import contextlib
import datetime
import dateutil
import fnmatch
import functools
import itertools
import io
import logging
import operator
import pytz
import re
import uuid
from collections import defaultdict, OrderedDict
from collections.abc import MutableMapping
from contextlib import closing
from inspect import getmembers, currentframe
from operator import attrgetter, itemgetter

import babel.dates
import dateutil.relativedelta
import psycopg2, psycopg2.extensions
from lxml import etree
from lxml.builder import E
from psycopg2.extensions import AsIs

from odoo.tools import OrderedSet


class VendorEvaluation(models.Model):
    _name = 'vendor.evaluation'
    _description = 'Vendor Evaluation'
    
    vendor_id = fields.Many2one('res.partner',string="Vendor",domain="[('is_accredited','=',True)]") 
    name = fields.Char('Name',related="vendor_id.name")
    evaluation_ids = fields.One2many('vendor.evaluation.lines','evaluation_id',string="Evaluations")
    evaluation_supplier_ids = fields.One2many('vendor.evaluation.supplier','evaluation_id',string="Evaluations")
    total_rating = fields.Float('Total Rating',compute="get_total_rating")
    address = fields.Char('Address')
    vendor_category = fields.Selection([('subcon','Sub-Contractor'),('supplier','Supplier')],string="Vendor Category",related="vendor_id.vendor_category",store=True)
    vendorcode = fields.Selection([('One Time','One Time'),('Trial Code','Trial Code'),('Permanent Code','Permanent Code')],string="Vendor Code",related="vendor_id.vendorcode",store=True)
    business_unit = fields.Selection([
        ('epc', 'EPC'),
        ('bu', 'BU'),
    ], 'Business Unit', default='bu', track_visibility='onchange')
    scm_head = fields.Many2one('res.users','SCM Head',domain=[('share','=',False)])
    
    state = fields.Selection([('draft','Draft'),('in_progress','In-Progress'),('approval','For Approval'),('done','Done')],string="State",default='draft')
    overall_status = fields.Selection([('5','Outstanding'),('4','Superior'),('3','Good'),('2','Needs Improvement'),('1','Poor')],string="Rating Summary",compute="get_status")
    result_status = fields.Selection([('pass','Passed'),('fail','Failed')],string="Status (Pass/Failed)",compute="get_pass",group_operator='min')
    
    def get_link_to_document(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action = self.env.ref('mega_vendor_evaluation.action_vendor_evaluation')
        
        db = {'db': self._cr.dbname}
        query = {
            'id'        : self.id,
            'view_type' : 'form',
            'model'     : self._name,
            'action'    : action.id,
        }
        
        path = "/web?%s#%s" % (parse.urlencode(db), parse.urlencode(query))
        full_url = base_url + path
        
        return full_url
    
    @api.depends('total_rating')
    def get_status(self):
        for rec in self:
            if rec.total_rating<2:
                rec.overall_status = '1'
            elif rec.total_rating>=2 and rec.total_rating<3:
                rec.overall_status  = '2'    
            elif rec.total_rating>=3 and rec.total_rating<4:
                rec.overall_status  = '3'
            elif rec.total_rating>=4 and rec.total_rating<5:
                rec.overall_status  = '4'
            elif rec.total_rating>=5:
                rec.overall_status  = '5'
               
    def get_pass(self):
        for rec in self:
            if rec.overall_status in ['1','2']:
                rec.result_status = 'fail'
            else:
                rec.result_status = 'pass'
    def get_total_rating(self):
        for rec in self:
            total_rating = 0
            count = 0
            if rec.vendor_category == 'subcon':
                for i in rec.evaluation_ids:
                    total_rating += i.rating
                    count +=1
                if total_rating == 0 or count == 0:
                    rec.total_rating = 0
                else:
                    rec.total_rating = total_rating / count
            elif rec.vendor_category == 'supplier':
                for i in rec.evaluation_supplier_ids:
                    total_rating += i.rating
                    count +=1
                if total_rating == 0 or count == 0:
                    rec.total_rating = 0
                else:
                    rec.total_rating = total_rating / count
    
    def print_evaluation(self):
        if self.vendor_category == 'supplier':
            return self.env.ref('mega_vendor_evaluation.vendor_evaluation_report_supplier').report_action(self.evaluation_supplier_ids)
        elif self.vendor_category == 'subcon':
            return self.env.ref('mega_vendor_evaluation.vendor_evaluation_report').report_action(self.evaluation_ids)
    
    
    @api.onchange('vendor_id')
    def _onchange_vendor(self):
        self.address = self.vendor_id.street
    
    def button_scm(self):
        if self.vendor_category == 'subcon':
            for i in self.evaluation_ids:
                if i.state != 'done':
                    raise ValidationError(_('Evaluation for Vendor %s is still in progress') % i.vendor_id.name)
        elif self.vendor_category == 'supplier':
            for i in self.evaluation_supplier_ids:
                if i.state != 'done':
                    raise ValidationError(_('Evaluation for Vendor %s is still in progress') % i.vendor_id.name)
        email = self.env.ref('mega_vendor_evaluation.email_template_approval_scm')
        if email:
            email.send_mail(self.id, force_send=True)
        print(email)
        self.state = 'approval'
    
    def button_done(self):
        if self.vendor_category == 'subcon':
            for i in self.evaluation_ids:
                if i.state != 'done':
                    raise ValidationError(_('Evaluation for Vendor %s is still in progress') % i.vendor_id.name)
        elif self.vendor_category == 'supplier':
            for i in self.evaluation_supplier_ids:
                if i.state != 'done':
                    raise ValidationError(_('Evaluation for Vendor %s is still in progress') % i.vendor_id.name)
        self.state = 'done'
    
    def button_evaluate(self):
        self.state = 'in_progress'
        question_ids = []
        for i in self.env['vendor.question'].search([]):
            list = self.env['vendor.question.list'].create({'question_id':i.id})
            question_ids.append(list.id)
        if self.vendor_id.vendor_category == 'subcon':
            return{
                'name': 'Evaluation',
                'res_model': 'vendor.evaluation.lines',
                'type': 'ir.actions.act_window',
                'context': {'default_evaluation_id': self.id,'default_question_ids':[(6,0,question_ids)],'default_vendor':self.vendor_id.id},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            }
        else:
            return{
                'name': 'Evaluation',
                'res_model': 'vendor.evaluation.supplier',
                'type': 'ir.actions.act_window',
                'context': {'default_evaluation_id': self.id,'default_question_ids':[(6,0,question_ids)],'default_vendor':self.vendor_id.id},
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new'
            } 
    
class VendorEvaluationLines(models.Model):
    _name = 'vendor.evaluation.lines'
    _description = 'Evaluations'
    
    APPROVAL_STATUS = [('pending','Pending'),('approved','Approved')]
    overall_status = fields.Selection([('5','Outstanding'),('4','Superior'),('3','Good'),('2','Needs Improvement'),('1','Poor')],string="Rating Summary",compute="get_status",store=True,group_operator='min')
    bu_selection = fields.Many2one('mega.vendor.bu',string="BU Selection")
    evaluation_id = fields.Many2one('vendor.evaluation',string="Vendor Evaluation")
    vendor_id = fields.Many2one('res.partner',string="Vendor",default=lambda self:self.env.context['default_vendor'])
    user_id = fields.Many2one('res.users','Evaluator',domain=[('share','=',False)])
    contact_person = fields.Char('Contact Person',related="vendor_id.contact_person")
    vendor_category = fields.Selection([('subcon','Sub-Contractor'),('supplier','Supplier')],string="Vendor Category",related="vendor_id.vendor_category",store=True)
    vendorcode = fields.Selection([('One Time','One Time'),('Trial Code','Trial Code'),('Permanent Code','Permanent Code')],string="Vendor Code",related="vendor_id.vendorcode",store=True)
    question_ids = fields.One2many('vendor.question.list','line_id',string="Evaluation",default=lambda self:self.env.context['default_question_ids'])
    resources_score_id = fields.Many2one('vendor.answer',string="Responsiveness")
    resources_score = fields.Float('Points',default=0,group_operator='avg')
    resources_weight = fields.Float('Rating',default=0,group_operator='avg')
    resources_eval_id = fields.Many2one('res.users','Evaluator',required=True,domain=[('share','=',False)])
    timeliness_score_id = fields.Many2one('vendor.answer',string="Timeliness")
    timeliness_score = fields.Float('Points',default=0,group_operator='avg')
    timeliness_weight = fields.Float('Rating',default=0,group_operator='avg')
    timeliness_eval_id = fields.Many2one('res.users','Evaluator',required=True,domain=[('share','=',False)])
    quality_score_id = fields.Many2one('vendor.answer',string="Quality")
    quality_score = fields.Float('Points',default=0,group_operator='avg')
    quality_weight = fields.Float('Weight',default=0,group_operator='avg')
    quality_eval_id = fields.Many2one('res.users','Evaluator',required=True,domain=[('share','=',False)])
    hsec_score_id = fields.Many2one('vendor.answer',string="HSEC")
    hsec_score = fields.Float('Points',default=0,group_operator='avg')
    hsec_weight = fields.Float('Weight',default=0,group_operator='avg')
    hsec_eval_id = fields.Many2one('res.users','Evaluator',required=True,domain=[('share','=',False)])
    response_score_id = fields.Many2one('vendor.answer',string="Reponsiveness")
    response_score = fields.Float('Points',default=0,group_operator='avg')
    response_weight = fields.Float('Weight',default=0,group_operator='avg')
    response_eval_id = fields.Many2one('res.users','Evaluator',required=True,domain=[('share','=',False)])
    rating = fields.Float('Rating',default=0,compute="_get_rating",group_operator='avg',store=True)
    evaluator_id = fields.Many2one('res.users',string="Evaluator",domain=[('share','=',False)])
    eval_date = fields.Datetime('Evaluation Date',default=fields.Datetime.now())
    supervisor_id = fields.Many2one('res.users','Project Superintendent/Supervisor',domain=[('share','=',False)])
    supervisor_status = fields.Selection(APPROVAL_STATUS,string="Supervisor Approval",default='pending')
    qaqc_id = fields.Many2one('res.users','Site QA/QC',domain=[('share','=',False)])
    qaqc_status = fields.Selection(APPROVAL_STATUS,string="QAQC Approval",default='pending')
    hsec_id = fields.Many2one('res.users','Site HSEC',domain=[('share','=',False)])
    hsec_status = fields.Selection(APPROVAL_STATUS,string="HSEC Approval",default='pending')
    pm_id = fields.Many2one('res.users','Project Manager',domain=[('share','=',False)])
    pm_status = fields.Selection(APPROVAL_STATUS,string="Project Manager Approval",default='pending')
    area_id = fields.Many2one('res.users','Area Manager',domain=[('share','=',False)])
    area_status = fields.Selection(APPROVAL_STATUS,string="Area Manager Approval",default='pending')
    enduser_id = fields.Many2one('res.users','End User',domain=[('share','=',False)])
    enduser_status = fields.Selection(APPROVAL_STATUS,string="End User Approval",default='pending')
    supervisor_date = fields.Datetime('Date Approved (Supervisor)',default=fields.Datetime.now())
    qaqc_date = fields.Datetime('Date Approved (QAQC)',default=fields.Datetime.now())
    hsec_date = fields.Datetime('Date Approved (HSEC)',default=fields.Datetime.now())
    area_date = fields.Datetime('Date Approved (Area)',default=fields.Datetime.now())
    pm_date = fields.Datetime('Date Approved (Project Manager)',default=fields.Datetime.now())
    enduser_date = fields.Datetime('Date Approved (End User)',default=fields.Datetime.now())
    wh_date = fields.Datetime('Date Approved (WH)',default=fields.Datetime.now())
    pro_date = fields.Datetime('Date Approved (Pro)',default=fields.Datetime.now())
    vm_date = fields.Datetime('Date Approved (VM)',default=fields.Datetime.now())
    fin_date = fields.Datetime('Date Approved (Finance)',default=fields.Datetime.now())
    wh_id = fields.Many2one('res.users','Warehouse',domain=[('share','=',False)])
    wh_status = fields.Selection(APPROVAL_STATUS,string="Warehouse Approval",default='pending')
    pro_id = fields.Many2one('res.users','Procurement Manager',domain=[('share','=',False)])
    pro_status = fields.Selection(APPROVAL_STATUS,string="Procurement Manager Approval",default='pending')
    vm_id = fields.Many2one('res.users','Vendor Management',domain=[('share','=',False)])
    vm_status = fields.Selection(APPROVAL_STATUS,string="Vendor Management Approval",default='pending')
    fin_id = fields.Many2one('res.users','Finance',domain=[('share','=',False)])
    fin_status = fields.Selection(APPROVAL_STATUS,string="Finance Approval",default='pending')
    is_enduser = fields.Boolean(string="Is End User",compute="get_current_enduser",default=lambda self: True if self.enduser_id == self.env.user  else False)
    is_wh= fields.Boolean(string="Is Warehouse",compute="get_current_wh",default=lambda self: True if self.wh_id == self.env.user  else False)
    is_pro = fields.Boolean(string="Is Procurement Manager",compute="get_current_pro",default=lambda self: True if self.pro_id == self.env.user  else False)
    is_vm = fields.Boolean(string="Is Vendor Management",compute="get_current_vm",default=lambda self: True if self.vm_id == self.env.user  else False)
    is_fin = fields.Boolean(string="Is Finance",compute="get_current_fin",default=lambda self: True if self.fin_id == self.env.user  else False)
    is_resources_eval_id = fields.Boolean(string="Is Resources Evaluator",compute="get_current_user_1",default=lambda self: True if self.resources_eval_id == self.env.user  else False)
    is_timeliness_eval_id = fields.Boolean(string="Is Timeliness Evaluator",compute="get_current_user_2",default=lambda self: True if self.timeliness_eval_id == self.env.user  else False)
    is_quality_eval_id = fields.Boolean(string="Is Quality Evaluator",compute="get_current_user_3",default=lambda self: True if self.quality_eval_id == self.env.user  else False)
    is_hsec_eval_id = fields.Boolean(string="Is HSEC Evaluator",compute="get_current_user_4",default=lambda self: True if self.hsec_eval_id == self.env.user  else False)
    is_response_eval_id = fields.Boolean(string="Is Response Evaluator",compute="get_current_user_5",default=lambda self: True if self.response_eval_id == self.env.user  else False)
    
    is_supervisor = fields.Boolean(string="Is Supervisor",compute="get_current_user_supervisor",default=lambda self: True if self.supervisor_id == self.env.user  else False)
    is_qaqc = fields.Boolean(string="Is QAQC",compute="get_current_user_qaqc",default=lambda self: True if self.qaqc_id == self.env.user  else False)
    is_hsec = fields.Boolean(string="Is HSEC",compute="get_current_user_hsec",default=lambda self: True if self.hsec_id == self.env.user  else False)
    is_pm = fields.Boolean(string="Is Project Manager",compute="get_current_user_pm",default=lambda self: True if self.pm_id == self.env.user  else False)
    is_area = fields.Boolean(string="Is Area Manager",compute="get_current_user_area",default=lambda self: True if self.area_id == self.env.user  else False)
    result_status = fields.Selection([('pass','Passed'),('fail','Failed')],string="Status (Pass/Failed)",compute="get_pass",store=True,group_operator='min')
    business_unit = fields.Selection([
        ('epc', 'EPC'),
        ('bu', 'BU'),
    ], 'Business Unit', track_visibility='onchange')
    name = fields.Char('Evaluator',default=lambda self:self.evaluator_id.name if (self.business_unit == 'epc') else 'BU')
    state = fields.Selection([('draft','Draft'),('in_progress','In Progress'),('approval','For Approval'),('done','Done')],string="State",default="draft")
    
    @api.onchange('evaluator_id')
    def onchange_eval_id(self):
        for rec in self:
            rec.name = rec.evaluator_id.name
            rec.resources_eval_id = rec.evaluator_id.id
            rec.timeliness_eval_id = rec.evaluator_id.id
            rec.quality_eval_id = rec.evaluator_id.id
            rec.hsec_eval_id = rec.evaluator_id.id
            rec.response_eval_id = rec.evaluator_id.id
    
    def print_evaluation(self):
        if self.vendor_category == 'supplier':
            return self.env.ref('mega_vendor_evaluation.vendor_evaluation_report_supplier').report_action(self)
        elif self.vendor_category == 'subcon':
            return self.env.ref('mega_vendor_evaluation.vendor_evaluation_report').report_action(self)
    
    @api.depends('overall_status')
    def get_pass(self):
        for rec in self:
            if rec.overall_status in ['1','2']:
                rec.result_status = 'fail'
            else:
                rec.result_status = 'pass'
    def supervisor_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_supervisor')
        if email:
            email.send_mail(self.id, force_send=True)
        self.supervisor_status = 'approved'
        self.supervisor_date = fields.Datetime.now()
    def qaqc_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_qaqc')
        if email:
            email.send_mail(self.id, force_send=True)
        self.qaqc_status = 'approved'
        self.qaqc_date = fields.Datetime.now()
    def hsec_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_hsec')
        if email:
            email.send_mail(self.id, force_send=True)
        self.hsec_status = 'approved'
        self.hsec_date = fields.Datetime.now()
    def pm_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_pm')
        if email:
            email.send_mail(self.id, force_send=True)
        self.pm_status = 'approved'
        self.pm_date = fields.Datetime.now()
    def area_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_area')
        if email:
            email.send_mail(self.id, force_send=True)
        self.area_status = 'approved'
        self.area_date = fields.Datetime.now()
        
    def enduser_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_enduser')
        if email:
            email.send_mail(self.id, force_send=True)
        self.enduser_status = 'approved'
        self.enduser_date = fields.Datetime.now()
    def wh_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_wh')
        if email:
            email.send_mail(self.id, force_send=True)
        self.wh_status = 'approved'
        self.wh_date = fields.Datetime.now()
    def pro_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_pro')
        if email:
            email.send_mail(self.id, force_send=True)
        self.pro_status = 'approved'
        self.pro_date = fields.Datetime.now()
    def vm_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_vm')
        if email:
            email.send_mail(self.id, force_send=True)
        self.vm_status = 'approved'
        self.vm_date = fields.Datetime.now()
    def fin_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_fin')
        if email:
            email.send_mail(self.id, force_send=True)
        self.fin_status = 'approved'
        self.fin_date = fields.Datetime.now()
        
    
    @api.onchange('resources_eval_id')
    def get_current_user_1(self):
        if self.resources_eval_id == self.env.user:
            self.is_resources_eval_id = True
        else:
            self.is_resources_eval_id = False
            
    @api.onchange('timeliness_eval_id')
    def get_current_user_2(self):
        if self.timeliness_eval_id == self.env.user:
            self.is_timeliness_eval_id = True
        else:
            self.is_timeliness_eval_id = False
    
    @api.onchange('quality_eval_id')
    def get_current_user_3(self):
        if self.quality_eval_id == self.env.user:
            self.is_quality_eval_id = True
        else:
            self.is_quality_eval_id = False
            
    @api.onchange('hsec_eval_id')
    def get_current_user_4(self):
        if self.hsec_eval_id == self.env.user:
            self.is_hsec_eval_id = True
        else:
            self.is_hsec_eval_id = False
            
    @api.onchange('response_eval_id')
    def get_current_user_5(self):
        if self.response_eval_id == self.env.user:
            self.is_response_eval_id = True
        else:
            self.is_response_eval_id = False
    
    def get_current_user_supervisor(self):
        if self.supervisor_id == self.env.user:
            self.is_supervisor = True
        else:
            self.is_supervisor = False
            
    def get_current_user_qaqc(self):
        if self.qaqc_id == self.env.user:
            self.is_qaqc = True
        else:
            self.is_qaqc = False
    
    def get_current_user_hsec(self):
        if self.hsec_id == self.env.user:
            self.is_hsec = True
        else:
            self.is_hsec = False
            
    def get_current_user_pm(self):
        if self.pm_id == self.env.user:
            self.is_pm = True
        else:
            self.is_pm = False
            
    def get_current_user_area(self):
        if self.area_id == self.env.user:
            self.is_area = True
        else:
            self.is_area = False
            
    def get_current_enduser(self):
        if self.enduser_id == self.env.user:
            self.is_enduser= True
        else:
            self.is_enduser = False
    
    def get_current_wh(self):
        if self.wh_id == self.env.user:
            self.is_wh= True
        else:
            self.is_wh = False
            
    def get_current_pro(self):
        if self.pro_id == self.env.user:
            self.is_pro= True
        else:
            self.is_pro = False
            
    def get_current_vm(self):
        if self.vm_id == self.env.user:
            self.is_vm= True
        else:
            self.is_vm = False
            
    def get_current_fin(self):
        if self.fin_id == self.env.user:
            self.is_fin= True
        else:
            self.is_fin = False
    
    @api.onchange('bu_selection')
    def _bu_selection_onchange(self):
        if self.bu_selection:
            self.name = self.bu_selection.name
                
    @api.onchange('resources_score_id')
    def _resources_score_id_onchange(self):
        if self.resources_score_id:
            self.resources_score = self.resources_score_id.score
            self.resources_weight = float(self.resources_score_id.score) * 0.30
    
    @api.onchange('timeliness_score_id')
    def _timeliness_score_id_onchange(self):
        if self.timeliness_score_id:
            self.timeliness_score = self.timeliness_score_id.score
            self.timeliness_weight = float(self.timeliness_score_id.score) * 0.20 
            
    @api.onchange('quality_score_id')
    def _quality_score_id_onchange(self):
        if self.quality_score_id:
            self.quality_score = self.quality_score_id.score
            self.quality_weight = float(self.quality_score_id.score) * 0.20 
            
    @api.onchange('hsec_score_id')
    def _hsec_score_id_onchange(self):
        if self.hsec_score_id:
            self.hsec_score = self.hsec_score_id.score
            self.hsec_weight = float(self.hsec_score_id.score) * 0.20 
            
    @api.onchange('response_score_id')
    def _response_score_id_onchange(self):
        if self.response_score_id:
            self.response_score = self.response_score_id.score
            self.response_weight = float(self.response_score_id.score) * 0.10 
       
    def button_submit(self):
        self.state = 'done'
    def button_draft(self):
        self.state = self.state   
    def button_invite(self):
        if self.business_unit == 'bu':
            if not self.resources_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Resources Criteria'))
            else:
                email = self.env.ref('mega_vendor_evaluation.email_template_invite_resources')
                if email:
                    email.send_mail(self.id, force_send=True)
            if not self.timeliness_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Timeliness Criteria'))
            else:
                email1 = self.env.ref('mega_vendor_evaluation.email_template_invite_timeliness')
                if email1:
                    email1.send_mail(self.id, force_send=True)
            if not self.quality_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Quality Criteria'))
            else:
                email2 = self.env.ref('mega_vendor_evaluation.email_template_invite_quality')
                if email2:
                    email2.send_mail(self.id, force_send=True)
            if not self.hsec_eval_id:
                raise ValidationError(_('Please Specify Evaluator for HSEC Criteria'))
            else:
                email3 = self.env.ref('mega_vendor_evaluation.email_template_invite_hsec')
                if email3:
                    email3.send_mail(self.id, force_send=True)
            if not self.response_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Response Criteria'))
            else:
                email4 = self.env.ref('mega_vendor_evaluation.email_template_invite_response')
                if email4:
                    email4.send_mail(self.id, force_send=True)
        elif self.business_unit == 'epc':
            if not self.evaluator_id:
                raise ValidationError(_('Please Specify Evaluator'))
            else:
                email5 = self.env.ref('mega_vendor_evaluation.email_template_invite_epc')
                if email5:
                    email5.send_mail(self.id, force_send=True)
        self.state = 'in_progress'   
    def button_approval(self):
        if not self.resources_score_id:
            raise ValidationError(_('Resources Criteria has not yet Evaluated'))
        if not self.timeliness_score_id:
            raise ValidationError(_('Timeliness Criteria has not yet Evaluated'))
        if not self.quality_score_id:
            raise ValidationError(_('Quality Criteria has not yet Evaluated'))
        if not self.hsec_score_id:
            raise ValidationError(_('HSEC Criteria has not yet Evaluated'))
        if not self.response_score_id:
            raise ValidationError(_('Response Criteria has not yet Evaluated'))
        if self.business_unit == 'epc':
            email1 = self.env.ref('mega_vendor_evaluation.email_template_approval_supervisor')
            if email1:
                email1.send_mail(self.id, force_send=True)
            email2 = self.env.ref('mega_vendor_evaluation.email_template_approval_qaqc')
            if email2:
                email2.send_mail(self.id, force_send=True)
            email3 = self.env.ref('mega_vendor_evaluation.email_template_approval_hsec')
            if email3:
                email3.send_mail(self.id, force_send=True)
            email4 = self.env.ref('mega_vendor_evaluation.email_template_approval_pm')
            if email4:
                email4.send_mail(self.id, force_send=True)
            email5 = self.env.ref('mega_vendor_evaluation.email_template_approval_area')
            if email5:
                email5.send_mail(self.id, force_send=True)
        elif self.business_unit == 'bu':
            email6 = self.env.ref('mega_vendor_evaluation.email_template_approval_enduser')
            if email6:
                email6.send_mail(self.id, force_send=True)
            email7 = self.env.ref('mega_vendor_evaluation.email_template_approval_wh')
            if email7:
                email7.send_mail(self.id, force_send=True)
            email8 = self.env.ref('mega_vendor_evaluation.email_template_approval_pro')
            if email8:
                email8.send_mail(self.id, force_send=True)
            email9 = self.env.ref('mega_vendor_evaluation.email_template_approval_fin')
            if email9:
                email9.send_mail(self.id, force_send=True)
            email10 = self.env.ref('mega_vendor_evaluation.email_template_approval_vm')
            if email10:
                email10.send_mail(self.id, force_send=True)
        self.state = 'approval'   
        
    def button_done(self):
        if self.business_unit == 'bu':
            if self.enduser_status == 'pending':
                raise ValidationError(_('Not yet approved by End User'))
            if self.wh_status == 'pending':
                raise ValidationError(_('Not yet approved by Warehouse'))
            if  self.fin_status == 'pending':
                raise ValidationError(_('Not yet approved by Finance'))
            if self.pro_status == 'pending':
                raise ValidationError(_('Not yet approved by Procurement Manager'))
            if self.vm_status == 'pending':
                raise ValidationError(_('Not yet approved by Vendor Management'))
        if self.business_unit == 'epc':
            if self.supervisor_status == 'pending':
                raise ValidationError(_('Not yet approved by Supervisor'))
            if self.qaqc_status == 'pending':
                raise ValidationError(_('Not yet approved by Site QAQC'))
            if self.hsec_status == 'pending':
                raise ValidationError(_('Not yet approved by Site HSEC'))
            if self.pm_status == 'pending':
                raise ValidationError(_('Not yet approved by Project Manager'))
            if self.area_status == 'pending':
                raise ValidationError(_('Not yet approved by Area Manager'))
        self.state = 'done'
        
    #@api.depends("response_score_id,hsec_score_id,quality_score_id,timeliness_score_id,resources_score_id")
    @api.depends('response_weight','hsec_weight','quality_weight','timeliness_weight','resources_weight')
    def _get_rating(self):
        for i in self:
            i.rating = i.response_weight + i.hsec_weight + i.quality_weight + i.timeliness_weight + i.resources_weight
    
    @api.depends('rating')
    def get_status(self):
        for rec in self:
            if rec.rating<2:
                rec.overall_status = '1'
            elif rec.rating>=2and rec.rating<3:
                rec.overall_status  = '2'    
            elif rec.rating>=3and rec.rating<4:
                rec.overall_status  = '3'
            elif rec.rating>=4 and rec.rating<5:
                rec.overall_status  = '4'
            elif rec.rating>=5:
                rec.overall_status  = '5'
     
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        """Get the list of records in list view grouped by the given ``groupby`` fields.

        :param list domain: :ref:`A search domain <reference/orm/domains>`. Use an empty
                     list to match all records.
        :param list fields: list of fields present in the list view specified on the object.
                Each element is either 'field' (field name, using the default aggregation),
                or 'field:agg' (aggregate field with aggregation function 'agg'),
                or 'name:agg(field)' (aggregate field with 'agg' and return it as 'name').
                The possible aggregation functions are the ones provided by PostgreSQL
                (https://www.postgresql.org/docs/current/static/functions-aggregate.html)
                and 'count_distinct', with the expected meaning.
        :param list groupby: list of groupby descriptions by which the records will be grouped.  
                A groupby description is either a field (then it will be grouped by that field)
                or a string 'field:groupby_function'.  Right now, the only functions supported
                are 'day', 'week', 'month', 'quarter' or 'year', and they only make sense for 
                date/datetime fields.
        :param int offset: optional number of records to skip
        :param int limit: optional max number of records to return
        :param str orderby: optional ``order by`` specification, for
                             overriding the natural sort ordering of the
                             groups, see also :py:meth:`~osv.osv.osv.search`
                             (supported only for many2one fields currently)
        :param bool lazy: if true, the results are only grouped by the first groupby and the 
                remaining groupbys are put in the __context key.  If false, all the groupbys are
                done in one call.
        :return: list of dictionaries(one dictionary for each record) containing:

                    * the values of fields grouped by the fields in ``groupby`` argument
                    * __domain: list of tuples specifying the search criteria
                    * __context: dictionary with argument like ``groupby``
        :rtype: [{'field_name_1': value, ...]
        :raise AccessError: * if user has no read rights on the requested object
                            * if user tries to bypass access rules for read on the requested object
        """
        result = self._read_group_raw(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)

        groupby = [groupby] if isinstance(groupby, str) else list(OrderedSet(groupby))
        dt = [
            f for f in groupby
            if self._fields[f.split(':')[0]].type in ('date', 'datetime')    # e.g. 'date:month'
        ]
        # iterate on all results and replace the "full" date/datetime value
        # (range, label) by just the formatted label, in-place\
        count = 0
        for group in result:
            rating = 0
            for i in result[count]:
                print(i,result[count][i])
                if i == 'rating':
                    rating = result[count][i]
                if isinstance(result[count][i], float):
                    print(result[count][i],i)
                    result[count].update({i:round(result[count][i],2)})
            if rating*20 <= 60:
                result[count].update({'overall_status':'1'})
                result[count].update({'result_status':'fail'})
            elif rating*20 > 60 and rating*20 < 75:
                result[count].update({'overall_status':'2'})   
                result[count].update({'result_status':'fail'})
            elif rating*20 >75 and rating*20 <81:
                result[count].update({'overall_status':'3'})
                result[count].update({'result_status':'pass'})
            elif rating*20 >80 and rating*20 <90:
                result[count].update({'overall_status':'4'})
                result[count].update({'result_status':'pass'})
            elif rating*20 >=90:
                result[count].update({'overall_status':'5'})
                result[count].update({'result_status':'pass'})
            print("end",result[count])
            count +=1
            for df in dt:
                print(df)
                # could group on a date(time) field which is empty in some
                # records, in which case as with m2o the _raw value will be
                # `False` instead of a (value, label) pair. In that case,
                # leave the `False` value alone
                if group.get(df):
                    group[df] = group[df][1]
        print(result)
        return result 
            
    def get_link_to_document(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action = self.env.ref('mega_vendor_evaluation.action_vendor_evaluation_form_wizard')
        
        db = {'db': self._cr.dbname}
        query = {
            'id'        : self.id,
            'view_type' : 'form',
            'model'     : self._name,
            'action'    : action.id,
        }
        
        path = "/web?%s#%s" % (parse.urlencode(db), parse.urlencode(query))
        full_url = base_url + path
        
        return full_url
    
class VendorEvaluationSupplier(models.Model):
    _name = 'vendor.evaluation.supplier'
    _description = 'Evaluations'
    

    APPROVAL_STATUS = [('pending','Pending'),('approved','Approved')]
    
    overall_status = fields.Selection([('5','Outstanding'),('4','Superior'),('3','Good'),('2','Fair'),('1','Poor')],string="Rating Summary",compute="get_status",store=True,group_operator='min')
    bu_selection = fields.Many2one('mega.vendor.bu',string="BU Selection")
    evaluation_id = fields.Many2one('vendor.evaluation',string="Vendor Evaluation")
    vendor_id = fields.Many2one('res.partner',string="Vendor",default=lambda self:self.env.context['default_vendor'])
    vendor_category = fields.Selection([('subcon','Sub-Contractor'),('supplier','Supplier')],string="Vendor Category",related="vendor_id.vendor_category",store=True)
    vendorcode = fields.Selection([('One Time','One Time'),('Trial Code','Trial Code'),('Permanent Code','Permanent Code')],string="Vendor Code",related="vendor_id.vendorcode",store=True)
    user_id = fields.Many2one('res.users','Evaluator')
    contact_person = fields.Char('Contact Person',related="vendor_id.contact_person")        
    services_total = fields.Float('Services Total Pts',compute='_get_services_total',store=True,group_operator='avg')
    services_avg = fields.Float('Services AVG',compute='_get_services_avg',store=True,group_operator='avg')
    services_weight = fields.Float('Services Weight',default=20)
    services_rating = fields.Float('Services Rating',compute='_get_services_rating',store=True,group_operator='avg')   
    feedback_rating = fields.Float('Feedback Rating',default=0)
    feedback_score_id = fields.Many2one('vendor.answer',string="Feedback")
    flex_rating = fields.Float('Flexibility Rating',default=0)
    flex_score_id = fields.Many2one('vendor.answer',string="Flexibility")
    cus_rating = fields.Float('Customer Relation Rating',default=0)
    cus_score_id = fields.Many2one('vendor.answer',string="Customer Relation")
    services_eval_id = fields.Many2one('res.users',string="Evaluator",required=True,domain=[('share','=',False)])
    quality_total = fields.Float('Quality Total Pts',compute='_get_quality_total',store=True,group_operator='avg')
    quality_avg = fields.Float('Quality AVG',compute='_get_quality_avg',store=True,group_operator='avg')
    quality_weight = fields.Float('Quality Weight',default=25)
    quality_rating = fields.Float('Quality Rating',compute='_get_quality_rating',store=True,group_operator='avg')   
    pquality_rating = fields.Float('Product Quality Rating',default=0)
    pquality_score_id = fields.Many2one('vendor.answer',string="Product Quality")
    pdurability_rating = fields.Float('Product Durability Rating',default=0)
    pdurability_score_id = fields.Many2one('vendor.answer',string="Product Durability")
    invoice_rating = fields.Float('Invoice and Documentation Rating',default=0)
    invoice_score_id = fields.Many2one('vendor.answer',string="Invoice and Documentation")
    qehs_rating = fields.Float('QEHS Rating',default=0)
    qehs_score_id = fields.Many2one('vendor.answer',string="QEHS Quality")
    quality_eval_id = fields.Many2one('res.users',string="Evaluator",required=True,domain=[('share','=',False)])
    cost_total = fields.Float('Cost Total Pts',compute='_get_cost_total',store=True,group_operator='avg')
    cost_weight = fields.Float('Cost Weight',default=30)
    cost_avg = fields.Float('Cost AVG',compute='_get_cost_avg',store=True,group_operator='avg')
    cost_rating = fields.Float('Cost Rating',compute='_get_cost_rating',store=True,group_operator='avg')
    price_rating = fields.Float('Price Competitiveness Rating',default=0)
    price_score_id = fields.Many2one('vendor.answer',string="Price Competitiveness")
    payment_rating = fields.Float('Payment Terms Rating',default=0)
    payment_score_id = fields.Many2one('vendor.answer',string="Payment Terms")
    savings_rating = fields.Float('Savings Rating',default=0)
    savings_score_id = fields.Many2one('vendor.answer',string="Savings")
    cost_eval_id = fields.Many2one('res.users',string="Evaluator",required=True,domain=[('share','=',False)])
    delivery_total = fields.Float('Delivery Total Pts',compute='_get_delivery_total',store=True,group_operator='avg')
    delivery_avg = fields.Float('Delivery AVG',compute='_get_delivery_avg',store=True,group_operator='avg')
    delivery_weight = fields.Float('Delivery Weight',default=25)
    delivery_rating = fields.Float('Delivery Rating',compute='_get_delivery_rating',store=True,group_operator='avg')   
    ontime_rating = fields.Float('On time Delivery Rating',default=0)
    ontime_score_id = fields.Many2one('vendor.answer',string="On time Delivery")
    full_rating = fields.Float('Delivery in Full Rating',default=0)
    full_score_id = fields.Many2one('vendor.answer',string="Delivery in Full")
    delivery_eval_id = fields.Many2one('res.users',string="Evaluator",required=True,domain=[('share','=',False)])
    rating = fields.Float('Rating',default=0,compute="_get_rating",group_operator='avg',store=True)
    is_services_eval_id = fields.Boolean(string="Is Services Evaluator",compute="get_current_user_2",default=lambda self: True if self.services_eval_id == self.env.user  else False)
    is_quality_eval_id = fields.Boolean(string="Is Quality Evaluator",compute="get_current_user_3",default=lambda self: True if self.quality_eval_id == self.env.user  else False)
    is_cost_eval_id = fields.Boolean(string="Is Cost Evaluator",compute="get_current_user_4",default=lambda self: True if self.cost_eval_id == self.env.user  else False)
    is_delivery_eval_id = fields.Boolean(string="Is Delivery Evaluator",compute="get_current_user_5",default=lambda self: True if self.delivery_eval_id == self.env.user  else False)
    evaluator_id = fields.Many2one('res.users',string="Evaluator",domain=[('share','=',False)])
    eval_date = fields.Datetime('Evaluation Date',default=fields.Datetime.now())
    supervisor_id = fields.Many2one('res.users','Project Superintendent/Supervisor',domain=[('share','=',False)])
    supervisor_status = fields.Selection(APPROVAL_STATUS,string="Supervisor Approval",default='pending')
    qaqc_id = fields.Many2one('res.users','Site QA/QC',domain=[('share','=',False)])
    qaqc_status = fields.Selection(APPROVAL_STATUS,string="QAQC Approval",default='pending')
    hsec_id = fields.Many2one('res.users','Site HSEC',domain=[('share','=',False)])
    hsec_status = fields.Selection(APPROVAL_STATUS,string="HSEC Approval",default='pending')
    pm_id = fields.Many2one('res.users','Project Manager',domain=[('share','=',False)])
    pm_status = fields.Selection(APPROVAL_STATUS,string="Project Manager Approval",default='pending')
    area_id = fields.Many2one('res.users','Area Manager',domain=[('share','=',False)])
    area_status = fields.Selection(APPROVAL_STATUS,string="Area Manager Approval",default='pending')
    enduser_id = fields.Many2one('res.users','End User',domain=[('share','=',False)])
    enduser_status = fields.Selection(APPROVAL_STATUS,string="End User Approval",default='pending')
    wh_id = fields.Many2one('res.users','Warehouse',domain=[('share','=',False)])
    wh_status = fields.Selection(APPROVAL_STATUS,string="Warehouse Approval",default='pending')
    pro_id = fields.Many2one('res.users','Procurement Manager',domain=[('share','=',False)])
    pro_status = fields.Selection(APPROVAL_STATUS,string="Procurement Manager Approval",default='pending')
    vm_id = fields.Many2one('res.users','Vendor Management',domain=[('share','=',False)])
    vm_status = fields.Selection(APPROVAL_STATUS,string="Vendor Management Approval",default='pending')
    fin_id = fields.Many2one('res.users','Finance',domain=[('share','=',False)])
    fin_status = fields.Selection(APPROVAL_STATUS,string="Finance Approval",default='pending')
    is_enduser = fields.Boolean(string="Is End User",compute="get_current_enduser",default=lambda self: True if self.enduser_id == self.env.user  else False)
    is_wh= fields.Boolean(string="Is Warehouse",compute="get_current_wh",default=lambda self: True if self.wh_id == self.env.user  else False)
    is_pro = fields.Boolean(string="Is Procurement Manager",compute="get_current_pro",default=lambda self: True if self.pro_id == self.env.user  else False)
    is_vm = fields.Boolean(string="Is Vendor Management",compute="get_current_vm",default=lambda self: True if self.vm_id == self.env.user  else False)
    is_fin = fields.Boolean(string="Is Finance",compute="get_current_fin",default=lambda self: True if self.fin_id == self.env.user  else False)
    is_supervisor = fields.Boolean(string="Is Supervisor",compute="get_current_user_supervisor",default=lambda self: True if self.supervisor_id == self.env.user  else False)
    is_qaqc = fields.Boolean(string="Is QAQC",compute="get_current_user_qaqc",default=lambda self: True if self.qaqc_id == self.env.user  else False)
    is_hsec = fields.Boolean(string="Is HSEC",compute="get_current_user_hsec",default=lambda self: True if self.hsec_id == self.env.user  else False)
    is_pm = fields.Boolean(string="Is Project Manager",compute="get_current_user_pm",default=lambda self: True if self.pm_id == self.env.user  else False)
    is_area = fields.Boolean(string="Is Area Manager",compute="get_current_user_area",default=lambda self: True if self.area_id == self.env.user  else False)
    result_status = fields.Selection([('pass','Passed'),('fail','Failed')],string="Status (Pass/Failed)",compute="get_pass",store=True,group_operator='min')
    supervisor_date = fields.Datetime('Date Approved (Supervisor)',default=fields.Datetime.now())    
    qaqc_date = fields.Datetime('Date Approved (QAQC)',default=fields.Datetime.now())
    hsec_date = fields.Datetime('Date Approved (HSEC)',default=fields.Datetime.now())
    area_date = fields.Datetime('Date Approved (Area)',default=fields.Datetime.now())
    pm_date = fields.Datetime('Date Approved (Project Manager)',default=fields.Datetime.now())
    enduser_date = fields.Datetime('Date Approved (End User)',default=fields.Datetime.now())
    wh_date = fields.Datetime('Date Approved (WH)',default=fields.Datetime.now())
    pro_date = fields.Datetime('Date Approved (Pro)',default=fields.Datetime.now())
    vm_date = fields.Datetime('Date Approved (VM)',default=fields.Datetime.now())
    fin_date = fields.Datetime('Date Approved (Finance)',default=fields.Datetime.now())
    business_unit = fields.Selection([
        ('epc', 'EPC'),
        ('bu', 'BU'),
    ], 'Business Unit', track_visibility='onchange')
    name = fields.Char('Evaluator',default=lambda self:self.evaluator_id.name if (self.business_unit == 'epc') else 'BU')
    state = fields.Selection([('draft','Draft'),('in_progress','In Progress'),('approval','For Approval'),('done','Done')],string="State",default="draft")
    
    def print_evaluation(self):
        if self.vendor_category == 'supplier':
            return self.env.ref('mega_vendor_evaluation.vendor_evaluation_report_supplier').report_action(self)
        elif self.vendor_category == 'subcon':
            return self.env.ref('mega_vendor_evaluation.vendor_evaluation_report').report_action(self)
    
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        """Get the list of records in list view grouped by the given ``groupby`` fields.

        :param list domain: :ref:`A search domain <reference/orm/domains>`. Use an empty
                     list to match all records.
        :param list fields: list of fields present in the list view specified on the object.
                Each element is either 'field' (field name, using the default aggregation),
                or 'field:agg' (aggregate field with aggregation function 'agg'),
                or 'name:agg(field)' (aggregate field with 'agg' and return it as 'name').
                The possible aggregation functions are the ones provided by PostgreSQL
                (https://www.postgresql.org/docs/current/static/functions-aggregate.html)
                and 'count_distinct', with the expected meaning.
        :param list groupby: list of groupby descriptions by which the records will be grouped.  
                A groupby description is either a field (then it will be grouped by that field)
                or a string 'field:groupby_function'.  Right now, the only functions supported
                are 'day', 'week', 'month', 'quarter' or 'year', and they only make sense for 
                date/datetime fields.
        :param int offset: optional number of records to skip
        :param int limit: optional max number of records to return
        :param str orderby: optional ``order by`` specification, for
                             overriding the natural sort ordering of the
                             groups, see also :py:meth:`~osv.osv.osv.search`
                             (supported only for many2one fields currently)
        :param bool lazy: if true, the results are only grouped by the first groupby and the 
                remaining groupbys are put in the __context key.  If false, all the groupbys are
                done in one call.
        :return: list of dictionaries(one dictionary for each record) containing:

                    * the values of fields grouped by the fields in ``groupby`` argument
                    * __domain: list of tuples specifying the search criteria
                    * __context: dictionary with argument like ``groupby``
        :rtype: [{'field_name_1': value, ...]
        :raise AccessError: * if user has no read rights on the requested object
                            * if user tries to bypass access rules for read on the requested object
        """
        result = self._read_group_raw(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)

        groupby = [groupby] if isinstance(groupby, str) else list(OrderedSet(groupby))
        dt = [
            f for f in groupby
            if self._fields[f.split(':')[0]].type in ('date', 'datetime')    # e.g. 'date:month'
        ]

        # iterate on all results and replace the "full" date/datetime value
        # (range, label) by just the formatted label, in-place
        count = 0 
        for group in result:
            rating = 0
            for i in result[count]:
                print(i,result[count][i])
                if i == 'rating':
                    rating = result[count][i]
                if isinstance(result[count][i], float):
                    print(result[count][i],i)
                    result[count].update({i:round(result[count][i],2)})
            if rating*20 <= 50:
                result[count].update({'overall_status':'1'})
                result[count].update({'result_status':'fail'})
            elif rating*20 > 50 and rating*20 < 65:
                result[count].update({'overall_status':'2'})   
                result[count].update({'result_status':'fail'})
            elif rating*20 >65 and rating*20 <81:
                result[count].update({'overall_status':'3'})
                result[count].update({'result_status':'pass'})
            elif rating*20 >80 and rating*20 <90:
                result[count].update({'overall_status':'4'})
                result[count].update({'result_status':'pass'})
            elif rating*20 >=90:
                result[count].update({'overall_status':'5'})
                result[count].update({'result_status':'pass'})
            print("end",result[count])
            count +=1
            for df in dt:
                # could group on a date(time) field which is empty in some
                # records, in which case as with m2o the _raw value will be
                # `False` instead of a (value, label) pair. In that case,
                # leave the `False` value alone
                if group.get(df):
                    group[df] = group[df][1]
        return result
    
    @api.onchange('bu_selection')
    def _bu_selection_onchange(self):
        if self.bu_selection:
            self.name = self.bu_selection.name
    
    @api.depends('overall_status')
    def get_pass(self):
        for rec in self:
            if rec.overall_status in ['1','2']:
                rec.result_status = 'fail'
            else:
                rec.result_status = 'pass'
    @api.onchange('evaluator_id')
    def onchange_eval_id(self):
        for rec in self:
            rec.name = rec.evaluator_id.name
            rec.services_eval_id = rec.evaluator_id.id
            rec.quality_eval_id = rec.evaluator_id.id
            rec.cost_eval_id = rec.evaluator_id.id
            rec.delivery_eval_id = rec.evaluator_id.id
    
    def supervisor_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_supervisor_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.supervisor_status = 'approved'
        self.supervisor_date = fields.Datetime.now()
    def qaqc_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_qaqc_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.qaqc_status = 'approved'
        self.qaqc_date = fields.Datetime.now()
    def hsec_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_hsec_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.hsec_status = 'approved'
        self.hsec_date = fields.Datetime.now()
    def pm_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_pm_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.pm_status = 'approved'
        self.pm_date = fields.Datetime.now()
    def area_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_area_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.area_status = 'approved'
        self.area_date = fields.Datetime.now()
        
    def enduser_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_enduser_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.enduser_date = fields.Datetime.now()    
        self.enduser_status = 'approved'
        self.enduser_date = fields.Datetime.now()
    def wh_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_wh_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.wh_status = 'approved'
        self.wh_date = fields.Datetime.now()
    def pro_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_pro_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.pro_status = 'approved'
        self.pro_date = fields.Datetime.now()
    def vm_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_vm_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.vm_status = 'approved'
        self.vm_date = fields.Datetime.now()
    def fin_approval(self):
        email = self.env.ref('mega_vendor_evaluation.email_template_approved_fin_supplier')
        if email:
            email.send_mail(self.id, force_send=True)
        self.fin_status = 'approved'
        self.fin_date = fields.Datetime.now()
    
    @api.onchange('feedback_score_id')
    def _feedback_id_onchange(self):
        if self.feedback_score_id:
            self.feedback_rating = self.feedback_score_id.score
    @api.onchange('flex_score_id')
    def _flex_id_onchange(self):
        if self.flex_score_id:
            self.flex_rating = self.flex_score_id.score
    @api.onchange('cus_score_id')
    def _cus_id_onchange(self):
        if self.cus_score_id:
            self.cus_rating = self.cus_score_id.score
    
    @api.depends('feedback_rating','flex_rating','cus_rating')
    def _get_services_total(self):
        for rec in self:
            rec.services_total = rec.feedback_rating + rec.flex_rating + rec.cus_rating
      
    @api.depends('feedback_rating','flex_rating','cus_rating')
    def _get_services_avg(self):
        for rec in self:
            rec.services_avg = (rec.feedback_rating + rec.flex_rating + rec.cus_rating)/3
    
    @api.depends('feedback_rating','flex_rating','cus_rating')
    def _get_services_rating(self):
        for rec in self:
            rec.services_rating = ((rec.feedback_rating + rec.flex_rating + rec.cus_rating)/3)*(rec.services_weight/100)
            
    @api.onchange('pquality_score_id')
    def _pquality_id_onchange(self):
        if self.pquality_score_id:
            self.pquality_rating = self.pquality_score_id.score
    @api.onchange('pdurability_score_id')
    def _pdurability_id_onchange(self):
        if self.pdurability_score_id:
            self.pdurability_rating = self.pdurability_score_id.score
    @api.onchange('invoice_score_id')
    def _invoice_id_onchange(self):
        if self.invoice_score_id:
            self.invoice_rating = self.invoice_score_id.score    
    @api.onchange('qehs_score_id')
    def _qehs_id_onchange(self):
        if self.qehs_score_id:
            self.qehs_rating = self.qehs_score_id.score      
            
    @api.depends('pquality_rating','pdurability_rating','invoice_rating','qehs_rating')
    def _get_quality_total(self):
        for rec in self:
            rec.quality_total = rec.pquality_rating + rec.pdurability_rating + rec.invoice_rating + rec.qehs_rating
    @api.depends('pquality_rating','pdurability_rating','invoice_rating','qehs_rating')
    def _get_quality_avg(self):
        for rec in self:
            rec.quality_avg = (rec.pquality_rating + rec.pdurability_rating + rec.invoice_rating + rec.qehs_rating)/4
    @api.depends('pquality_rating','pdurability_rating','invoice_rating','qehs_rating')
    def _get_quality_rating(self):
        for rec in self:
            rec.quality_rating = ((rec.pquality_rating + rec.pdurability_rating + rec.invoice_rating + rec.qehs_rating)/4)*(rec.quality_weight/100)
    
    @api.onchange('price_score_id')
    def _price_id_onchange(self):
        if self.price_score_id:
            self.price_rating = self.price_score_id.score
    @api.onchange('payment_score_id')
    def _payment_id_onchange(self):
        if self.payment_score_id:
            self.payment_rating = self.payment_score_id.score
    @api.onchange('savings_score_id')
    def _savings_id_onchange(self):
        if self.savings_score_id:
            self.savings_rating = self.savings_score_id.score
    
    @api.depends('price_rating','payment_rating','savings_rating')
    def _get_cost_total(self):
        for rec in self:
            rec.cost_total = rec.price_rating + rec.payment_rating + rec.savings_rating
    @api.depends('price_rating','payment_rating','savings_rating')
    def _get_cost_avg(self):
        for rec in self:
            rec.cost_avg = (rec.price_rating + rec.payment_rating + rec.savings_rating)/3
    @api.depends('price_rating','payment_rating','savings_rating')
    def _get_cost_rating(self):
        for rec in self:
            rec.cost_rating = ((rec.price_rating + rec.payment_rating + rec.savings_rating)/3)*(rec.cost_weight/100)
    
    @api.onchange('ontime_score_id')
    def _ontime_id_onchange(self):
        if self.ontime_score_id:
            self.ontime_rating = self.ontime_score_id.score
    @api.onchange('full_score_id')
    def _full_id_onchange(self):
        if self.full_score_id:
            self.full_rating = self.full_score_id.score
    
    @api.depends('ontime_rating','full_rating')
    def _get_delivery_total(self):
        for rec in self:
            rec.delivery_total = rec.ontime_rating + rec.full_rating
    @api.depends('ontime_rating','full_rating')
    def _get_delivery_avg(self):
        for rec in self:
            rec.delivery_avg = (rec.ontime_rating + rec.full_rating)/2
    @api.depends('ontime_rating','full_rating')
    def _get_delivery_rating(self):
        for rec in self:
            rec.delivery_rating = ((rec.ontime_rating + rec.full_rating)/2)*(rec.delivery_weight/100)
            
    @api.depends('services_rating','quality_rating','cost_rating','delivery_rating')        
    def _get_rating(self):
        for i in self:
            i.rating = i.services_rating + i.quality_rating + i.cost_rating + i.delivery_rating
            
    @api.onchange('services_eval_id')
    def get_current_user_2(self):
        if self.services_eval_id == self.env.user:
            self.is_services_eval_id = True
        else:
            self.is_services_eval_id = False
    @api.onchange('quality_eval_id')
    def get_current_user_3(self):
        if self.quality_eval_id == self.env.user:
            self.is_quality_eval_id = True
        else:
            self.is_quality_eval_id = False
    @api.onchange('cost_eval_id')
    def get_current_user_4(self):
        if self.cost_eval_id == self.env.user:
            self.is_cost_eval_id = True
        else:
            self.is_cost_eval_id = False
    @api.onchange('delivery_eval_id')
    def get_current_user_5(self):
        if self.delivery_eval_id == self.env.user:
            self.is_delivery_eval_id = True
        else:
            self.is_delivery_eval_id = False
    def get_current_user_supervisor(self):
        if self.supervisor_id == self.env.user:
            self.is_supervisor = True
        else:
            self.is_supervisor = False
            
    def get_current_user_qaqc(self):
        if self.qaqc_id == self.env.user:
            self.is_qaqc = True
        else:
            self.is_qaqc = False
    
    def get_current_user_hsec(self):
        if self.hsec_id == self.env.user:
            self.is_hsec = True
        else:
            self.is_hsec = False
            
    def get_current_user_pm(self):
        if self.pm_id == self.env.user:
            self.is_pm = True
        else:
            self.is_pm = False
            
    def get_current_user_area(self):
        if self.area_id == self.env.user:
            self.is_area = True
        else:
            self.is_area = False 

    def get_current_enduser(self):
        if self.enduser_id == self.env.user:
            self.is_enduser= True
        else:
            self.is_enduser = False
    
    def get_current_wh(self):
        if self.wh_id == self.env.user:
            self.is_wh= True
        else:
            self.is_wh = False
            
    def get_current_pro(self):
        if self.pro_id == self.env.user:
            self.is_pro= True
        else:
            self.is_pro = False
            
    def get_current_vm(self):
        if self.vm_id == self.env.user:
            self.is_vm= True
        else:
            self.is_vm = False
            
    def get_current_fin(self):
        if self.fin_id == self.env.user:
            self.is_fin= True
        else:
            self.is_fin = False

    @api.depends('rating')
    def get_status(self):
        for rec in self:
            if rec.rating<2:
                rec.overall_status = '1'
            elif rec.rating>=2and rec.rating<3:
                rec.overall_status  = '2'    
            elif rec.rating>=3 and rec.rating<4:
                rec.overall_status  = '3'
            elif rec.rating>=4 and rec.rating<5:
                rec.overall_status  = '4'
            elif rec.rating>=5:
                rec.overall_status  = '5'

    def get_link_to_document(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action = self.env.ref('mega_vendor_evaluation.action_vendor_evaluation_supplier_form_wizard')
        
        db = {'db': self._cr.dbname}
        query = {
            'id'        : self.id,
            'view_type' : 'form',
            'model'     : self._name,
            'action'    : action.id,
        }
        
        path = "/web?%s#%s" % (parse.urlencode(db), parse.urlencode(query))
        full_url = base_url + path
        
        return full_url
    
    def button_draft(self):
        self.state = self.state   
    def button_invite(self):
        if self.business_unit == 'bu':
            if not self.services_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Services Criteria'))
            else:
                email = self.env.ref('mega_vendor_evaluation.email_template_invite_services')
                if email:
                    email.send_mail(self.id, force_send=True)
            if not self.quality_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Quality Criteria'))
            else:
                email1 = self.env.ref('mega_vendor_evaluation.email_template_invite_quality_supplier')
                if email1:
                    email1.send_mail(self.id, force_send=True)
            if not self.cost_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Cost Criteria'))
            else:
                email2 = self.env.ref('mega_vendor_evaluation.email_template_invite_cost')
                if email2:
                    email2.send_mail(self.id, force_send=True)
            if not self.delivery_eval_id:
                raise ValidationError(_('Please Specify Evaluator for Delivery Criteria'))
            else:
                email3 = self.env.ref('mega_vendor_evaluation.email_template_invite_delivery')
                if email3:
                    email3.send_mail(self.id, force_send=True)
        elif self.business_unit == 'epc':
            if not self.evaluator_id:
                raise ValidationError(_('Please Specify Evaluator'))
            else:
                email5 = self.env.ref('mega_vendor_evaluation.email_template_invite_epc_supplier')
                if email5:
                    email5.send_mail(self.id, force_send=True)
        self.state = 'in_progress'   
    def button_approval(self):
        if not self.feedback_score_id:
            raise ValidationError(_('Services - Feedback has not yet Evaluated'))
        if not self.flex_score_id:
            raise ValidationError(_('Services - Flexibility has not yet Evaluated'))
        if not self.cus_score_id:
            raise ValidationError(_('Services - Customer Relations has not yet Evaluated'))
        if not self.pquality_score_id:
            raise ValidationError(_('Quality - Product Quality has not yet Evaluated'))
        if not self.pdurability_score_id:
            raise ValidationError(_('Quality - Product Durability has not yet Evaluated'))
        if not self.invoice_score_id:
            raise ValidationError(_('Quality - Invoice and Documentation has not yet Evaluated'))
        if not self.qehs_score_id:
            raise ValidationError(_('Quality - QEHS has not yet Evaluated'))
        if not self.price_score_id:
            raise ValidationError(_('Cost - Price Competitiveness has not yet Evaluated'))
        if not self.payment_score_id:
            raise ValidationError(_('Cost - Payment Terms has not yet Evaluated'))
        if not self.savings_score_id:
            raise ValidationError(_('Cost - Savings has not yet Evaluated'))
        if not self.ontime_score_id:
            raise ValidationError(_('Delivery - On time has not yet Evaluated'))
        if not self.full_score_id:
            raise ValidationError(_('Delivery -  Full Delivery has not yet Evaluated'))
        if self.business_unit == 'epc':
            email1 = self.env.ref('mega_vendor_evaluation.email_template_approval_supervisor_supplier')
            if email1:
                email1.send_mail(self.id, force_send=True)
            email2 = self.env.ref('mega_vendor_evaluation.email_template_approval_qaqc_supplier')
            if email2:
                email2.send_mail(self.id, force_send=True)
            email3 = self.env.ref('mega_vendor_evaluation.email_template_approval_hsec_supplier')
            if email3:
                email3.send_mail(self.id, force_send=True)
            email4 = self.env.ref('mega_vendor_evaluation.email_template_approval_pm_supplier')
            if email4:
                email4.send_mail(self.id, force_send=True)
            email5 = self.env.ref('mega_vendor_evaluation.email_template_approval_area_supplier')
            if email5:
                email5.send_mail(self.id, force_send=True)
        elif self.business_unit == 'bu':
            email6 = self.env.ref('mega_vendor_evaluation.email_template_approval_enduser_supplier')
            if email6:
                email6.send_mail(self.id, force_send=True)
            email7 = self.env.ref('mega_vendor_evaluation.email_template_approval_wh_supplier')
            if email7:
                email7.send_mail(self.id, force_send=True)
            email8 = self.env.ref('mega_vendor_evaluation.email_template_approval_pro_supplier')
            if email8:
                email8.send_mail(self.id, force_send=True)
            email9 = self.env.ref('mega_vendor_evaluation.email_template_approval_fin_supplier')
            if email9:
                email9.send_mail(self.id, force_send=True)
            email10 = self.env.ref('mega_vendor_evaluation.email_template_approval_vm_supplier')
            if email10:
                email10.send_mail(self.id, force_send=True)
        self.state = 'approval'   
    def button_done(self):
        if self.business_unit == 'bu':
            if self.enduser_status == 'pending':
                raise ValidationError(_('Not yet approved by End User'))
            if self.wh_status == 'pending':
                raise ValidationError(_('Not yet approved by Warehouse'))
            if self.fin_status == 'pending':
                raise ValidationError(_('Not yet approved by Finance'))
            if self.pro_status == 'pending':
                raise ValidationError(_('Not yet approved by Procurement Manager'))
            if self.vm_status == 'pending':
                raise ValidationError(_('Not yet approved by Vendor Management'))
        if self.business_unit == 'epc':
            if self.supervisor_status == 'pending':
                raise ValidationError(_('Not yet approved by Supervisor'))
            if self.qaqc_status == 'pending':
                raise ValidationError(_('Not yet approved by Site QAQC'))
            if self.hsec_status == 'pending':
                raise ValidationError(_('Not yet approved by Site HSEC'))
            if self.pm_status == 'pending':
                raise ValidationError(_('Not yet approved by Project Manager'))
            if self.area_status == 'pending':
                raise ValidationError(_('Not yet approved by Area Manager'))
        self.state = 'done'

class VendorQuestionList(models.Model):
    _name = 'vendor.question.list'
    _description = 'Evaluation'
    
    line_id = fields.Many2one('vendor.evaluation.lines',string="Line")
    question_id = fields.Many2one('vendor.question',string="Criteria") 
    answer_id = fields.Many2one('vendor.answer',string="Rating")
    score = fields.Float('Points',default=0,store=True)
    weight = fields.Float('Rating',default=0,store=True)
    eval_date = fields.Datetime('Evaluation Date')


    @api.onchange('answer_id')
    def _answer_onchange(self):
        if self.answer_id:
            self.score = self.answer_id.score
            self.weight = float(self.answer_id.score) * float(self.question_id.percentage)
    
class VendorQuestion(models.Model):
    _name = 'vendor.question'
    _description = 'Vendor Evaluation Criteria'
    
    name = fields.Char('Criteria',required=True)
    percentage = fields.Float('Percentage',required=True,default=0)
    
class VendorAnswers(models.Model):
    _name = 'vendor.answer'
    _description = 'Criteria Answers'
    
    name = fields.Char('Answer',required=True)   
    score = fields.Char('Points (1-5)',required=True,default=0)
    
    