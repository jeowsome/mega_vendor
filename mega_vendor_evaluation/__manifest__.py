# -*- coding: utf-8 -*-
{
    'name': "MEGA-Vendor Accreditation Project",

    'summary': """
        Vendor Portal Evaluation""",

    'description': """
        Megawide Vendor Accreditation Evaluation
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Information Technology',
    'version': '1.0.0',
    'sequence': -301,

    # any module necessary for this one to work correctly
    'depends': ['mega_vendor'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/ir.model.access.csv',
        'data/vendor_evaluation_masterdata.xml',
        'data/vendor_evaluation_email_templates.xml',
        'views/vendor_evaluation_views.xml',
        'report/vendor_evaluation_report_template.xml',
        'report/report.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],    
}
