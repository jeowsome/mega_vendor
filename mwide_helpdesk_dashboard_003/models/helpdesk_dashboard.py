# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
import pandas as pd
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils
from odoo.exceptions import ValidationError, Warning

ROUNDING_FACTOR = 16
DATE_FILTER = [('ticket_date','!=',False),('ticket_date','!=',False)]

class MwideHelpdeskDashboard(models.Model):
    _inherit = 'mwide.helpdesk'

    # @api.model
    # def get_date_range_selector(self,range):
    #     date_now = fields.Datetime.now()
    #     date_range = date_now - relativedelta(months=range)
    #     condition1 = ('ticket_date','>=',date_range)
    #     condition2=('ticket_date','<=',date_now)
    #     if range ==0:
    #         condition1 = ('ticket_date','!=',False)
    #         condition2=('ticket_date','!=',False)
    #     global DATE_FILTER
    #     DATE_FILTER = [condition1,condition2]
    #     return DATE_FILTER
    #     #return {
    #     #    'type':'ir.actions.client',
    #     #    'name':'custom_helpdesk_dashboard_tag',
    #     #    'tag':'custom_helpdesk_dashboard_tag tag',
    #     #    'params': {'DATE_FILTER':DATE_FILTER},
    #     #}
    #
    # @api.model
    # def get_date_date_selector(self,from_date,to_date):
    #     if from_date == '' or to_date == '':
    #         raise ValidationError(_('You need to set both dates'))
    #     if datetime.strptime(from_date,'%Y-%m-%d') > datetime.strptime(to_date,'%Y-%m-%d'):
    #         raise ValidationError(_('Invalid input! From Date cannot be greater than To Date'))
    #     condition1 = ('ticket_date','>=', datetime.strptime(from_date,'%Y-%m-%d'))
    #     condition2=('ticket_date','<=',datetime.strptime(to_date,'%Y-%m-%d'))
    #     global DATE_FILTER
    #     DATE_FILTER=[condition1,condition2]
    #     print(DATE_FILTER)
    #     #return {
    #     #    'type': 'ir.actions.client',
    #     #    'tag':'custom_helpdesk_dashboard_tag'
    #     #}
    #     return DATE_FILTER
    #
    #
    # @api.model
    # def get_closed_tickets(self):
    #     print(DATE_FILTER,"DATE_FILTER")
    #     closed_tickets = self.env['mwide.helpdesk'].sudo().search_count(['&','&',DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state', '=', '09resolved')]) or 0
    #     print(closed_tickets,"closed")
    #     return {
    #         'closed_tickets': closed_tickets
    #     }
    #
    # @api.model
    # def get_overdue_tickets(self):
    #     overdue_tickets = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],('is_sla','=',True),'&',('type','=','it'),('html_color','in',["#FEB580","#F78585"])]) or 0
    #     return {
    #         'overdue_tickets': overdue_tickets or 0
    #     }
    #
    # @api.model
    # def get_total_tickets(self):
    #     total_tickets = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state', '!=', '09resolved')]) or 0
    #     return {
    #         'total_tickets': total_tickets
    #     }
    #
    # @api.model
    # def get_open_tickets(self):
    #
    #     open_tickets = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),'|','&',('state','=','04approved_it'),('approval_selection','=','it_manager'),'|','&',('state_b','=','03approved_head'),('approval_selection','=','manager'),'&',('state_c','=','02request'),('approval_selection','=','none')]) or 0
    #     return {
    #         'open_tickets': open_tickets or 0
    #     }
    #
    # @api.model
    # def get_for_approval(self):
    #     for_approval = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),'|','&',('state','=','03approved_head'),('approval_selection','=','it_manager'),'&',('state_b','=','02request'),('approval_selection','=','manager')]) or 0
    #     return {
    #         'for_approval': for_approval or 0
    #     }
    #
    # @api.model
    # def get_assigned(self):
    #     assigned = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state','=','05assigned')]) or 0
    #     return {
    #         'assigned': assigned or 0
    #     }
    #
    # @api.model
    # def get_onhold(self):
    #     onhold = 0
    #     onhold = self.env['mwide.helpdesk'].sudo().search_count([DATE_FILTER[0],DATE_FILTER[1],'&',('type','=','it'),('state','=','06on_hold')])
    #     return {
    #         'onhold': onhold or 0
    #     }
    #
    #
    #
    #
    # @api.model
    # def get_open_tickets_access(self):
    #
    #     open_tickets_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_access': open_tickets_access or 0
    #     }
    #
    # @api.model
    # def get_for_approval_access(self):
    #
    #     for_approval_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_access': for_approval_access or 0
    #     }
    #
    # @api.model
    # def get_draft_access(self):
    #
    #     draft_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),'|',('state','=','01draft')])
    #     return {
    #         'draft_access': draft_access or 0
    #     }
    #
    # @api.model
    # def get_all_access(self):
    #
    #     all_access = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Access Management')])
    #     return {
    #         'all_access': all_access or 0
    #     }
    #
    # @api.model
    # def get_assigned_access(self):
    #     assigned_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),('state','=','05routed')])
    #     return {
    #         'assigned_access': assigned_access or 0
    #     }
    #
    # @api.model
    # def get_inprogress_access(self):
    #     inprogress_access = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Access Management'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_access': inprogress_access or 0
    #     }
    #
    #
    # @api.model
    # def get_open_tickets_application(self):
    #
    #     open_tickets_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_application': open_tickets_application or 0
    #     }
    #
    # @api.model
    # def get_for_approval_application(self):
    #
    #     for_approval_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_application': for_approval_application or 0
    #     }
    #
    # @api.model
    # def get_draft_application(self):
    #
    #     draft_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),'|',('state','=','01draft')])
    #     return {
    #         'draft_application': draft_application or 0
    #     }
    #
    # @api.model
    # def get_all_application(self):
    #
    #     all_application = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Application Management')])
    #     return {
    #         'all_application': all_application or 0
    #     }
    #
    # @api.model
    # def get_assigned_application(self):
    #     assigned_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),('state','=','05routed')])
    #     return {
    #         'assigned_application': assigned_application or 0
    #     }
    #
    # @api.model
    # def get_inprogress_application(self):
    #     inprogress_application = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Application Management'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_application': inprogress_application or 0
    #     }
    #
    #
    # @api.model
    # def get_open_tickets_it_clearance(self):
    #
    #     open_tickets_it_clearance= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_it_clearance': open_tickets_it_clearance or 0
    #     }
    #
    # @api.model
    # def get_for_approval_it_clearance(self):
    #
    #     for_approval_it_clearance= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_it_clearance': for_approval_it_clearance or 0
    #     }
    #
    # @api.model
    # def get_draft_it_clearance(self):
    #
    #     draft_it_clearance = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),'|',('state','=','01draft')])
    #     return {
    #         'draft_it_clearance': draft_it_clearance or 0
    #     }
    #
    # @api.model
    # def get_all_it_clearance(self):
    #
    #     all_it_clearance = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','IT Clearance')])
    #     return {
    #         'all_it_clearance': all_it_clearance  or 0
    #     }
    #
    # @api.model
    # def get_assigned_it_clearance(self):
    #     assigned_it_clearance = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),('state','=','05routed')])
    #     return {
    #         'assigned_it_clearance': assigned_it_clearance or 0
    #     }
    #
    # @api.model
    # def get_inprogress_it_clearance(self):
    #     inprogress_it_clearance = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT Clearance'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_it_clearance': inprogress_it_clearance or 0
    #     }
    #
    # @api.model
    # def get_open_tickets_network(self):
    #
    #     open_tickets_network= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_network': open_tickets_network or 0
    #     }
    #
    # @api.model
    # def get_for_approval_network(self):
    #
    #     for_approval_network= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_network': for_approval_network or 0
    #     }
    #
    # @api.model
    # def get_draft_network(self):
    #
    #     draft_network = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),'|',('state','=','01draft')])
    #     return {
    #         'draft_network': draft_network or 0
    #     }
    #
    # @api.model
    # def get_all_network(self):
    #
    #     all_network = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Network')])
    #     return {
    #         'all_network': all_network or 0
    #     }
    #
    # @api.model
    # def get_assigned_network(self):
    #     assigned_network = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),('state','=','05routed')])
    #     return {
    #         'assigned_network': assigned_network or 0
    #     }
    #
    # @api.model
    # def get_inprogress_network(self):
    #     inprogress_network = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Network'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_network': inprogress_network or 0
    #     }
    #
    # @api.model
    # def get_open_tickets_incident(self):
    #
    #     open_tickets_incident= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_network': open_tickets_incident or 0
    #     }
    #
    # @api.model
    # def get_for_approval_incident(self):
    #
    #     for_approval_incident= self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_incident': for_approval_incident or 0
    #     }
    #
    # @api.model
    # def get_draft_incident(self):
    #
    #     draft_incident = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),'|',('state','=','01draft')])
    #     return {
    #         'draft_incident': draft_incident or 0
    #     }
    #
    # @api.model
    # def get_all_incident(self):
    #
    #     all_incident = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Report an Incident')])
    #     return {
    #         'all_incident': all_incident or 0
    #     }
    #
    # @api.model
    # def get_assigned_incident(self):
    #     assigned_incident = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),('state','=','05routed')])
    #     return {
    #         'assigned_incident': assigned_incident or 0
    #     }
    #
    # @api.model
    # def get_inprogress_incident(self):
    #     inprogress_incident = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Report an Incident'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_incident': inprogress_incident or 0
    #     }
    #
    # @api.model
    # def get_open_tickets_it_gen(self):
    #
    #     open_tickets_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_it_gen': open_tickets_it_gen or 0
    #     }
    #
    # @api.model
    # def get_for_approval_it_gen(self):
    #
    #     for_approval_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_it_gen': for_approval_it_gen or 0
    #     }
    #
    # @api.model
    # def get_draft_it_gen(self):
    #
    #     draft_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),'|',('state','=','01draft')])
    #     return {
    #         'draft_it_gen': draft_it_gen or 0
    #     }
    #
    # @api.model
    # def get_all_it_gen(self):
    #
    #     all_it_gen = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','IT General Inquiry')])
    #     return {
    #         'all_it_gen': all_it_gen or 0
    #     }
    #
    # @api.model
    # def get_assigned_it_gen(self):
    #     assigned_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),('state','=','05routed')])
    #     return {
    #         'assigned_it_gen': assigned_it_gen or 0
    #     }
    #
    # @api.model
    # def get_inprogress_it_gen(self):
    #     inprogress_it_gen = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','IT General Inquiry'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_it_gen': inprogress_it_gen or 0
    #     }
    #
    #
    # @api.model
    # def get_open_tickets_hardware(self):
    #
    #     open_tickets_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_hardware': open_tickets_hardware or 0
    #     }
    #
    # @api.model
    # def get_for_approval_hardware(self):
    #
    #     for_approval_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_hardware': for_approval_hardware or 0
    #     }
    #
    # @api.model
    # def get_draft_hardware(self):
    #
    #     draft_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),'|',('state','=','01draft')])
    #     return {
    #         'draft_hardware': draft_hardware or 0
    #     }
    #
    # @api.model
    # def get_all_hardware(self):
    #
    #     all_hardware = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Hardware')])
    #     return {
    #         'all_hardware': all_hardware or 0
    #     }
    #
    # @api.model
    # def get_assigned_hardware(self):
    #     assigned_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),('state','=','05routed')])
    #     return {
    #         'assigned_hardware': assigned_hardware or 0
    #     }
    #
    # @api.model
    # def get_inprogress_hardware(self):
    #     inprogress_hardware = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Hardware'),('state','=','06in_progress')])
    #     return {
    #         'inprogress_hardware': inprogress_hardware or 0
    #     }
    #
    #
    # @api.model
    # def get_open_tickets_asset(self):
    #
    #     open_tickets_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_asset': open_tickets_asset or 0
    #     }
    #
    # @api.model
    # def get_for_approval_asset(self):
    #
    #     for_approval_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_asset': for_approval_asset or 0
    #     }
    #
    # @api.model
    # def get_draft_asset(self):
    #
    #     draft_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),'|',('state','=','01draft')])
    #     return {
    #         'draft_asset': draft_asset or 0
    #     }
    #
    # @api.model
    # def get_all_asset(self):
    #
    #     all_asset = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Asset Management')])
    #     return {
    #         'all_asset': all_asset or 0
    #     }
    #
    # @api.model
    # def get_assigned_asset(self):
    #     assigned_asset = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Management'),('state','=','05routed')])
    #     return {
    #         'assigned_asset': assigned_asset or 0
    #     }
    #
    # @api.model
    # def get_open_tickets_asset_req(self):
    #
    #     open_tickets_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_asset_req': open_tickets_asset_req or 0
    #     }
    #
    # @api.model
    # def get_for_approval_asset_req(self):
    #
    #     for_approval_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_asset_req': for_approval_asset_req or 0
    #     }
    #
    # @api.model
    # def get_draft_asset_req(self):
    #
    #     draft_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),'|',('state','=','01draft')])
    #     return {
    #         'draft_asset_req': draft_asset_req or 0
    #     }
    #
    # @api.model
    # def get_all_asset_req(self):
    #
    #     all_asset_req = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Asset Requisition')])
    #     return {
    #         'all_asset_req': all_asset_req or 0
    #     }
    #
    # @api.model
    # def get_assigned_asset_req(self):
    #     assigned_asset_req = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Asset Requisition'),('state','=','05routed')])
    #     return {
    #         'assigned_asset_req': assigned_asset_req or 0
    #     }
    #
    # @api.model
    # def get_open_tickets_software(self):
    #
    #     open_tickets_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'open_tickets_software': open_tickets_software or 0
    #     }
    #
    # @api.model
    # def get_for_approval_software(self):
    #
    #     for_approval_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #     return {
    #         'for_approval_software': for_approval_software or 0
    #     }
    #
    # @api.model
    # def get_draft_software(self):
    #
    #     draft_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),'|',('state','=','01draft')])
    #     return {
    #         'draft_software': draft_software or 0
    #     }
    #
    # @api.model
    # def get_all_software(self):
    #
    #     all_software = self.env['mwide.helpdesk'].sudo().search_count([('category_id','=','Software')])
    #     return {
    #         'all_software': all_software or 0
    #     }
    #
    # @api.model
    # def get_assigned_software(self):
    #     assigned_software = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=','Software'),('state','=','05routed')])
    #     return {
    #         'assigned_software': assigned_software or 0
    #     }
    #
    #
    # @api.model
    # def get_number_transaction_state(self):
    #     #query = '''SELECT mh.category_id, mh.state as ticket, count(mh.name) as total FROM mwide_helpdesk mh LEFT JOIN mwide_service_type mst on mh.category_id = mst.id and mh.state in ('') GROUP BY state ORDER by state;'''
    #     #self._cr.execute(query)
    #     #docs = self._cr.dictfetchall()
    #     #trans = []
    #     mst = []
    #     ticket_count= []
    #     total_open = []
    #     total_approval = []
    #     total_overdue = []
    #     total_assigned = []
    #     #open tickets, waiting for appproval, assigned, in_progress
    #     #'03approved_head','04approved_it','05routed','06in_progress'
    #     #service_types = ['Access Management', 'Application Management', 'IT Clearance','Network','Report an Incident', 'IT General Inquiry','Hardware']
    #     category_ids = self.env['mwide.category'].sudo().search([('type','=','it')])
    #     for type_id in category_ids:
    #         #open_tickets = self.sudo().search_count(['&',('category_id','=',type_id.id),'|',('state','=','04approved_it'),'&',('state_b','=','03approved_head'),('bol_it_mngr','=',False)])
    #         open_tickets = 0
    #         open_tickets = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=',type_id.id),'|','&',('state','=','04approved_it'),('approval_selection','=','it_manager'),'|','&',('state_b','=','03approved_head'),('approval_selection','=','manager'),'&',('state_c','=','02request'),('approval_selection','=','none')])
    #         #for_approval = self.sudo().search_count(['&',('category_id','=',type_id.id),'|',('state','=','03approved_head'),('state_b','=','02request')])
    #         for_approval = 0
    #         for_approval = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=',type_id.id),'|','&',('state','=','03approved_head'),('approval_selection','=','it_manager'),'&',('state_b','=','02request'),('approval_selection','=','manager')]) or 0
    #         assigned = 0
    #         assigned = self.env['mwide.helpdesk'].sudo().search_count(['&',('category_id','=',type_id.id),('state','=','05assigned')])
    #         overdue = 0
    #         overdue= self.env['mwide.helpdesk'].sudo().search_count([('is_sla','=',True),'&',('category_id','=',type_id.id),('html_color','in',["#FEB580","#F78585"])]) or 0
    #         #inprogress = self.sudo().search_count(['&',('category_id','=',type_id.id),('state','=','06in_progress')])
    #         mst.append(type_id.name)
    #         total_open.append(open_tickets)
    #         total_approval.append(for_approval)
    #         total_overdue.append(overdue)
    #         total_assigned.append(assigned)
    #         ticket_count.append([open_tickets,overdue,for_approval,assigned])
    #
    #     final = [mst, ticket_count,[total_open,total_approval,total_overdue,total_assigned]]
    #     return final
    #
    # @api.model
    # def get_number_transaction_pie(self):
    #     total_pass = 0
    #     total_fail = 0
    #     open_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('type','=','it'),('state','=','05assigned')])
    #     for open in open_resolution_tickets:
    #         if open.html_color == '#F78585':
    #             if open.is_sla ==True:
    #                 total_fail+=1
    #             else:
    #                 total_pass+=1
    #         else:
    #             total_pass +=1
    #     close_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('type','=','it'),('state','not in',['05assigned','09resolved'])])
    #     for close in close_resolution_tickets:
    #         if close.resolution_time + close.response_time > close.time_resolution:
    #             if close.is_sla ==True:
    #                 total_fail+=1
    #             else:
    #                 total_pass +=1
    #         else:
    #             total_pass +=1
    #     resolved = self.env['mwide.helpdesk'].search(['&',('state','=','09resolved'),('type','=','it')])
    #     for ticket in resolved:
    #         if ticket.resolution_time + ticket.response_time > ticket.time_resolution:
    #             if ticket.is_sla == True:
    #                 total_fail +=1
    #             else:
    #                 total_pass+=1
    #         else:
    #             total_pass +=1
    #
    #     final = [total_pass,total_fail]
    #     return final
    #
    # @api.model
    # def get_record(self):
    #     cr = self._cr
    #     cr.execute("""SELECT mh.name, he.name, mh.ticket_date, mh.state FROM mwide_helpdesk mh JOIN hr_employee he ON mh.requested_id = he.user_id ORDER BY mh.name DESC LIMIT 5""")
    #     get_record = cr.fetchall()
    #     return {
    #         'get_record': get_record
    #     }
    #
    # @api.model
    # def get_record_count(self):
    #     #cr = self._cr
    #     #cr.execute("""SELECT mh.name, he.name, mh.ticket_date, mh.state FROM mwide_helpdesk mh JOIN hr_employee he ON mh.requested_id = he.user_id ORDER BY mh.name DESC LIMIT 5""")
    #     #get_record = cr.fetchall()
    #     categ_ids = self.env['mwide.category'].search([('type','=','it')])
    #     rec_count = []
    #     rec_response_count = []
    #     rec_resolution_count = []
    #     total_resolved = 0
    #     total_new = 0
    #     total_assigned = 0
    #     total_all = 0
    #     total_after_15 = 0
    #     total_within_15=0
    #     total_response_all =0
    #     total_fail = 0
    #     total_pass = 0
    #     for categ in categ_ids:
    #         resolved = 0
    #         new = 0
    #         assigned = 0
    #         total = 0
    #         for itss in self.env['mwide.helpdesk'].search([('category_id','=',categ.id)]):
    #             if itss.state == '09resolved':
    #                 resolved += 1
    #             elif itss.state in ['01draft','02request','03approved_head','04approved_it']:
    #                 new += 1
    #             elif itss.state in ['05assigned','06on_hold','07validation','08unresolved']:
    #                 assigned += 1
    #         total = resolved + assigned + new
    #         total_resolved += resolved
    #         total_new += new
    #         total_assigned += assigned
    #         total_all += total
    #         rec = (categ.name,resolved,new,assigned,total)
    #         rec_count.append(rec)
    #         after_15 = 0
    #         within_15 = 0
    #         #REPONSE
    #         open_response_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','in',['02request','03approved_head','04approved_it'])])
    #         for open in open_response_tickets:
    #             #if open.html_color == '#F78585':
    #             #     after_15 += 1
    #             #else:
    #                 within_15 +=1
    #         close_response_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','not in',['02request','03approved_head','04approved_it'])])
    #         for close in close_response_tickets:
    #             within_15+=1
    #         response_all = after_15 + within_15
    #         total_after_15 += after_15
    #         total_within_15+= within_15
    #         total_response_all = total_after_15 + total_within_15
    #         if response_all <=0:
    #             response_all =1
    #         rec_response = (after_15,within_15,after_15 + within_15,str(round((within_15/response_all)*100,2))+'%')
    #         rec_response_count.append(rec_response)
    #         #RESOLUTION START
    #         fail=0
    #         res_pass=0
    #         open_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','=','05assigned')])
    #         for open in open_resolution_tickets:
    #             if open.html_color == '#F78585':
    #                 if open.is_sla == True:
    #                     fail += 1
    #                 else:
    #                     res_pass +=1
    #             else:
    #                 res_pass +=1
    #         close_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','not in',['05assigned','09resolved'])])
    #         for close in close_resolution_tickets:
    #
    #             if close.resolution_time + close.response_time > close.time_resolution:
    #                 if close.is_sla == True:
    #                     fail+=1
    #                 else:
    #                     res_pass +=1
    #             else:
    #                 res_pass +=1
    #         resolved_resolution_tickets = self.env['mwide.helpdesk'].search(['&',('category_id','=',categ.id),('state','=','09resolved')])
    #         for resolved in resolved_resolution_tickets:
    #             if resolved.resolution_time + resolved.response_time > resolved.time_resolution:
    #                 if resolved.is_sla == True:
    #                     fail+=1
    #                 else:
    #                     res_pass +=1
    #             else:
    #                 res_pass +=1
    #         res_all = fail+res_pass
    #         if res_all <=0:
    #             res_all =1
    #         rec_resolution = (fail,res_pass,fail+res_pass,str(round((res_pass/res_all)*100,2))+'%')
    #         rec_resolution_count.append(rec_resolution)
    #         total_fail += fail
    #         total_pass += res_pass
    #         total_resolution_all = total_fail+total_pass
    #         #percent
    #         if total_response_all <=0:
    #             total_response_all=1
    #         if total_resolution_all<=0:
    #             total_resolution_all=1
    #         total_response_percent = (total_within_15/total_response_all)*100
    #         total_resolution_percent = (total_pass/total_resolution_all)*100
    #     return {'get_response_count':rec_response_count,'get_resolution_count':rec_resolution_count,'total_response_percent':str(round(total_response_percent,2))+'%','total_resolution_percent':str(round(total_resolution_percent,2))+'%',
    #         'get_record_count': rec_count,'total_resolved':total_resolved,'total_new':total_new,'total_assigned':total_assigned,'total_all':total_all,
    #         'total_after_15':total_after_15,'total_within_15':total_within_15,'total_response_all':total_response_all,'total_pass':total_pass,'total_fail':total_fail,'total_resolution_all':total_resolution_all
    #     }

    #
    @api.model
    def get_summary(self,start_date,end_date):
        query = """SELECT data.category as category,
        SUM(data.draft) as draft, SUM(data.request) as request, SUM(data.pending) as pending, SUM(data.assigned) as assigned,
        SUM(data.confirmation) as confirmation, SUM(data.unresolved) as unresolved, SUM(data.resolved) as resolved, SUM(data.total) as total
        FROM (
        SELECT mc.name as category, count(*) as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (mh.state = '01draft'
        OR mh.state = '03approved_head'
        OR mh.state = '04approved_it')
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, count(*) as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '02request'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, count(*) as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '06on_hold'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, count(*) as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '05assigned'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, count(*) as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '07validation'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, count(*) as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '08unresolved'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, count(*) as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '09resolved'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        GROUP BY mc.name
        ) as data
        GROUP BY data.category
        ORDER BY data.category asc
        """
        args = {
            'start': start_date,
            'end': end_date,
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_draft = 0
        for i in range(0, len(get_record)):
            total_draft += get_record[i][1]

        total_request = 0
        for i in range(0, len(get_record)):
            total_request += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_assigned = 0
        for i in range(0, len(get_record)):
            total_assigned += get_record[i][4]

        total_confirmation = 0
        for i in range(0, len(get_record)):
            total_confirmation += get_record[i][5]

        total_unresolved = 0
        for i in range(0, len(get_record)):
            total_unresolved += get_record[i][6]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][7]

        total_summary = 0
        for i in range(0, len(get_record)):
            total_summary += get_record[i][8]

        result_draft = round(total_draft)
        result_request = round(total_request)
        result_pending = round(total_pending)
        result_assigned = round(total_assigned)
        result_confirmation = round(total_confirmation)
        result_unresolved = round(total_unresolved)
        result_resolved = round(total_resolved)
        result_summary = round(total_summary)

        result_summary_hold = result_draft + result_pending
        result_summary_resolve = result_confirmation + result_resolved
        result_summary_outstanding = result_request + result_assigned

        return {
            'summary': get_record,
            'result_draft': result_draft,
            'result_request': result_request,
            'result_pending': result_pending,
            'result_assigned': result_assigned,
            'result_confirmation': result_confirmation,
            'result_unresolved': result_unresolved,
            'result_resolved': result_resolved,
            'result_summary': result_summary,
            'result_summary_hold': result_summary_hold,
            'result_summary_resolve': result_summary_resolve,
            'result_summary_outstanding': result_summary_outstanding,
        }

    @api.model
    def get_severity(self,start_date,end_date):
        query = """SELECT data.category as category,
        SUM(data.severity1) as severity1, SUM(data.severity2) as severity2, SUM(data.severity3) as severity3, SUM(data.severity4) as severity4, SUM(data.false) as false, SUM(data.total) as total
        FROM
        (
        SELECT mc.name as category, count(*) as severity1, 0 as severity2, 0 as severity3, 0 as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 1'
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  count(*) as severity2, 0 as severity3, 0 as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 2'
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, count(*) as severity3, 0 as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 3'
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, 0 as severity3, count(*) as severity4, 0 as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND ms.name = 'Severity 4'
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, 0 as severity3, 0 as severity4, count(*) as false, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND ms.name is NULL
        GROUP BY category
        UNION ALL
        SELECT mc.name as category, 0 as severity1,  0 as severity2, 0 as severity3, 0 as severity4, 0 as false, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        GROUP BY category
        ) as data
        GROUP BY data.category
        ORDER BY data.category asc
        """
        args = {
            'start': start_date,
            'end': end_date,
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_severity1 = 0
        for i in range(0, len(get_record)):
            total_severity1 += get_record[i][1]

        total_severity2 = 0
        for i in range(0, len(get_record)):
            total_severity2 += get_record[i][2]

        total_severity3 = 0
        for i in range(0, len(get_record)):
            total_severity3 += get_record[i][3]

        total_severity4 = 0
        for i in range(0, len(get_record)):
            total_severity4 += get_record[i][4]

        total_false = 0
        for i in range(0, len(get_record)):
            total_false += get_record[i][5]

        total_severity = 0
        for i in range(0, len(get_record)):
            total_severity += get_record[i][6]

        result_severity1 = round(total_severity1)
        result_severity2 = round(total_severity2)
        result_severity3 = round(total_severity3)
        result_severity4 = round(total_severity4)
        result_false = round(total_false)
        result_severity = round(total_severity)

        return {
            'severity': get_record,
            'result_severity1': result_severity1,
            'result_severity2': result_severity2,
            'result_severity3': result_severity3,
            'result_severity4': result_severity4,
            'result_false': result_false,
            'result_severity': result_severity,
        }

    @api.model
    def get_comparison_week(self, start_date, end_date):
        query = """SELECT data.name as category,
        SUM(data.created_case) as created_case, SUM(data.total_resolved_cases) as total_resolved_cases, SUM(data.pending_cases) as pending_cases,
        SUM(data.incoming_created_case) as incoming_created_case, SUM(data.total_outstanding_case) as total_outstanding_case
        FROM
        (
        SELECT mc.name  as name, count(mc.name) as created_case, 0 as total_resolved_cases, 0 as pending_cases, 0 as incoming_created_case, 0 as total_outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date - INTERVAL '7 days'
        AND DATE(mh.create_date) <= %(end)s::timestamp::date - INTERVAL '7 days'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as name, 0 as case_created_case, count(mc.name) as total_resolved_cases, 0 as pending_cases, 0 as incoming_created_case, 0 as total_outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date - INTERVAL '7 days'
        AND DATE(mh.create_date) <= %(end)s::timestamp::date - INTERVAL '7 days'
        AND mh.state = '09resolved'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name  as name, 0 as case_created_case, 0 as total_resolved_cases, count(mc.name) as pending_cases, 0 as incoming_created_case, 0 as total_outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date - INTERVAL '7 days'
        AND DATE(mh.create_date) <= %(end)s::timestamp::date - INTERVAL '7 days'
        AND mh.state != '09resolved'
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as name, 0 as case_created_case, 0 as total_resolved_cases, 0 as pending_cases, count(mc.name) as case_created, 0 as outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as name, 0 as case_created_case, 0 as total_resolved_cases, 0 as pending_cases, 0 as case_created, count(mc.name) as outstanding_case
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND state != '09resolved'
        GROUP BY mc.name
        ) as data
        GROUP BY data.name
        ORDER BY data.name
        """
        args = {
            'start': start_date,
            'end': end_date,
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_cases = 0
        for i in range(0, len(get_record)):
            total_cases += get_record[i][1]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_incoming = 0
        for i in range(0, len(get_record)):
            total_incoming += get_record[i][4]

        total_outstanding = 0
        for i in range(0, len(get_record)):
            total_outstanding += get_record[i][5]

        result_total_cases = round(total_cases)
        result_total_resolved = round(total_resolved)
        result_total_pending = round(total_pending)
        result_total_incoming = round(total_incoming)
        result_total_outstanding = round(total_outstanding)

        result_summary_incoming = abs(result_total_pending + result_total_incoming)

        return {
            'get_comparison': get_record,
            'result_total_cases': result_total_cases,
            'result_total_resolved': result_total_resolved,
            'result_total_pending': result_total_pending,
            'result_total_incoming': result_total_incoming,
            'result_total_outstanding': result_total_outstanding,
            'result_summary_incoming': result_summary_incoming,
        }

    @api.model
    def get_sbu(self, start_date, end_date):
        query = """SELECT data.group_sbu as group_sbu,
        SUM(data.draft) as draft, SUM(data.request) as request, SUM(data.pending) as pending, SUM(data.assigned) as assigned,
        SUM(data.confirmation) as confirmation, SUM(data.unresolved) as unresolved, SUM(data.resolved) as resolved, SUM(data.total) as total
        FROM (
        SELECT mh.sbu as group_sbu, count(*) as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (mh.state = '01draft'
        OR mh.state = '03approved_head'
        OR mh.state = '04approved_it')
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, count(*) as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '02request'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, count(*) as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '06on_hold'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, count(*) as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '05assigned'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, count(*) as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '07validation'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, count(*) as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '08unresolved'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, count(*) as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '09resolved'
        GROUP BY mh.sbu
        UNION ALL
        SELECT mh.sbu as group_sbu, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        GROUP BY mh.sbu
        ) as data
        GROUP BY data.group_sbu
        ORDER BY data.group_sbu asc
        """
        args = {
            'start': start_date,
            'end': end_date,
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_draft = 0
        for i in range(0, len(get_record)):
            total_draft += get_record[i][1]

        total_request = 0
        for i in range(0, len(get_record)):
            total_request += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_assigned = 0
        for i in range(0, len(get_record)):
            total_assigned += get_record[i][4]

        total_confirmation = 0
        for i in range(0, len(get_record)):
            total_confirmation += get_record[i][5]

        total_unresolved = 0
        for i in range(0, len(get_record)):
            total_unresolved += get_record[i][6]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][7]

        total_summary = 0
        for i in range(0, len(get_record)):
            total_summary += get_record[i][8]

        result_sbu_draft = round(total_draft)
        result_sbu_request = round(total_request)
        result_sbu_pending = round(total_pending)
        result_sbu_assigned = round(total_assigned)
        result_sbu_confirmation = round(total_confirmation)
        result_sbu_unresolved = round(total_unresolved)
        result_sbu_resolved = round(total_resolved)
        result_sbu_summary = round(total_summary)

        return {
            'get_sbu': get_record,
            'result_sbu_draft': result_sbu_draft,
            'result_sbu_request': result_sbu_request,
            'result_sbu_pending': result_sbu_pending,
            'result_sbu_assigned': result_sbu_assigned,
            'result_sbu_confirmation': result_sbu_confirmation,
            'result_sbu_unresolved': result_sbu_unresolved,
            'result_sbu_resolved': result_sbu_resolved,
            'result_sbu_summary': result_sbu_summary,
        }

    @api.model
    def get_sap(self, start_date, end_date):
        query = """SELECT data.sap_case as sap_case,
        SUM(data.draft) as draft, SUM(data.request) as request, SUM(data.pending) as pending, SUM(data.assigned) as assigned,
        SUM(data.confirmation) as confirmation, SUM(data.unresolved) as unresolved, SUM(data.resolved) as resolved, SUM(data.total) as total
        FROM (
        SELECT mst.name as sap_case, count(*) as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (mh.state = '01draft'
        OR mh.state = '03approved_head'
        OR mh.state = '04approved_it')
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, count(*) as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '02request'
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, count(*) as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '06on_hold'
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, count(*) as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '05assigned'
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, count(*) as confirmation, 0 as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '07validation'
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation,  count(*) as unresolved, 0 as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '08unresolved'
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, count(*) as resolved, 0 as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND mh.state = '09resolved'
        GROUP BY mst.name
        UNION ALL
        SELECT mst.name as sap_case, 0 as draft, 0 as request, 0 as pending, 0 as assigned, 0 as confirmation, 0 as unresolved, 0 as resolved, count(*) as total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_service_type mst ON mh.service_type_id = mst.id
        WHERE mc.type = 'it'
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        GROUP BY mst.name
        ) as data
        WHERE data.sap_case LIKE 'SAP%%'
        GROUP BY data.sap_case
        ORDER BY data.sap_case asc
        """
        args = {
            'start': start_date,
            'end': end_date,
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_draft = 0
        for i in range(0, len(get_record)):
            total_draft += get_record[i][1]

        total_request = 0
        for i in range(0, len(get_record)):
            total_request += get_record[i][2]

        total_pending = 0
        for i in range(0, len(get_record)):
            total_pending += get_record[i][3]

        total_assigned = 0
        for i in range(0, len(get_record)):
            total_assigned += get_record[i][4]

        total_confirmation = 0
        for i in range(0, len(get_record)):
            total_confirmation += get_record[i][5]

        total_unresolved = 0
        for i in range(0, len(get_record)):
            total_unresolved += get_record[i][6]

        total_resolved = 0
        for i in range(0, len(get_record)):
            total_resolved += get_record[i][7]

        total_summary = 0
        for i in range(0, len(get_record)):
            total_summary += get_record[i][8]

        result_sap_draft = round(total_draft)
        result_sap_request = round(total_request)
        result_sap_pending = round(total_pending)
        result_sap_assigned = round(total_assigned)
        result_sap_confirmation = round(total_confirmation)
        result_sap_unresolved = round(total_unresolved)
        result_sap_resolved = round(total_resolved)
        result_sap_summary = round(total_summary)

        return {
            'get_sap': get_record,
            'result_sap_draft': result_sap_draft,
            'result_sap_request': result_sap_request,
            'result_sap_pending': result_sap_pending,
            'result_sap_assigned': result_sap_assigned,
            'result_sap_confirmation': result_sap_confirmation,
            'result_sap_unresolved': result_sap_unresolved,
            'result_sap_resolved': result_sap_resolved,
            'result_sap_summary': result_sap_summary,
        }

    @api.model
    def get_response_resolution(self, start_date, end_date):
        query = """SELECT data.category as category, SUM(data.response_fail) as response_fail, SUM(data.response_pass) as response_pass, SUM(data.response_total) as response_total,
        SUM(data.resolution_fail) as resolution_fail, SUM(data.resolution_pass) as resolution_pass, SUM(data.resolution_total) as resolution_total
        FROM
        (
        SELECT mc.name as category, count(*) as response_fail, 0 as response_pass, 0 as response_total, 0 as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mh.response_time * 60 > ms.time_acknowledged
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (state = '05assigned' OR state = '06on_hold' OR state = '07validation' OR state = '09resolved')
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail, count(*) as response_pass, 0 as response_total, 0 as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE (mh.response_time * 60 <= ms.time_acknowledged OR ms.time_acknowledged IS NULL)
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (state = '05assigned' OR state = '06on_hold' OR state = '07validation' OR state = '09resolved')
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, count(*) as response_total, 0 as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE
        DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (state = '05assigned' OR state = '06on_hold' OR state = '07validation' OR state = '09resolved')
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, 0 as response_total, count(*) as resolution_fail, 0 as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE mh.resolution_time * 60 > ms.time_resolution * 60
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (state = '07validation' OR state = '09resolved')
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, 0 as response_total, 0 as resolution_fail, count(*) as resolution_pass, 0 as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        LEFT JOIN mwide_severity ms ON mh.severity_id = ms.id
        WHERE (mh.resolution_time * 60 >= 0 AND mh.resolution_time * 60 <= ms.time_resolution * 60 OR ms.time_acknowledged IS NULL)
        AND DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (state = '07validation' OR state = '09resolved')
        GROUP BY mc.name
        UNION ALL
        SELECT mc.name as category, 0 as response_fail,0 as response_pass, 0 as response_total, 0 as resolution_fail, 0 as resolution_pass, count(*) as resolution_total
        FROM mwide_helpdesk mh
        LEFT JOIN mwide_category mc ON mh.category_id = mc.id
        WHERE
        DATE(mh.create_date) >= %(start)s::timestamp::date
        AND DATE(mh.create_date) <= %(end)s::timestamp::date
        AND (state = '07validation' OR state = '09resolved')
        GROUP BY mc.name
        ) AS data
        WHERE data.category IS NOT NULL
        GROUP BY data.category
        ORDER BY data.category asc
        """
        args = {
            'start': start_date,
            'end': end_date,
        }
        # self._cr.execute(query,[year,month])
        self._cr.execute(query,args)
        get_record = self.env.cr.fetchall()

        total_response_fail = 0
        for i in range(0, len(get_record)):
            total_response_fail += get_record[i][1]

        total_response_pass = 0
        for i in range(0, len(get_record)):
            total_response_pass += get_record[i][2]

        total_response = 0
        for i in range(0, len(get_record)):
            total_response += get_record[i][3]

        total_resolution_fail = 0
        for i in range(0, len(get_record)):
            total_resolution_fail += get_record[i][4]

        total_resolution_pass = 0
        for i in range(0, len(get_record)):
            total_resolution_pass += get_record[i][5]

        total_resolution = 0
        for i in range(0, len(get_record)):
            total_resolution += get_record[i][6]

        result_response_fail = round(total_response_fail)
        result_response_pass = round(total_response_pass)
        result_response = round(total_response)
        result_resolution_fail = round(total_resolution_fail)
        result_resolution_pass = round(total_resolution_pass)
        result_resolution = round(total_resolution)

        if total_response == 0:
            response_percentage = 0
        else:
            response_percentage = round((total_response_pass / total_response) * 100, 2)

        if total_resolution == 0:
            resolution_percentage = 0

        else:
            resolution_percentage = round((total_resolution_pass / total_resolution) * 100,2)

        return {
            'get_response_resolution': get_record,
            'result_response_fail': result_response_fail,
            'result_response_pass': result_response_pass,
            'result_response': result_response,
            'result_resolution_fail': result_resolution_fail,
            'result_resolution_pass': result_resolution_pass,
            'result_resolution': result_resolution,
            'response_percentage': response_percentage,
            'resolution_percentage': resolution_percentage,
        }
