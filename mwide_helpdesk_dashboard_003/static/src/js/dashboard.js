odoo.define('mwide_helpdesk_dashboard.HelpdeskDashboard', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var core = require('web.core');
var QWeb = core.qweb;
var rpc = require('web.rpc');
var ajax = require('web.ajax');

var HelpdeskDashboard = AbstractAction.extend({
    template: 'HelpdeskDashboard',

    events: {
            'click .button-ticket-summary':'ticket_summary',
   },

    init: function(parent, context) {
          this._super(parent, context);
          this.dashboards_templates = ['DashboardTable'];
          this.total_request = [];
          this.new_request = [];
          this.ext_request = [];
          this.cari_form = [];
          this.eari_form = [];
          this.pbi_form = [];
          this.sbi_form = [];
          this.transaction = [];

          //For State Table
          this.summary = [];
          this.draft = [];
          this.request = [];
          this.pending = [];
          this.assigned = [];
          this.confirmation = [];
          this.resolved = [];
          this.total_summary = [];

          // For Severity Table
          this.severity = [];
          this.severity1 = [];
          this.severity2 = [];
          this.severity3 = [];
          this.severity4 = [];
          this.severityFalse = [];
          this.severityTotal = [];

          // For Week Comparison Table
          this.comparison = [];
          this.prev_total_cases = [];
          this.prev_total_resolved = [];
          this.prev_total_prev = [];
          this.total_incoming = [];
          this.total_outstanding = [];

          //For SBU Table
          this.sbu = [];
          this.sbu_draft = [];
          this.sbu_request = [];
          this.sbu_pending = [];
          this.sbu_assigned = [];
          this.sbu_confirmation = [];
          this.sbu_unresolved = [];
          this.sbu_resolved = [];
          this.total_sbu_summary = [];

          //For Sap Cases Table
          this.sap = [];
          this.sap_draft = [];
          this.sap_request = [];
          this.sap_pending = [];
          this.sap_assigned = [];
          this.sap_confirmation = [];
          this.sap_unresolved = [];
          this.sap_resolved = [];
          this.total_sap_summary = [];

          //For Response Resolution Table
          this.response_resolution = [];
          this.response_fail = [];
          this.response_pass = [];
          this.response = [];
          this.resolution_fail = [];
          this.resolution_pass = [];
          this.resolution = [];
          this.response_percentage = [];
          this.resolution_percentage = [];

          // Summary Header
          this.total_summary_incoming =[];
          this.result_summary_hold = [];
          this.result_summary_resolve = [];
          this.result_summary_outstanding = [];

          // Get Date Labels
          const monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

          let startDate = this.searchModelConfig.context.default_start_date;
          let endDate = this.searchModelConfig.context.default_end_date;
          let startDateParse = new Date(Date.parse(startDate));
          // startDateParse.getFullYear();
          // Current Week
          let startDateFormat = monthList[startDateParse.getMonth()] + ' ' + startDateParse.getDate();
          this.startDateLabel = [startDateFormat];

          let endDateParse = new Date(Date.parse(endDate));
          let endDateFormat = monthList[endDateParse.getMonth()] + ' ' + endDateParse.getDate();
          this.endDateLabel = [endDateFormat];

          // Previous Week
          let previousStartDateParse = new Date(Date.parse(startDate));
          let previousStartDate = previousStartDateParse.setDate(previousStartDateParse.getDate() - 7);
          let getPreviousStartDate = new Date(previousStartDate);
          let previousStartDateFormat = monthList[getPreviousStartDate.getMonth()] + ' ' + getPreviousStartDate.getDate();
          this.previousStartDateLabel = [previousStartDateFormat]

          let previousEndDateParse = new Date(Date.parse(endDate));
          let previousEndDate = previousEndDateParse.setDate(previousEndDateParse.getDate() - 7);
          let getPreviousEndDate = new Date(previousEndDate);
          let previousEndDateFormat = monthList[getPreviousEndDate.getMonth()] + ' ' + getPreviousEndDate.getDate();
          this.previousEndDateLabel = [previousEndDateFormat]

      },


      start: function() {
              var self = this;
              this.set("title", 'HelpdeskDashboard');
              return this._super().then(function() {
                  self.render_dashboards();
                  // self.render_graphs();
              });
      },


	  // events: {
	  //   "click .period1": "period1","click .period3": "period3","click .period6": "period6","click .period12": "period12","click .period_all": "period_all","click .period_go": "period_go",
		// 	},

			// period1: function () {
			// 	var self=this
			//     rpc.query({
			//               model: 'mwide.helpdesk',
			//               method: 'get_date_range_selector',
			// 			  args : [1]
			//       })
			// 	self.start();
			// },
      //
			// period3: function () {
			// 	var self=this
			//     rpc.query({
			//               model: 'mwide.helpdesk',
			//               method: 'get_date_range_selector',
			// 			  args : [3]
			//       })
			// 	self.fetch_data();
			// },
      //
			// period6: function () {
			// 	var self=this
			//     rpc.query({
			//               model: 'mwide.helpdesk',
			//               method: 'get_date_range_selector',
			// 			  args : [6]
			//       })
			// 	self.start();
			// },
      //
			// period12: function () {
			// 	var self=this
			//     rpc.query({
			//               model: 'mwide.helpdesk',
			//               method: 'get_date_range_selector',
			// 			  args : [12]
			//       })
			// 	self.start();
			// },
      //
			// period_all: function () {
			// 	var self=this
			//     rpc.query({
			//               model: 'mwide.helpdesk',
			//               method: 'get_date_range_selector',
			// 			  args : [0]
			//       })
			// 	self.fetch_data();
			// 	self.start();
			// },
      //
			// period_go: function () {
			// 	var self=this;
			// 	var minValue = $( "#fromDate" ).val();
  		// 		var maxValue = $ ( "#toDate" ).val();
			//     rpc.query({
			//               model: 'mwide.helpdesk',
			//               method: 'get_date_date_selector',
			// 			        args : [minValue,maxValue]
			//       })
			// 	self.fetch_data();
			// },

    //   render_graphs: function() {
    //       var self = this;
    //       self.render_transaction_state_graph();
		//   self.render_transaction_pie_graph();
    //   },
    //
    //   render_transaction_state_graph: function(){
    //       var self = this;
    //       var ctx = self.$(".transaction_state");
    //       rpc.query({
    //           model: "mwide.helpdesk",
    //           method: "get_number_transaction_state",
    //       }).then(function (arrays) {
    //
    //       var data = {
    //         labels:arrays[0],
    //         datasets:[{
    //           label:"Open Tickets",
    //           data: arrays[2][0],
    //           backgroundColor: "rgb(148,22,227)",
    //           borderColor: "rgb(148,22,227)",
    //         },
		// 	{
    //           label:"Assigned",
    //           data: arrays[2][3],
    //           backgroundColor: "rgb(105, 243, 20 )",
    //           borderColor: "rgb(105, 243, 20 )",
    //         },
		// 	{
    //           label:"For Approval",
    //           data: arrays[2][1],
    //           backgroundColor: "rgba(54,162,235)",
    //           borderColor: "rgba(54,162,235)",
    //         },
		// 	{
    //           label:"Overdue",
    //           data: arrays[2][2],
    //           backgroundColor: "rgb(243, 43, 20)",
    //           borderColor: "rgb(243, 43, 20)",
    //         },
    //       ]
    //     };
		// /*var options = {
		// 	responsive: true
		// };*/
    //     var options = {
    //       responsive: true,
    //       title: {
    //         display: true,
    //         position: "top",
    //         text: "Summary of Tickets",
    //         borderWidth: 1,
    //         fontSize: 18,
    //         fontColor: "#111"
    //       },
    //         scales: {
    //           yAxes: [{
    //             ticks: {
    //               min: 0
    //             }
    //           }]
    //         }
    //     };
    //
    //     var chart = new Chart(ctx, {
    //       type: "bar",
    //       data: data,
    //       options: options
    //     });
    //
    //     });
    //   },
    //
    //
    //   render_transaction_pie_graph: function(){
    //       var self = this;
    //       var ctx = self.$(".transaction_pie");
    //       rpc.query({
    //           model: "mwide.helpdesk",
    //           method: "get_number_transaction_pie",
    //       }).then(function (arrays1) {
    //
    //       var data = {
    //         labels:['Pass','Fail'],
    //         datasets:[{
    //           label:"Overall Resolution SLA",
    //           data: arrays1,
    //           backgroundColor: ["rgb(40, 165, 67)","rgb(243, 43, 20)"]
    //         }]
    //     };
		// /*var options = {
		// 	responsive: true
		// };*/
    //     var options = {
    //       responsive: true,
    //       title: {
	  //       display:true,
    //         text: "Overall Resolution SLA",
    //         position: "center",
    //         borderWidth: 1,
    //         fontSize: 18,
    //         fontColor: "#111"
    //       },
    //     };
    //
    //     var chart = new Chart(ctx, {
    //       type: "pie",
    //       data: data,
    //       options: options
    //     });
    //
    //     });
    //   },

      render_dashboards: function() {
          var self = this;
          _.each(this.dashboards_templates, function(template) {
                self.$('.o_helpdesk_dashboard').append(QWeb.render(template, {widget: self}));
        });
      },

      willStart: function(){
      var self = this;
      return $.when(ajax.loadLibs(this), this._super()).then(function() {
        return self.fetch_data();
        });
      },


      fetch_data: function() {
      var self = this;
      let startDate = self.searchModelConfig.context.default_start_date;
      let endDate = self.searchModelConfig.context.default_end_date;
    //   var def1 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_total_tickets'
    //   }).then(function(result) {
    //           self.total_tickets = result['total_tickets']
    //   });
    //   var def2 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_open_tickets'
    //   }).then(function(result) {
    //           self.open_tickets = result['open_tickets']
    //   });
    //   var def3 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_for_approval'
    //   }).then(function(result) {
    //           self.for_approval = result['for_approval']
    //   });
    //   var def31 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_assigned'
    //   }).then(function(result) {
    //           self.assigned = result['assigned']
    //   });
    //   var def32 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_onhold'
    //   }).then(function(result) {
    //           self.onhold = result['onhold']
    //   });
    //   var def4 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_access'
    //   }).then(function(result) {
    //           self.all_access = result['all_access']
    //   });
    //   var def5 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_application'
    //   }).then(function(result) {
    //           self.all_application = result['all_application']
    //   });
    //   var def6 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_it_clearance'
    //   }).then(function(result) {
    //           self.all_it_clearance = result['all_it_clearance']
    //   });
    //   var def7 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_network'
    //   }).then(function(result) {
    //           self.all_network = result['all_network']
    //   });
    //   var def8 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_incident'
    //   }).then(function(result) {
    //           self.all_incident = result['all_incident']
    //   });
    //   var def9 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_it_gen'
    //   }).then(function(result) {
    //           self.all_it_gen = result['all_it_gen']
    //   });
    //   var def10 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_hardware'
    //   }).then(function(result) {
    //           self.all_hardware = result['all_hardware']
    //   });
    //   var def11 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record'
    //   }).then(function(result) {
    //           self.transaction = result['get_record']
    //   });
    //   var def11b1 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.get_response_count = result['get_response_count']
    //   });
    //   var def11b2 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.get_resolution_count = result['get_resolution_count']
    //   });
    //   var def11b =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.count_transaction = result['get_record_count']
    //   });
    //   var def11c =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_resolved = result['total_resolved']
    //   });
    //   var def11d =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_new = result['total_new']
    //   });
    //   var def11e =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_assigned = result['total_assigned']
    //   });
    //   var def11f =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_all = result['total_all']
    //   });
    //   var def11g =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_after_15 = result['total_after_15']
    //   });
    //   var def11h =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_within_15 = result['total_within_15']
    //   });
    //   var def11i =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_response_all = result['total_response_all']
    //   });
    //   var def11j =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_pass = result['total_pass']
    //   });
    //   var def11k =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_fail = result['total_fail']
    //   });
    //   var def11l =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_resolution_all = result['total_resolution_all']
    //   });
    //   var def11m =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_response_percent= result['total_response_percent']
    //   });
    //   var def11n =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_record_count'
    //   }).then(function(result) {
    //           self.total_resolution_percent = result['total_resolution_percent']
    //   });
    //   var def12 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_overdue_tickets'
    //   }).then(function(result) {
    //           self.overdue_tickets = result['overdue_tickets']
    //   });
	  // var def13 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_asset'
    //   }).then(function(result) {
    //           self.all_asset = result['all_asset']
	  // });
    //   var def14 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_asset_req'
    //   }).then(function(result) {
    //           self.all_asset_req = result['all_asset_req']
	  // });
    //   var def15 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_all_software'
    //   }).then(function(result) {
    //           self.all_software = result['all_software']
	  // });
    //   var def16 =  this._rpc({
    //           model: 'mwide.helpdesk',
    //           method: 'get_closed_tickets'
    //   }).then(function(result) {
    //           self.closed_tickets = result['closed_tickets']
    //   });
      var def17 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_severity',
              args: [startDate,endDate]
      }).then(function(result) {
              self.severity = result['severity']
              self.severity1 = result['result_severity1']
              self.severity2 = result['result_severity2']
              self.severity3 = result['result_severity3']
              self.severity4 = result['result_severity4']
              self.severityFalse = result['result_false']
              self.severityTotal = result['result_severity']
      });
      // var def18 =  this._rpc({
      //         model: 'mwide.helpdesk',
      //         method: 'get_previous_week',
      //         args: [startDate,endDate]
      // }).then(function(result) {
      //         self.previous_week = result['previous_week']
      // });
      var def18 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_comparison_week',
              args: [startDate,endDate]
      }).then(function(result) {
              self.comparison = result['get_comparison']
              self.prev_total_cases = result['result_total_cases']
              self.prev_total_resolved = result['result_total_resolved']
              self.prev_total_pending = result['result_total_pending']
              self.total_incoming = result['result_total_incoming']
              self.total_outstanding = result['result_total_outstanding']
              self.result_summary_incoming = result['result_summary_incoming']
      });
      var def19 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_summary',
              args: [startDate,endDate]
      }).then(function(result) {
              self.summary = result['summary']
              self.draft = result['result_draft']
              self.request = result['result_request']
              self.pending = result['result_pending']
              self.assigned = result['result_assigned']
              self.confirmation = result['result_confirmation']
              self.unresolved = result['result_unresolved']
              self.resolved = result['result_resolved']
              self.total_summary =  result['result_summary']
              self.result_summary_hold =  result['result_summary_hold']
              self.result_summary_resolve =  result['result_summary_resolve']
              self.result_summary_outstanding =  result['result_summary_outstanding']
      });
      var def20 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_sbu',
              args: [startDate,endDate]
      }).then(function(result) {
              self.sbu = result['get_sbu']
              self.sbu_draft = result['result_sbu_draft']
              self.sbu_request = result['result_sbu_request']
              self.sbu_pending = result['result_sbu_pending']
              self.sbu_assigned = result['result_sbu_assigned']
              self.sbu_confirmation = result['result_sbu_confirmation']
              self.sbu_unresolved = result['result_sbu_unresolved']
              self.sbu_resolved = result['result_sbu_resolved']
              self.total_sbu_summary = result['result_sbu_summary']
      });
      var def21 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_sap',
              args: [startDate,endDate]
      }).then(function(result) {
              self.sap = result['get_sap']
              self.sap_draft = result['result_sap_draft']
              self.sap_request = result['result_sap_request']
              self.sap_pending = result['result_sap_pending']
              self.sap_assigned = result['result_sap_assigned']
              self.sap_confirmation = result['result_sap_confirmation']
              self.sap_unresolved = result['result_sap_unresolved']
              self.sap_resolved = result['result_sap_resolved']
              self.total_sap_summary = result['result_sap_summary']
      });
      var def22 =  this._rpc({
              model: 'mwide.helpdesk',
              method: 'get_response_resolution',
              args: [startDate,endDate]
      }).then(function(result) {
              self.response_resolution = result['get_response_resolution']
              self.response_fail = result['result_response_fail']
              self.response_pass = result['result_response_pass']
              self.response = result['result_response']
              self.resolution_fail = result['result_resolution_fail']
              self.resolution_pass = result['result_resolution_pass']
              self.resolution = result['result_resolution']
              self.response_percentage = result['response_percentage']
              self.resolution_percentage = result['resolution_percentage']
      });
      return $.when(def17,def18,def19,def20,def21,def22);
      },

      ticket_summary: function(ev){
        var self = this;
        ev.stopPropagation();
        ev.preventDefault();
        let select_category = ev.currentTarget.childNodes[1].innerHTML;
        let startDate = self.searchModelConfig.context.default_start_date;
        let endDate = self.searchModelConfig.context.default_end_date;

        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };

        this.do_action({
            name: (select_category),
            type: 'ir.actions.act_window',
            res_model: 'mwide.helpdesk',
            view_mode: 'tree,form',
            views: [[false, 'list'],[false, 'form']],
            domain: [['category_id.name','=', select_category],['create_date','>=', startDate],['create_date','<=', endDate]],
            target: 'current'
        });
    },

});

core.action_registry.add('custom_helpdesk_dashboard_tag', HelpdeskDashboard);

return HelpdeskDashboard;

});
