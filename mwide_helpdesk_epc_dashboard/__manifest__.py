# -*- coding: utf-8 -*-
{
    'name': "ITSS Helpdesk EPC Dashboard",

    'summary': """
        ITSS Helpdesk EPC Dashboard """,

    'description': """
        ITSS Helpdesk EPC Dashboard
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'ITSS',
    'version': '1.0.0',
    'sequence': -1,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'mega_helpdesk', 'hr'],

    # always loaded
    'init_xml': [],
    'data': [
        'security/security.xml',
        # 'security/ir.model.access.csv',
        'views/helpdesk_epc.xml',
        'views/inherit_employee.xml',
        # 'views/inherit_helpdesk.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
