#-*- coding: utf-8 -*-
# Inherit from Odoo base res.partners

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, Warning
from odoo.modules.module import get_module_resource
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
import random
import logging
import time
import os
import sys
import xmlrpc.client
from xmlrpc.client import Transport as XMLTransport
import ssl

_logger = logging.getLogger(__name__)

class MwideHelpdeskEpcDashboard(models.Model):
    _inherit = "mwide.helpdesk"

    def action_end_of_the_week_summary(self):
        for rec in self:
            print(rec.requested_id.email)
