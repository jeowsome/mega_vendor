# -*- coding: utf-8 -*-
{
    'name': "MEGA-HELPDESK ANNOUNCEMENT",

    'summary': """
        MEGA-HELPDESK ANNOUNCEMENT """,

    'description': """
        MEGA-HELPDESK ANNOUNCEMENT
    """,

    'author': "Megawide Development Team",
    'website': "https://www.megawide.com.ph",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Helpdesk',
    'version': '1.0.0',
    'sequence': -1,

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail'],

    # always loaded
    'init_xml': [],
    'data': [
        'views/dashboard_action.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
    'qweb': ["static/src/xml/dashboard.xml"],
    'installable': True,
    'application': True,
    'auto_install': False,
}
