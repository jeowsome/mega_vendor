odoo.define('mega_helpdesk_announcement.CustomHelpdeskAnnouncement', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var core = require('web.core');
var QWeb = core.qweb;
var rpc = require('web.rpc');
var ajax = require('web.ajax');

var HelpdeskAnnouncementDashboard = AbstractAction.extend({
    template: 'CustomHelpdeskAnnouncement',

    init: function(parent, context) {
          this._super(parent, context);
          this.dashboards_templates = ['DashboardContent'];
      },

      start: function() {
          var self = this;
          this.set("title", 'Helpdesk Announcement Dashboard');
          return this._super().then(function() {
              self.render_dashboards();
          });
      },

      render_dashboards: function() {
          var self = this;
          _.each(this.dashboards_templates, function(template) {
                self.$('.o_helpdesk_announcement_dashboard').append(QWeb.render(template, {widget: self}));
        });
      },

      willStart: function(){
          var self = this;
          return $.when(ajax.loadLibs(this), this._super()).then(function() {
            return self.fetch_data();
            });
      },

      fetch_data: function() {
          var self = this;
      },

});

core.action_registry.add('custom_helpdesk_announcement_tag', HelpdeskAnnouncementDashboard);

return HelpdeskAnnouncementDashboard;

});
